/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Print handler routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Window.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern BOOL			graphicPrinter, pagePrinter;

extern TextChar	printerName[], strBuff[], strPrint[], strSave[], strSavePrAs[], strPRT[];

extern DlgTemplPtr	dlgList[];

extern BOOL			destinationFile;

extern WindowPtr	dlgWindow;

extern Defaults		defaults;

/*
 *	Local variables and definitions
 */

enum {
	USLETTER_RADBTN = 2,
	USLEGAL_RADBTN,
	A4LETTER_RADBTN,
	WIDECARRIAGE_RADBTN,
	CUSTOM_RADBTN,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PICA_RADBTN,
	ELITE_RADBTN,
	CONDENSED_RADBTN,
	SIXLPI_RADBTN,
	EIGHTLPI_RADBTN,
	NOGAPS_BOX,
	PRINTERNAME1_TEXT
};

enum {
	OPTIONS_BUTTON = 2,
	POSTSCRIPT_RADBTN,
	NLQ_RADBTN,
	DRAFT_RADBTN,
	ALL_RADBTN,
	FROM_RADBTN,
	AUTOMATIC_RADBTN,
	HANDFEED_RADBTN,
	COPIES_TEXT,
	FIRSTPAGE_TEXT,
	LASTPAGE_TEXT,
	FONTUP_ARROW,
	FONTDOWN_ARROW,
	FONTNUM_TEXT,
	COLLATE_BOX,
	BACKFRONT_BOX,
	TOPRINTER_RADBTN,
	TOFILE_RADBTN,
	PRINTERNAME2_TEXT
};

#define MIN_PAPERWIDTH	1440		/* 2 inch min width */
#define MIN_PAPERHEIGHT	720		/* 1 inch min height (plus top/bottom margins) */

#define MIN_SCALE	25
#define MAX_SCALE	400

#define MIN_FONTNUM	0
#define MAX_FONTNUM	10

static WORD cpi, lpi;

/*
 *	Print options for SetPrintOption()
 */

#define NUM_OPTIONS	(sizeof(printOptNames)/sizeof(TextPtr))

static TextPtr printOptNames[] = {
	"PostScript",
	"NLQ",			"Draft",
	"AutoFeed",		"HandFeed",
	"Collate",		"NoCollate",
	"BackToFront",	"FrontToBack",
};

enum {
	OPT_POSTSCRIPT,
	OPT_NLQ,				OPT_DRAFT,
	OPT_AUTOFEED,		OPT_HANDFEED,
	OPT_COLLATE,		OPT_NOCOLLATE,
	OPT_BACKTOFRONT,		OPT_FRONTTOBACK
};

/*
 *	Local prototypes
 */

WORD	NormalizeDPI(WORD);
void	SetDPI(PrintRecPtr);
void	SetPrRects(PrintRecPtr);
void	SetPrintFlag(PrintRecPtr, UWORD, BOOL);
void	CalcWidthHeight(WORD, WORD *, WORD *);
void	ShowWidthHeight(PrintRecPtr, WORD, WORD);
void	SetDensityText(WORD, TextPtr);
BOOL	PageSetupDialogFilter(IntuiMsgPtr, WORD *);
BOOL	PrintDialogFilter(IntuiMsgPtr, WORD *);
void  UpdatePrintButon(UWORD);

/*
 *	Initialize the print handler
 *	Checks for the presence of ProScript
 */

void InitPrintHandler()
{
}


/*
 *	Set x and y DPI values
 */

static void SetDPI(printRec)
register PrintRecPtr printRec;
{
	if (printRec->PrintPitch == PRT_PICA)
		printRec->xDPI = 80;
	else if (printRec->PrintPitch == PRT_ELITE)
		printRec->xDPI = 96;
	else
		printRec->xDPI = 137;
	printRec->yDPI = 72;

}

/*
 *	Toggle the state of flag in printRec
 */

static void TogglePrintFlag(PrintRecPtr printRec, UWORD flagBit)
{
	if (printRec->Flags & flagBit)
		printRec->Flags &= ~flagBit;
	else
		printRec->Flags |= flagBit;
}

/*
 *	Set paper and page rectangles according to print record settings
 */

static void SetPrRects(printRec)
register PrintRecPtr printRec;
{
	register WORD xDPI, yDPI, width, height;
	WORD topMarg, botMarg, leftMarg, rightMarg;
	BOOL noGaps;

	xDPI = printRec->xDPI;
	yDPI = printRec->yDPI;
	noGaps = printRec->Flags & PRT_NOGAPS;
	switch (printRec->PageSize) {
	case PRT_USLETTER:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	case PRT_USLEGAL:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 10080;		/* 14 in */
		break;
	case PRT_A4LETTER:
		printRec->PaperWidth = 5953;			/* 210 mm */
		printRec->PaperHeight = 8419;			/* 297 mm */
		break;
	case PRT_WIDECARRIAGE:
		printRec->PaperWidth = 10080;			/* 14 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	}
	if (printRec->PaperWidth < MIN_PAPERWIDTH)
		printRec->PaperWidth = MIN_PAPERWIDTH;
	if (noGaps) {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT)
			printRec->PaperHeight = MIN_PAPERHEIGHT;
	}
	else {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT + 720)
			printRec->PaperHeight = MIN_PAPERHEIGHT + 720;
	}
	if (printRec->Orientation == PRT_PORTRAIT) {
		width = DECIPOINTS_TO_DOTS(printRec->PaperWidth, xDPI);
		height = DECIPOINTS_TO_DOTS(printRec->PaperHeight, yDPI);
		leftMarg = rightMarg = DECIPOINTS_TO_DOTS(180, xDPI);
		topMarg = botMarg = (noGaps) ? 0 : DECIPOINTS_TO_DOTS(360, yDPI);
	}
	else {
		width = DECIPOINTS_TO_DOTS(printRec->PaperHeight, xDPI);
		height = DECIPOINTS_TO_DOTS(printRec->PaperWidth, yDPI);
		leftMarg = rightMarg = (noGaps) ? 0 : DECIPOINTS_TO_DOTS(360, xDPI);
		topMarg = botMarg = DECIPOINTS_TO_DOTS(180, yDPI);
	}
	SetRect(&printRec->PaperRect, 0, 0, width, height);
	SetRect(&printRec->PageRect, leftMarg, topMarg, (WORD) (width - rightMarg),
			(WORD) (height - botMarg));
}

/*
 *	Validate the print record
 */

void PrValidate(printRec)
register PrintRecPtr printRec;
{
	GetPrinterType();
	if (printRec->Quality == PRT_GRAPHIC)
		printRec->Quality = PRT_NLQ;
	SetDPI(printRec);
	printRec->Orientation = PRT_PORTRAIT;
	printRec->Flags |= PRT_ASPECTADJ;
	printRec->Flags &= ~(PRT_OPTSPACE | PRT_PICTURES | PRT_FONTS | PRT_SMOOTH);
	if (pagePrinter)
		printRec->Flags &= ~PRT_NOGAPS;
	if (printRec->FontNum < MIN_FONTNUM)
		printRec->FontNum = MIN_FONTNUM;
	else if (printRec->FontNum > MAX_FONTNUM)
		printRec->FontNum = MAX_FONTNUM;
	SetPrRects(printRec);
	printRec->Copies = 1;
	if (printRec->FirstPage < 1)
		printRec->FirstPage = 1;
	if (printRec->LastPage > 9999)
		printRec->LastPage = 9999;
	SetPrintFlag( printRec, PRT_TOFILE, 0);
	strcpy( printRec->FileName, strPRT );
}

/*
 *	Set print record to default values
 */

void PrintDefault(printRec)
register PrintRecPtr printRec;
{
	WORD density;
	struct Preferences prefs;
	
	GetPrinterType();
	printRec->Version = PRINTHANDLER_VERSION;
	printRec->Flags = 0;
/*
	Set preferences values
*/
	GetPrefs(&prefs, sizeof(struct Preferences));
	printRec->PageSize = (prefs.PaperSize == W_TRACTOR) ?
						 PRT_WIDECARRIAGE : PRT_USLETTER;
	printRec->Orientation = (prefs.PrintAspect == ASPECT_HORIZ) ?
							PRT_PORTRAIT : PRT_LANDSCAPE;
	printRec->PaperFeed = (prefs.PaperType == FANFOLD) ?
						  PRT_CONTINUOUS : PRT_CUTSHEET;
	if (prefs.PrintPitch == PICA)
		printRec->PrintPitch = PRT_PICA;
	else if (prefs.PrintPitch == ELITE)
		printRec->PrintPitch = PRT_ELITE;
	else
		printRec->PrintPitch = PRT_CONDENSED;
	printRec->PrintSpacing = (prefs.PrintSpacing == SIX_LPI) ?
							 PRT_SIXLPI : PRT_EIGHTLPI;
	printRec->Quality = (prefs.PrintQuality == LETTER) ? PRT_NLQ : PRT_DRAFT;
	density = prefs.PrintDensity;
	if (density < 1)
		density = 1;
	else if (density > 7)
		density = 7;
	printRec->PrintDensity = density;
	if (prefs.PrintFlags & ANTI_ALIAS)
		printRec->Flags |= PRT_SMOOTH;
/*
	Set other defaults
*/
	printRec->XScale = printRec->YScale = 100;
	printRec->Flags |= PRT_ASPECTADJ;
	printRec->FontNum = MIN_FONTNUM;
	printRec->Copies = 1;
	printRec->FirstPage = 1;
	printRec->LastPage = 9999;
	
/*
	Validate the print record
*/
	PrValidate(printRec);
}

/*
 *	Set or clear flag in printRec
 */

static void SetPrintFlag(PrintRecPtr printRec, UWORD flagBit, BOOL setIt)
{
	if (setIt)
		printRec->Flags |= flagBit;
	else
		printRec->Flags &= ~flagBit;
}

/*
 *	Calculate new width and height for given page size
 */

static void CalcWidthHeight(pageSize, width, height)
register WORD pageSize, *width, *height;
{
	switch (pageSize) {
	case USLETTER_RADBTN:
		*width = 6120;			/* 8.5 in */
		*height = 7920;		/* 11 in */
		break;
	case USLEGAL_RADBTN:
		*width = 6120;			/* 8.5 in */
		*height = 10080;		/* 14 in */
		break;
	case A4LETTER_RADBTN:
		*width = 5953;			/* 210 mm */
		*height = 8419;		/* 297 mm */
		break;
	case WIDECARRIAGE_RADBTN:
		*width = 10080;		/* 14 in */
		*height = 7920;		/* 11 in */
		break;
	}
}

/*
 *	Show new width and height in page setup dialog
 */

static void ShowWidthHeight(printRec, width, height)
register PrintRecPtr printRec;
register WORD width, height;
{
	DecipointToText(width, strBuff, 2);
	SetEditItemText( dlgWindow->FirstGadget, WIDTH_TEXT,dlgWindow,NULL, strBuff);
	DecipointToText(height, strBuff, 2);
	SetEditItemText( dlgWindow->FirstGadget,HEIGHT_TEXT,dlgWindow,NULL, strBuff);
}

/*
 *	Page setup dialog filter
 */

static BOOL PageSetupDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;
	register UWORD qualifier;
		
	class = intuiMsg->Class;
	if( intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETDOWN || class == GADGETUP)) {
		qualifier = intuiMsg->Qualifier;
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN &&
			itemHit >= WIDTH_TEXT && itemHit <= HEIGHT_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = CUSTOM_RADBTN;
			return (TRUE);
		}
		if (class == GADGETUP && !(qualifier & IEQUALIFIER_NUMERICPAD) &&
			itemHit >= WIDTH_TEXT && itemHit <= HEIGHT_TEXT) {
			if (qualifier & SHIFTKEYS)
				itemHit--;
			else
				itemHit++;
			if (itemHit < WIDTH_TEXT || itemHit > HEIGHT_TEXT)
				return (FALSE);
			ReplyMsg((MsgPtr) intuiMsg);
			ActivateGadget(GadgetItem(dlgWindow->FirstGadget,itemHit), dlgWindow, NULL);
			Delay(5);					/* Wait for gadget to become active */
			*item = -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display and handle page setup dialog in given window
 *	Return success status
 */

BOOL PageSetupDialog(WindowPtr window, PrintRecPtr printRec)
{
	register WORD item, offItem, onItem, strItem;
	WORD pageSize, width, height, pitch, spacing, charsWidth, charsHeight;
	BOOL success, done, noGaps;
	register GadgetPtr gadget, gadgList;
	/*GadgetPtr widthGadget;*/
	register BOOL recalc = FALSE;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	dlgWindow = window;
	if (printRec->PageSize == PRT_USLETTER)
		pageSize = USLETTER_RADBTN;
	else if (printRec->PageSize == PRT_USLEGAL)
		pageSize = USLEGAL_RADBTN;
	else if (printRec->PageSize == PRT_A4LETTER)
		pageSize = A4LETTER_RADBTN;
	else if (printRec->PageSize == PRT_WIDECARRIAGE)
		pageSize = WIDECARRIAGE_RADBTN;
	else
		pageSize = CUSTOM_RADBTN;
	width = printRec->PaperWidth;
	height = printRec->PaperHeight;
	cpi = docData->CPI;
	lpi = docData->LPI;

	if (printRec->PrintPitch == PRT_PICA)
		pitch = PICA_RADBTN;
	else if (printRec->PrintPitch == PRT_ELITE)
		pitch = ELITE_RADBTN;
	else
		pitch = CONDENSED_RADBTN;
	spacing = (printRec->PrintSpacing == PRT_SIXLPI) ? SIXLPI_RADBTN : EIGHTLPI_RADBTN;
	noGaps = (printRec->Flags & PRT_NOGAPS);
	dlgList[DLG_PAGESETUP]->Gadgets[PRINTERNAME1_TEXT].Info = printerName;

	success = FALSE;
	BeginWait();
	AutoActivateEnable(FALSE);
	dlgWindow = GetDialog(dlgList[DLG_PAGESETUP], screen, mainMsgPort);
	AutoActivateEnable(TRUE);
	if( dlgWindow == NULL ) {
		EndWait();
		Error(ERR_NO_MEM);
	} else {
		gadgList = dlgWindow->FirstGadget;
		OutlineOKButton(dlgWindow);
		SetGadgetItemValue(gadgList, pageSize, dlgWindow, NULL, 1);
		SetGadgetItemValue(gadgList, pitch, dlgWindow, NULL, 1);
		SetGadgetItemValue(gadgList, spacing, dlgWindow, NULL, 1);
		SetGadgetItemValue(gadgList, NOGAPS_BOX, dlgWindow, NULL, noGaps);
		if (pagePrinter) {
			gadget = GadgetItem(gadgList, NOGAPS_BOX);
			OffGList(gadget, dlgWindow, NULL, 1);
		}
		ShowWidthHeight(printRec, width, height);
/*
	Handle dialog
*/
		done = FALSE;
		do {
			offItem = onItem = -1;
			strItem = -1;
			item = ModalDialog(mainMsgPort, dlgWindow, PageSetupDialogFilter);
			switch (item) {
			case OK_BUTTON:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case USLETTER_RADBTN:
			case USLEGAL_RADBTN:
			case A4LETTER_RADBTN:
			case WIDECARRIAGE_RADBTN:
				offItem = pageSize;
				pageSize = onItem = item;
				recalc = TRUE;
				break;
			case CUSTOM_RADBTN:
				offItem = pageSize;
				pageSize = onItem = item;
				strItem = WIDTH_TEXT;
				break;
			case NOGAPS_BOX:
				if(!pagePrinter) {
					ToggleCheckbox(&noGaps, item, dlgWindow);
				}
				break;
			case PICA_RADBTN:
			case ELITE_RADBTN:
			case CONDENSED_RADBTN:
				offItem = pitch;
				pitch = onItem = item;
				switch (pitch){
				case PICA_RADBTN:
					cpi = 10;
					break;
				case ELITE_RADBTN:
					cpi = 12;
					break;
				case CONDENSED_RADBTN:
				default:
					cpi = 17;
					break;
				}
				recalc = TRUE;
				break;
			case SIXLPI_RADBTN:
			case EIGHTLPI_RADBTN:
				offItem = spacing;
				spacing = onItem = item;
				lpi = ( spacing == SIXLPI_RADBTN ) ? 6 : 8 ;
				recalc = TRUE;
				break;
			}
			if( recalc && ( pageSize != CUSTOM_RADBTN ) ) {
				recalc = FALSE;
				CalcWidthHeight( pageSize, &width, &height ) ;
				ShowWidthHeight( printRec, width, height ) ;
			}
			if (!done && item != -1) {
				if (offItem != -1)
					SetGadgetItemValue(gadgList, offItem, dlgWindow, NULL, 0);
				if (onItem != -1)
					SetGadgetItemValue(gadgList, onItem, dlgWindow, NULL, 1);
				gadget = GadgetItem(gadgList, strItem);
				ActivateGadget(gadget, dlgWindow, NULL);
			}
		} while (!done);
		docData->CPI = cpi;
		docData->LPI = lpi;

		width = GetFracValue(gadgList, WIDTH_TEXT);
		height = GetFracValue(gadgList, HEIGHT_TEXT);
		charsWidth = ((width - 360) * docData->CPI) / 720 ;
		charsHeight = (height * docData->LPI) / 720 ;

		DisposeDialog(dlgWindow);
		EndWait();
		if(item != CANCEL_BUTTON) {
/*
	Save options
*/
			if (pageSize == USLETTER_RADBTN)
				printRec->PageSize = PRT_USLETTER;
			else if (pageSize == USLEGAL_RADBTN)
				printRec->PageSize = PRT_USLEGAL;
			else if (pageSize == A4LETTER_RADBTN)
				printRec->PageSize = PRT_A4LETTER;
			else if (pageSize == WIDECARRIAGE_RADBTN)
				printRec->PageSize = PRT_WIDECARRIAGE;
			else
				printRec->PageSize = PRT_CUSTOM;
			if (width < MIN_PAPERWIDTH || height < MIN_PAPERHEIGHT ||
				(!noGaps && height < MIN_PAPERHEIGHT + 720)) {
				Error(ERR_PAGE_SIZE);		/* Will be fixed by PrValidate */
			} else {
				docData->PageWidth = charsWidth;
				docData->PageHeight = charsHeight;
				printRec->PaperWidth = width;
				printRec->PaperHeight = height;
			}
			if (pitch == PICA_RADBTN)
				printRec->PrintPitch = PRT_PICA;
			else if (pitch == ELITE_RADBTN)
				printRec->PrintPitch = PRT_ELITE;
			else
				printRec->PrintPitch = PRT_CONDENSED;
			printRec->PrintSpacing = (spacing == SIXLPI_RADBTN) ? PRT_SIXLPI : PRT_EIGHTLPI;
			SetPrintFlag(printRec, PRT_NOGAPS, noGaps);			
/*
	Set paper and page rectangles
*/
			PrValidate(printRec);
			success = TRUE;
		}
	}
	return(success);
}

/*
 *	Set and enable/disable print dialog buttons
 */

static void SetPrintButtons(DialogPtr dlg, PrintRecPtr printRec, BOOL printAll,
								WORD dest)
{
	WORD quality, feed, fontNum;
	UWORD flags;
	BOOL isPSPrint, isCharPrint;
	GadgetPtr gadgList;

	quality	= printRec->Quality;
	feed	= printRec->PaperFeed;
	flags	= printRec->Flags;
	fontNum	= printRec->FontNum;
	isPSPrint		= (quality == PRT_POSTSCRIPT);
	isCharPrint		= (quality == PRT_NLQ || quality == PRT_DRAFT);
/*
	First enable/disable buttons
*/
	gadgList = dlg->FirstGadget;

	EnableGadgetItem(gadgList, OPTIONS_BUTTON, dlg, NULL, (isPSPrint &&
									dest == TOPRINTER_RADBTN));

	EnableGadgetItem(gadgList, FONTDOWN_ARROW, dlg, NULL,
					 (fontNum > MIN_FONTNUM && isCharPrint));
	EnableGadgetItem(gadgList, FONTUP_ARROW, dlg, NULL,
					 (fontNum < MAX_FONTNUM && isCharPrint));
/*
	Now set buttons
*/
	SetGadgetItemValue(gadgList, POSTSCRIPT_RADBTN, dlg, NULL, isPSPrint);
	SetGadgetItemValue(gadgList, NLQ_RADBTN, dlg, NULL, (quality == PRT_NLQ));
	SetGadgetItemValue(gadgList, DRAFT_RADBTN, dlg, NULL, (quality == PRT_DRAFT));
	SetGadgetItemValue(gadgList, ALL_RADBTN, dlg, NULL, printAll);
	SetGadgetItemValue(gadgList, FROM_RADBTN, dlg, NULL, !printAll);
	SetGadgetItemValue(gadgList, AUTOMATIC_RADBTN, dlg, NULL, (feed == PRT_CONTINUOUS));
	SetGadgetItemValue(gadgList, HANDFEED_RADBTN, dlg, NULL, (feed == PRT_CUTSHEET));
	SetGadgetItemValue(gadgList, COLLATE_BOX, dlg, NULL, (flags & PRT_COLLATE));
	SetGadgetItemValue(gadgList, BACKFRONT_BOX, dlg, NULL, (flags & PRT_BACKTOFRONT));
	SetGadgetItemValue(gadgList, TOFILE_RADBTN, dlg, NULL, (dest == TOFILE_RADBTN));
	SetGadgetItemValue(gadgList, TOPRINTER_RADBTN, dlg, NULL,(dest == TOPRINTER_RADBTN));
}

/*
 *	Print dialog filter
 */

static BOOL PrintDialogFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register ULONG class;
	register UWORD qualifier;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETUP || class == GADGETDOWN) ) {
		qualifier = intuiMsg->Qualifier;
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN &&
			itemHit >= FIRSTPAGE_TEXT && itemHit <= LASTPAGE_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = FROM_RADBTN;
			return (TRUE);
		}
		if (class == GADGETUP && !(qualifier & IEQUALIFIER_NUMERICPAD) &&
			itemHit >= FIRSTPAGE_TEXT && itemHit <= LASTPAGE_TEXT) {
			if (qualifier & SHIFTKEYS)
				itemHit--;
			else
				itemHit++;
			if (itemHit < FIRSTPAGE_TEXT || itemHit > LASTPAGE_TEXT)
				return (FALSE);
			ReplyMsg((MsgPtr) intuiMsg);
			ActivateGadget(GadgetItem(dlgWindow->FirstGadget,itemHit), dlgWindow, NULL);
			Delay(5);					/* Wait for gadget to become active */
			*item = -1;
			return (TRUE);
		}
		if ((class == GADGETDOWN || class == GADGETUP) &&
			(itemHit == FONTUP_ARROW || itemHit == FONTDOWN_ARROW)) {
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

static void UpdatePrintButton(UWORD dest )
{
	SetButtonItem( dlgWindow->FirstGadget, OK_BUTTON, dlgWindow, NULL,
		(dest == TOFILE_RADBTN) ? &strSave[0] : &strPrint[0],
		(dest == TOFILE_RADBTN) ? 'S' : 'P' ) ;
}		

/*
 *	Display and handle print dialog
 *	Return success status
 */

BOOL PrintDialog(WindowPtr window, PrintRecPtr printRec)
{
	register WORD item, copies, firstPage, lastPage;
	WORD fontNum;
	BOOL done, printAll;
	UWORD destination;
	register GadgetPtr gadgList;
	SFReply sfReply;

	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	dlgWindow = window;

	printAll = TRUE;
	copies = printRec->Copies;
	firstPage = printRec->FirstPage;
	lastPage = printRec->LastPage;
	fontNum = printRec->FontNum;
	
	if( printRec->Flags & PRT_TOFILE ) {
		destination = TOFILE_RADBTN;
		dlgList[DLG_PRINT]->Gadgets[OK_BUTTON].Info = &strSave[0];
	} else {
		destination = TOPRINTER_RADBTN;
		dlgList[DLG_PRINT]->Gadgets[OK_BUTTON].Info = &strPrint[0];
	}
/*
	Get dialog and set initial values
*/
	BeginWait();
	if( (dlgWindow = GetDialog(dlgList[DLG_PRINT], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	
	gadgList = dlgWindow->FirstGadget;
	OutlineOKButton(dlgWindow);
	SetPrintButtons(dlgWindow, printRec, printAll,destination);	
	NumToString(printRec->FontNum, strBuff);
	SetGadgetItemText(gadgList, FONTNUM_TEXT, dlgWindow, NULL, strBuff);
	SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlgWindow, NULL, 
						(printRec->Quality == PRT_POSTSCRIPT) ? 
							PSDeviceName() : printerName);
	NumToString(copies, strBuff);
	SetEditItemText(dlgWindow->FirstGadget, COPIES_TEXT,dlgWindow,NULL, strBuff);
	NumToString(firstPage, strBuff);
	SetEditItemText(dlgWindow->FirstGadget, FIRSTPAGE_TEXT,dlgWindow,NULL, strBuff);
	NumToString(lastPage, strBuff);
	SetEditItemText(dlgWindow->FirstGadget,LASTPAGE_TEXT,dlgWindow,NULL, strBuff);

/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, dlgWindow, PrintDialogFilter);
		switch (item) {
		case OK_BUTTON:
			sfReply.Result = SFP_OK;
			if( destination == TOFILE_RADBTN ) {
				SFPPutFile(screen, mainMsgPort, strSavePrAs, "PrintFile", DialogFilter, NULL, NULL, &sfReply);
				printRec->FileDir = sfReply.DirLock;
				strcpy(printRec->FileName, sfReply.Name);
			} else {
				strcpy(printRec->FileName, strPRT);
			}
			if( sfReply.Result != SFP_OK )
				break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case OPTIONS_BUTTON:
			if (printRec->Quality != PRT_POSTSCRIPT) 
				break;
			SetStdPointer(dlgWindow, POINTER_WAIT);
			PostScriptOptsDialog(); 
			SetStdPointer(dlgWindow, POINTER_ARROW);
			SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlgWindow, NULL,
							  PSDeviceName());
			break;
		case POSTSCRIPT_RADBTN:
			if (printRec->Quality != PRT_POSTSCRIPT)
				SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlgWindow, NULL,
								  PSDeviceName());
			printRec->Quality = PRT_POSTSCRIPT;
			break;
		case NLQ_RADBTN:
			if (printRec->Quality == PRT_POSTSCRIPT)
				SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlgWindow, NULL,
					printerName);
			printRec->Quality = PRT_NLQ;
			break;
		case DRAFT_RADBTN:
			if (printRec->Quality == PRT_POSTSCRIPT)
				SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlgWindow, NULL,
					printerName);
			printRec->Quality = PRT_DRAFT;
			break;
		case ALL_RADBTN:
			printAll = TRUE;
			break;
		case FROM_RADBTN:
			printAll = FALSE;
			ActivateGadget(GadgetItem(gadgList, FIRSTPAGE_TEXT), dlgWindow, NULL);
			break;
		case AUTOMATIC_RADBTN:
			printRec->PaperFeed = PRT_CONTINUOUS;
			break;
		case HANDFEED_RADBTN:
			printRec->PaperFeed = PRT_CUTSHEET;
			break;
		case COLLATE_BOX:
			TogglePrintFlag(printRec, PRT_COLLATE);
			break;
		case BACKFRONT_BOX:
			TogglePrintFlag(printRec, PRT_BACKTOFRONT);
			break;
		case TOFILE_RADBTN:
		case TOPRINTER_RADBTN:
			destination = item;
			UpdatePrintButton(item);
			break;
		case FONTUP_ARROW:
		case FONTDOWN_ARROW:
			if (item == FONTUP_ARROW) {
				if (fontNum < MAX_FONTNUM)
					fontNum++;
			}
			else {
				if (fontNum > MIN_FONTNUM)
					fontNum--;
			}
			printRec->FontNum = fontNum;
			NumToString(fontNum, strBuff);
			SetGadgetItemText(gadgList, FONTNUM_TEXT, dlgWindow, NULL, strBuff);
			break;
		}
		if (!done && item != -1) 
			SetPrintButtons(dlgWindow,printRec,printAll,destination);
	} while (!done);
	GetEditItemText(gadgList, COPIES_TEXT, strBuff);
	copies = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	GetEditItemText(gadgList, FIRSTPAGE_TEXT, strBuff);
	firstPage = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	GetEditItemText(gadgList, LASTPAGE_TEXT, strBuff);
	lastPage = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	DisposeDialog(dlgWindow);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Check values for acceptability and set return values
*/

	if (copies <= 0) {
		Error(ERR_BAD_COPIES);
		return (FALSE);
	}
	if (printAll) {
		firstPage = 1;
		lastPage = 9999;
	}
	else if (firstPage < 0 || lastPage < 0 || firstPage > lastPage) {
		Error(ERR_PAGE_NUM);
		return (FALSE);
	}
	
	SetPrintFlag(printRec, PRT_TOFILE, destination == TOFILE_RADBTN);
	printRec->FontNum = fontNum;
	printRec->Copies = copies;
	printRec->FirstPage = firstPage;
	printRec->LastPage = lastPage;
	return (TRUE);
}

/*
 *	Set print option of given option name
 *	If printRec is NULL, set default print record
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

BOOL SetPrintOption(PrintRecPtr printRec, TextPtr optName, WORD len)
{
	register WORD i;

	if (printRec == NULL)
		printRec = &defaults.PrintRec;
	for (i = 0; i < NUM_OPTIONS; i++) {
		if (CmpString(optName, printOptNames[i], len, (WORD) strlen(printOptNames[i]), FALSE)
			== 0)
			break;
	}
	if (i >= NUM_OPTIONS)
		return (FALSE);
	switch (i) {
	case OPT_POSTSCRIPT:
		printRec->Quality = PRT_POSTSCRIPT;
		break;
	case OPT_NLQ:
		printRec->Quality = PRT_NLQ;
		break;
	case OPT_DRAFT:
		printRec->Quality = PRT_DRAFT;
		break;
	case OPT_AUTOFEED:
		printRec->PaperFeed = PRT_CONTINUOUS;
		break;
	case OPT_HANDFEED:
		printRec->PaperFeed = PRT_CUTSHEET;
		break;
	case OPT_COLLATE:
	case OPT_NOCOLLATE:
		SetPrintFlag(printRec, PRT_COLLATE, (BOOL) (i == OPT_COLLATE));
		break;
	case OPT_BACKTOFRONT:
	case OPT_FRONTTOBACK:
		SetPrintFlag(printRec, PRT_BACKTOFRONT, (BOOL) (i == OPT_BACKTOFRONT));
		break;
	}
	PrValidate(printRec);
	return (TRUE);
}
