/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <Toolbox/ScrollList.h>
#include <Toolbox/List.h>

#include <REXX/rxslib.h>

#include "Flow.h"

extern TextChar	strMacroNames1[10][32], strMacroNames2[10][32];

/*
 *	Library and device base addresses
 */

struct IntuitionBase	*IntuitionBase;
struct GfxBase			*GfxBase;
struct Library			*LayersBase;
struct Library			*IconBase;
struct Library			*WorkbenchBase;
struct Library			*DiskfontBase;
struct Library			*TranslatorBase;
struct Library			*IFFParseBase;
struct Device			*ConsoleDevice;
struct RexxLib			*RexxSysBase;

/*
 *	General use buffer
 */

TextChar	strBuff[256];

/*
 *	General info
 */

TextChar	progPathName[100];				/* Path name to program */
TextChar	printerName[FILENAME_SIZE + 3];

BOOL	fromCLI;		/* Started from CLI */

WORD	intuiVersion;	/* Intuition version */

ScreenPtr	screen;
WindowPtr	backWindow;
MsgPortPtr	mainMsgPort;
MsgPortPtr	appIconMsgPort;
MsgPort		monitorMsgPort;			/* Not a pointer! */
MsgPort		rexxMsgPort;

WORD		numWindows;				/* Inited to 0 */
WindowPtr	windowList[MAX_WINDOWS];
MenuPtr		docMenuStrip;
MenuItemPtr	headerSubMenu, footerSubMenu;

WindowPtr	cmdWindow;				/* Last active window */

WindowPtr	dlgWindow;

WindowPtr	closeWindow;
BOOL		closeFlag, closeAllFlag, quitFlag;	/* Inited to FALSE */

BOOL		drawOn=TRUE;

/*
 *	Edit menu variables
 */

Ptr		pasteBuff=NULL;			/* Inited to NULL */
WORD	pasteType = PASTE_NONE;
BOOL	scrapValid;			/* TRUE if internal clip is same as clipboard */
LONG	isClip=0;

/*
 *	General parameters
 */

Defaults			defaults;		/* Document defaults */
Options				options;		/* Global options */
PostScriptOptions	postScriptOptions;

struct DateStamp	dateStamp;

BOOL	cursorRepeat;			/* Repeated up/down cursor movement */
BOOL	titleChanged;			/* Screen title was changed by Error() */
BOOL	inMacro;				/* Executing macro, inited to FALSE */

BOOL	busy;					/* Busy processing keypress, ignore repeats */
WORD	waitCount;				/* BeginWait() called, inited to 0 */

BOOL	dragging;				/* TRUE if dragging ruler image */
WORD	dragType;				/* Type of dragging operation in progress */
HeadPtr	dragHead;
WORD	dragDepth;
WORD	dragTabNum;				/* For when dragging tabs */
WORD	dragXPos;				/* Position for dragging of picture */
LONG	dragYPos;
WORD	dragWidth, dragHeight;	/* Sizes for re-sizing picture */
BOOL	doubleClick, tripleClick;		/* Inited to FALSE */

TextChar	fileBuff[FILEBUFF_SIZE];
LONG		iffError;			/* Error number from IFF load/save */

BOOL	spellEnabled;
BOOL	idleEvent;

ScrollListPtr	scrollList;

/*
 *	Function key menu command and AREXX command tables (without/with shift key)
 */

FKey	fKeyTable1[] = {
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, CLOSE_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, REVERT_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, PRINT_ITEM,NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(HEADING_MENU, MOVEUP_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(HEADING_MENU, MOVEDOWN_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(HEADING_MENU, EXPANDALL_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(HEADING_MENU, SORT_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(NOMENU, NOITEM, NOSUB) }
};

FKey	fKeyTable2[] = {
	{ FKEY_MACRO, &strMacroNames2[0][0] },
	{ FKEY_MACRO, &strMacroNames2[1][0] },
	{ FKEY_MACRO, &strMacroNames2[2][0] },
	{ FKEY_MACRO, &strMacroNames2[3][0] },
	{ FKEY_MACRO, &strMacroNames2[4][0] },
	{ FKEY_MACRO, &strMacroNames2[5][0] },
	{ FKEY_MACRO, &strMacroNames2[6][0] },
	{ FKEY_MACRO, &strMacroNames2[7][0] },
	{ FKEY_MACRO, &strMacroNames2[8][0] },
	{ FKEY_MACRO, &strMacroNames2[9][0] },
};

/*
 *	Parameters determined from printer driver
 */

BOOL	graphicPrinter, colorPrinter, pagePrinter;

/*
 *	Font information
 */

TextFontPtr	smallFont;


WORD	charBaseline, charWidth, charHeight;

/*
 *	Screen palette
 */
/*
Palette		screenPalette;
*/
/*
 *	ISO color number and color names, indexed by color number
 */

UBYTE isoColors[NUM_STDCOLORS];		/* Index by color number of ISO printer colors */

TextPtr colorNames[NUM_STDCOLORS];

RGBColor	screenColors[256];

/*
 *	Color numbers of Color submenu
 */

UWORD colorList[] = {
	COLOR_BLACK,	COLOR_RED,	COLOR_YELLOW,	COLOR_GREEN,
	COLOR_CYAN,		COLOR_BLUE,	COLOR_MAGENTA,	COLOR_WHITE
};

/*
 *	Gray patterns
 */

UWORD	blackPat[]	= { 0xFFFF, 0xFFFF };
UWORD	grayPat[]	= { 0xAAAA, 0x5555 };
UWORD	ltGrayPat[]	= { 0x8888, 0x2222 };
UWORD	dkGrayPat[]	= { 0xDDDD, 0x7777 };

/*
	Parse as follows:	%n = section number (decimal)
						%i = roman numeral
						%a = alphabetic character
						; = end of loop
						,  = separator
						all others = string characters to print directly
*/

TextPtr headingStrs[] = {
	"",					/* Empty string */
	NULL,					/* Custom label for this doc, which is filled in at runtime */
	"^n.",
	"^I.,^A.;^n.,^a),^i)",
	"^I.,^A.,^n.,^a);(^n),(^a),^n)",
};

DialogPtr findDialog, changeDialog, spellDialog, insertDialog = NULL;

UWORD modelessStrItem;

UWORD levelArray[GADG_MAX_STRING/4];
