/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Screen routines
 */

#include <exec/types.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/workbench.h>
#include <workbench/startup.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Screen.h>

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

extern BOOL	_tbOnPubScreen, _tbSmartWindows;

/*
 *	Local variables and definitions
 */

#define MIN(a, b)	(((a)<(b))?(a):(b))

static TextAttr screenAttr = {
	NULL, 11, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextChar topazFontName[] = "topaz.font";
static TextChar	systemFontName[] = "System.font";
static TextChar	systemThinFontName[] = "SystemThin.font";

static TextFontPtr screenFont;

static ScreenPtr pubScreen;

/*
 *	Local prototypes
 */

WORD	MaxScreenDepth(ULONG);

ScreenPtr	GetPublicScreen(TextPtr);

BOOL	MatchOptions(TextPtr, TextPtr, TextPtr);

void	DoScreenOption(struct NewScreen *, ULONG *, TextPtr);
BOOL	SetScreenData(struct NewScreen *, ULONG *, int, char **);
void	SetScreenColors(ScreenPtr, ColorTablePtr, WORD);

void	SetNewLookPens(UWORD *, ColorTablePtr, WORD, WORD);

ScreenPtr	GetNewScreen(struct NewScreen *, ULONG, UWORD *, TextPtr);

/*
 *	Return the maximum screen depth for the given screen modeID
 */

static WORD MaxScreenDepth(ULONG modeID)
{
	WORD maxDepth;
	DisplayInfoHandle dispInfo;
	struct DimensionInfo dimInfo;

	maxDepth = 1;
/*
	Handle old system version
*/
	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0) {
		switch (modeID) {
		case LORES_KEY:
		case LORESLACE_KEY:
			maxDepth = 5;
			break;
		default:
			maxDepth = 4;
			break;
		}
	}
/*
	Handle new system veresion
*/
	else {
		if ((dispInfo = FindDisplayInfo(modeID)) == NULL ||
			GetDisplayInfoData(dispInfo, (BYTE *) &dimInfo, sizeof(struct DimensionInfo),
							   DTAG_DIMS, NULL) == 0)
			maxDepth = 4;
		else
			maxDepth = dimInfo.MaxDepth;
	}
	return (maxDepth);
}

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 *	Note: Under 2.0, this routine will lock the screen, the caller
 *		  should unlock the screen after the first window is openned on the screen
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG intuiLock;
	register ScreenPtr screen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
		screen = LockPubScreen(scrnName);
	else if (scrnName == NULL) {
		intuiLock = LockIBase(0);
		for (screen = IntuitionBase->FirstScreen; screen; screen = screen->NextScreen) {
			if ((screen->Flags & SCREENTYPE) == WBENCHSCREEN)
				break;
		}
		UnlockIBase(intuiLock);
	}
	else
		screen = NULL;
	return (screen);
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}

/*
 *	Set screen mode from given option
 *	If public screen requested, get lock on public screen
 */

static void DoScreenOption(struct NewScreen *newScrn, ULONG *modeID, TextPtr text)
{
	register TextPtr param;
	WORD intuiVersion;
	LONG num;

	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Get pointer to command param (if any)
*/
	for (param = text; *param && *param != ':' && *param != '='; param++) ;
	if (*param)
		param++;
/*
	Check for non-screen related params
*/
	if (MatchOptions(text, "smartwindows", "sw")) {
		_tbSmartWindows = TRUE;
		return;
	}
	if (MatchOptions(text, "dumbwindows", "dw")) {
		_tbSmartWindows = FALSE;
		return;
	}
	if (MatchOptions(text, "asl", "asl")) {
		SFPUseAslRequest(TRUE);
		return;
	}
/*
	If already have public screen, ignore all other screen commands
*/
	if (_tbOnPubScreen)
		return;
/*
	Check to open on workbench screen
*/
	if (MatchOptions(text, "workbench", "wb")) {
		pubScreen = GetPublicScreen(NULL);
		if (pubScreen != NULL)
			_tbOnPubScreen = TRUE;
		return;
	}
/*
	Check for other public screen request
*/
	if (MatchOptions(text, "screen:", "screen=")) {
		if (intuiVersion < OSVERSION_2_0)
			return;
		pubScreen = GetPublicScreen(param);
		if (pubScreen != NULL)
			_tbOnPubScreen = TRUE;
		return;
	}
/*
	Check for screen depth command
*/
	if (MatchOptions(text, "colors:", "colors=") ||
		MatchOptions(text, "colours:", "colours=")) {
		num = StringToNum(param);
		if (num <= 0 || num > 256)
			return;
		newScrn->Depth = 1;
		while (num > 2) {
			newScrn->Depth += 1;
			num >>= 1;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
/*
	Check for other screen settings
*/
	if (MatchOptions(text, "productivity", "pr")) {
		if (intuiVersion >= OSVERSION_2_0) {
			newScrn->ViewModes = (HIRES | LACE);
			*modeID = VGAPRODUCT_KEY;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "superhires", "sr")) {
		if (intuiVersion >= OSVERSION_2_0) {
			newScrn->ViewModes = (HIRES | LACE);
			*modeID = SUPERLACE_KEY;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "hires", "hr")) {
		newScrn->ViewModes = (HIRES | LACE);
		*modeID = HIRESLACE_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "medres", "mr")) {
		newScrn->ViewModes = HIRES;
		*modeID = HIRES_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "lores", "lr")) {
		newScrn->ViewModes = 0;
		*modeID = LORES_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
}

/*
 *	Set screen data
 *	If _tbOnPubScreen is set by this routine, then screen is set to public screen
 *		and newScrn settings are undefined
 */

static BOOL SetScreenData(struct NewScreen *newScrn, ULONG *modeID, int argc, char *argv[])
{
	WORD i, maxDepth, intuiVersion;
	UWORD viewModes;
	BOOL highRes, interlace;
	register TextPtr text;
	struct DiskObject *icon;
	struct WBStartup *wbMsg;
	ScreenPtr screen;
	Screen screenData;

	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Set default mode from Workbench screen settings
*/
	_tbOnPubScreen = FALSE;
	if (intuiVersion >= OSVERSION_2_0) {
		if ((screen = LockPubScreen(NULL)) == NULL)
			return (FALSE);
		screenData = *screen;			/* Copy default screen data */
		UnlockPubScreen(NULL, screen);
	}
	else {
		if (!GetScreenData((BYTE *) &screenData, sizeof(Screen), WBENCHSCREEN, NULL))
			return (FALSE);
	}
	if (screenData.ViewPort.Modes & LACE) {
		newScrn->ViewModes = (HIRES | LACE);
		*modeID = HIRESLACE_KEY;
	}
	else {
		newScrn->ViewModes = HIRES;
		*modeID = HIRES_KEY;
	}
/*
	Get user parameters
*/
	if (argc) {				/* Running under CLI */
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				DoScreenOption(newScrn, modeID, text);
		}
	}
	else {					/* Running under Workbench */
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					DoScreenOption(newScrn, modeID, text);
			}
			FreeDiskObject(icon);
		}
	}
/*
	Set screen dimensions and adjust depth to upper bound
*/
	if (!_tbOnPubScreen) {
		newScrn->Height = STDSCREENHEIGHT;
		if (intuiVersion >= OSVERSION_2_0)
			newScrn->Width = STDSCREENWIDTH;
		else if (newScrn->ViewModes & HIRES)
			newScrn->Width = screenData.Width;
		else
			newScrn->Width = screenData.Width/2;
		maxDepth = MaxScreenDepth(*modeID);
		if (newScrn->Depth > maxDepth)
			newScrn->Depth = maxDepth;
	}
/*
	Set the default screen font
*/
	viewModes = (_tbOnPubScreen) ? pubScreen->ViewPort.Modes : newScrn->ViewModes;
	highRes   = ((viewModes & HIRES) != 0);
	interlace = ((viewModes & LACE) != 0);
	screenAttr.ta_Name = (highRes) ? systemFontName : systemThinFontName;
	screenAttr.ta_YSize = (interlace) ? 11 : 8;
	if ((screenFont = GetFont(&screenAttr)) == NULL) {
		screenAttr.ta_Name = topazFontName;
		if ((screenFont = GetFont(&screenAttr)) == NULL) {
			screenAttr.ta_YSize = 8;		/* Can always get this font */
			screenFont = GetFont(&screenAttr);
		}
	}
	newScrn->Font = &screenAttr;
	return (TRUE);
}

/*
 *	Set screen colors
 */

static void SetScreenColors(ScreenPtr screen, ColorTablePtr colorTable, WORD numColors)
{
	register WORD i, entries, num;
	RGBColor colors[256];

	num = GetColorTable(screen, &colors);	/* In case screen has more than numColors */
	if (num == 2) {
		colors[0] = (*colorTable)[0];
		colors[1] = (*colorTable)[1];
	}
	else {
		entries = MIN(num, numColors);
		for (i = 0; i < entries/2; i++) {
			colors[i] = (*colorTable)[i];
			colors[num - i - 1] = (*colorTable)[numColors - i - 1];
		}
	}
	LoadRGB4(&screen->ViewPort, colors, num);
}

/*
 *	Set new look pens from color table
 */

static void SetNewLookPens(UWORD *newLookPens, ColorTablePtr colorTable, WORD numColors,
						   WORD screenDepth)
{
	WORD i, num, numScrnColors, penBlack, penWhite, penLight;
	WORD color, rgbBlack, rgbWhite, rgbLight;
	ColorMapPtr colorMap;

	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0)
		return;
/*
	Set up color map that will be used
*/
	numScrnColors = 1 << screenDepth;
	num = MIN(numColors, numScrnColors);
	if ((colorMap = GetColorMap(num)) == NULL)
		return;
	for (i = 0; i < num/2; i++) {
		color = (*colorTable)[i];
		SetRGB4CM(colorMap, i, RED(color), GREEN(color), BLUE(color));
		color = (*colorTable)[num - i - 1];
		SetRGB4CM(colorMap, num - i - 1, RED(color), GREEN(color), BLUE(color));
	}
/*
	Now figure out pens to use
	(Can't use toolbox colors, since they are not set up yet)
*/
	penBlack = PenNumber(0x000, colorMap, num);
	penWhite = PenNumber(0xFFF, colorMap, num);
	rgbBlack = GetRGB4(colorMap, penBlack);
	rgbWhite = GetRGB4(colorMap, penWhite);
	rgbLight = RGBCOLOR( 9*(  RED(rgbWhite) -   RED(rgbBlack))/15 +   RED(rgbBlack),
						11*(GREEN(rgbWhite) - GREEN(rgbBlack))/15 + GREEN(rgbBlack),
						11*( BLUE(rgbWhite) -  BLUE(rgbBlack))/15 +  BLUE(rgbBlack));
	penLight = PenNumber(rgbLight, colorMap, num);
	if (numScrnColors > num) {
		if (penBlack > num/2)
			penBlack += numScrnColors - num;
		if (penWhite > num/2)
			penWhite += numScrnColors - num;
		if (penLight > num/2)
			penLight += numScrnColors - num;
	}
/*
	Set new look pens
*/
	newLookPens[DETAILPEN]			= 0;
	newLookPens[BLOCKPEN]			= 1;
	newLookPens[TEXTPEN]			= penLight;
	newLookPens[SHINEPEN]			= penWhite;
	newLookPens[SHADOWPEN]			= penBlack;
	newLookPens[FILLPEN]			= penLight;
	newLookPens[FILLTEXTPEN]		= penBlack;
	newLookPens[BACKGROUNDPEN]		= 0;
	newLookPens[HIGHLIGHTTEXTPEN]	= penWhite;
	newLookPens[9]					= ~0;
	FreeColorMap(colorMap);
}

/*
 *	Open new screen from newScrn parameters
 */

static ScreenPtr GetNewScreen(struct NewScreen *newScrn, ULONG modeID,
							  UWORD *newLookPens, TextPtr screenName)
{
	ScreenPtr screen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0) {
		screen = LockPubScreen(screenName);
		if (screen == NULL) {
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, modeID,
									SA_FullPalette, TRUE,
									SA_PubName, screenName,
									TAG_END);
			if (screen)
				PubScreenStatus(screen, 0);
		}
		else {			/* Public screen already exists */
			UnlockPubScreen(NULL, screen);
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, modeID,
									SA_FullPalette, TRUE,
									TAG_END);
		}
	}
	else
		screen = OpenScreen(newScrn);
	return (screen);
}

/*
 *	Get screen settings and open screen (or get pointer to public screen)
 */

ScreenPtr GetScreen(int argc, char *argv[], struct NewScreen *newScrn,
					ColorTablePtr colorTable, WORD numColors, TextPtr screenName)
{
	ScreenPtr screen;
	ULONG modeID;
	UWORD newLookPens[10];

	if (!SetScreenData(newScrn, &modeID, argc, argv))
		return (NULL);
	if (_tbOnPubScreen)
		screen = pubScreen;
	else if (newScrn->Type == WBENCHSCREEN) {
		screen = GetPublicScreen(NULL);
		if (screen)
			_tbOnPubScreen = TRUE;
	}
	else {
		SetNewLookPens(newLookPens, colorTable, numColors, newScrn->Depth);
		screen = GetNewScreen(newScrn, modeID, newLookPens, screenName);
	}
/*
	Set default screen colors
*/
	if (screen && !_tbOnPubScreen)
		SetScreenColors(screen, colorTable, numColors);
	return (screen);
}

/*
 *	Close screen openned by GetScreen()
 */

void DisposeScreen(ScreenPtr screen)
{
	if (screen && !_tbOnPubScreen)
		CloseScreen(screen);
	if (screenFont)
		CloseFont(screenFont);
}

/*
 *	Get screen color table
 *	Return number of colors in table
 */

WORD GetColorTable(ScreenPtr scrn, register ColorTablePtr colorTable)
{
	register WORD i, numColors;

	numColors = 1 << scrn->RastPort.BitMap->Depth;
	for (i = 0; i < numColors; i++)
		(*colorTable)[i] = GetRGB4(scrn->ViewPort.ColorMap, i);
	return (numColors);
}

/*
 *	Get rectange containing viewable portion of screen
 */

void GetScreenViewRect(register ScreenPtr screen, register RectPtr rect)
{
	ULONG modeID;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0 &&
		(modeID = GetVPModeID(&screen->ViewPort)) != INVALID_ID &&
		QueryOverscan(modeID, rect, OSCAN_TEXT)) {
		if (screen->LeftEdge < 0)
			OffsetRect(rect, -screen->LeftEdge, 0);
		if (screen->TopEdge < 0)
			OffsetRect(rect, 0, -screen->TopEdge);
		if (rect->MaxX - rect->MinX + 1 > screen->Width)
			rect->MaxX = rect->MinX + screen->Width - 1;
		if (rect->MaxY - rect->MinY + 1 > screen->Height)
			rect->MaxY = rect->MinY + screen->Height - 1;
	}
	else
		SetRect(rect, 0, 0, screen->Width - 1, screen->Height - 1);
}

/*
 *	Determine whether the specified screen is a public screen
 */

BOOL IsPubScreen(register ScreenPtr screen)
{
	register struct PubScreenNode *psn;

	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0)
		return (FALSE);
	psn = (struct PubScreenNode *) LockPubScreenList();
	while (psn) {
		if (psn->psn_Screen == screen)
			break;
		psn = (struct PubScreenNode *) psn->psn_Node.ln_Succ;
	}
	UnlockPubScreenList();
	return (psn != NULL);
}
