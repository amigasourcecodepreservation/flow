/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Screen routines
 */

#ifndef TOOLBOX_SCREEN_H
#define TOOLBOX_SCREEN_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

/*
 *	Prototypes
 */

ScreenPtr	GetScreen(int, char **, struct NewScreen *, ColorTablePtr, WORD, TextPtr);
void		DisposeScreen(ScreenPtr);

WORD	GetColorTable(ScreenPtr, ColorTablePtr);
void	GetScreenViewRect(ScreenPtr, RectPtr);
BOOL	IsPubScreen(ScreenPtr);

#endif
