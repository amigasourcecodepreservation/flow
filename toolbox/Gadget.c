/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/sghooks.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>			/* For Delay() prototype */

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/IntuiText.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct Library	*IntuitionBase;

extern ScreenPtr	_tbScreen;

extern BOOL	_tbNoShadows;

extern TextAttr	_tbTextAttr;

extern WORD	_tbXSize, _tbYSize;

extern UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight, _tbPenRed;

/*
 *	Local variables and definitions
 */

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

typedef struct {
	Gadget		Gadget;
	UBYTE		Type;
	UBYTE		Number;
	UBYTE		Value;
	UBYTE		KeyEquiv;
	UWORD		Flags;
} SuperGadget, *SuperGadgPtr;

enum {
	STATE_OFF, STATE_OFFSEL, STATE_ON, STATE_ONSEL
};

static TextChar	textUndoBuff[GADG_MAX_STRING];	/* Undo buff for all EDIT_TEXT gadgets */

static struct StringExtend stringExtend;		/* Initialized to all 0's */

/*
 *	Local prototypes
 */

BOOL	AddGadgText(GadgetPtr, TextPtr, WORD, WORD, WORD);
BOOL	SetGadgetIntuiText(GadgetPtr, TextPtr, WORD);

void	SetKeyEquivText(GadgetPtr, TextChar);

void	DrawBox(RastPtr, WORD, WORD, WORD, WORD);

BOOL	MakePushButton(GadgetPtr, TextPtr, TextChar);
void	DisposePushButton(GadgetPtr);

void	DrawCheckBox(ImagePtr, WORD);
BOOL	MakeCheckBox(GadgetPtr, TextPtr, TextChar);
void	DisposeCheckBox(GadgetPtr);

void	DrawRadioButton(ImagePtr, WORD);
BOOL	MakeRadioButton(GadgetPtr, TextPtr, TextChar);
void	DisposeRadioButton(GadgetPtr);

BOOL	MakeSpecialBtn(GadgetPtr, TextPtr, TextChar, BOOL);
void	DisposeSpecialBtn(GadgetPtr);

/*
 *	Add or append intuiText to gadget
 */

static BOOL AddGadgText(GadgetPtr gadget, TextPtr text, WORD len, WORD left, WORD top)
{
	TextChar ch;
	IntuiTextPtr intuiText;

	if (text) {
		ch = text[len];
		text[len] = '\0';
	}
	intuiText = NewIntuiText(left, top, text, FS_NORMAL, _tbPenBlack);
	if (text)
		text[len] = ch;
	if (intuiText == NULL)
		return (FALSE);
	if (gadget->GadgetText)
		AppendIntuiText(gadget->GadgetText, intuiText);
	else
		gadget->GadgetText = intuiText;
	return (TRUE);
}

/*
 *	Set gadget text, making new lines for each line feed character
 */

static BOOL SetGadgetIntuiText(GadgetPtr gadget, TextPtr text, WORD left)
{
	register WORD len, top;

	if (text) {
		len = top = 0;
		for (;;) {
			if (text[len] != '\n' && text[len] != '\0')
				len++;
			else {
				if (!AddGadgText((GadgetPtr) gadget, text, len, left, top))
					return (FALSE);
				if (text[len] == '\0')
					break;
				text += len + 1;
				len = 0;
				top += _tbYSize + 2;
			}
		}
	}
	else {
		if (!AddGadgText((GadgetPtr) gadget, NULL, 0, left, 0))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Add marker for gadget key equivalent
 */

static void SetKeyEquivText(GadgetPtr gadget, TextChar keyEquiv)
{
	TextChar ch;
	TextPtr text;
	IntuiTextPtr intuiText, newIntuiText;

	intuiText = gadget->GadgetText;
	newIntuiText = NewIntuiText(intuiText->LeftEdge, intuiText->TopEdge + 1,
								"_", FS_NORMAL, intuiText->FrontPen);
	if (newIntuiText == NULL)
		return;
	for (text = intuiText->IText; *text; text++) {
		if (toUpper[*text] == toUpper[keyEquiv]) {
			ch = *text;
			*text = '\0';
			newIntuiText->LeftEdge += IntuiTextLength(intuiText);
			*text = ch;
			break;
		}
	}
	AppendIntuiText(intuiText, newIntuiText);
}

/*
 *	Draw box
 */

static void DrawBox(RastPtr rPort, WORD width, WORD height, WORD pen, WORD inset)
{
	BorderPtr border;

	if ((border = BoxBorder(width, height, pen, inset)) != NULL) {
		DrawBorder(rPort, border, 0, 0);
		FreeBorder(border);
	}
}

/*
 *	Make push button gadget
 */

static BOOL MakePushButton(GadgetPtr gadget, TextPtr text, TextChar keyEquiv)
{
	WORD i, left, top, width, height, depth, len, planeSize, keyOffset;
	UBYTE *imageData;
	BOOL success;
	TextFontPtr font;
	RastPtr rPort;

	success = FALSE;
	font = NULL;
	rPort = NULL;
/*
	Get font, rPort, and image structures
*/
	width  = gadget->Width;
	height = gadget->Height;
	depth = _tbScreen->RastPort.BitMap->Depth;
	if ((font = OpenFont(&_tbTextAttr)) == NULL ||
		(rPort = CreateRastPort(width, height, depth)) == NULL ||
		(gadget->GadgetRender = NewImage(width, height, depth)) == NULL ||
		(gadget->SelectRender = NewImage(width, height, depth)) == NULL)
		goto Exit;
	SetFont(rPort, font);
	SetDrMd(rPort, JAM1);
	len = strlen(text);
	left = (width - TextLength(rPort, text, len))/2;
	top  = (height - _tbYSize + 1)/2 + font->tf_Baseline;
	planeSize = RASSIZE(width, height);
/*
	Find offset to key equiv char
*/
	keyOffset = 0;
	for (i = 0; text[i]; i++) {
		if (toUpper[text[i]] == toUpper[keyEquiv]) {
			keyOffset = TextLength(rPort, text, i);
			break;
		}
	}
/*
	Draw normal image
*/
	SetRast(rPort, _tbPenLight);
	SetAPen(rPort, _tbPenBlack);
	Move(rPort, left, top);
	Text(rPort, text, len);
	if (keyEquiv) {
		Move(rPort, left + keyOffset, top + 1);
		Text(rPort, "_", 1);
	}
	DrawShadowBox(rPort, 0, 0, width, height, 0, TRUE);
	if (!_tbNoShadows)
		DrawShadowBox(rPort, 0, 0, width, height, 1, TRUE);
	WaitBlit();
	imageData = (UBYTE *) ((ImagePtr) gadget->GadgetRender)->ImageData;
	for (i = 0; i < depth; i++)
		BlockMove(rPort->BitMap->Planes[i], imageData + i*planeSize, planeSize);
/*
	Draw selected image
*/
	SetRast(rPort, _tbPenDark);
	SetAPen(rPort, _tbPenWhite);
	SetDrMd(rPort, JAM1);
	Move(rPort, left, top);
	Text(rPort, text, len);
	if (keyEquiv) {
		Move(rPort, left + keyOffset, top + 1);
		Text(rPort, "_", 1);
	}
	DrawShadowBox(rPort, 0, 0, width, height, 0, FALSE);
	if (!_tbNoShadows)
		DrawShadowBox(rPort, 0, 0, width, height, 1, FALSE);
	WaitBlit();
	imageData = (UBYTE *) ((ImagePtr) gadget->SelectRender)->ImageData;
	for (i = 0; i < depth; i++)
		BlockMove(rPort->BitMap->Planes[i], imageData + i*planeSize, planeSize);
/*
	Set gadget data
*/
	gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
	gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
	gadget->GadgetType	|= GTYP_BOOLGADGET;
	((SuperGadgPtr) gadget)->KeyEquiv = keyEquiv;
/*
	All done
*/
	success = TRUE;
Exit:
	if (font)
		CloseFont(font);
	if (rPort)
		DisposeRastPort(rPort);
	return (success);
}

/*
 *	Dispose of push button data
 */

static void DisposePushButton(GadgetPtr gadget)
{
	FreeImage(gadget->GadgetRender);
	FreeImage(gadget->SelectRender);
	gadget->GadgetRender = gadget->SelectRender = NULL;
}

/*
 *	Draw check box state into image
 */

static void DrawCheckBox(ImagePtr image, WORD state)
{
	register WORD i, planeSize, width, height, depth;
	RastPort rPort;
	BitMap bitMap;
	register PLANEPTR imageData;

	width  = image->Width;
	height = image->Height;
	depth  = image->Depth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE(width, height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw into rPort
*/
	switch (state) {
	case STATE_OFF:
	case STATE_OFFSEL:
		i =_tbPenLight;
		break;
	case STATE_ON:
	case STATE_ONSEL:
		i = _tbPenRed;
		break;
	}
	SetRast(&rPort, i);
	switch (state) {
	case STATE_OFF:
		DrawShadowBox(&rPort, 0, 0, width, height, 0, TRUE);
		break;
	case STATE_ON:
		DrawShadowBox(&rPort, 0, 0, width, height, 0, FALSE);
		break;
	case STATE_OFFSEL:
	case STATE_ONSEL:
		DrawShadowBox(&rPort, 0, 0, width, height, 0, FALSE);
		DrawShadowBox(&rPort, 0, 0, width, height, 1, FALSE);
		break;
	}
}

/*
 *	Make check box gadget
 */

static BOOL MakeCheckBox(GadgetPtr gadget, TextPtr text, TextChar keyEquiv)
{
	return (MakeSpecialBtn(gadget, text, keyEquiv, FALSE));
}

/*
 *	Dipose of check box data
 */

static void DisposeCheckBox(GadgetPtr gadget)
{
	DisposeSpecialBtn(gadget);
}

/*
 *	Draw radio button state into image
 */

static void DrawRadioButton(ImagePtr image, WORD state)
{
	register WORD i, planeSize, width, height, depth;
	WORD penBlack, penWhite;
	RastPort rPort;
	BitMap bitMap;
	register PLANEPTR imageData;
	Point ptList[9];

	width  = image->Width;
	height = image->Height;
	depth  = image->Depth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE(width, height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw into rPort
*/
	SetRast(&rPort, _tbPenLight);
	ptList[0].x = width - 1;	ptList[0].y = (height - 1)/2;
	ptList[1].x = 0;			ptList[1].y = 0;
	ptList[2].x = 0;			ptList[2].y = height - 1;
	ptList[3].x = width - 1;	ptList[3].y = (height)/2;
	if (state == STATE_ON || state == STATE_ONSEL) {
		SetAPen(&rPort, _tbPenRed);
		FillPoly(&rPort, 4, ptList);
	}
	penBlack = _tbPenBlack;
	penWhite = (!_tbNoShadows) ? _tbPenWhite : _tbPenBlack;
	switch (state) {
	case STATE_OFF:
		SetAPen(&rPort, penBlack);
		FramePoly(&rPort, 2, &ptList[2]);
		SetAPen(&rPort, penWhite);
		FramePoly(&rPort, 3, &ptList[0]);
		break;
	case STATE_ON:
		SetAPen(&rPort, penWhite);
		FramePoly(&rPort, 2, &ptList[2]);
		SetAPen(&rPort, penBlack);
		FramePoly(&rPort, 3, &ptList[0]);
		break;
	case STATE_OFFSEL:
	case STATE_ONSEL:
		SetAPen(&rPort, penWhite);
		FramePoly(&rPort, 2, &ptList[2]);
		SetAPen(&rPort, penBlack);
		FramePoly(&rPort, 3, &ptList[0]);
		for (i = 0; i < 9; i++) {
			if (ptList[i].x == 0)
				ptList[i].x++;
			else if (ptList[i].x == width - 1)
				ptList[i].x--;
			if (ptList[i].y == 0)
				ptList[i].y++;
			else if (ptList[i].y == height - 1)
				ptList[i].y--;
		}
		SetAPen(&rPort, penWhite);
		FramePoly(&rPort, 2, &ptList[2]);
		SetAPen(&rPort, penBlack);
		FramePoly(&rPort, 3, &ptList[0]);
		break;
	}
}

/*
 *	Make radio button gadget
 */

static BOOL MakeRadioButton(GadgetPtr gadget, TextPtr text, TextChar keyEquiv)
{
	return (MakeSpecialBtn(gadget, text, keyEquiv, TRUE));
}

/*
 *	Dispose of radio button data
 */

static void DisposeRadioButton(GadgetPtr gadget)
{
	DisposeSpecialBtn(gadget);
}

/*
 *	Make check box or radio button gadget
 */

static BOOL MakeSpecialBtn(GadgetPtr gadget, TextPtr text, TextChar keyEquiv,
						   BOOL radBtn)
{
	WORD boxWidth, boxHeight, depth;
	BOOL success;

	success = FALSE;
	gadget->Height	 = boxHeight = _tbYSize;
	boxWidth		 = (12*_tbXSize)/8;
/*
	Get font, rPort, and image structures
*/
	depth = _tbScreen->RastPort.BitMap->Depth;
	if ((gadget->GadgetRender = NewImage(boxWidth, boxHeight, depth)) == NULL ||
		(gadget->SelectRender = NewImage(boxWidth, boxHeight, depth)) == NULL)
		goto Exit;
/*
	Draw "off" state images
*/
	if (radBtn) {
		DrawRadioButton(gadget->GadgetRender, STATE_OFF);
		DrawRadioButton(gadget->SelectRender, STATE_OFFSEL);
	}
	else {
		DrawCheckBox(gadget->GadgetRender, STATE_OFF);
		DrawCheckBox(gadget->SelectRender, STATE_OFFSEL);
	}
/*
	Set gadget data
*/
	gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
	gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
	gadget->GadgetType	|= GTYP_BOOLGADGET;
	((SuperGadgPtr) gadget)->KeyEquiv = keyEquiv;
	if (!SetGadgetIntuiText(gadget, text, _tbXSize*2))
		goto Exit;
	if (keyEquiv)
		SetKeyEquivText(gadget, keyEquiv);
/*
	All done
*/
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Dispose of check box or radio button
 */

static void DisposeSpecialBtn(GadgetPtr gadget)
{
	FreeImage(gadget->GadgetRender);
	FreeImage(gadget->SelectRender);
	gadget->GadgetRender = gadget->SelectRender = NULL;
	FreeIntuiText(gadget->GadgetText);
	gadget->GadgetText = NULL;
}

/*
 *	GetGadgets
 *	Process gadgets template and return pointer to gadget list
 *	Return NULL if error
 */

GadgetPtr GetGadgets(GadgetTemplate gadgTempl[])
{
	register WORD item, type, flags, width, height, left, top;
	TextChar keyEquiv;
	Ptr gadgInfo;
	register SuperGadgPtr gadget, firstGadg, prevGadg;
	StrInfoPtr stringInfo;
	PropInfoPtr propInfo;
	BorderPtr border;

/*
	Process gadget templates
*/
	firstGadg = prevGadg = NULL;
	for (item = 0; gadgTempl[item].Type != GADG_ITEM_NONE; item++) {
		if ((gadget = MemAlloc(sizeof(SuperGadget), MEMF_CLEAR)) == NULL) {
			DisposeGadgets((GadgetPtr) firstGadg);
			return (NULL);
		}
		if (firstGadg == NULL)
			firstGadg = gadget;
		if (prevGadg)
			prevGadg->Gadget.NextGadget = (GadgetPtr) gadget;
/*
	Assign common parameters
*/
		gadget->Gadget.LeftEdge		= left		=
			(gadgTempl[item].LeftEdge*_tbXSize)/8 + gadgTempl[item].LeftOffset;
		gadget->Gadget.TopEdge		= top		=
			(gadgTempl[item].TopEdge*_tbYSize)/11 + gadgTempl[item].TopOffset;
		gadget->Gadget.Width		= width		=
			(gadgTempl[item].Width*_tbXSize)/8   + gadgTempl[item].WidthOffset;
		gadget->Gadget.Height		= height	=
			(gadgTempl[item].Height*_tbYSize)/11 + gadgTempl[item].HeightOffset;
		gadget->Type	= type = (gadgTempl[item].Type & GADG_TYPEBITS);
		flags			= (gadgTempl[item].Type & GADG_FLAGBITS);
		gadget->Number	= item;
		keyEquiv		= gadgTempl[item].KeyEquiv;
		gadgInfo		= gadgTempl[item].Info;
		if (gadget->Gadget.LeftEdge < 0)
			gadget->Gadget.Flags |= GFLG_RELRIGHT;
		if (gadget->Gadget.TopEdge < 0)
			gadget->Gadget.Flags |= GFLG_RELBOTTOM;
		if (gadget->Gadget.Width < 0)
			gadget->Gadget.Flags |= GFLG_RELWIDTH;
		if (gadget->Gadget.Height < 0)
			gadget->Gadget.Flags |= GFLG_RELHEIGHT;
/*
	Find and process the specified gadget type
*/
		switch (type) {
/*
	User Item
*/
		case GADG_USER_ITEM:
			if (firstGadg == gadget)
				firstGadg = gadgInfo;
			if (prevGadg)
				prevGadg->Gadget.NextGadget = gadgInfo;
			MemFree(gadget, sizeof(SuperGadget));
			gadget = (SuperGadgPtr) gadgInfo;
			break;
/*
	Push Button
*/
		case GADG_PUSH_BUTTON:
			if (!MakePushButton((GadgetPtr) gadget, gadgInfo, keyEquiv)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Check Box
*/
		case GADG_CHECK_BOX:
			if (!MakeCheckBox((GadgetPtr) gadget, gadgInfo, keyEquiv)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Radio Button
*/
		case GADG_RADIO_BUTTON:
			if (!MakeRadioButton((GadgetPtr) gadget, gadgInfo, keyEquiv)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Static Text
*/
		case GADG_STAT_TEXT:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			if (!SetGadgetIntuiText((GadgetPtr) gadget, (TextPtr) gadgInfo, 0)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Edit Text
*/
		case GADG_EDIT_TEXT:
			gadget->Gadget.Flags		|= GFLG_GADGHCOMP;
			gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_STRGADGET;
			gadget->Gadget.Height		 = height = _tbYSize;
			if ((gadget->Gadget.GadgetRender = ShadowBoxBorder(width, height, -2, FALSE)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			if (!_tbNoShadows && (border = ShadowBoxBorder(width, height, -3, TRUE)) != NULL)
				AppendBorder(gadget->Gadget.GadgetRender, border);
			if ((stringInfo = MemAlloc(sizeof(StringInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			gadget->Gadget.SpecialInfo = stringInfo;
			stringInfo->MaxChars = GADG_MAX_STRING;
			if ((gadgInfo && strlen((BYTE *) gadgInfo) >= GADG_MAX_STRING) ||
				(stringInfo->Buffer = MemAlloc(GADG_MAX_STRING, 0)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			stringInfo->UndoBuffer = textUndoBuff;
			if (gadgInfo)
				strcpy(stringInfo->Buffer, (BYTE *) gadgInfo);
			else
				stringInfo->Buffer[0] = '\0';
			if (flags & GADG_EDIT_NUMONLY)
				gadget->Gadget.Activation	|= GACT_LONGINT;
			if (flags & GADG_EDIT_RETURNCYCLE)
				gadget->Flags = GADG_EDIT_RETURNCYCLE;
			if (LibraryVersion(IntuitionBase) >= OSVERSION_2_0) {
				gadget->Gadget.Flags	|= GFLG_TABCYCLE | GFLG_STRINGEXTEND;
				stringInfo->Extension = &stringExtend;
				stringExtend.Pens[0] = _tbPenBlack;
				stringExtend.Pens[1] = _tbPenLight;
				stringExtend.ActivePens[0] = _tbPenWhite;
				stringExtend.ActivePens[1] = _tbPenBlack;
			}
			break;
/*
	Border item
*/
		case GADG_STAT_BORDER:
		case GADG_ACTIVE_BORDER:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			if (type == GADG_ACTIVE_BORDER)
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = gadgInfo;
			break;
/*
	Image item
*/
		case GADG_STAT_IMAGE:
		case GADG_ACTIVE_IMAGE:
			if (type == GADG_ACTIVE_IMAGE) {
				gadget->Gadget.Flags		|= GFLG_GADGHCOMP | GFLG_GADGIMAGE;
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.GadgetType		|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = gadgInfo;
			break;
/*
	Standard border
*/
		case GADG_STAT_STDBORDER:
		case GADG_ACTIVE_STDBORDER:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			if (type == GADG_ACTIVE_STDBORDER)
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = GetStdBorder((WORD) gadgInfo, width, height);
			break;
/*
	Standard Image item
*/
		case GADG_STAT_STDIMAGE:
		case GADG_ACTIVE_STDIMAGE:
			if (type == GADG_ACTIVE_STDIMAGE) {
				gadget->Gadget.Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.GadgetType		|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender		 = GetStdImage((WORD) gadgInfo, width, height, FALSE);
			if (type == GADG_ACTIVE_STDIMAGE)
				gadget->Gadget.SelectRender	 = GetStdImage((WORD) gadgInfo, width, height, TRUE);
			break;
/*
	Proportional gadgets
*/
		case GADG_PROP_VERT:
		case GADG_PROP_HORIZ:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_PROPGADGET;
			if ((gadget->Gadget.GadgetRender = MemAlloc(sizeof(Image), MEMF_CLEAR)) == NULL ||
				(propInfo = MemAlloc(sizeof(PropInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			gadget->Gadget.SpecialInfo = propInfo;
			propInfo->Flags = (type == GADG_PROP_VERT) ? 
							  AUTOKNOB | FREEVERT : AUTOKNOB | FREEHORIZ;
			if (flags & GADG_PROP_NOBORDER) {
				propInfo->Flags |= PROPBORDERLESS;
				gadget->Gadget.LeftEdge	+= 1;
				gadget->Gadget.TopEdge	+= 1;
				gadget->Gadget.Width	-= 2;
				gadget->Gadget.Height	-= 2;
			}
			if (flags & GADG_PROP_NEWLOOK)
				propInfo->Flags |= PROPNEWLOOK;
			propInfo->HorizBody = propInfo->VertBody = MAXBODY;
			break;
		}
		prevGadg = gadget;
	}
	return ((GadgetPtr) firstGadg);
}

/*
 *	DisposeGadgets
 *	Release all memory used by gadget list created by GetGadgets
 *	Ignores all system gadgets
 */

void DisposeGadgets(register GadgetPtr gadgList)
{
	register GadgetPtr nextGadget;
	register StrInfoPtr stringInfo;

	while (gadgList) {
		nextGadget = gadgList->NextGadget;
		if ((gadgList->GadgetType & GTYP_SYSGADGET) == 0) {
			switch (GadgetType(gadgList)) {
			case GADG_USER_ITEM:
				break;
			case GADG_PUSH_BUTTON:
				DisposePushButton(gadgList);
				break;
			case GADG_CHECK_BOX:
				DisposeCheckBox(gadgList);
				break;
			case GADG_RADIO_BUTTON:
				DisposeRadioButton(gadgList);
				break;
			case GADG_STAT_TEXT:
				FreeIntuiText(gadgList->GadgetText);
				break;
			case GADG_EDIT_TEXT:
				FreeBorder(gadgList->GadgetRender);
				if ((stringInfo = gadgList->SpecialInfo) != NULL) {
					if (stringInfo->Buffer)
						MemFree(stringInfo->Buffer, GADG_MAX_STRING);
					MemFree(stringInfo, sizeof(StringInfo));
				}
				break;
			case GADG_STAT_STDBORDER:
			case GADG_ACTIVE_STDBORDER:
				FreeBorder(gadgList->GadgetRender);
				break;
			case GADG_STAT_STDIMAGE:
			case GADG_ACTIVE_STDIMAGE:
				FreeImage(gadgList->GadgetRender);
				FreeImage(gadgList->SelectRender);
				break;
			case GADG_STAT_BORDER:
			case GADG_ACTIVE_BORDER:
			case GADG_STAT_IMAGE:
			case GADG_ACTIVE_IMAGE:
				break;
			case GADG_PROP_VERT:
			case GADG_PROP_HORIZ:
				if (gadgList->GadgetRender)
					MemFree(gadgList->GadgetRender, sizeof(Image));
				if (gadgList->SpecialInfo)
					MemFree(gadgList->SpecialInfo, sizeof(PropInfo));
				break;
			}
			MemFree(gadgList, sizeof(SuperGadget));
		}
		gadgList = nextGadget;
	}
}

/*
 *	GadgetItem
 *	Return a pointer to the gadget identified by its item number
 *	Ignore all system gadgets
 *	If no such gadget then return NULL
 */

GadgetPtr GadgetItem(GadgetPtr gadgList, register WORD item)
{
	register GadgetPtr gadget;

	for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
		if ((gadget->GadgetType & GTYP_SYSGADGET) == 0 && GadgetNumber(gadget) == item)
			break;
	}
	return (gadget);
}

/*
 *	GadgetNumber
 *	Return the gadget number of this gadget
 */

WORD GadgetNumber(GadgetPtr gadget)
{
	return ((WORD) ((SuperGadgPtr) gadget)->Number);
}

/*
 *	GadgetType
 *	Return the gadget type
 */

WORD GadgetType(GadgetPtr gadget)
{
	return ((WORD) ((SuperGadgPtr) gadget)->Type);
}

/*
 *	GetGadgetValue
 *	Get the gadget's value
 *	Valid for gadget types of GADG_CHECK_BOX and GADG_RADIO_BUTTON only!
 */

WORD GetGadgetValue(register GadgetPtr gadget)
{
	register WORD gadgType;

	if (gadget == NULL)
		return (0);
	gadgType = GadgetType(gadget);
	if (gadgType != GADG_CHECK_BOX && gadgType != GADG_RADIO_BUTTON)
		return (0);
	return ((WORD) ((SuperGadgPtr) gadget)->Value);
}

/*
 *	SetGadgetValue
 *	Set the gadget's value to on or off and redraw the gadget
 *	Valid for gadget types of GADG_CHECK_BOX and GADG_RADIO_BUTTON only!
 */

void SetGadgetValue(GadgetPtr gadget, WindowPtr window, RequestPtr request, BOOL value)
{
	register WORD gadgType, position;

	if (gadget) {
		gadgType = GadgetType(gadget);
		if ((gadgType != GADG_CHECK_BOX && gadgType != GADG_RADIO_BUTTON) ||
			((SuperGadgPtr) gadget)->Value == value)
			return;
		position = RemoveGadget(window, gadget);
		if (value) {
			((SuperGadgPtr) gadget)->Value = 1;
			if (gadgType == GADG_CHECK_BOX) {
				DrawCheckBox(gadget->GadgetRender, STATE_ON);
				DrawCheckBox(gadget->SelectRender, STATE_ONSEL);
			}
			else {
				DrawRadioButton(gadget->GadgetRender, STATE_ON);
				DrawRadioButton(gadget->SelectRender, STATE_ONSEL);
			}
		}
		else {
			((SuperGadgPtr) gadget)->Value = 0;
			if (gadgType == GADG_CHECK_BOX) {
				DrawCheckBox(gadget->GadgetRender, STATE_OFF);
				DrawCheckBox(gadget->SelectRender, STATE_OFFSEL);
			}
			else {
				DrawRadioButton(gadget->GadgetRender, STATE_OFF);
				DrawRadioButton(gadget->SelectRender, STATE_OFFSEL);
			}
		}
		AddGList(window, gadget, position, 1, request);
		RefreshGList(gadget, window, request, 1);
	}
}

/*
 *	SetGadgetItemValue
 *	Set gadget value given item number
 */

void SetGadgetItemValue(GadgetPtr gadgList, WORD item, WindowPtr window,
						RequestPtr request, BOOL value)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	SetGadgetValue(gadget, window, request, value);
}

/*
 *	HiliteGadget
 *	Set the gadget's display appearance to selected or not selected
 */

void HiliteGadget(register GadgetPtr gadget, WindowPtr window, RequestPtr request,
				  BOOL hilite)
{
	register WORD position;
	register UWORD flags;
	BOOL wasSelected;
	RastPtr rPort;
	Rectangle gadgRect;

	if ((rPort = (request) ? request->ReqLayer->rp : window->RPort) == NULL)
		return;
	wasSelected = (gadget->Flags & GFLG_SELECTED);
	if ((hilite && wasSelected) || (!hilite && !wasSelected))
		return;
	if ((position = RemoveGadget(window, gadget)) == -1)
		return;
	if (hilite)
		gadget->Flags |= GFLG_SELECTED;
	else
		gadget->Flags &= ~GFLG_SELECTED;
	GetGadgetRect(gadget, window, request, &gadgRect);
	flags = gadget->Flags;
	if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHCOMP) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &gadgRect);
	}
	else if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHBOX && wasSelected) {
		SetDrMd(rPort, COMPLEMENT);
		RectFill(rPort, gadgRect.MinX - 4, gadgRect.MinY - 2,
						gadgRect.MaxX + 4, gadgRect.MaxY + 2);
	}
	(void) AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
}

/*
 *	GadgetSelected
 *	Return TRUE if gadget is selected
 */

BOOL GadgetSelected(GadgetPtr gadgList, WORD gadgNum)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	return ((gadget->Flags & GFLG_SELECTED) != 0);
}

/*
 *	Enable/disable gadget list
 *	Waits until gadget are not selected before changing
 */

void OnOffGList(register GadgetPtr gadgList, WindowPtr window, RequestPtr request,
				register WORD num, BOOL on)
{
	register WORD i, position;
	register BOOL selected;
	register GadgetPtr gadget;
	RastPtr rPort;
	Rectangle gadgRect;

	if ((rPort = (request) ? request->ReqLayer->rp : window->RPort) == NULL)
		return;
/*
	Scan gadget list and wait until none are selected
*/
	do {
		selected = FALSE;
		for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
			if (gadget->Flags & GFLG_SELECTED)
				selected = TRUE;
		}
	} while (selected);
/*
	Turn on/off the gadgets
*/
	if ((position = RemoveGList(window, gadgList, num)) == -1)
		return;
	for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
		if (on)
			gadget->Flags &= ~GFLG_DISABLED;
		else
			gadget->Flags |= GFLG_DISABLED;
		GetGadgetRect(gadget, window, request, &gadgRect);
		SetAPen(rPort, _tbPenLight);
		SetDrMd(rPort, JAM1);
		FillRect(rPort, &gadgRect);
	}
	(void) AddGList(window, gadgList, position, num, request);
	RefreshGList(gadgList, window, request, num);
}

/*
 *	OnGList
 *	Enable the specified number of gadgets
 *	Unlike OnGadget(), this function only refreshes the specified gadgets
 */

void OnGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, TRUE);
}

/*
 *	OffGList
 *	Disable the specified number of gadgets
 *	Unlike OffGadget(), this function only refreshes the specified gadgets
 */

void OffGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, FALSE);
}

/*
 *	EnableGadgetItem
 *	Enable or disable the specified gadget
 *	Only refreshes if gadget state changed
 */

void EnableGadgetItem(GadgetPtr gadgList, WORD gadgNum, WindowPtr window,
					  RequestPtr request, BOOL enable)
{
	register GadgetPtr gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	if (enable) {
		if (gadget->Flags & GFLG_DISABLED)
			OnOffGList(gadget, window, request, 1, TRUE);
	}
	else {
		if ((gadget->Flags & GFLG_DISABLED) == 0)
			OnOffGList(gadget, window, request, 1, FALSE);
	}
}

/*
 *	OutlineButton
 *	Add or remove outlining of specified button
 */

void OutlineButton(GadgetPtr gadget, WindowPtr window, RequestPtr request, BOOL on)
{
	WORD width, height;
	BorderPtr border;
	RastPtr rPort;
	Rectangle gadgRect;

	if ((rPort = (request) ? request->ReqLayer->rp : window->RPort) == NULL ||
		GadgetType(gadget) != GADG_PUSH_BUTTON)
		return;
	GetGadgetRect(gadget, window, request, &gadgRect);
	width  = gadgRect.MaxX - gadgRect.MinX + 1;
	height = gadgRect.MaxY - gadgRect.MinY + 1;
	border = (on) ? ShadowBoxBorder(width, height, -2, FALSE) :
					BoxBorder(width, height, _tbPenLight, -2);
	if (border) {
		DrawBorder(rPort, border, gadgRect.MinX, gadgRect.MinY);
		FreeBorder(border);
	}
}

/*
 *	GetEditItemText
 *	Get edit text and put in specified buffer
 */

void GetEditItemText(GadgetPtr gadgList, WORD item, TextPtr buff)
{
	register GadgetPtr gadget;
	StrInfoPtr stringInfo;

	gadget = GadgetItem(gadgList, item);
	stringInfo = gadget->SpecialInfo;
	strcpy(buff, stringInfo->Buffer);
}

/*
 *	SetEditItemText
 *	Set specified edit text buffer to new contents
 *	If buff is NULL, clear edit text
 */

void SetEditItemText(GadgetPtr gadgList, WORD item, WindowPtr window, RequestPtr request,
					 TextPtr buff)
{
	register WORD position;
	register GadgetPtr gadget;
	register StrInfoPtr stringInfo;
	BOOL wasSelected;

	gadget = GadgetItem(gadgList, item);
	wasSelected = gadget->Flags & GFLG_SELECTED;
	position = RemoveGadget(window, gadget);
	gadget->Flags &= ~GFLG_SELECTED;			/* Need this for proper redrawing */
	stringInfo = gadget->SpecialInfo;
	if (buff)
		strcpy(stringInfo->Buffer, buff);
	else
		stringInfo->Buffer[0] = '\0';
	stringInfo->BufferPos = stringInfo->DispPos = 0;
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
	if (wasSelected) {
		ActivateGadget(gadget, window, request);
		Delay(5);
	}
}

/*
 *	GetGadgetItemText
 *	Get gadget text (not edit text)
 */

void GetGadgetItemText(GadgetPtr gadgList, WORD item, TextPtr text)
{
	IntuiTextPtr intuiText;
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	intuiText = gadget->GadgetText;
	if (intuiText && intuiText->IText)
		strcpy(text, intuiText->IText);
	else
		*text = '\0';
}

/*
 *	SetGadgetText
 *	Set gadget text (not edit text) to new text
 */

void SetGadgetText(GadgetPtr gadget, WindowPtr window, RequestPtr request, TextPtr text)
{
	WORD position, left;
	IntuiTextPtr intuiText;

	if ((intuiText = gadget->GadgetText) != NULL) {
		left = intuiText->LeftEdge;
		while (intuiText) {
			intuiText->FrontPen = _tbPenLight;
			intuiText = intuiText->NextText;
		}
		RefreshGList(gadget, window, request, 1);
	}
	else
		left = 0;
	position = RemoveGadget(window, gadget);
	FreeIntuiText(gadget->GadgetText);
	gadget->GadgetText = NULL;
	SetGadgetIntuiText(gadget, text, left);
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
	((SuperGadgPtr) gadget)->KeyEquiv = '\0';
}


/*
 *	SetGadgetItemText
 *	Set gadget text, given item number
 */

void SetGadgetItemText(GadgetPtr gadgList, WORD item, WindowPtr window,
					   RequestPtr request, TextPtr text)
{
	SetGadgetText(GadgetItem(gadgList, item), window, request, text);
}

/*
 *	SetButtonItem
 *	Change button text & key equivalent
 */

void SetButtonItem(GadgetPtr gadgList, WORD item, WindowPtr window, RequestPtr request,
				   TextPtr text, TextChar keyEquiv)
{
	WORD position;
	GadgetPtr gadget;
	RastPtr rPort;
	Rectangle rect;

	gadget = GadgetItem(gadgList, item);
	position = RemoveGadget(window, gadget);
	switch (GadgetType(gadget)) {
	case GADG_PUSH_BUTTON:
		DisposePushButton(gadget);
		MakePushButton(gadget, text, keyEquiv);
		break;
	case GADG_CHECK_BOX:
		DisposeCheckBox(gadget);
		MakeCheckBox(gadget, text, keyEquiv);
		if (((SuperGadgPtr) gadget)->Value) {
			DrawCheckBox(gadget->GadgetRender, STATE_ON);
			DrawCheckBox(gadget->SelectRender, STATE_ONSEL);
		}
		break;
	case GADG_RADIO_BUTTON:
		DisposeRadioButton(gadget);
		MakeRadioButton(gadget, text, keyEquiv);
		if (((SuperGadgPtr) gadget)->Value) {
			DrawRadioButton(gadget->GadgetRender, STATE_ON);
			DrawRadioButton(gadget->SelectRender, STATE_ONSEL);
		}
		break;
	}
	GetGadgetRect(gadget, window, request, &rect);
	if ((rPort = (request) ? request->ReqLayer->rp : window->RPort) != NULL) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &rect);
	}
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
}

/*
 *	GetGadgetRect
 *	Return rectangle containing gadget
 */

void GetGadgetRect(register GadgetPtr gadget, WindowPtr window, RequestPtr request,
				   RectPtr rect)
{
	register WORD leftEdge, topEdge, width, height;
	WORD elemWidth, elemHeight, flags;

	if (gadget->GadgetType & GTYP_REQGADGET) {
		elemWidth  = request->Width;
		elemHeight = request->Height;
	}
	else {
		elemWidth  = window->Width;
		elemHeight = window->Height;
	}
	leftEdge = gadget->LeftEdge;
	topEdge = gadget->TopEdge;
	width = gadget->Width;
	height = gadget->Height;
	flags = gadget->Flags;
	if (flags & GFLG_RELRIGHT)
		leftEdge += elemWidth - 1;
	if (flags & GFLG_RELBOTTOM)
		topEdge += elemHeight - 1;
	if (flags & GFLG_RELWIDTH)
		width += elemWidth - 1;
	if (flags & GFLG_RELHEIGHT)
		height += elemHeight - 1;
	rect->MinX = leftEdge;
	rect->MinY = topEdge;
	rect->MaxX = leftEdge + width - 1;
	rect->MaxY = topEdge + height - 1;
}

/*
 *	TrackGadget
 *	Get messages from message port and check for gadget being selected
 *	Continuously call actionProc(window, gadgetNum) while gadget is selected
 *	Will call actionProc at least once
 *	Return when mouse button is up
 */

void TrackGadget(MsgPortPtr msgPort, register WindowPtr window,
				 register GadgetPtr gadget, void (*actionProc)(WindowPtr, WORD))
{
	register WORD gadgetNum;
	register IntuiMsgPtr intuiMsg;
	register ULONG class;
	register UWORD qualifier;
	BOOL actionDone;
	WindowPtr msgWindow;

	gadgetNum = GadgetNumber(gadget);
	actionDone = FALSE;
/*
	Get and process messages
	Should only get GADGETUP and possible MOUSEMOVE or INTUITICKS messages,
		anything else is invalid
*/
	for (;;) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) {
			class     = intuiMsg->Class;
			qualifier = (class != IDCMP_INTUITICKS) ? intuiMsg->Qualifier :
												IEQUALIFIER_LEFTBUTTON;
			msgWindow = intuiMsg->IDCMPWindow;
/*
	If message is not for us, then put it back and return
*/
			if (msgWindow != window ||
				(class != IDCMP_MOUSEMOVE && class != IDCMP_INTUITICKS) ||
				!SELECTBUTTON(qualifier)) {
				PutMsg(msgPort, (MsgPtr) intuiMsg);
				if (actionProc && !actionDone)
					(*actionProc)(window, gadgetNum);
				return;
			}
			ReplyMsg((MsgPtr) intuiMsg);
/*
	If INTUITICKS message and actionProc, then call actionProc
*/
			if (class == IDCMP_INTUITICKS && actionProc &&
				(gadget->Flags & GFLG_SELECTED)) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
/*
	If no messages pending, then call actionProc or wait for message
	(Only call actionProc here if INTUITICKS is not enabled)
*/
		if ((window->IDCMPFlags & IDCMP_INTUITICKS) == 0 && actionProc) {
			if (gadget->Flags & GFLG_SELECTED) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
		else
			Wait(1 << msgPort->mp_SigBit);
	}
}

/*
 *	IsEditReturnCycle
 *	Return TRUE if gadget is return-cycle text box
 */

BOOL IsEditReturnCycle(register GadgetPtr gadget)
{
	return ((gadget->GadgetType & GTYP_SYSGADGET) == 0 &&
			(gadget->GadgetType & GTYP_STRGADGET) &&
			(((SuperGadgPtr) gadget)->Flags & GADG_EDIT_RETURNCYCLE));
}

/*
 *	IsGadgetKey
 *	Determine if character is key equivalent for gadget
 */

BOOL IsGadgetKey(GadgetPtr gadget, TextChar ch)
{
	WORD type;

	type = GadgetType(gadget);
	if (type == GADG_PUSH_BUTTON || type == GADG_CHECK_BOX || type == GADG_RADIO_BUTTON)
		return (toUpper[((SuperGadgPtr) gadget)->KeyEquiv] == toUpper[ch]);
	return (FALSE);
}
