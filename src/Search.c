/*
 * Flow
 * Copyright (c) 1991 New Horizons Software
 *
 * Search/Sort functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>
#include <proto/exec.h>

#include <string.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Flow.h"
#include "Proto.h"

/*
 * Global variables
 */
 
TextChar findBuffer[GADG_MAX_STRING];
TextChar changeBuffer[GADG_MAX_STRING];
BOOL wholeWord, matchCase, openOnly;
BOOL stopSearch = FALSE;		/* Always disabled for this version */

/*
 * External variables
 */

extern DialogPtr	findDialog, changeDialog;

extern WindowPtr	cmdWindow;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern DlgTemplPtr	dlgList[];

extern TextChar	strBuff[];
extern TextChar	strChangedNum[], strStop[], strDone[];

extern WORD			modelessStrItem;

/*
 * Local variables
 */

#define GOTO_TEXT	3
						
static BOOL wrapped;
static HeadPtr firstHeadSearched;
static WORD firstLocSearched;

/*
 * Print options for SetFindOption()
 */

#define NUM_OPTIONS  (sizeof(findOptNames)/sizeof(TextPtr))

static TextPtr findOptNames[] = {
	"WholeWord",	"NoWholeWord", "MatchCase",	"NoMatchCase",
	"SkipHidden",	"NoSkipHidden"
};

enum {
	OPT_WHOLEWORD, 	OPT_NOWHOLEWORD,  OPT_MATCHCASE,	OPT_NOMATCHCASE,
	OPT_SKIPHIDDEN,	OPT_NOSKIPHIDDEN
};

/*
 * Local prototypes
 */

void InitSearchDialog(DialogPtr);

/*
 *	Set find option given option name
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

BOOL SetFindOption(TextPtr optName, WORD len)
{
	register WORD i;

	for (i = 0; i < NUM_OPTIONS; i++) {
		if (CmpString(optName, findOptNames[i], len, (WORD) strlen(findOptNames[i]), FALSE)
			== 0)
			break;
	}
	if (i >= NUM_OPTIONS)
		return (FALSE);
	switch (i) {
	case OPT_WHOLEWORD:
	case OPT_NOWHOLEWORD:
		wholeWord = (BOOL) (i == OPT_WHOLEWORD);
		break;
	case OPT_MATCHCASE:
	case OPT_NOMATCHCASE:
		matchCase = (BOOL) (i == OPT_MATCHCASE);
		break;
	case OPT_SKIPHIDDEN:
	case OPT_NOSKIPHIDDEN:
		openOnly = (BOOL) (i == OPT_SKIPHIDDEN);
		break;
	}
	return (TRUE);
}

/*
 *	Return length of word at given location (used by Document Info routine)
 */

WORD WordLen(head, loc)
HeadPtr head;
register WORD loc;
{
	register TextPtr text = head->Text + loc;
	register WORD len, maxLen = head->Len - loc;

	for (len = 0; len < maxLen && wordChar[*text++]; len++) ;
	return (len);
}

/*
 * Skip spaces in heading from current position
 * Return new position
 */

WORD SkipSpace(HeadPtr head, WORD loc)
{
	register TextPtr text = head->Text + loc;
	register WORD maxLoc = head->Len;

	while (loc < maxLoc && !wordChar[*text++])
		loc++;
	return (loc);
}

/*
 * Skip to beginning of next word
 * Return new position
 */

WORD SkipWord(HeadPtr head, WORD loc)
{
	register TextPtr text = head->Text + loc;
	register WORD maxLoc = head->Len;

	while (loc < maxLoc && wordChar[*text++])
		loc++;
	return (SkipSpace(head, loc));
}

/*
 * Find text in findBuffer subject to settings of wholeWord and matchCase
 * Set selStart and selEnd to located text
 * If text not found, do not change selStart and selEnd
 * Return success status
 */

BOOL FindText(TextPtr findBuff )
{
	register TextPtr text;
	register BOOL success = FALSE;
	register WORD loc, len, maxLoc, item;
	register HeadPtr head;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	
	if( len = strlen(findBuff) ) {
/*
	Start from current selEnd and search forward
*/
		head = docData->SelHead;
		loc = docData->SelEnd;
		
		UnSelectHeads(docData);

		while( (head != NULL) && !success ) {
			text = head->Text;
/*
	The end of the head is the length unless SelOnly flag and only one head to do
*/
			maxLoc = head->Len;
				
			if (wholeWord)
				loc = SkipSpace(head, loc);
			
			while( loc + len  <= maxLoc ) {
				if( head == firstHeadSearched && loc >= firstLocSearched && wrapped)
					break;
				if( (!wholeWord || loc + len == maxLoc || !wordChar[text[loc+len]]) &&
					CmpString(text + loc, findBuff, len, len, matchCase) == 0 ) {
					success = TRUE;
					break;
				}
				if( wholeWord )
					loc = SkipWord(head,loc);
				else
					loc++;
			}
			if( head == firstHeadSearched && wrapped )
				break;
/*
	If not found then skip to next head
*/
			if( !success ) {

				head = openOnly ? NextHead(head) : NextHeadAll(head);
				
				if( head != NULL )
					loc = head->BodyStart;
/*
	Check to see if we need to wrap to beginning of document
*/
				else {
					if( stopSearch || wrapped || (firstHeadSearched == docData->FirstHead && firstLocSearched == 0))
						break;
					
					item = StdDialog(DLG_FINDCONTINUE);

					SetStdPointer(cmdWindow, POINTER_WAIT);
					if( item == CANCEL_BUTTON )
						break;
					head = docData->FirstHead;
					loc = 0;
					wrapped = TRUE;
					ScrollToOffset(cmdWindow, 0, docData->LeftOffset);
					CursorOff(cmdWindow);
				}
			}
		} 
	}
/*
	Set return values and return success status
*/
	if( success ) {
		docData->SelHead = head;
		docData->SelStart = loc;
		docData->SelEnd = loc + len;
	}
	return(success);
}

/*
 *	Check to see if word is currently selected in the document
 */

BOOL WordSelected(DocDataPtr docData, TextPtr text)
{
	WORD len;
	HeadPtr selHead;

	selHead = docData->SelHead;
	len = strlen(text);
	return ((BOOL) ( len && ( len == (docData->SelEnd - docData->SelStart) ) &&
					CmpString(&selHead->Text[docData->SelStart], text, len, len, matchCase) == 0));
}

/*
 *	Enable/disable find button in find dialog
 */

void SetFindButton()
{
	GadgetPtr gadgList;

	gadgList = findDialog->FirstGadget;
	GetEditItemText(gadgList, FIND_TEXT, strBuff);
	EnableGadgetItem(gadgList, OK_BUTTON, findDialog, NULL, (BOOL) (strlen(strBuff) != 0) && (cmdWindow != NULL) );
}

/*
 * Find item
 */

BOOL DoFind(TextPtr text)
{
	register BOOL success = TRUE;
	WindowPtr window = ActiveWindow();

/*
	If text specified, set up to find this text
*/
	if( text != NULL ) {
		if( strlen(text) > GADG_MAX_STRING ) {
			ErrBeep();
			success = FALSE;
		} else {
			strcpy(findBuffer, text);
			if (!DoFindNext(window))
				success = FALSE;
				
		}
	} else {
/*
	Otherwise, get text to find
*/
		if( findDialog == NULL ) {
			dlgList[DLG_FIND]->Gadgets[FIND_TEXT].Info = findBuffer;
			if ((findDialog = GetDialog(dlgList[DLG_FIND], screen, mainMsgPort)) == NULL) {
				Error(ERR_NO_MEM);
				success = FALSE;
			} else {
				SetFindButton();
				InitSearchDialog(findDialog);
			}
		} else {
			SelectWindow(findDialog);
		}
	}
	SetSearchMenu();
	return(success);
}

/*
 * Find already specified item
 */

BOOL DoFindNext(WindowPtr window)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = FALSE;
	BOOL found;
	
	if( strlen(findBuffer) == 0 ) {
		Error(ERR_BAD_TEXT);
	} else {
		SetStdPointer(window, POINTER_WAIT);
		CursorOff(window);
		if( WordSelected(docData, findBuffer))
			SetFindEnd(docData->SelHead, docData->SelStart);
		else
			SetFindEnd(docData->SelHead, docData->SelEnd);
		found = FindText(findBuffer);
		CursorOn(window);
		if (!found )
			Error(ERR_NO_FIND);
/*
	Heading found, so expand all super headings
*/
		else {
			RevealHead(docData);
		}
		SetPointerShape();
		success = TRUE;
	}
	return(success);
}

/*
	Draw the selected head by expanding if necessary
*/

void RevealHead(register DocDataPtr docData)
{
	register HeadPtr head;
	
	for( head = docData->SelHead->Super; head != NULL ; head = head->Super) {
		head->Flags |= HD_EXPANDED;
	}
	CountLines(docData);
	DrawWindow(docData->Window);
	AdjustScrollBars(docData->Window);
	ScrollToCursor(docData->Window);
	SetEditMenu();
	SetSearchMenu();
	SetSubHeadMenu();
	SetFormatMenu();
}

/*
 *	Enable/disable buttons in Change dialog
 */

void SetChangeButtons()
{
	register BOOL enable;
	register GadgetPtr gadgList;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	register DialogPtr dlg = changeDialog;
	
	gadgList = dlg->FirstGadget;
	GetEditItemText(gadgList, FIND_TEXT, findBuffer);
/*
	If no find text then disable all buttons
*/
	if (strlen(findBuffer) == 0)
		OffChangeButtons();
/*
	Set Find and Change buttons
*/
	else {
		EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, TRUE);
		enable = WordSelected(docData, findBuffer);
		EnableGadgetItem(gadgList, CHANGEFIND_BUTTON, dlg, NULL, enable);
		EnableGadgetItem(gadgList, CHANGE_BUTTON, dlg, NULL, enable);
		EnableGadgetItem(gadgList, CHANGEALL_BUTTON, dlg, NULL, enable);
	}
}

/*
 *	Turn off change buttons
 */

void OffChangeButtons()
{
	register GadgetPtr gadgList;
	register DialogPtr dlg = changeDialog;
	
	gadgList = dlg->FirstGadget;
	EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, FALSE);
	EnableGadgetItem(gadgList, CHANGEFIND_BUTTON, dlg, NULL, FALSE);
	EnableGadgetItem(gadgList, CHANGE_BUTTON, dlg, NULL, FALSE);
	EnableGadgetItem(gadgList, CHANGEALL_BUTTON, dlg, NULL, FALSE);
}

/*
 *	Find/Change specified text
 */

BOOL DoChange()
{
 	BOOL success = TRUE;

	if( changeDialog == NULL ) {
/*
	Get text to find and change to
*/
		dlgList[DLG_CHANGE]->Gadgets[FIND_TEXT].Info = findBuffer;
		dlgList[DLG_CHANGE]->Gadgets[CHANGE_TEXT].Info = changeBuffer;
/*
	Get request, and make "Find" button a non-end gadget
*/
		if ((changeDialog = GetDialog(dlgList[DLG_CHANGE], screen, mainMsgPort)) == NULL) {
			Error(ERR_NO_MEM);
			success = FALSE;
		} else {
			SetChangeButtons();
			InitSearchDialog(changeDialog);
			SetPointerShape();
		}
	} else {
		SelectWindow(changeDialog);
	}	
	SetAllMenus();
	return(success);
}

/*
 * Set the checkboxes for the find or change modeless dialog
 * Perform other necessary initialization for modeless dialogs
 */
 
static void InitSearchDialog( register DialogPtr dlg )
{
	GadgetPtr gadgList = dlg->FirstGadget;
	
	DoDialogOpen(dlg);
	if( wholeWord )
		SetGadgetItemValue(gadgList, WHOLEWORD_BOX, dlg, NULL, 1);
	if( matchCase )
		SetGadgetItemValue(gadgList, MATCHCASE_BOX, dlg, NULL, 1);
	if( openOnly )
		SetGadgetItemValue(gadgList, OPENONLY_BOX, dlg, NULL, 1);
/*	if( stopSearch )
		SetGadgetItemValue(gadgList, STOPSEARCH_BOX, dlg, NULL, 1);
*/
	modelessStrItem = FIND_TEXT;
	return;
}

/*
 *	Change text in selection range to text in changeBuff
 *	Return success status
 */

BOOL ChangeText(TextPtr changeBuff)
{
	register WORD startLoc, endLoc, len, inc;
	register HeadPtr selHead;
	register BOOL success = FALSE;	
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	
	selHead = docData->SelHead;
	startLoc = docData->SelStart;
	endLoc = docData->SelEnd;
	len = strlen(changeBuff);
	inc = len - (endLoc - startLoc);
	if( startLoc < endLoc ) {
		success = ExpandBuffer(selHead, endLoc, inc) ;
		if( success ) {
			if (len)
				BlockMove(changeBuff, selHead->Text + startLoc, len);
			if (inc) {
				docData->SelEnd += inc;
			}
			selHead->Len += inc;
			CalcLineStarts(docData, selHead);
		}
	}
	return(success);
}

/*
 *	Set end of find operation (for wrapping around to top)
 *	Must call this before FindText()
 */

void SetFindEnd(HeadPtr head, WORD loc)
{
	firstHeadSearched = head;
	firstLocSearched = loc - head->BodyStart;
	wrapped = FALSE;
}

/*
 *	Go to specified page in document
 */

BOOL DoGoToPage(WindowPtr window)
{
	register BOOL done;
	register WORD item;
	register LONG page;
	register DialogPtr dlg;
	GadgetPtr gadgList;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	TextChar goToBuff[GADG_MAX_STRING + 1];
	register BOOL success = FALSE;
	HeadPtr head;
	UWORD line, index;
/*
	Set up Go To request
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_GOTO], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
	} else {
		OutlineOKButton(dlg);
		gadgList = dlg->FirstGadget;
		EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, FALSE);
/*
	Get desired page number
*/
		done = FALSE;
		do {
			WaitPort(mainMsgPort);
			item = CheckDialog(mainMsgPort, dlg, DialogFilter);
			GetEditItemText(gadgList, GOTO_TEXT, goToBuff);
			switch(item) {
			case -1:					/* INTUITICKS message */
				EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL,
							 (BOOL) (strlen(goToBuff) != 0));
				break;
			case OK_BUTTON:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			}
		} while (!done);
		DisposeDialog(dlg);
		EndWait();
		if (item == OK_BUTTON) {
/*
	Check page number for validity
*/
			if (CheckNumber(goToBuff) == FALSE) {
				Error(ERR_PAGE_NUM);
			} else {
				success = TRUE;
				page = StringToNum(goToBuff) - docData->StartPage;
				if (page < 0)
					page = 0;
				else if (page >= docData->Pages)
					page = docData->Pages - 1;
/*
	Scroll to desired page and move cursor with it
*/
				CursorOff(window);
				head = FindFirstPageLine(docData, page, &index, &line);
/*
 * If went throught the whole tree (not finding it), just leave cursor where it is.
 * This would happen when there are not enough lines to reach "pageLine".
 */
 				if( head != NULL ) {
					docData->SelHead = head;
					docData->SelEnd = docData->DragStart = docData->SelStart = head->LineStarts[index] ;
				}
				CursorOn(window);
				ScrollToOffset(window, line, docData->LeftOffset);
				SetAllMenus();
			}
		}
	}
	return( success );
}

/*
 * Find first line and head of a page (also returns which line in the head)
 */
 
HeadPtr FindFirstPageLine( docData, page, resultIndex, resultLine )
register DocDataPtr docData;
UWORD page;
UWORD *resultIndex;
UWORD *resultLine;
{
	register HeadPtr head = docData->FirstHead;
	register UWORD currPage;
	register BOOL found = FALSE;
	register UWORD pageLine = 0;
	register UWORD headLine = 0;
	register UWORD numLines;
	UWORD totalLines = 0;
	
	*resultLine = 0;
	for( currPage = 0 ; currPage < page ; currPage++ ) {
		while( !found ) {
			numLines = head->NumLines;
			for( ; headLine < numLines && !found ; headLine++ ) {
				found = ++pageLine == docData->PageHeight;
				totalLines++;
			}
			if( !found || headLine == numLines ) {
				head = NextHead(head);
				headLine = 0;
				if( head == NULL || ( head->Flags & HD_PAGEBREAK ) ) {
					found = TRUE;
				}
			}
		}
		pageLine = 0;
		found = FALSE;
	}
	*resultLine = totalLines;
	*resultIndex = headLine;
	return(head);
}

/*
 *	Set document mark
 */

static BOOL DoSetMark(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	docData->Mark.Head = docData->SelHead;
	docData->Mark.Loc = docData->SelStart;
	SetSearchMenu();
	return (TRUE);
}

/*
 *	Go to document mark
 */

static BOOL DoGoToMark(WindowPtr window)
{
	TextLocPtr	mark;
	register DocDataPtr docData = GetWRefCon(window);
	
	mark = &docData->Mark;
	if (mark->Head == NULL) {
		ErrBeep();
		return (FALSE);
	}
	
	if (mark->Loc > mark->Head->Len)
		mark->Loc = mark->Head->Len;

	docData->SelStart = docData->SelEnd = mark->Loc;
	docData->SelHead = mark->Head;
	RevealHead(docData);
	
	return (TRUE);
}
/*
 * Process Search menu selection
 */

BOOL DoSearchMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;
	
	switch (item) {
	case FIND_ITEM:
		success = DoFind(NULL);
		break;
	case FINDNEXT_ITEM:
		success = DoFindNext(window);
		break;
	case CHANGE_ITEM:
		success = DoChange();
		break;
	case GOTOPAGE_ITEM:
		success = DoGoToPage(window);
		break;
	case GOTOSELECTION_ITEM:
		ScrollToCursor(window);
		success = TRUE;
		break;
	case SPELLING_ITEM:
		success = DoSpellingMenu(window, sub, modifier);
		break;
	case SETMARK_ITEM:
		success = DoSetMark(window);
		break;
	case GOTOMARK_ITEM:
		success = DoGoToMark(window);
		break;
	}

	return(success);
}
