/*
 *	ProWrite
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	PostScript printing routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <stdio.h>
#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ColorPick.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern PostScriptOptions	postScriptOptions;

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern UBYTE	fileBuff[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

static TextChar	strPrologName[] = "PostScript Prep";

static TextChar	strSerName[] = "SER:";
static TextChar	strParName[] = "PAR:";

static TextChar	strCourier[]			= "Courier";
static TextChar	strCourierBold[]		= "Courier-Bold";
static TextChar	strCourierItalic[]		= "Courier-Oblique";
static TextChar	strCourierBoldItalic[]	= "Courier-BoldOblique";

static TextChar	psTextBuff[256];
static WORD		prevFontNum = -1;
static WORD		prevStyleBits;

enum {
	SERIAL_RADBTN = 2,
	PARALLEL_RADBTN,
	CUSTOM_RADBTN,
	CUSTOM_TEXT
};

static DialogPtr	psDlg;

/*
 *	Metric file format
 */

typedef struct {
	TextChar	PSFontName[66];
	ULONG		PropWidthTable;
	ULONG		KerningTable;
	ULONG		TrackingTable;
	WORD		TrackCount;
	UBYTE		Proportional;
	UBYTE		AmigaEncoding;
/*
	WORD		BBoxMinX, BBoxMinY, BBoxMaxX, BBoxMaxY;
	WORD		UnderlinePosition, UnderlineWeight;
	WORD		CapHeight, XHeight;
	WORD		dAscenderHeight, pDescenderHeight;
	UWORD		Style;
*/
} FontMetric;

/*
 *	Local prototypes
 */

void	AdjustCoords(PrintRecPtr, PointPtr);
WORD	GrayValue(RGBColor);

BOOL	SendText(File, TextPtr);

BOOL	PostScriptDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Return pointer to PostScript output device name
 */

TextPtr PSDeviceName()
{
	register TextPtr devName;

	switch (postScriptOptions.Device) {
	case PS_SERIAL:
		devName = strSerName;
		break;
	case PS_PARALLEL:
		devName = strParName;
		break;
	default:
		devName = postScriptOptions.CustomName;
		break;
	}
	return (devName);
}

/*
 *	Adjust point coordinates from document to PostScript user space
 */

static void AdjustCoords(register PrintRecPtr printRec, register PointPtr pt)
{
	pt->x = pt->x - printRec->PaperRect.MinX;
	pt->y = printRec->PaperRect.MaxY - pt->y;
}

/*
 *	Return gray value (0 to 127) for given color values (0 to 15)
 */

static WORD GrayValue(register RGBColor color)
{
	return (30*RED(color) + 59*GREEN(color) + 11*BLUE(color))*0x80L/1501L;
}

/*
 *	Write text string to file
 */

static BOOL SendText(File file, TextPtr text)
{
	register WORD len;

	len = strlen(text);
	return ((BOOL) (Write(file, text, len) == len));
}

/*
 *	Send prologue and set up printer
 */

BOOL PSBeginJob(PrintRecPtr printRec, TextPtr jobTitle)
{
	WORD buffSize, paperType;
	BOOL success;
	File prologFile;
	File file = printRec->File;

	prologFile = NULL;
	success = FALSE;
	prevFontNum = -1;
/*
	Send header
*/
	if (!SendText(file, "%!PS-Adobe-3.0\n") ||
		!SendText(file, "%%Creator: Flow\n"))
		goto Exit;
	sprintf(psTextBuff, "%%%%Title: %s\n", jobTitle);
	if (!SendText(file, psTextBuff))
		goto Exit;
	sprintf(psTextBuff, "%%%%Orientation: %s\n",
			(printRec->Orientation == PRT_PORTRAIT) ? "Portrait" : "Landscape");
	if (!SendText(file, psTextBuff))
		goto Exit;
	sprintf(psTextBuff, "%%%%Pages: %d\n", printRec->LastPage - printRec->FirstPage + 1);
	if (!SendText(file, psTextBuff))
		goto Exit;
	sprintf(psTextBuff, "%%%%PageOrder: %s\n",
			(printRec->Flags & PRT_BACKTOFRONT) ? "Descend" : "Ascend");
	if (!SendText(file, psTextBuff))
		goto Exit;
	sprintf(psTextBuff, "%%%%BoundingBox: %d %d %d %d\n",
			0, 0, printRec->PaperWidth/10, printRec->PaperHeight/10);
	if (!SendText(file, psTextBuff))
		goto Exit;
	if (!SendText(file, "%%DocumentData: Clean7Bit\n") ||
		!SendText(file, "%%EndComments\n"))
		goto Exit;
/*
	Send prologue
*/
	if (!SendText(file, "%%BeginProlog\n"))
		goto Exit;
	SetPathName(psTextBuff, strPrologName, TRUE);
	if ((prologFile = Open(psTextBuff, MODE_OLDFILE)) == NULL)
		goto Exit;
	while ((buffSize = Read(prologFile, fileBuff, FILEBUFF_SIZE)) > 0) {
		if (Write(file, fileBuff, buffSize) != buffSize)
			goto Exit;
	}
	Close(prologFile);
	prologFile = NULL;
	if (!SendText(file, "%%EndProlog\n"))
		goto Exit;
/*
	Set document defaults
*/
	if (!SendText(file, "%%BeginSetup\n"))
		goto Exit;
	sprintf(psTextBuff, "/#copies %d def\n",
			(printRec->Flags & PRT_COLLATE) ? 1 : printRec->Copies);
	if (!SendText(file, psTextBuff))
		goto Exit;
	psTextBuff[0] = '\0';
	switch (printRec->PageSize) {
	case PRT_USLEGAL:
		paperType = 2;
		break;
	case PRT_A4LETTER:
		paperType = 3;
		break;
	default:
		paperType = 1;
		break;
	}
	sprintf(psTextBuff, "%d PaperSize\n", paperType);
	if (!SendText(file, psTextBuff))
		goto Exit;
	sprintf(psTextBuff, "%s ManualFeed\n",
			(printRec->PaperFeed == PRT_CUTSHEET) ? "true" : "false");
	if (!SendText(file, psTextBuff))
		goto Exit;
	if (!SendText(file, "%%EndSetup\n"))
		goto Exit;
/*
	Clean up
*/
	success = TRUE;
Exit:
	if (prologFile)
		Close(prologFile);
	return (success);
}

/*
 *	Send trailer and end job
 */

BOOL PSEndJob(PrintRecPtr printRec)
{
	BOOL success;
	File file = printRec->File;

	success = FALSE;
	if (!SendText(file, "%%Trailer\n"))
		goto Exit;
	if (!SendText(file, "%%EOF\n\004"))		/* Send ctrl-D */
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Begin page
 */

BOOL PSBeginPage(PrintRecPtr printRec, WORD page)
{
	BOOL success;
	File file = printRec->File;

	success = FALSE;
	prevFontNum = -1;
	sprintf(psTextBuff, "%%%%Page: %d %d\n", page + 1, page + 1);
	if (!SendText(file, psTextBuff))
		goto Exit;
/*
	Page setup (goes here, since showpage clears these settings)
*/
	if (!SendText(file, "%%BeginPageSetup\n") ||
		!SendText(file, "/pageSave save def\n"))
		goto Exit;
	if (printRec->Orientation == PRT_LANDSCAPE) {
		sprintf(psTextBuff, "0 %d translate -90 rotate\n", printRec->PaperHeight/10);
		if (!SendText(file, psTextBuff))
			goto Exit;
	}
	sprintf(psTextBuff, "72 %d div 72 %d div scale\n", printRec->xDPI, printRec->yDPI);
	if (!SendText(file, psTextBuff))
		goto Exit;
	if (!SendText(file, "1 setlinewidth 2 setlinecap 0 setlinejoin\n") ||
		!SendText(file, "%%EndPageSetup\n"))
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	End and print page
 */

BOOL PSEndPage(PrintRecPtr printRec)
{
	BOOL success;
	File file = printRec->File;

	success = FALSE;
	if (!SendText(file, "pageSave restore showpage\n"))
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Select font
 */

BOOL PSSelectFont(PrintRecPtr printRec, WORD fontNum, WORD style,WORD lpi)
{
	TextPtr fontName;
	WORD 	psStyleBits;
	WORD	ySize;

	ySize = (lpi == 6) ? 12 :10;
	psStyleBits = (style & (FSF_BOLD | FSF_ITALIC));
	if (prevFontNum != -1 && psStyleBits == prevStyleBits)
		return (TRUE);
	prevFontNum = fontNum;
	prevStyleBits = psStyleBits;
/*
	No metric file, so set to default font
*/
	switch (psStyleBits) {
		case FS_NORMAL:
			fontName = strCourier;
			break;
		case FSF_BOLD:
			fontName = strCourierBold;
			break;
		case FSF_ITALIC:
			fontName = strCourierItalic;
			break;
		default:				/* FSF_BOLD and FSF_ITALIC */
			fontName = strCourierBoldItalic;
			break;
	}
/*
	Select the font
*/
	sprintf(psTextBuff, "%d (%s) %s SetFont\n", ySize, fontName, "true" );
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Text
 */

BOOL PSText(PrintRecPtr printRec, WORD x, WORD y, WORD cpi,WORD rotate, UBYTE style,
			TextPtr text, WORD len,WORD spaces)
{
	register WORD srcIndx, dstIndx,xDPI;
	register TextChar ch;
	Point pt;

	if (len == 0)
		return (TRUE);
	pt.x = x;
	pt.y = y;
	AdjustCoords(printRec, &pt);
	xDPI = printRec->xDPI;
	sprintf(psTextBuff, "%d %d %d %d div mul add %d %d %d %d div mul %d %s\n", pt.x, 
						spaces,xDPI, cpi, pt.y, len, xDPI,cpi, rotate,
						(style & FSF_UNDERLINED) ? "true" : "false");
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
/*
	Send text string in blocks of 200 or fewer characters
*/
	srcIndx = 0;
	while (srcIndx < len) {
		if (srcIndx == 0) {
			psTextBuff[0] = '(';
			dstIndx = 1;
		}
		else
			dstIndx = 0;
		do {
			ch = text[srcIndx++];
			if (ch > 0x7F) {
				psTextBuff[dstIndx++] = '\\';
				psTextBuff[dstIndx++] = ((ch >> 6) & 7) + '0';
				psTextBuff[dstIndx++] = ((ch >> 3) & 7) + '0';
				psTextBuff[dstIndx++] = ((ch     ) & 7) + '0';
			}
			else if (ch == '(' || ch == ')' || ch == '\\') {
				psTextBuff[dstIndx++] = '\\';
				psTextBuff[dstIndx++] = ch;
			}
			else
				psTextBuff[dstIndx++] = ch;
		} while (srcIndx < len && dstIndx < 200);
		strcpy(psTextBuff + dstIndx, (srcIndx < len) ? "\\\n" : ")\n");
		if (!SendText(printRec->File, psTextBuff))
			return (FALSE);
	}
	return (SendText(printRec->File, "Text\n"));
}

/*
 *	PostScript options dialog filter
 */

static BOOL PostScriptDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == psDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN && itemHit == CUSTOM_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = CUSTOM_RADBTN;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	PostScript options dialog
 */

void PostScriptOptsDialog()
{
	WORD item, devItem, onItem, offItem;
	BOOL done;
	GadgetPtr gadgList, gadget;

	switch (postScriptOptions.Device) {
	case PS_SERIAL:
		devItem = SERIAL_RADBTN;
		break;
	case PS_PARALLEL:
		devItem = PARALLEL_RADBTN;
		break;
	default:
		devItem = CUSTOM_RADBTN;
		break;
	}
/*
	Get dialog and set initial values
*/
	AutoActivateEnable(FALSE);
	psDlg = GetDialog(dlgList[DLG_POSTSCRIPT], screen, mainMsgPort);
	AutoActivateEnable(TRUE);
	if (psDlg == NULL) {
		Error(ERR_NO_MEM);
		return;
	}
	OutlineOKButton(psDlg);
	gadgList = psDlg->FirstGadget;
	SetGadgetItemValue(gadgList, devItem, psDlg, NULL, 1);
	SetEditItemText(gadgList, CUSTOM_TEXT, psDlg, NULL, postScriptOptions.CustomName);
	if (devItem == CUSTOM_RADBTN)
		ActivateGadget(GadgetItem(gadgList,CUSTOM_TEXT),psDlg,NULL);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, psDlg, PostScriptDialogFilter);
		offItem = onItem = -1;
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case SERIAL_RADBTN:
		case PARALLEL_RADBTN:
		case CUSTOM_RADBTN:
			offItem = devItem;
			onItem = devItem = item;
			break;
		}
		if (!done && item != -1) {
			if (offItem != -1)
				SetGadgetItemValue(gadgList, offItem, psDlg, NULL, 0);
			if (onItem != -1)
				SetGadgetItemValue(gadgList, onItem, psDlg, NULL, 1);
			if (devItem == CUSTOM_RADBTN) {
				gadget = GadgetItem(gadgList, CUSTOM_TEXT);
				ActivateGadget(gadget, psDlg, NULL);
			}
		}
	} while (!done);
	if (item == OK_BUTTON)
		GetEditItemText(gadgList, CUSTOM_TEXT, postScriptOptions.CustomName);
	DisposeDialog(psDlg);
	if (item == CANCEL_BUTTON)
		return;
/*
	Set values
*/
	switch (devItem) {
	case SERIAL_RADBTN:
		postScriptOptions.Device = PS_SERIAL;
		break;
	case PARALLEL_RADBTN:
		postScriptOptions.Device = PS_PARALLEL;
		break;
	default:
		postScriptOptions.Device = PS_CUSTOM;
		break;
	}
	postScriptOptions.DeviceNumber = 0;
}

