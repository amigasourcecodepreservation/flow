/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Memory allocation definitions
 */

#ifndef TOOLBOX_MEMORY_H
#define TOOLBOX_MEMORY_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef EXEC_MEMORY_H
#include <exec/memory.h>
#endif

#ifndef TYPEDEF_H
#include <TypeDefs.h>
#endif

/*
 *	Memory allocation routines
 *	All memory allocations are long word aligned
 */

Ptr		MemAlloc(ULONG, LONG);
void	MemFree(Ptr, ULONG);
ULONG	MemAvail(LONG);
void	BlockClear(Ptr, ULONG);
void	BlockMove(Ptr, Ptr, ULONG);
void	SwapMem(Ptr, Ptr, ULONG);

#endif
