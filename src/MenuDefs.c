/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>

#include "Flow.h"


extern TextChar strMacroNames2[10][32];

/*
 *	Project menu items
 */

static MenuItemTemplate defaultsItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Load...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate projectItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'N', 0, 0, "New", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'O', 0, 0, "Open...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'W', 0, 0, "Close", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'S', 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Revert", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Page Setup...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Print One", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Print...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Settings", &defaultsItems[0] },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit", NULL },
	{ MENU_NO_ITEM }
};

/*
 *	Edit menu items
 */

static MenuItemTemplate changeCaseItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "UPPER CASE", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "lower case", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Mixed Case", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate insertItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Date", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Time", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Literal...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, '/', 0, 0, "Space", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate editItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'X', 0, 0, "Cut", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'C', 0, 0, "Copy", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'V', 0, 0, "Paste", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Erase", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Change Case", &changeCaseItems[0] },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Insert", &insertItems[0] },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Item Formats..." },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'A', 0, 0, "Select All", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Preferences...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Screen Colors...", NULL },
	{ MENU_NO_ITEM }
};

/*
 *	Search menu
 */

static MenuItemTemplate spellItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Check...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Change Dict...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate searchItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'F', 0, 0, "Find...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'D', 0, 0, "Find Next", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Change...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'G', 0, 0, "Go To Page...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Go To Selection", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Set Mark", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Go To Mark", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Spelling", &spellItems[0] },
	{ MENU_NO_ITEM }
};

/*
 *	Headings menu
 */
static MenuItemTemplate sortItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "A to Z", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Z to A", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,  0,  0, 0, "Random", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate headingItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'K', 0, 0, "Collapse", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'E', 0, 0, "Expand", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Expand All", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Indent", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Un-Indent", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Move Up", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Move Down", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Split", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Join", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0,  0, 0, "Sort", &sortItems[0]  },
	{ MENU_NO_ITEM }
};

/*
 *	Format menu
 */

static MenuItemTemplate formatItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED, 'P', 0, 0x0E, "Plain", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'B', FSF_BOLD, 0x01, "Bold", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'I', FSF_ITALIC, 0x01, "Italic", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'U', FSF_UNDERLINED, 0x01, "Underline", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 0, 0, 0, "Page Break Before", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Layout...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Heading Labels...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Document Colors...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Document Info...", NULL },
	{ MENU_NO_ITEM }
};

/*
 * View menu (by popular demand)
 */
 
static MenuItemTemplate viewItems[] = {
	{ MENU_TEXT_ITEM, MENUENABLED, 0, 0, 0, "About Flow...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_NO_ITEM }
};

/*
 *	Macro menu
 */

static MenuItemTemplate macroItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, &strMacroNames2[0][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[1][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[2][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[3][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[4][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[5][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[6][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[7][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[8][0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, &strMacroNames2[9][0], NULL }, 
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	0, 0, 0, "Customize...", NULL },
	{ MENU_NO_ITEM }
};

/*
 *	Menu strip templates for outline window
 */

MenuTemplate docWindMenus[] = {
	{ " Project ", &projectItems[0] },
	{ " Edit ", &editItems[0] },
	{ " Search ", &searchItems[0] },
	{ " Heading ", &headingItems[0] },
	{ " Format ", &formatItems[0] },
	{ " View ", &viewItems[0] },
	{ " Macro ", &macroItems[0] },
	{ NULL, NULL }
};

MenuTemplate altWindMenus[] = {			/* For Low Res screens */
	{ " Proj", &projectItems[0] },
	{ " Edit", &editItems[0] },
	{ " Srch", &searchItems[0] },
	{ " Head", &headingItems[0] },
	{ " Fmt", &formatItems[0] },
	{ " View", &viewItems[0] },
	{ " Mac", &macroItems[0] },
	{ NULL, NULL }
};
