/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Heading text edit functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Window.h>
#include <Toolbox/Memory.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WORD			dragStart;

extern BYTE			wordChar[];
extern Options		options;

extern TextPtr		headingStrs[];
extern WORD			levelArray[];

/*
 *	Local variables
 */

#define CHAR_BUFF_SIZE 10

static TextChar	charBuff[CHAR_BUFF_SIZE];
static WORD			numChars;		/* Inited to 0 */
static WindowPtr	charWindow;		/* Window where chars go */

/*
 * Local prototypes
 */

void  InsertChars(WindowPtr, TextPtr, WORD);
WORD  CalcHeadLevel(HeadPtr, WORD);

BOOL ExpandBuffer(HeadPtr head, WORD selEnd, WORD inc)
{
	TextPtr text;
	WORD oldSize, newSize, num;

	newSize = head->Len + inc;
	if (newSize <= 0)
		newSize = 0;
	else
		newSize = ((newSize - 1)/TEXT_CHUNK + 1)*TEXT_CHUNK;
	oldSize = GetPtrSize(head->Text);
	if (oldSize == newSize) {
		BlockMove( &head->Text[selEnd], &head->Text[selEnd+inc], ABS(head->Len-selEnd) ) ;
	} else {
		num = MIN(oldSize, newSize);
		if (newSize == 0)
			text = NULL;
		else {
			if ((text = (UBYTE *) NewPtr(newSize)) == NULL)
				return (FALSE);
		}
		if (head->Text) {
			if (num) {
/*
 * First make an exact copy of the data in the correct size memory block
 */
				BlockMove(head->Text, text, num);
/* 
 * Now move just the portion that will be shifted, leaving a perfect-sized gap.
 */
 
				BlockMove( &head->Text[selEnd], &text[selEnd+inc], ABS(head->Len-selEnd) );
			}
			DisposePtr(head->Text);
		}
		head->Text = text;
	}
	return (TRUE);
}
	
/*
 *	Adjust buffer for specified heading up or down by the given amount,
 *		or create buffer if non-existent
 *	Return success status
 */

BOOL AdjustBuffer(HeadPtr head, WORD inc)
{
	TextPtr text;
	WORD oldSize, newSize, num;

	newSize = head->Len + inc;
	if (newSize <= 0)
		newSize = 0;
	else
		newSize = ((newSize - 1)/TEXT_CHUNK + 1)*TEXT_CHUNK;
	oldSize = GetPtrSize(head->Text);
	if (oldSize != newSize) {
		num = MIN(oldSize, newSize);
		if(newSize != 0) {
			if ((text = (UBYTE *) NewPtr(newSize)) == NULL)
				return (FALSE);
			if (head->Text) {
				if (num)
					BlockMove(head->Text, text, num);
				DisposePtr(head->Text);
			}
			head->Text = text;
		}
	}
	return (TRUE);
}

/*
 *	Add text to specified heading
 */

BOOL AddText(HeadPtr head, TextPtr text, WORD len)
{
	if (!AdjustBuffer(head, len))
		return (FALSE);
	BlockMove(text, &head->Text[head->Len], len);
	head->Len += len;
	return (TRUE);
}

/*
 *	Delete character or range
 */

BOOL DoDelete(WindowPtr window, TextChar ch, UWORD qualifier)
{
	register WORD selStart;
	register LONG yPos;
	BOOL headDel;
	register HeadPtr selHead, newHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = TRUE;

	
	if (docData->SelHead->Flags & HD_SELECTED) {
		DoErase(window);
	} else {
		CursorOff(window);
		selHead = docData->SelHead;
/*
	If no range selected then set up range to delete
	When ALT key down at beginning or end of text, merge adjacent heads
*/
		headDel = FALSE;
		if (docData->SelStart == docData->SelEnd) {
			success = FALSE;
			if (ch == BS) {
				if( selHead->BodyStart != docData->SelStart ) {
					docData->SelStart--;
					if (qualifier & SHIFTKEYS) {
						while ((selHead->Text[docData->SelStart-1] != ' ') &&
								(selHead->BodyStart != docData->SelStart))
							docData->SelStart--;
					}
					success = TRUE;
				} else {
					if( qualifier & ALTKEYS ) {
						if( (newHead = PrevHead(selHead)) != NULL &&
								selHead->Sub == NULL &&
								AdjustBuffer(newHead, selHead->Len)) {
							selHead->Len -= selHead->BodyStart;
							selStart = newHead->Len;
							BlockMove(selHead->Text + selHead->BodyStart,
									newHead->Text + selStart, selHead->Len);
							newHead->Len += selHead->Len;
							DeleteHead(docData);
							docData->SelHead = selHead = newHead;
							docData->SelStart = docData->SelEnd = selStart;
							success = headDel = TRUE;
						}
					}
				} 
			} else if (ch == DEL) {
				if (docData->SelEnd < selHead->Len) {
					docData->SelEnd++;
					if (qualifier & SHIFTKEYS) {
						while ((selHead->Text[docData->SelEnd] != ' ') &&
								(docData->SelEnd < selHead->Len))
							docData->SelEnd++;
					}
					success = TRUE;
				} else if (qualifier & ALTKEYS) {
					success = headDel = JoinHead(docData);
				}
			}
		}
		if( success ) {
			if (docData->SelStart != docData->SelEnd)
				DeleteText(docData);
			selHead = docData->SelHead;	/* May have been changed by DeleteText() */
/*
	Show modified text line on screen
*/
			docData->DragStart = docData->SelEnd = docData->SelStart;
			if (CalcLineStarts(docData, selHead) || headDel) {	/* May change selHead */
				CountLines(docData);
				yPos = VertPosition(window, docData->SelHead, 0);
				DrawWindowRange(window, (WORD) yPos,	/* Draws cursor */
						(WORD) (window->Height - window->BorderBottom - 1));
				AdjustScrollBars(window);
			} else {
				DrawHead(window, docData->SelHead);
				CursorOn(window);
			}
			ScrollToCursor(window);
			docData->SelHead->Flags &= ~HD_SPELL;
			DocumentModified(docData);
			SetEditMenu();
			if (headDel) {
				SetSearchMenu();
				SetSubHeadMenu();
 				SetFormatMenu();
			}
		} else
			ErrBeep();
	}
	return(success);
}

/*
 * Merge head below selected head if possible
 */
 
BOOL JoinHead(register DocDataPtr docData)
{
	register HeadPtr newHead;
	register HeadPtr selHead = docData->SelHead;
	BOOL success = FALSE;
	WORD selStart;
	register UWORD len;
	
	if( (newHead = NextHead(selHead)) != NULL && newHead->Sub == NULL ) {
		len = newHead->Len - newHead->BodyStart;
		if( AdjustBuffer(selHead, newHead->Len) ) {
			selStart = selHead->Len;
			BlockMove( &newHead->Text[newHead->BodyStart], selHead->Text + selStart, len);
			selHead->Len += len;
			docData->SelHead = newHead;		/* Only temporary */
			DeleteHead(docData);
			docData->SelHead = selHead;
			docData->SelEnd = docData->SelStart = selStart;
			success = TRUE;
		}
	}
	return(success);
}
	
/*
 *	Insert character into line
 */


void InsertChars(WindowPtr window, TextPtr buff, WORD len)
{
	TextPtr text;
	LONG yPos;
	HeadPtr selHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->SelHead->Flags & HD_SELECTED) {
		ErrBeep();
	} else {
		yPos = VertPosition(window, docData->SelHead, 0);
		CursorOff(window);
/*
	If range is selected, then delete range
*/
		if (docData->SelStart != docData->SelEnd)
			DeleteText(docData);

		selHead = docData->SelHead;		/* May have been changed by DeleteText() */
		if( selHead->Len + len >= MAX_TEXTLEN ) {
			len = MAX_TEXTLEN - selHead->Len;
		}
/*
	Insert character
*/
		if( !len || !AdjustBuffer(selHead, len) )
			Error(ERR_NO_MEM);
		else {
			selHead->Flags &= ~HD_SPELL;
			text = selHead->Text + docData->SelStart;
			if( !options.Typeover ) {
				BlockMove( text, text+len, selHead->Len - docData->SelStart );
			}
			BlockMove( buff, text, len );
			if( !options.Typeover || ( selHead->Len == docData->SelStart ) ) {
				selHead->Len += len;
		
			}
			docData->SelStart += len;
		}
/*
	Show modified text line on screen
*/
		docData->DragStart = docData->SelEnd = docData->SelStart;
		if (CalcLineStarts(docData, selHead)) {		/* May change selHead */
			CountLines(docData);
			DrawWindowRange(window, (WORD) yPos,	/* Draws cursor */
						(WORD) (window->Height - window->BorderBottom - 1));
			AdjustScrollBars(window);
		} else {
			DrawHead(window, docData->SelHead);
			CursorOn(window);
		}
		ScrollToCursor(window);
		DocumentModified(docData);
		SetEditMenu();
	}
	return;
}

/*
 *	Add character to buffer, update window if buffer is full
 */

void AddChar(WindowPtr window, TextChar ch)
{
/*
	If character is for a different window, or buffer is full then update
*/
	if (window != charWindow || numChars >= CHAR_BUFF_SIZE)
		UpdateChars();
	charWindow = window;
/*
	Add character to buffer
*/
	charBuff[numChars++] = ch;
}

/*
 *	Update window display with characters in buffer
 */

void UpdateChars()
{
	if (numChars) {
		InsertChars(charWindow, charBuff, numChars);
	}
	numChars = 0;
}

/*
 *	Create new heading and link after CurrentHead
 */

BOOL NewHead(WindowPtr window, UWORD qualifier)
{
	register WORD len;
	register LONG yPos;
	register HeadPtr newHead, selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = FALSE;
	WORD selStart;
		
	selHead = docData->SelHead;
	yPos = VertPosition(window, selHead, 0) + selHead->NumLines;
	if ((newHead = AllocHead()) == NULL) {
		Error(ERR_NO_MEM);
	} else {
		CursorOff(window);
		UnSelectHeads(docData);
/*
	If SHIFT key down, then make new subhead
*/
		if ((qualifier & SHIFTKEYS) &&
			HeadDepth(selHead) + SubDepth(selHead) >= MAX_DEPTH) {
			ErrBeep();
			qualifier &= ~SHIFTKEYS;
		}
		if (qualifier & SHIFTKEYS) {
			newHead->Prev = NULL;
			newHead->Next = selHead->Sub;
			if (selHead->Sub)
				selHead->Sub->Prev = newHead;
			selHead->Sub = newHead;
			newHead->Super = selHead;
			selHead->Flags |= HD_EXPANDED;
			newHead->Num = levelArray[LabelDepth(docData, newHead)];
		} else {
			newHead->Prev = selHead;
			newHead->Next = selHead->Next;
			if (selHead->Next)
				selHead->Next->Prev = newHead;
			selHead->Next = newHead;
			newHead->Super = selHead->Super;
			newHead->Num = selHead->Num + 1;
		}
		newHead->Sub = NULL;
		newHead->Style = selHead->Style;

/*
	If ALT key down, then move text after selStart to new head
*/
		if (qualifier & ALTKEYS) {
			if (docData->SelStart != docData->SelEnd)
				DeleteText(docData);
			selHead = docData->SelHead;	/* May have been changed by DeleteText() */
			len = selHead->Len - docData->SelStart;
		}
		docData->SelHead = newHead;
		selStart = docData->SelStart;
		success = CreateLabel(docData, newHead);
		docData->SelStart = docData->SelEnd = docData->DragStart = docData->SelHead->BodyStart;

		if (qualifier & ALTKEYS) {
/*
	Copy and remove text
*/
			if (!AdjustBuffer(newHead, len)) {
				Error(ERR_NO_MEM);
				success = FALSE;
			} else {
				BlockMove(selHead->Text + selStart, newHead->Text + newHead->Len, len);
				newHead->Len += len;
				(void) AdjustBuffer(selHead, (WORD) -len);
				selHead->Len -= len;
			}
/*
	Get new line breaks (note trick to keep track of newHead)
*/
			(void) CalcLineStarts(docData, selHead);	/* May change selHead */
			docData->SelHead = newHead;
			(void) CalcLineStarts(docData, newHead);	/* May change newHead */
			newHead = docData->SelHead;
		}
/*
	Draw new window & cursor
*/
		BumpLevel(docData, newHead->Next, 1);
		ReformatRestOfLevel(docData, newHead);
		CountLines(docData);
		DrawWindowRange(window, (WORD) yPos,		/* Draws cursor */
					(WORD) (window->Height - window->BorderBottom - 1));
		AdjustScrollBars(window);
		ScrollToCursor(window);
		DocumentModified(docData);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

BOOL CreateLabel(DocDataPtr docData, HeadPtr head)
{
	register TextPtr srcStr, dstStr;
	register WORD len, level, num, depth;
	TextPtr origSrcStr;
	TextChar ch;
	TextChar buff[GADG_MAX_STRING];
	BOOL success;
	BOOL outEnabled = docData->LabelFlags & LABEL_FORMAT_MASK ;
	UWORD loopStart;
	
	headingStrs[1] = docData->CustomLabel;
	srcStr = origSrcStr = headingStrs[docData->LabelStyle];
	dstStr = &buff[0];
	level = 0;
	num = CalcHeadLevel(head, level);
	depth = HeadDepth(head);
	if( level == depth )
		outEnabled = TRUE;
	loopStart = 0;

	while( level <= depth ) {		
		ch = *srcStr++;
		switch(ch) {
		case '^':
			ch = *srcStr++;
			switch(ch) {
			case ',':
			case ';':
				if( outEnabled )
					*dstStr++ = ch;
				break;
			case LOWER_ROMAN_CHAR:
			case UPPER_ROMAN_CHAR:
			case LOWER_NUMERIC_CHAR:
			case UPPER_NUMERIC_CHAR:
			case LOWER_CASE_CHAR:
			case UPPER_CASE_CHAR:
				if( outEnabled ) {
					while( *srcStr >= '0' && *srcStr <= '9' ) {
						*srcStr++;
					}
					dstStr = AppendNumberStyle(num, dstStr, ch);
				}
				break;
			case LOWER_KILL_CHAR:
			case UPPER_KILL_CHAR:
				dstStr = &buff[0];
				break;
			default:
				break;
			}
			break;
		/*
		case ';':
			if( loopStart == 0 ) {
				loopStart = level;
				origSrcStr = srcStr;
			}
			break;
		*/
		case 0:
			srcStr = origSrcStr;
		case ',':
		case ';':
			if( level != depth )
				num = CalcHeadLevel(head, level + 1);
			level++;
			if( level == depth )
				outEnabled = TRUE;
			if( ch == ';' ) {
				if( loopStart == 0 ) {
					loopStart = level;
					origSrcStr = srcStr;
				}
			}
			break; 
		default:
			if( outEnabled )
				*dstStr++ = ch;
			break;
		}
	}
	if( docData->LabelFlags & LABEL_APPEND_MASK )
		*dstStr++ = ' ';
	
	len = dstStr - &buff[0];
	success = AdjustBuffer(head, len) ;
	if( success ) {
		BlockMove( head->Text, head->Text + len, head->Len );
		BlockMove( &buff[0], head->Text, (WORD) len);
		head->Len += len;
	}
	
	head->BodyStart = len;
	return(success);
}

/*
 * Scan only the origin numbers for each level and build a table.
 */
  
BOOL BuildLevelTable(DocDataPtr docData, UWORD *repeat, UWORD *range)
{
	register UWORD i;
	register BOOL more;
	TextChar ch;
	register UWORD num, digits;
	register TextPtr srcStr = docData->CustomLabel;
	
	*repeat = 0;
	for( i = 0 ; ( i < GADG_MAX_STRING / 4 ) && *srcStr ; ) {
		for( more = TRUE; more && *srcStr ; ) {
			ch = *srcStr++;
			switch(ch) {
			case '^':
				switch(*srcStr++) {
				case LOWER_ROMAN_CHAR:
				case UPPER_ROMAN_CHAR:
				case LOWER_NUMERIC_CHAR:
				case UPPER_NUMERIC_CHAR:
				case LOWER_CASE_CHAR:
				case UPPER_CASE_CHAR:
					num = 1;
					digits = 0;
					while( *srcStr >= '0' && *srcStr <= '9' ) {
						if( digits == 0 )
							num = 0;
						if( digits < 4 ) {
							num *= 10;
							num += ((*srcStr) - '0');
						}
						digits++;
						*srcStr++;
					}
					levelArray[i] = num;
					break;
				default:
					break;
				}
				break;
			case ';':
				*repeat = i + 1;
			/* break; */
			case ',':
				more = FALSE;
			default:
				break;
			}
		}
		i++;
	}
	*range = i - *repeat;
/*
	Zero range not allowed, we have to divide by this!
*/
	if( *range == 0 )
		*range += 1;

	return( (docData->LabelRepeat != *repeat) || (docData->LabelRange != *range) );
}

/*
 *	Renumbers the heads	
 */

void RenumberHeads(DocDataPtr docData)
{
	register UWORD i;
	BOOL renumber;
	register HeadPtr head;
	UWORD repeatWord, rangeWord;
	register UWORD repeat, range;

	UWORD oldLevelArray[GADG_MAX_STRING/4];

	BlockMove( (Ptr) &levelArray[0], (Ptr) &oldLevelArray[0], GADG_MAX_STRING / 2 );
	renumber = BuildLevelTable(docData, &repeatWord, &rangeWord);
	repeat = repeatWord;
	range = rangeWord;
	for( i = 0 ; i < GADG_MAX_STRING / 4 ; i++ ) {
		if( oldLevelArray[i] != levelArray[i] ) {
			renumber = TRUE;
			break;
		}
	}
	if( renumber ) {
		for( head = docData->FirstHead ; head != NULL ; head = NextHeadAll(head) ) {
			i = HeadDepth(head);
			if( i > repeat )
				i = repeat + (i - repeat) % range;
			head->Num += levelArray[i] - oldLevelArray[LabelDepth(docData, head)];
		}
		docData->LabelRepeat = repeat;
		docData->LabelRange = range;
	}
	
}
	
/*
	Determine the correct label depth for the given head,
	to use to index into the label level array table.
*/

UWORD LabelDepth(DocDataPtr docData, HeadPtr head)
{
	return( FindDepth(docData, HeadDepth(head) ) );
}

/*
	Given the specified depth, calculate its correct label origin
*/

UWORD FindDepth(DocDataPtr docData, UWORD depth)
{	
	if( depth > docData->LabelRepeat )
		depth = docData->LabelRepeat + ((depth - docData->LabelRepeat) % docData->LabelRange);
	return( depth );
}

/*
	Bump all labels from "head" by "inc" and reconstruct their labels
*/

void BumpLevel( DocDataPtr docData, HeadPtr head, WORD inc)
{
	WORD depth = HeadDepth(head);
	register HeadPtr endHead;
	register BOOL done;
	
	if( head != NULL ) {
		SetBusyPointer(docData->Window);
		endHead = (docData->LabelFlags & LABEL_FORMAT_MASK) ? head->Super : NULL;
		if( endHead != NULL ) {
			do {
				if( endHead->Next != NULL ) {
					endHead = endHead->Next;
					done = endHead != NULL;
				} else {
					endHead = endHead->Super;
					done = endHead == NULL;
				}
			} while(!done);
		}
		while( head != endHead ) {
			if( depth == HeadDepth(head) )
				head->Num += inc;
			RedoLabel(docData, head);
/* 
 * Update all subordinates if build from parent enabled, else just get next sibling.
 */
			if( docData->LabelFlags & ( LABEL_FORMAT_MASK + LABEL_CHANGED_MASK ) )
				head = NextHeadAll(head);
			else
				head = head->Next;
			NextBusyPointer();
		}	
		SetPointerShape();
	}
}

/*
	Reconstruct only the label passed in "head"
*/

void RedoLabel( DocDataPtr docData, HeadPtr head )
{
	BlockMove( head->Text + head->BodyStart, head->Text, head->Len - head->BodyStart );
	AdjustBuffer(head, -head->BodyStart);
	head->Len -= head->BodyStart;
	(void) CreateLabel(docData, head);
}

/*
	Calculate the number of this level of the label "head".
*/

static WORD CalcHeadLevel( HeadPtr head, WORD level )
{
	register WORD depth = HeadDepth(head);
	while( depth != level ) {
		--depth;
		head = head->Super;
	}
	return( head->Num );	
}

/*
 * Call this whenever a document undergoes a change that can be undone by "Revert".
 */
 
void DocumentModified(DocDataPtr docData)
{
	docData->Flags |= WD_MODIFIED;
	OnOffRevertMenuItem();
}

/*
 * Backup over spaces/tabs within text
 */
 
WORD BackupSpaces(text, loc, minLoc)
register TextPtr text;
register WORD loc, minLoc;
{
	while( loc > minLoc && text[loc - 1] <= ' ')
		loc--;
	return(loc);
}

/*
 * Advance over spaces/tabs in paragraph text
 */
 
WORD AdvanceSpaces(text, loc, maxLoc)
register TextPtr text;
register WORD loc, maxLoc;
{
	while( loc < maxLoc && text[loc] <= ' ')
		loc++;
	return(loc);
}

/*
 * Backup location to beginning of sentence
 */
 
WORD BackupSentence(text, loc, minLoc, maxLoc)
register TextPtr text;
register WORD loc;
register WORD minLoc;
register WORD maxLoc;
{
	register TextChar ch;
	
	loc = BackupSpaces(text, loc, minLoc);
	if( ( loc > minLoc ) && ((ch = text[loc-1]) == '.' || ch == '!' || ch == '?'))
		loc--;
	for( ; loc > minLoc ; loc--) {
		ch = text[loc - 1];
		if( (ch == '.' || ch == '!' || ch == '?') && text[loc] <= ' ')
			break;
	}
	return( AdvanceSpaces(text, loc, maxLoc) );
}

/*
 * Advance location to end of sentence
 */
 
WORD AdvanceSentence(text, loc, len)
register TextPtr text;
register WORD loc;
register WORD len;
{
	register TextChar ch;
	
	for(;loc < len; loc++ ) {
		ch = text[loc];
		if( (ch == '.' || ch == '!' || ch == '?') &&
			(loc + 1 == len || text[loc + 1] <= ' ')) {
			loc++;
			break;
		}
	}
	loc = AdvanceSpaces(text, loc, len);
	return( loc );
}
