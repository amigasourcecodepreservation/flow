/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Utility routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <devices/audio.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Color.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Utility.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>
#include <proto/console.h>

/*
 *	External variables
 */

extern struct IntuitionBase *IntuitionBase;
extern struct Library		*DiskfontBase;

extern ScreenPtr	_tbScreen;

extern BOOL		_tbHiRes, _tbNoShadows;

extern TextAttr	_tbTextAttr;

extern WORD	_tbXSize, _tbYSize;

extern UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight, _tbPenRed;

extern BOOL	_tbAutoActivate;

extern TextPtr	_monthNames[], _monthNamesAbbr[], _dayNames[], _dayNamesAbbr[];
extern TextPtr	_dateFormats[];

/*
 *	Local variables and definitions
 */

#define RAWKEY_F1	0x50
#define RAWKEY_F10	0x59
#define RAWKEY_HELP	0x5F

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

/*
 *	Pointers
 */

static UWORD chip nullPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x0000
};

static UWORD chip arrowPointerData[] = {
	0x0000, 0x0000, 0x0000, 0xC000, 0x4000, 0xE000, 0x6000, 0xB000,
	0x7000, 0x9800, 0x7800, 0x8C00, 0x7C00, 0x8600, 0x7E00, 0x8300,
	0x7800, 0x8F00, 0x5800, 0xAC00, 0x0C00, 0xD600, 0x0C00, 0x1600,
	0x0000, 0x0E00, 0x0000, 0x0000
};

static UWORD chip iBeamPointerData[] = {
	0x0000, 0x0000, 0x4400, 0x8800, 0x2800, 0x5000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x2800, 0x5000, 0x4400, 0x8800,
	0x0000, 0x0000
};

static UWORD chip plusPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x3E00, 0x1C00, 0x3E00, 0x1C00, 0xE780,
	0x7F00, 0xE780, 0x7F00, 0x8180, 0x7F00, 0x8180, 0x1C00, 0xE780,
	0x1C00, 0x2600, 0x0000, 0x3E00, 0x0000, 0x0000
};

static UWORD chip crossPointerData[] = {
	0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000, 0xF1E0, 0x0400, 0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000
};

static UWORD chip waitPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1860, 0x2790, 0x37B0, 0x4FC8,
	0x2DD0, 0x5FE8, 0x5DE8, 0xBFF4, 0x5DE8, 0xBFF4, 0x5C68, 0xBFF4,
	0x5FE8, 0xBFF4, 0x2FD0, 0x5FE8, 0x37B0, 0x4FC8, 0x1860, 0x2790,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static Pointer nullPointer = {
	0, 0, 0, 0, &nullPointerData[0]
};

static Pointer arrowPointer = {
	8, 12, -1, 0, &arrowPointerData[0]
};

static Pointer iBeamPointer = {
	6, 11, -4, -5, &iBeamPointerData[0]
};

static Pointer crossPointer = {
	11, 11, -6, -5, &crossPointerData[0]
};

static Pointer plusPointer = {
	9, 9, -5, -4, &plusPointerData[0]
};

static Pointer waitPointer = {
	14, 14, -7, -7, &waitPointerData[0]
};

/*
 *	Pointer array
 *	Must be in order given by pointer type definitions
 */

static Pointer *pointerArray[] = {
	&arrowPointer,
	&iBeamPointer,
	&crossPointer,
	&plusPointer,
	&waitPointer
};

/*
 *	Date info
 */

static UBYTE daysPerMonth[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : 366)	/* Valid till 2100 */

/*
 *	Array of upper and lower case letters for Amiga character set
 */

TextChar toUpper[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!','\"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.', '/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	 '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[','\\', ']', '^', '_',
	 '`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~',0x7F,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
	0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
	0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
	0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
};

TextChar toLower[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!', '"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.', '/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	 '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[','\\', ']', '^', '_',
	 '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',0x7F,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
	0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
	0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
	0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',

};

/*
 *	Flag for which characters are valid word characters
 *	A word character is either a number, a single quote, a soft hyphen
 *		or a character that could be part of a spelling check dictionary
 */

BYTE wordChar[] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
};

/*
 *	InitToolbox
 *	Initialize toolbox for specified screen
 *	Default state is med-res and Topaz-8 font
 */

void InitToolbox(register ScreenPtr screen)
{
	WORD numColors;
	RGBColor rgbBlack, rgbWhite, rgbDark, rgbLight;
	TextFontPtr font;
	RastPort rPort;
	ColorMapPtr colorMap;

	_tbScreen = screen;
	_tbHiRes = (screen->Font->ta_YSize > 8);
	_tbTextAttr = *(screen->Font);
	_tbTextAttr.ta_Style = FS_NORMAL;	/* Needs to be like this */
/*
	Get font dimensions
	(Use width of "e" rather than font's XSize,
		since this works better for proportional fonts)
*/
	InitRastPort(&rPort);
	if ((font = OpenFont(&_tbTextAttr)) == NULL)
		_tbXSize = 8;
	else {
		SetFont(&rPort, font);
		_tbXSize = TextLength(&rPort, "e", 1);
		CloseFont(font);
	}
	_tbYSize = _tbTextAttr.ta_YSize;
/*
	Get "new look" pen numbers
*/
	colorMap = screen->ViewPort.ColorMap;
	numColors = 1 << screen->RastPort.BitMap->Depth;
	_tbPenBlack = PenNumber(0x000, colorMap, numColors);
	_tbPenWhite = PenNumber(0xFFF, colorMap, numColors);
	rgbBlack = GetRGB4(colorMap, _tbPenBlack);
	rgbWhite = GetRGB4(colorMap, _tbPenWhite);
	rgbDark  = RGBCOLOR( 4*(  RED(rgbWhite) -   RED(rgbBlack))/15 +   RED(rgbBlack),
						 4*(GREEN(rgbWhite) - GREEN(rgbBlack))/15 + GREEN(rgbBlack),
						 6*( BLUE(rgbWhite) -  BLUE(rgbBlack))/15 +  BLUE(rgbBlack));
	rgbLight = RGBCOLOR( 9*(  RED(rgbWhite) -   RED(rgbBlack))/15 +   RED(rgbBlack),
						11*(GREEN(rgbWhite) - GREEN(rgbBlack))/15 + GREEN(rgbBlack),
						11*( BLUE(rgbWhite) -  BLUE(rgbBlack))/15 +  BLUE(rgbBlack));
	_tbPenDark  = PenNumber(rgbDark, colorMap, numColors);	/* Dark blue-gray */
	_tbPenLight = PenNumber(rgbLight, colorMap, numColors);	/* Light cyan-gray */
	_tbPenRed	= PenNumber(0xF00, colorMap, numColors);	/* Red */
	if (_tbPenRed == _tbPenBlack)
		_tbPenRed = _tbPenDark;
	_tbNoShadows = (_tbPenLight == _tbPenWhite);
}

/*
 *	Enable or disable auto-activation of text boxes in requesters and dialogs
 *	Default is enabled
 */

void AutoActivateEnable(BOOL enable)
{
	_tbAutoActivate = enable;
}

/*
 *	LibraryVersion
 *	Return version number of library
 */

UWORD LibraryVersion(struct Library *libBase)
{
	return (libBase->lib_Version);
}

/*
 *	SystemVersion
 *	Return version number of system
 */

UWORD SystemVersion()
{
	struct Library *AbsExecBase;

	AbsExecBase = *((struct Library **) 4L);		/* Absolute address */
	return (LibraryVersion(AbsExecBase));
}

/*
 *	SetStdPointer
 *	Set pointer to one of standard types
 *	Does not change pointer if already set to requested pointer
 */

static UWORD	*sysPointerData;

void SetStdPointer(WindowPtr window, UWORD pointerType)
{
	register PointerPtr pointer;

	if (pointerType > POINTER_MAX_TYPE)
		return;
	if (pointerType == POINTER_ARROW &&
		LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0) {
		if (window->Pointer != sysPointerData) {
			ClearPointer(window);
			sysPointerData = window->Pointer;
		}
	}
	else {
		pointer = pointerArray[pointerType];
		if (window->Pointer != pointer->PointerData)
			SetPointer(window, pointer->PointerData,
					   pointer->Height, pointer->Width,
					   pointer->XOffset, pointer->YOffset);
	}
}

/*
 *	ObscurePointer
 *	Hide the pointer from view
 *	(Since we compile with -b option, we must reference off a Pointer struct)
 */

void ObscurePointer(WindowPtr window)
{
	if (window->Pointer != nullPointer.PointerData)
		SetPointer(window, nullPointer.PointerData, 0, 0, 0, 0);
}

/*
 *	SysBeep
 *	Produce beep for specified ticks (1/50 of sec.)
 */

#define SOUND_FREQ	330		/* In Hertz */

static BYTE chip soundData[] = {
	0,  64,  127,  127,  127,  127,  127,  64,
	0, -64, -127, -127, -127, -127, -127, -64
};

#define LEFTCHAN_MASK	0x09
#define RIGHTCHAN_MASK	0x06

static UBYTE audChanMasks[] = { 0x03, 0x05, 0x0A, 0x0C };

void SysBeep(UWORD beepTime)
{
	WORD channels;
	MsgPortPtr replyPort;
	struct IOAudio ioa1, ioa2;

	ioa1.ioa_Request.io_Message.mn_Node.ln_Pri = 80;
	ioa1.ioa_Data = audChanMasks;
	ioa1.ioa_Length = sizeof(audChanMasks);
	if ((replyPort = CreatePort(NULL, 0)) == NULL)
		return;
	ioa1.ioa_Request.io_Message.mn_ReplyPort = replyPort;
/*
	Open device, allocate left & right channels, and set up for sound
*/
	if (OpenDevice(AUDIONAME, 0, (IOReqPtr) &ioa1, 0) == 0) {
		ioa1.ioa_Request.io_Command = CMD_WRITE;
		ioa1.ioa_Request.io_Flags = ADIOF_PERVOL;
		ioa1.ioa_Data = soundData;
		ioa1.ioa_Length = 16;
		ioa1.ioa_Period = (3579545L/(16*SOUND_FREQ));
		ioa1.ioa_Volume = 64;
		ioa1.ioa_Cycles = (SOUND_FREQ*beepTime)/50;
/*
	Do both left and right channels
*/
		ioa2 = ioa1;
		channels = (WORD) ioa1.ioa_Request.io_Unit;
		ioa1.ioa_Request.io_Unit = (struct Unit *) (channels & LEFTCHAN_MASK);
		ioa2.ioa_Request.io_Unit = (struct Unit *) (channels & RIGHTCHAN_MASK);
		BeginIO((IOReqPtr) &ioa1);
		BeginIO((IOReqPtr) &ioa2);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
/*
	Free channels and close device
*/
		ioa1.ioa_Request.io_Unit = (struct Unit *) channels;
		CloseDevice((IOReqPtr) &ioa1);
	}
	DeletePort(replyPort);
}

/*
 *	ConvertKeyMsg
 *	Convert RAWKEY messages for ASCII keys to VANILLAKEY messages
 *	Must be called before message is replied to,
 *		and ConsoleDevice base address must have obtained from console.device
 */

static struct InputEvent keyEvent = {
	NULL, IECLASS_RAWKEY, 0, 0, 0, (WORD) 0, (WORD) 0, (ULONG) 0, (ULONG) 0
};

void ConvertKeyMsg(register IntuiMsgPtr intuiMsg)
{
	register UWORD code;
	register UWORD qualifier;
	register WORD num;
	UBYTE key;				/* Need to take its address */

	code = intuiMsg->Code;
	qualifier = intuiMsg->Qualifier;
	if (intuiMsg->Class != RAWKEY					||
		(code & IECODE_UP_PREFIX)					||
		(qualifier & AMIGAKEYS)						||
		(code >= CURSORUP && code <= CURSORLEFT)	||
		(code >= RAWKEY_F1 && code <= RAWKEY_F10)	||
		code == RAWKEY_HELP)
		return;
	keyEvent.ie_Code = code;
	keyEvent.ie_Qualifier = qualifier;
	keyEvent.ie_position.ie_addr = *((APTR *) intuiMsg->IAddress);
	if ((num = RawKeyConvert(&keyEvent, &key, 1, NULL)) == 1) {
		intuiMsg->Code = key;
		intuiMsg->Class = VANILLAKEY;
	}
	else if (num == 0)		/* Was SHIFT, ALT, etc. */
		intuiMsg->Code |= IECODE_UP_PREFIX;	/* Make caller ignore it */
}

/*
 *	WaitMouseUp
 *	Check msgPort for mouse up event in window
 *	If message is not for specified window, or is not MOUSEMOVE, MOUSEBUTTONS,
 *	RAWKEY or INTUITICKS, put message back and return FALSE
 *	Otherwise reply to message and return status of select button
 *	Will wait until at least one message is received
 *	(Having INTUITICKS enabled allows us to auto-scroll while dragging)
 */

BOOL WaitMouseUp(MsgPortPtr msgPort, WindowPtr window)
{
	register BOOL mouseDown;
	register ULONG class;
	register UWORD code, modifier;
	register IntuiMsgPtr intuiMsg;

	mouseDown = TRUE;		/* Assume mouse down (for INTUITICKS msgs) */
	(void) WaitPort(msgPort);
	while (mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) != NULL) {
		class = intuiMsg->Class;
		code = intuiMsg->Code;
		modifier = intuiMsg->Qualifier;
		if (intuiMsg->IDCMPWindow != window ||
			(class != MOUSEMOVE && class != MOUSEBUTTONS &&
			 class != RAWKEY && class != INTUITICKS)) {
			PutMsg(msgPort, (MsgPtr) intuiMsg);
			return (FALSE);
		}
		ReplyMsg((MsgPtr) intuiMsg);
		if (class == MOUSEBUTTONS && code == SELECTUP)
			mouseDown = FALSE;
		else if (class != INTUITICKS)
			mouseDown = SELECTBUTTON(modifier);
		if (class == MOUSEMOVE)
			return (mouseDown);			/* Return immediately with MOUSEMOVE */
	}
	return (mouseDown);
}

/*
 *	ActiveWindow
 *	Return pointer to window that is currently active
 */

WindowPtr ActiveWindow()
{
	register LONG intuiLock;
	register WindowPtr window;

	intuiLock = LockIBase(0);
	window = IntuitionBase->ActiveWindow;
	UnlockIBase(intuiLock);
	return (window);
}

/*
 *	CmpString
 *	Compare to strings, ignoring capitalization if caseSense == FALSE
 *	Return -1 if s1 < s2, 0 if s1 = s2, +1 if s1 > s2
 */

WORD CmpString(register TextPtr s1, register TextPtr s2, WORD l1, WORD l2, BOOL caseSense)
{
	register TextChar ch1, ch2;
	register WORD len;

	len = (l1 < l2) ? l1 : l2;
	if (caseSense) {
		while (len--) {
			ch1 = *s1++;
			ch2 = *s2++;
#if SWEDISH
			if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			else if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
			else if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
#endif
			if (ch1 != ch2) {		/* Only one comparison if same */
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	else {
		while (len--) {
			ch1 = *s1++;
			ch2 = *s2++;
			if ((ch1 >= 'A' && ch1 <= 'Z') || (ch1 >= 0xC0 && ch1 <= 0xDF))
				ch1 += 0x20;
			if ((ch2 >= 'A' && ch2 <= 'Z') || (ch2 >= 0xC0 && ch2 <= 0xDF))
				ch2 += 0x20;
#if SWEDISH
			if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			else if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
			else if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
#endif
			if (ch1 != ch2) {		/* Only one comparison if same */
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	if (l1 < l2)
		return (-1);
	else if (l1 > l2)
		return (1);
	return (0);
}

/*
 *	NumToString
 *	Convert number to string
 *	String buffer must have enough room for string and trailing NULL
 */

void NumToString(register LONG num, register TextPtr s)
{
	register LONG divisor;

	if (num < 0) {
		*s++ = '-';
		num = -num;
	}
	for (divisor = 1; num/divisor > 9;	divisor *= 10) ;
	while  (divisor) {
		*s++ = num/divisor + '0';
		num %= divisor;
		divisor /= 10;
	}
	*s = '\0';
}

/*
 *	StringToNum
 *	Convert string to number (long)
 *	Skips initial spaces
 */

LONG StringToNum(register TextPtr s)
{
	register WORD sign;
	register LONG num;

	while (*s == ' ')
		s++;
	if (*s == '-') {
		sign = -1;
		s++;
	}
	else
		sign = 1;
	num = 0;
	while (*s >= '0' && *s <= '9') {
		num *= 10;
		num += *s++ & 0x0F;
	}
	if (sign == -1)
		num = -num;
	return (num);
}

/*
 *	MonthName
 *	Return month name in buffer
 *	Buffer must be large enough for entire month name, even if abbrev is TRUE
 *	monthNum is in range 1..12
 */

void MonthName(WORD month, BOOL abbrev, TextPtr buff)
{
	month--;
	if (month < 0)
		month = 0;
	else if (month > 11)
		month = 11;
	strcpy(buff, _monthNames[month]);
	if (abbrev)
		buff[3] = '\0';
}

/*
 *	DateString
 *	Return date string in indicated form
 */

void DateString(struct DateStamp *dateStamp, WORD dateForm, TextPtr date)
{
	register WORD day, month, year, daysInYear, weekDay;
	register TextPtr s1, s2;
	UBYTE yearBuff[5], buff[100];

/*
	Get year, month, and day
*/
	day = dateStamp->ds_Days;
	weekDay = day % 7;				/* 0 to 6 for Sunday through Saturday */
	year = 1978;
	while (day >= (daysInYear = DAYS_IN_YEAR(year))) {
		day -= daysInYear;
		year++;
	}
	daysPerMonth[1] = (year % 4) ? 28 : 29;		/* Valid till 2100 */
	for (month = 0; month < 12; month++) {
		if (day < daysPerMonth[month])
			break;
		day -= daysPerMonth[month];
	}
	day++;		/* Days are numbered from 1 */
	month++;	/* Months are numbered from 1 */
/*
	Get the format string
*/
	switch (dateForm) {
	case DATE_SHORT:
	case DATE_ABBR:
	case DATE_LONG:
	case DATE_ABBRDAY:
	case DATE_LONGDAY:
	case DATE_MILITARY:
		strcpy(buff, _dateFormats[dateForm]);
		break;
	case DATE_CUSTOM:
		strcpy(buff, date);
		break;
	default:
		return;
	}
/*
	Build date string
*/
	for (s1 = buff, s2 = date; *s1; s1++) {
		switch (*s1) {
		case DAY_NUMZERO:
			if (day < 10)
				*s2++ = '0';
			;						/* Fall through */
		case DAY_NUMBER:
			NumToString(day, s2);
			s2 += strlen(s2);
			break;
		case DAY_NAME:
			strcpy(s2, _dayNames[weekDay]);
			s2 += strlen(s2);
			break;
		case DAY_NAMEABBREV:
			strcpy(s2, _dayNamesAbbr[weekDay]);
			s2 += strlen(s2);
			break;
		case MONTH_NUMZERO:
			if (month < 10)
				*s2++ = '0';
			;						/* Fall through */
		case MONTH_NUMBER:
			NumToString(month, s2);
			s2 += strlen(s2);
			break;
		case MONTH_NAME:
			strcpy(s2, _monthNames[month - 1]);
			s2 += strlen(s2);
			break;
		case MONTH_NAMEABBREV:
			strcpy(s2, _monthNamesAbbr[month - 1]);
			s2 += strlen(s2);
			break;
		case YEAR_LONG:
		case YEAR_SHORT:
			NumToString(year, yearBuff);
			strcpy(s2, (*s1 == YEAR_SHORT) ? yearBuff + 2 : yearBuff);
			s2 += strlen(s2);
			break;
		default:
			*s2++ = *s1;
			break;
		}
	}
	*s2 = '\0';
}

/*
 *	TimeString
 *	Return time string
 */

void TimeString(struct DateStamp *dateStamp, BOOL needSecs, BOOL hour12, register TextPtr time)
{
	register WORD second, minute, hour;
	register BOOL am;

/*
	Get hour, minute, and second
*/
	second = dateStamp->ds_Tick/TICKS_PER_SECOND;
	minute = dateStamp->ds_Minute;
	hour = minute/60;
	minute -= hour*60;
	if (hour12) {
		am = (hour < 12);
		if (hour > 12)
			hour -= 12;
		else if (hour == 0)
			hour = 12;
	}
/*
	Build time string
*/
	NumToString(hour, time);
	time += strlen(time);
	*time++ = ':';
	if (minute < 10)
		*time++ = '0';
	NumToString(minute, time);
	time += strlen(time);
	if (needSecs) {
		*time++ = ':';
		if (second < 10)
			*time++ = '0';
		NumToString(second, time);
		time += strlen(time);
	}
	if (hour12)
		strcpy(time, (am) ? " AM" : " PM");
}

/*
 *	GetFont
 *	Get specified font, either from memory or disk
 */

TextFontPtr GetFont(TextAttrPtr textAttr)
{
	register TextFontPtr font;

	font = OpenFont(textAttr);
	if (font == NULL || font->tf_YSize != textAttr->ta_YSize) {
		if (font)
			CloseFont(font);
		if (DiskfontBase) {
			font = OpenDiskFont(textAttr);
			if (font == NULL || font->tf_YSize != textAttr->ta_YSize) {
				if (font)
					CloseFont(font);
				font = NULL;
			}
		}
		else
			font = NULL;
	}
	return (font);
}
