/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Window operations
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfxmacros.h>
#include <graphics/regions.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Screen.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct NewWindow		newWindow, newBackWindow;

extern BOOL			_tbOnPubScreen;
extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		monitorMsgPort;
extern WindowPtr	backWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;
extern MenuPtr		docMenuStrip;

extern WindowPtr	cmdWindow;		/* Last active window */

extern WindowPtr	closeWindow;
extern BOOL			closeFlag, closeAllFlag;

extern GadgetTemplate	windowGadgets[];

extern TextChar	screenTitle[];

extern BYTE	paperColor;

extern UWORD		grayPat[];

extern UWORD		wbColors[];
extern UWORD		colors[];

extern WindowPtr	findDialog, changeDialog, spellDialog, insertDialog;

extern UWORD		waitCount;

extern TextChar	strAltChDict[];
extern BOOL			_tbHiRes;

extern BOOL			_tbSmartWindows;

extern BOOL			inMacro;

extern UBYTE		_tbPenDark, _tbPenLight, _tbPenWhite, _tbPenBlack;

extern Ptr			pasteBuff;

/*
 *	Local variables and definitions
 */

typedef struct  {
	LayerPtr	Layer;
	Rectangle	Bounds;
	LONG		OffsetX, OffsetY;
} BackFillMsg, *BFMsgPtr;

static struct Hook	backFillHook, docBackFillHook;

/*
 *	Local prototypes
 */

void __saveds __asm BackFill(register struct Hook *, register RastPtr, register BFMsgPtr);
void __saveds __asm DocBackFill(register struct Hook *, register RastPtr, register BFMsgPtr);

void	ClearBackWindow(RastPtr, Rectangle *);

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm BackFill(register __a0 struct Hook *hook,
									register __a2 RastPtr rPort,
									register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	ClearBackWindow(&newRPort, &bfMsg->Bounds);
}

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm DocBackFill(register __a0 struct Hook *hook,
										register __a2 RastPtr rPort,
										register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	SetAPen(&newRPort, _tbPenWhite);
	SetDrMd(&newRPort, JAM1);
	RectFill(&newRPort, bfMsg->Bounds.MinX, bfMsg->Bounds.MinY,
			 bfMsg->Bounds.MaxX, bfMsg->Bounds.MaxY);
}

/*
 *	Clear backWindow
 */

static void ClearBackWindow(RastPtr rPort, Rectangle *rect)
{
	UBYTE	numColors;
	
	numColors = 1 << screen->BitMap.Depth;
	if( numColors >= 8 ) {
		SetAPen(rPort, _tbPenDark);
		SetDrMd(rPort, JAM1);
	} else {
		SetAPen(rPort, _tbPenBlack);
		SetBPen(rPort, _tbPenWhite);
		SetDrMd(rPort, JAM2);
		SetAfPt(rPort, grayPat, 1);
	}
	RectFill(rPort, rect->MinX, rect->MinY, rect->MaxX, rect->MaxY);
}

/*
 *	Open backWindow
 */

WindowPtr OpenBackWindow()
{
	ULONG oldIDCMPFlags;
	Rectangle rect;

	newBackWindow.Screen = screen;
	newBackWindow.Type = CUSTOMSCREEN;
	if (_tbOnPubScreen) {
		newBackWindow.Width = newBackWindow.Height = 1;
		newBackWindow.Flags |= WFLG_NOCAREREFRESH;
	}
	else {
		newBackWindow.Width = screen->Width;
		newBackWindow.Height = screen->Height;
	}
	oldIDCMPFlags = newBackWindow.IDCMPFlags;
	newBackWindow.IDCMPFlags = 0;
	if (intuiVersion < OSVERSION_2_0)
		backWindow = OpenWindow(&newBackWindow);
	else {
		if (_tbOnPubScreen)
			newBackWindow.Type = PUBLICSCREEN;
		newBackWindow.Flags |= WFLG_NOCAREREFRESH;
		backFillHook.h_Entry = (ULONG (*)()) BackFill;
		backWindow = OpenWindowTags(&newBackWindow,
									WA_BackFill, &backFillHook,
									TAG_END);
	}
	if (backWindow != NULL) {
		backWindow->UserPort = &monitorMsgPort;
		ModifyIDCMP(backWindow, oldIDCMPFlags);
		if(_tbOnPubScreen) {
			ScreenToFront(screen);	  /* In case it is not in front */
			WindowToBack(backWindow);
			SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
		}
		else if (intuiVersion < OSVERSION_2_0) {
			SetRect(&rect, 0, 0, (WORD) (backWindow->Width - 1), (WORD) (backWindow->Height - 1));
			ClearBackWindow(backWindow->RPort, &rect);
		}
		SetStdPointer(backWindow, POINTER_ARROW);
	}
	return (backWindow);
}

/*
 *	Get window text region rectangle
 */

void GetTextRect(WindowPtr window, Rectangle *rect)
{
	GetWindowRect(window, rect);
}

/*
 *	Install clip region for window
 */

void SetWindowClip(WindowPtr window)
{
	Rectangle rect;

	GetWindowRect(window, &rect);
	SetRectClip(window->WLayer, &rect);
}

/*
 *	Set clip region of window to horizontal scrolling region
 */

void SetHorizScrollClip(WindowPtr window)
{
	Rectangle rect;

	GetWindowRect(window, &rect);
	rect.MinX += LEFT_MARGIN;
	SetRectClip(window->WLayer, &rect);
}

/*
 *	Determine if window is document window
 */

BOOL IsDocWindow(WindowPtr window)
{
	return ((BOOL) (WindowNum(window) != -1));
}

/*
 *	Return number of given window in windowList, or -1 if not present
 */

WORD WindowNum(WindowPtr window)
{
	register WORD num;

	for (num = 0; num < numWindows; num++) {
		if (window == windowList[num])
			return (num);
	}
	return (-1);
}

/*
 *	Create new document window with the specified title
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr CreateWindow(TextPtr title)
{
	register ULONG oldIDCMPFlags;
	register WindowPtr window;
	register GadgetPtr gadgList;
	DocDataPtr docData;
	WORD zoomSize[4];
/*
	Create and attach window gadgets
*/
	if ((gadgList = GetGadgets(windowGadgets)) == NULL)
		return (NULL);
	GadgetItem(gadgList, UP_ARROW)->Activation |= GACT_RIGHTBORDER;
	GadgetItem(gadgList, DOWN_ARROW)->Activation |= GACT_RIGHTBORDER;
	GadgetItem(gadgList, LEFT_ARROW)->Activation |= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, RIGHT_ARROW)->Activation |= GACT_BOTTOMBORDER;

	GadgetItem(gadgList, VERT_SCROLL)->Activation |= GACT_RIGHTBORDER;
	GadgetItem(gadgList, HORIZ_SCROLL)->Activation |= GACT_BOTTOMBORDER;
	
	newWindow.FirstGadget = gadgList;
/*
	Open window and set message port to monitorMsgPort
	All windows are SIMPLEREFRESH windows
*/
	newWindow.Screen = screen;
	newWindow.Type = CUSTOMSCREEN;
	newWindow.Flags = newWindow.Flags & ~WFLG_REFRESHBITS |
		 (_tbSmartWindows ? WFLG_SMART_REFRESH : WFLG_SIMPLE_REFRESH );
	newWindow.LeftEdge = 0;
	newWindow.Title = NULL;
	newWindow.TopEdge = (numWindows+1) * screen->BarHeight + 1;
	if( screen->ViewPort.Modes & LACE )
		newWindow.TopEdge++;	 /* Two pixel wide line reduces flicker */

	newWindow.Width = screen->Width;
	newWindow.Height = screen->Height - newWindow.TopEdge;
	
	oldIDCMPFlags = newWindow.IDCMPFlags;
	newWindow.IDCMPFlags = 0;
	if (LibraryVersion(IntuitionBase) < OSVERSION_2_0)
		window = OpenWindow(&newWindow);
	else {
		zoomSize[0] = zoomSize[1] = 0;
		zoomSize[2] = screen->Width;
		zoomSize[3] = screen->Height;
		docBackFillHook.h_Entry = (ULONG (*)()) DocBackFill;
		window = OpenWindowTags(&newWindow,
								WA_Zoom, zoomSize,
								WA_BackFill, &docBackFillHook,
								TAG_END);
	}
	newWindow.IDCMPFlags = oldIDCMPFlags;		/* For next CreateWindow */
	newWindow.TopEdge -= numWindows*screen->BarHeight;
	newWindow.Height += numWindows*screen->BarHeight;
	if (window == NULL) {
		DisposeGadgets(gadgList);
		return (NULL);
	}
	SetStdPointer(window, POINTER_ARROW);
	SetWTitle(window, title);			/* Set to copy of title */
	if (_tbOnPubScreen)
		SetWindowTitles(window, (TextPtr) -1, screenTitle);
	window->UserPort = &monitorMsgPort;
	ModifyIDCMP(window, oldIDCMPFlags);
/*
	Fill window with white (while menus are being created)
*/
	SetWindowClip(window);
	SetAPen(window->RPort, COLOR_WHITE);
	SetBPen(window->RPort, COLOR_WHITE);
	SetRast(window->RPort, COLOR_WHITE);
/*
	Attach document menu strip
*/
	InsertMenuStrip(window, docMenuStrip);
/*
	Create new DocData record
*/
	if ((docData = MemAlloc(sizeof(DocData), MEMF_CLEAR)) == NULL) {
		RemoveWindow(window);
		return (NULL);
	}
	SetWRefCon( window, (Ptr) docData ) ;
	docData->Window = window;
	docData->PrintRec = MemAlloc(sizeof(PrintRecord), MEMF_CLEAR);
	if( docData->PrintRec == NULL ) {
		RemoveWindow(window);
		return(NULL);
	}
	SetPageIndic(window,1);
/*
	Insert window into window list
*/
	if( !_tbHiRes )
		SetMenuItem(window, (WORD) MENUITEM(
			SEARCH_MENU, SPELLING_ITEM, CHANGEDICT_SUBITEM), strAltChDict);
	AddWindowItem(window);
	return (window);
}

/*
 *	Remove the specified outline window and free all memory it allocated
 */

void RemoveWindow(WindowPtr window)
{
	register GadgetPtr gadgList;
	RegionPtr clipRgn;
	DocDataPtr docData;

/*
	Shut down window messages, get pointers to structures, and close window
*/
	ClearMenuStrip(window);
	gadgList = GadgetItem(window->FirstGadget, 0);	/* First user gadget */
	clipRgn = InstallClipRegion(window->WLayer, NULL);
	docData = (DocDataPtr) GetWRefCon(window);
	SetWTitle(window, NULL);

	CloseWindowSafely(window, mainMsgPort);
/*
	Find window in window list and remove - must be done before getting cmdWindow
*/
	RemoveWindowItem(window);
	GetCmdWindow();
/*
	Free memory used by menus, gadgets, and clipRegion
*/
	if (docData) {
		if( docData->PrintRec )
			MemFree(docData->PrintRec, sizeof(PrintRecord));
		MemFree(docData, sizeof(DocData));
	}
	if (gadgList)
		DisposeGadgets(gadgList);
	if (clipRgn)
		DisposeRegion(clipRgn);
	if( !numWindows )
		DoDialogCloseAll();
		
}

/*
 	Handle click in go-away region of window
*/

void DoGoAwayWindow(WindowPtr window, UWORD modifier)
{
	closeFlag = IsDocWindow(window) || ( GetWKind(window) == WKIND_DIALOG) ;
	if( closeFlag ) {
		closeWindow = window;
/*
	If alt key down then close all windows
*/
		if (closeFlag && (modifier & ALTKEYS)) {
			closeWindow = NULL;
			closeFlag = FALSE;
			closeAllFlag = TRUE;
		}
	}
}

/*
 *	Handle window activate/inactivate events
 */

void  DoWindowActivate(WindowPtr window, BOOL activate)
{
	register WindowPtr activeWindow;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL isDialog;
	UWORD dummy;
	
	if (IsDocWindow(window)) {
		activeWindow = ActiveWindow();
		isDialog = (activeWindow == findDialog || activeWindow == changeDialog ||
			activeWindow == spellDialog || activeWindow == insertDialog);
		SetPageIndic(window, docData->PageIndic);
		if( activate ) {
			if( !isDialog ) {
				if(IsDocWindow(cmdWindow)) {
					CursorOff(cmdWindow);
				}
				(void) BuildLevelTable(docData, &dummy, &dummy);
			}
			
			SetPointerShape();
/*
	Turn on the cursor if not in a modal dialog
*/
			if( waitCount == 0 )
				CursorOn(window);
		} else {
			if( !isDialog )
				CursorOff(window);
			SetStdPointer(window, POINTER_ARROW);
		}
		cmdWindow = window;
	}
	SetAllMenus();
}

/*
 *	Install clip region for window, and adjust scroll bars
 */

void DoNewSize(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	
	SetWindowClip(window);
	SetPageIndic(window, docData->PageIndic);
	AdjustScrollBars(window);
}

/*
 * Redraw ENTIRE window contents
 */
 
void RefreshWindow(window)
register WindowPtr window;
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	SetPageIndic(window, docData->PageIndic);
	DrawWindow(window);
}
 
/*
 *	Update outline window contents (refresh through damage list)
 */

void DoWindowUpdate(WindowPtr window)
{
	WORD minY, maxY;
	RegionPtr oldRgn;

	minY = window->WLayer->DamageList->bounds.MinY;
	maxY = window->WLayer->DamageList->bounds.MaxY;
	BeginUpdate(window->WLayer);
	oldRgn = GetClip(window->WLayer);
	SetWindowClip(window);
	DrawWindowRange(window, minY, maxY);
	SetClip(window->WLayer, oldRgn);
	EndUpdate(window->WLayer, TRUE);
	window->WLayer->Flags &= ~LAYERREFRESH;		/* EndUpdate doesn't do this */
}

/*
 *	Check to see if any document windows need updating, and update if so
 */

void UpdateWindows()
{
	register WORD i;
	register BOOL updateActive;
	register WindowPtr window, activeWindow;

	CheckColorTable();
	activeWindow = ActiveWindow();
	updateActive = FALSE;
	for (i = 0; i < numWindows; i++) {
		window = windowList[i];
		if (window->WLayer->Flags & LAYERREFRESH) {
			if( window == activeWindow ) {
				SetStdPointer(window, POINTER_WAIT);
				updateActive = TRUE;
			}
			DoWindowUpdate(window);
		}			
	}
	if( updateActive )
		SetPointerShape();
}

/*
 *	Refresh window contents (intuition generated events)
 */

void DoWindowRefresh(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	CheckColorTable();
	if( window != backWindow || (!_tbOnPubScreen) ) {
		if( window && (IsDocWindow(window) || window == backWindow)) {
/*
	Intuition often erases border gadgets, so do this outside of Begin/EndRefresh
*/
			if( IsDocWindow(window) )
				SetPageIndic(window, docData->PageIndic);
/*
	Do the actual refresh
*/
			BeginRefresh(window);
			GetWindowRect(window, &rect);
			if (window == backWindow)
				ClearBackWindow(window->RPort, &rect);
			else
				RefreshWindow(window);
			EndRefresh(window, TRUE);
		}
	}
}

/*
	Select the window (bring to front, activate)
*/

void SelectWindow(WindowPtr window)
{
	if( window->WLayer->front)
		WindowToFront(window);
	ActivateWindow(window);
	Delay(5);
}	

/*
	Get window to send messages to
*/

void GetCmdWindow()
{
	WindowPtr activeWindow;
	
	activeWindow = ActiveWindow();
	if (IsDocWindow(activeWindow))
		cmdWindow = activeWindow;
	else if (!IsDocWindow(cmdWindow)) {
		if (numWindows)
			cmdWindow = windowList[numWindows - 1];
		else
			cmdWindow = backWindow;
	}
/*
	If inside AREXX macro, don't activate the window every time
*/
	if (cmdWindow != activeWindow && !inMacro) {
		ActivateWindow(cmdWindow);
		DoWindowActivate(cmdWindow, TRUE);
	}
}

/*
 * Cause update of entire window by accumulation or redrawing immediately
 */
 
void CauseUpdate(WindowPtr window)
{
	Rectangle rect;
	
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	if( _tbSmartWindows )
		DrawWindow(window);
}

/*
 *	Check for and handle window refresh messages
 */

void RefreshWindows()
{
	WORD item;
	IntuiMsgPtr intuiMsg;

	Delay(5);			/* Wait until dialog is gone */
	while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
		if (!DialogFilter(intuiMsg, &item))	/* Get refresh message */
			ReplyMsg((MsgPtr) intuiMsg);
	}
}
