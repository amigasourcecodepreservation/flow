/*
 *	Flow
 *
 *	REXX interface routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <rexx/storage.h>
#include <rexx/rxslib.h>
#include <rexx/errors.h>

#include <IFF/WORD.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	AREXX definitions and prototypes
 */

typedef struct RexxMsg	RexxMsg, *RexxMsgPtr;

ULONG		InitPort(MsgPortPtr, TextPtr);
void		FreePort(MsgPortPtr);
BOOL		IsRexxMsg(RexxMsgPtr);
RexxMsgPtr	CreateRexxMsg(MsgPortPtr, TextPtr, TextPtr);
BOOL		FillRexxMsg(RexxMsgPtr, ULONG, ULONG);
TextPtr		CreateArgstring(TextPtr, ULONG);
void		ClearRexxMsg(RexxMsgPtr, ULONG);
void		DeleteRexxMsg(RexxMsgPtr);

/*
 *	External variables
 */

extern struct RexxLib	*RexxSysBase;

extern MsgPort		rexxMsgPort;
extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;
extern WindowPtr	backWindow, windowList[];
extern WORD			numWindows;

extern WindowPtr	cmdWindow;

extern BOOL		inMacro, fromCLI;

extern TextChar	strProgName[];
extern TextChar	strFKeysName[];
extern TextChar	strAutoExecName[];
extern TextChar	strMacroNames1[10][32], strMacroNames2[10][32];

extern FKey	fKeyTable1[], fKeyTable2[];

extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

extern BOOL			drawOn;

/*
 *	Local variables and definitions
 */

#define USERREQ_EDIT	2
#define USERREQ_TEXT	3

#define MACRONAME_TEXT	2
#define MACROARG_TEXT	3

#define SAVE_BUTTON	2
#define NAME1_TEXT	3

static TextChar	progPortName[15];

#define MENUCMD_NULL	MENUITEM(NOMENU, NOITEM, NOSUB)

#define MENUCMD_NEW				MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB)
#define MENUCMD_OPEN			MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB)
#define MENUCMD_QUIT			MENUITEM(PROJECT_MENU, QUIT_ITEM, NOSUB)
#define MENUCMD_PREFERENCES		MENUITEM(EDIT_MENU, PREFERENCES_ITEM, NOSUB)
#define MENUCMD_SCREENCOLORS	MENUITEM(EDIT_MENU, SCREENCOLORS_ITEM, NOSUB)

#define MENUCMD_FIND	MENUITEM(SEARCH_MENU, FIND_ITEM, NOSUB)

typedef struct MenuCmdList {
	TextPtr	Name;
	UWORD	Num;
} MenuCmdList;

#define NUM_MENU_COMMANDS	(sizeof(menuCmds)/sizeof(MenuCmdList))

static MenuCmdList menuCmds[] = {
	{ "New",			MENUITEM(PROJECT_MENU,	NEW_ITEM,		NOSUB) },
	{ "Open",		MENUITEM(PROJECT_MENU,	OPEN_ITEM,		NOSUB) },
	{ "Close",		MENUITEM(PROJECT_MENU,	CLOSE_ITEM,	NOSUB) },
	{ "Save",		MENUITEM(PROJECT_MENU,	SAVE_ITEM,		NOSUB) },
	{ "SaveAs",		MENUITEM(PROJECT_MENU,	SAVEAS_ITEM,	NOSUB) },
	{ "Revert",		MENUITEM(PROJECT_MENU,	REVERT_ITEM,	NOSUB) },
	{ "PageSetup",	MENUITEM(PROJECT_MENU,	PAGESETUP_ITEM,	NOSUB) },
	{ "PrintOne",	MENUITEM(PROJECT_MENU,	PRINTONE_ITEM,	NOSUB) },
	{ "Print",		MENUITEM(PROJECT_MENU,	PRINT_ITEM,	NOSUB) },
	{ "Quit",		MENUITEM(PROJECT_MENU,	QUIT_ITEM,		NOSUB) },
		
	{ "Cut",			MENUITEM(EDIT_MENU,		CUT_ITEM,		NOSUB) },
	{ "Copy",		MENUITEM(EDIT_MENU,		COPY_ITEM,		NOSUB) },
	{ "Paste",		MENUITEM(EDIT_MENU,		PASTE_ITEM,	NOSUB) },
	{ "Erase",		MENUITEM(EDIT_MENU,		ERASE_ITEM,	NOSUB) },
	{ "UpperCase",	MENUITEM(EDIT_MENU,		CHANGECASE_ITEM,	UPPERCASE_SUBITEM) },
	{ "LowerCase",	MENUITEM(EDIT_MENU,		CHANGECASE_ITEM,	LOWERCASE_SUBITEM) },
	{ "MixedCase",	MENUITEM(EDIT_MENU,		CHANGECASE_ITEM,	MIXEDCASE_SUBITEM) },
	{ "InsertDate",	MENUITEM(EDIT_MENU,		INSERT_ITEM,	DATE_SUBITEM) },
	{ "InsertTime",	MENUITEM(EDIT_MENU,		INSERT_ITEM,	TIME_SUBITEM) },
	{ "InsertLiteral",MENUITEM(EDIT_MENU,		INSERT_ITEM,	LITERAL_SUBITEM) },
	{ "InsertSpace",	MENUITEM(EDIT_MENU,		INSERT_ITEM,	SPACE_SUBITEM) },
	{ "ItemFormats",	MENUITEM(EDIT_MENU,		ITEMFORMATS_ITEM,	NOSUB) },
	{ "SelectAll",	MENUITEM(EDIT_MENU,		SELECTALL_ITEM,	NOSUB) },
	{ "Preferences",	MENUITEM(EDIT_MENU,		PREFERENCES_ITEM,	NOSUB) },
	{ "ScreenColors",	MENUITEM(EDIT_MENU,		SCREENCOLORS_ITEM,	NOSUB) },

	{ "Find",		MENUITEM(SEARCH_MENU,	FIND_ITEM,		NOSUB) },
	{ "FindNext",	MENUITEM(SEARCH_MENU,	FINDNEXT_ITEM,	NOSUB) },
	{ "Change",		MENUITEM(SEARCH_MENU,	CHANGE_ITEM,	NOSUB) },
	{ "GoToPage",	MENUITEM(SEARCH_MENU,	GOTOPAGE_ITEM,	NOSUB) },
	{ "GoToSelection",MENUITEM(SEARCH_MENU,	GOTOSELECTION_ITEM,NOSUB) },
	{ "SpellCheck",	MENUITEM(SEARCH_MENU,	SPELLING_ITEM,	CHECK_SUBITEM) },
	{ "SetMark",			MENUITEM(SEARCH_MENU, SETMARK_ITEM, NOSUB) },
	{ "GoToMark",			MENUITEM(SEARCH_MENU, GOTOMARK_ITEM, NOSUB) },
	{ "SpellChangeDict", MENUITEM(SEARCH_MENU,SPELLING_ITEM,	CHANGEDICT_SUBITEM) },
	
	{ "Collapse",	MENUITEM(HEADING_MENU,	COLLAPSE_ITEM,	NOSUB) },
	{ "Expand",		MENUITEM(HEADING_MENU,	EXPAND_ITEM,	NOSUB) },
	{ "ExpandAll",	MENUITEM(HEADING_MENU,	EXPANDALL_ITEM,	NOSUB) },
	{ "Indent",		MENUITEM(HEADING_MENU,	INDENT_ITEM,	NOSUB) },
	{ "UnIndent",	MENUITEM(HEADING_MENU,	UNINDENT_ITEM,	NOSUB) },
	{ "MoveUp",		MENUITEM(HEADING_MENU,	MOVEUP_ITEM,	NOSUB) },
	{ "MoveDown",	MENUITEM(HEADING_MENU,	MOVEDOWN_ITEM,	NOSUB) },
	{ "Split",		MENUITEM(HEADING_MENU,	SPLIT_ITEM,	NOSUB) },
	{ "Join",		MENUITEM(HEADING_MENU,	JOIN_ITEM,		NOSUB) },
	{ "SortAtoZ",	MENUITEM(HEADING_MENU,	SORT_ITEM,		ATOZ_SUBITEM) },
	{ "SortZtoA",	MENUITEM(HEADING_MENU,	SORT_ITEM,		ZTOA_SUBITEM) },
	{ "SortRandom",	MENUITEM(HEADING_MENU,	SORT_ITEM,		RANDOM_SUBITEM) },
	
	{ "StylePlain",	MENUITEM(FORMAT_MENU,	PLAIN_ITEM,	NOSUB) },
	{ "StyleBold",	MENUITEM(FORMAT_MENU,	BOLD_ITEM,		NOSUB) },
	{ "StyleItalic",	MENUITEM(FORMAT_MENU,	ITALIC_ITEM,	NOSUB) },
	{ "StyleUnderline",MENUITEM(FORMAT_MENU,	UNDERLINE_ITEM,	NOSUB) },
	{ "PageBreakBefore",MENUITEM(FORMAT_MENU,	PAGEBREAK_ITEM,	NOSUB) },
	{ "Layout",		MENUITEM(FORMAT_MENU,	LAYOUT_ITEM,	NOSUB) },
	{ "HeadingLabels",MENUITEM(FORMAT_MENU,	LABEL_ITEM,	NOSUB) },
	{ "DocumentColors", MENUITEM(FORMAT_MENU,	COLORS_ITEM,	NOSUB) },
	{ "DocumentInfo",	MENUITEM(FORMAT_MENU,	DOCUMENTINFO_ITEM,	NOSUB) },
		
	{ "About",		MENUITEM(VIEW_MENU,		ABOUT_ITEM,	NOSUB) },	
};

#define NUM_MISC_COMMANDS	(sizeof(miscCmdNames)/sizeof(TextPtr))

static TextPtr miscCmdNames[] = {
	"ProgName", "ProgVersion", "DocName",
	"Type", "ASCII",			
	"NewHead",	"SelectHead",	"NextHead",		"PrevHead",
	"HeadLevel","HasSubHead",	"GoToHead",
	"BackSpace", "Delete",
	"CursorUp", "CursorDown", "CursorLeft", "CursorRight",
	"ShiftDown", "ShiftUp", "AltDown", "AltUp", "CtrlDown", "CtrlUp",
	"Extract",
	"ScreenToFront", "ScreenToBack", "Window",
	"SetPrint", "SetFind",
	"Request1", "Request2", "Request3", "RequestText",
	"SetPath",
	"DrawOff",	"DrawOn"
};

enum {
	CMD_PROGNAME,		CMD_PROGVERSION,	CMD_DOCNAME,
	CMD_TYPE,			CMD_ASCII,		
	CMD_NEWHEAD,		CMD_SELECTHEAD,		CMD_NEXTHEAD,	CMD_PREVHEAD,
	CMD_HEADLEVEL,		CMD_HASSUBLEVEL,	CMD_GOTOLINE,
	CMD_BACKSPACE,		CMD_DELETE,
	CMD_CURSORUP,		CMD_CURSORDOWN,		CMD_CURSORLEFT,	CMD_CURSORRIGHT,
	CMD_SHIFTDOWN,		CMD_SHIFTUP,		CMD_ALTDOWN,	CMD_ALTUP,
	CMD_CTRLDOWN,		CMD_CTRLUP,
	CMD_EXTRACT,
	CMD_SCREENFRONT,	CMD_SCREENBACK,		CMD_WINDOW,
	CMD_SETPRINT,		CMD_SETFIND,
	CMD_REQUEST1,		CMD_REQUEST2,		CMD_REQUEST3,	CMD_REQUESTTEXT,
	CMD_SETPATH,
	CMD_DRAWOFF,		CMD_DRAWON
};

static UWORD	rexxModifiers;		/* Inited to 0 */

static RexxMsgPtr	macroMsg;		/* For macro commands (Inited to NULL) */

/*
 *	Local prototypes
 */

BOOL	MatchCommand(TextPtr, UWORD, TextPtr);
void	ExtractText(RexxMsgPtr, WindowPtr);

void	UserRequest(WORD, RexxMsgPtr, TextPtr);

BOOL	DoOtherMacro(void);

void	SaveFKeys(void);

BOOL	DoCustomizeMacro(WindowPtr);

/*
	Disable all drawing
*/

static void DrawOff(void)
{
	CursorOff(cmdWindow);
	drawOn = FALSE;
}

/*
	Enable all drawing
*/

static void DrawOn(void)
{
	drawOn = TRUE;
	CursorOn(cmdWindow);
	CauseUpdate(cmdWindow);
	AdjustScrollBars(cmdWindow);
}

/*
 *	Open REXX library and init REXX port
 *	If REXX port already exists, then do not init REXX interface
 */

void InitRexx()
{
	WORD portNum;

/*
	Open REXX
*/
	RexxSysBase = (struct RexxLib *) OpenLibrary("rexxsyslib.library", 0);
	if (RexxSysBase == NULL)
		return;
/*
	Find unused port name and set up port
*/
	portNum = -1;
	do {
		strcpy(progPortName, strProgName);
		portNum++;				/* Compiler bug, this must go after strcpy() */
		if (portNum > 0) {
			NumToString(portNum, strBuff);
			strcat(progPortName, ".");
			strcat(progPortName, strBuff);
		}
	} while (FindPort(progPortName));
	InitPort(&rexxMsgPort, progPortName);
	AddPort(&rexxMsgPort);
}

/*
 *	Shut down REXX port and close library
 */

void ShutDownRexx()
{
	if (RexxSysBase) {
		RemPort(&rexxMsgPort);
		FreePort(&rexxMsgPort);
		CloseLibrary(RexxSysBase);
	}
}

/*
 *	Check for a match to a command string
 *	Return TRUE if equal
 */

static BOOL MatchCommand(TextPtr cmdText, UWORD cmdLen, TextPtr text)
{
	if (CmpString(cmdText, text, cmdLen, strlen(text), FALSE) == 0)
		return (TRUE);
	return (FALSE);
}

/*
 *	Extract selected text from selected paragraph and return REXX string
 */

static void ExtractText(RexxMsgPtr rexxMsg, WindowPtr window)
{
	WORD loc, len;
	HeadPtr		head;
	register DocDataPtr docData = GetWRefCon(window);

	head = docData->SelHead;
	loc = docData->SelStart;
	len = (head->Flags & HD_SELECTED) ? head->Len : docData->SelEnd;
	len -= loc;
	if (len == 0)
		rexxMsg->rm_Result1 = RC_WARN;
	rexxMsg->rm_Result2 = (LONG) CreateArgstring(head->Text + loc, len);
}

/*
 *	Handle user request dialogs
 */

static void UserRequest(WORD cmdNum, RexxMsgPtr rexxMsg, TextPtr prompt)
{
	WORD dlgNum;
	DialogPtr dlg;

	switch (cmdNum) {
	case CMD_REQUEST1:
		dlgNum = DLG_USERREQ1;
		break;
	case CMD_REQUEST2:
		dlgNum = DLG_USERREQ2;
		break;
	case CMD_REQUEST3:
		dlgNum = DLG_USERREQ3;
		break;
	case CMD_REQUESTTEXT:
		if ((rexxMsg->rm_Action & RXFF_RESULT) == 0) {
			rexxMsg->rm_Result1 = RC_FATAL;
			return;
		}
		dlgNum = DLG_USERREQTEXT;
		break;
	default:
		rexxMsg->rm_Result1 = RC_FATAL;
		return;
	}
	dlgList[dlgNum]->Gadgets[USERREQ_TEXT].Info = prompt;
	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		rexxMsg->rm_Result1 = RC_ERROR;
		return;
	}
	OutlineOKButton(dlg);
	rexxMsg->rm_Result1 = ModalDialog(mainMsgPort, dlg, DialogFilter);
	if (cmdNum == CMD_REQUESTTEXT && rexxMsg->rm_Result1 == OK_BUTTON) {
		GetEditItemText(dlg->FirstGadget, USERREQ_EDIT, strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
	}
	DisposeDialog(dlg);
	EndWait();
}

/*
 *	Handle REXX messages
 */

void DoRexxMsg(RexxMsgPtr rexxMsg)
{
	register UWORD i, cmd, cmdLen, len, menuCmd,j;
	BOOL isDocWindow, resultEnabled, success;
	LONG num;
	register TextPtr cmdText, argText;
	register TextChar ch;
	WindowPtr activeWindow;
	DocDataPtr docData;
	PrintRecPtr printRec;
	HeadPtr		head;
	Dir			dir;

	activeWindow = ActiveWindow();

/*
	Handle message reply
*/
	if (macroMsg == rexxMsg) {
		if (macroMsg->rm_Result1 != RC_OK) 
			Error(ERR_MACRO_FAIL);
		ClearRexxMsg(macroMsg, (macroMsg->rm_Action & 0xFF) + 1);
		DeleteRexxMsg(macroMsg);
		macroMsg = NULL;
		rexxModifiers = 0;
		inMacro = FALSE;
		SetPointerShape();
		SetAllMenus();
		return;
	}
/*
	Handle other messages
*/
	if (!IsRexxMsg(rexxMsg))
		return;
	rexxMsg->rm_Result1 = RC_OK;		/* Assume success */
	rexxMsg->rm_Result2 = 0;
/*
	Get the command and data portions of argument
*/
	cmdText = rexxMsg->rm_Args[0];
	while (*cmdText == ' ')
		cmdText++;
	argText = cmdText;
	while (*argText && *argText != ' ')
		argText++;
	cmdLen = (UWORD) (argText - cmdText);
	while (*argText == ' ')
		argText++;
/*
	Get window to send messages to
*/
	if (IsDocWindow(activeWindow))
		cmdWindow = activeWindow;
	else if (!IsDocWindow(cmdWindow)) {
		if (numWindows)
			cmdWindow = windowList[numWindows - 1];
		else
			cmdWindow = backWindow;
	}
	isDocWindow = IsDocWindow(cmdWindow);
	
	docData = GetWRefCon(cmdWindow);
/*
	Check for menu commands
*/
	for (cmd = 0; cmd < NUM_MENU_COMMANDS; cmd++) {
		if (MatchCommand(cmdText, cmdLen, menuCmds[cmd].Name))
			break;
	}
/*
	Handle menu commands
*/
	if (cmd < NUM_MENU_COMMANDS) {
		menuCmd = menuCmds[cmd].Num;
		if (!isDocWindow &&
			menuCmd != MENUCMD_NEW && menuCmd != MENUCMD_OPEN &&
			menuCmd != MENUCMD_QUIT && menuCmd != MENUCMD_PREFERENCES &&
			menuCmd != MENUCMD_SCREENCOLORS)
			goto Error;
		if (menuCmd == MENUCMD_NULL)
			goto Error;
/*
	Handle modified menu commands
*/
		if (*argText) {
			switch (menuCmd) {
			case MENUCMD_OPEN:
				if (!DoOpen(cmdWindow, rexxModifiers, argText))
					rexxMsg->rm_Result1 = RC_ERROR;
				goto Done;
			case MENUCMD_FIND:
				if (!DoFind(argText))
					rexxMsg->rm_Result1 = RC_ERROR;
				goto Done;
			default:
				rexxMsg->rm_Result1 = RC_WARN;		/* Invalid option for command */
				break;
			}
		}
		if (!DoMenu(cmdWindow, menuCmd, rexxModifiers, FALSE))
			rexxMsg->rm_Result1 = RC_ERROR;
		goto Done;
	}
/*
	Check for non-menu commands
*/
	for (i = 0; i < NUM_MISC_COMMANDS; i++) {
		if (MatchCommand(cmdText, cmdLen, miscCmdNames[i]))
			break;
	}
	if (i >= NUM_MISC_COMMANDS)
		goto Error;
/*
	Handle non-menu commands
*/
	resultEnabled = ((rexxMsg->rm_Action & RXFF_RESULT) != 0);
	switch (i) {
	case CMD_PROGNAME:
		if (!resultEnabled)
			goto Error;
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strProgName, strlen(strProgName));
		break;
	case CMD_PROGVERSION:
		if (!resultEnabled)
			goto Error;
		NumToString((FLOW_VERSION >> 8) & 0xFF, strBuff);
		strcat(strBuff, ".");
		NumToString((FLOW_VERSION >> 4) & 0x0F, strBuff + strlen(strBuff));
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_DOCNAME:
		if (!resultEnabled || !isDocWindow)
			goto Error;
		GetWTitle(cmdWindow, strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_TYPE:
		if (!isDocWindow)
			goto Error;
		for (i = 0; i < strlen(argText); i++) {
			ch = argText[i];
			if ((ch & 0x7F) >= 0x20 && (ch & 0x7F) <= 0x7F && ch != 0x7F)
				AddChar(cmdWindow, ch);
			else
				rexxMsg->rm_Result1 = RC_WARN;	/* Invalid character */
		}
		break;
	case CMD_ASCII:
		if (!isDocWindow)
			goto Error;
		num = StringToNum(argText);
		if (num < 0xFF &&
			((num & 0x7F) >= 0x20 || num == TAB || num == CR) && num != 0x7F)
			AddChar(cmdWindow, (TextChar) num);
		else
			rexxMsg->rm_Result1 = RC_WARN;	/* Invalid character */
		break;
	case CMD_NEWHEAD:
		if( !isDocWindow)
			goto Error;
		if( !NewHead(cmdWindow, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_SELECTHEAD:
		if (!isDocWindow)
			goto Error;
		DoSelectHead(cmdWindow);
		break;
	case CMD_NEXTHEAD:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorHeading(cmdWindow,CURSORDOWN, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_PREVHEAD:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorHeading(cmdWindow,CURSORUP, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_HEADLEVEL:
		if (!resultEnabled || !isDocWindow)
			goto Error;
		docData = GetWRefCon(cmdWindow);
		NumToString(HeadDepth(docData->SelHead),strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_HASSUBLEVEL:
		if (!isDocWindow)
			goto Error;
		if (docData->SelHead->Sub == NULL)
			NumToString(0,strBuff);
		else
			NumToString(1,strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_GOTOLINE:
		if (!isDocWindow)
			goto Error;
		CursorOff(cmdWindow);
		num = StringToNum(argText)-1;
		head = docData->FirstHead;
		for (j=0; j<num; j++)
			head = NextHeadAll(head);
		docData->SelHead = head;
		docData->SelStart = docData->SelEnd = docData->SelHead->BodyStart;	
		RevealHead(docData);
		break;
	case CMD_BACKSPACE:
		if (!isDocWindow)
			goto Error;
		DoDelete(cmdWindow, BS, rexxModifiers);
		break;
	case CMD_DELETE:
		if (!isDocWindow)
			goto Error;
		DoDelete(cmdWindow, DEL, rexxModifiers);
		break;
	case CMD_CURSORUP:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORUP, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORDOWN:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORDOWN, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORLEFT:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORLEFT, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORRIGHT:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORRIGHT, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_SHIFTDOWN:
		rexxModifiers |= SHIFTKEYS;
		break;
	case CMD_SHIFTUP:
		rexxModifiers &= ~SHIFTKEYS;
		break;
	case CMD_ALTDOWN:
		rexxModifiers |= ALTKEYS;
		break;
	case CMD_ALTUP:
		rexxModifiers &= ~ALTKEYS;
		break;
	case CMD_CTRLDOWN:
		rexxModifiers |= IEQUALIFIER_CONTROL;
		break;
	case CMD_CTRLUP:
		rexxModifiers &= ~IEQUALIFIER_CONTROL;
		break;
	case CMD_EXTRACT:
		if (!resultEnabled || !isDocWindow)
			goto Error;
		ExtractText(rexxMsg, cmdWindow);
		break;
	case CMD_SCREENFRONT:
		ScreenToFront(screen);
		break;
	case CMD_SCREENBACK:
		ScreenToBack(screen);
		break;
	case CMD_WINDOW:
		for (i = 0; i < numWindows; i++) {
			GetWTitle(windowList[i], strBuff);
			if (CmpString(argText, strBuff, strlen(argText), strlen(strBuff), FALSE) == 0)
				break;
		}
		if (i < numWindows)
			DoViewMenu(backWindow, i + WINDOW_ITEM, NOSUB);
		else
			goto Error;
		break;
	case CMD_SETPRINT:
	case CMD_SETFIND:
		if (i == CMD_SETPRINT) {
			if (isDocWindow) {
				printRec = docData->PrintRec;
			}
			else
				printRec = NULL;
		}
		while (*argText) {
			for (len = 0; argText[len] && argText[len] != ' '; len++) ;
			if (i == CMD_SETPRINT)
				success = SetPrintOption(printRec, argText, len);
			else
				success = SetFindOption(argText, len);
			if (!success)
				rexxMsg->rm_Result1 = RC_WARN;
			argText += len;
			while (*argText == ' ')
				argText++;
		}
		break;
	case CMD_REQUEST1:
	case CMD_REQUEST2:
	case CMD_REQUEST3:
	case CMD_REQUESTTEXT:
		UserRequest(i, rexxMsg, argText);
		break;
	case CMD_SETPATH:
		if (strlen(argText) == 0 || (dir = Lock(argText, ACCESS_READ)) == NULL)
			goto Error;
		SetCurrentDir(dir);
		UnLock(dir);
		break;
	case CMD_DRAWOFF:
		DrawOff();
		break;
	case CMD_DRAWON:
		DrawOn();
		break;
	}
	goto Done;
/*
	Command not found
*/
Error:
	rexxMsg->rm_Result1 = RC_FATAL;
/*
	Reply to message
*/
Done:
	ReplyMsg((MsgPtr) rexxMsg);
}

/*
 *	Initiate AREXX macro
 *	If macroMsg is not NULL then we are already executing a macro, so ignore
 *	This routine will use a copy of macroName
 */

void DoMacro(TextPtr macroName, TextPtr arg)
{
	WORD i, len, argCount;
	BOOL success, hasPath;
	MsgPortPtr rexxPort;

	if (RexxSysBase == NULL || macroMsg)
		return;
/*
	Add path to program if no path specified
*/
	hasPath = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':') {
			hasPath = TRUE;
			break;
		}
	}
	if (hasPath)
		strcpy(strBuff, macroName);
	else
		SetPathName(strBuff, macroName, FALSE);
/*
	Find REXX port and start macro
*/
	argCount = (arg && (strlen(arg) != 0)) ? 2 : 1;
	success = FALSE;
	if ((macroMsg = (RexxMsgPtr) CreateRexxMsg(&rexxMsgPort, NULL, progPortName)) == NULL)
		goto Error;
	macroMsg->rm_Action = RXFUNC + argCount - 1;
	if (!fromCLI)
		macroMsg->rm_Action |= (1 << RXFB_NOIO);
	macroMsg->rm_Args[0] = (STRPTR) strBuff;
	macroMsg->rm_Args[1] = (STRPTR) arg;
	if (!FillRexxMsg(macroMsg, argCount, 0))
		goto Error;
	Forbid();
	rexxPort = FindPort("REXX");
	if (rexxPort)
		PutMsg(rexxPort, (MsgPtr) macroMsg);
	Permit();
	if (rexxPort) {
		inMacro = success = TRUE;
		SetPointerShape();
	}
	else
		ClearRexxMsg(macroMsg, argCount);
Error:
	if (!success) {
		if (macroMsg) {
			DeleteRexxMsg(macroMsg);
			macroMsg = NULL;
		}
		Error((rexxPort) ? ERR_NO_MEM : ERR_NO_REXX);
	}
}

/*
 *	Do auto exec macro
 *	If auto exec macro does not exist, don't report an error
 */

void DoAutoExec(void)
{
	LONG lock;

	if (RexxSysBase == NULL)
		return;
	SetPathName(strBuff, strAutoExecName, TRUE);
	if ((lock = Lock(strBuff, ACCESS_READ)) != NULL) {
		UnLock(lock);
		DoMacro(strAutoExecName, NULL);
	}
}

/*
 *	Get macro name from user and execute
 */

static BOOL DoOtherMacro(void)
{
	WORD i, len, dlgItem;
	BOOL done, hasColon, hasSlash;
	DialogPtr dlg;
	TextChar macroName[GADG_MAX_STRING], arg[GADG_MAX_STRING];

/*
	Get macro name from user
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_MACRONAME], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		dlgItem = CheckDialog(mainMsgPort, dlg, DialogFilter);
		GetEditItemText(dlg->FirstGadget, MACRONAME_TEXT, macroName);
		switch (dlgItem) {
		case -1:
			EnableGadgetItem(dlg->FirstGadget, OK_BUTTON, dlg, NULL,
							 (strlen(macroName) != 0));
			break;
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		}
	} while (!done);
	GetEditItemText(dlg->FirstGadget, MACROARG_TEXT, arg);
	DisposeDialog(dlg);
	EndWait();
	if (dlgItem == CANCEL_BUTTON)
		return (FALSE);
/*
	Check for valid name (relative path names are not allowed)
*/
	hasColon = hasSlash = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':')
			hasColon = TRUE;
		if (macroName[i] == '/')
			hasSlash = TRUE;
	}
	if (len == 0 || (hasSlash && !hasColon)) {
		Error(ERR_BAD_MACRO);
		return (FALSE);
	}
	DoMacro(macroName, arg);
	return (TRUE);
}

/*
 *	Save FKeys file
 */

static void SaveFKeys()
{
	WORD i, version;
	ULONG fileID;
	BOOL success;
	File file;
	TextChar pathName[200];

	success = FALSE;
	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	Write fileID and version
*/
	fileID = ID_FKEYSFILE;
	version = FLOW_VERSION;
	if (Write(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Write(file, &version, sizeof(WORD)) != sizeof(WORD))
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable1[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable1[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable1[i].Data, 32) != 32)
			goto Exit;
	}
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable2[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable2[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable2[i].Data, 32) != 32)
			goto Exit;
	}
	success = TRUE;
/*
	All done
*/
Exit:
	if (file)
		Close(file);
	if (success)
		SaveIcon(pathName, ICON_FKEYS);
}

/*
 *	Load FKeys file
 */

void LoadFKeys(void)
{
	WORD i, version;
	ULONG fileID;
	BOOL success;
	File file;
	FKey fKey;
	TextChar macroName[32], pathName[200];

	success = FALSE;
	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_OLDFILE)) == NULL)
		goto Exit;
/*
	Read fileID and version
*/
	if (Read(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Read(file, &version, sizeof(WORD)) != sizeof(WORD) ||
		fileID != ID_FKEYSFILE || version != FLOW_VERSION)
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, 32) != 32)
			goto Exit;
		fKeyTable1[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(strMacroNames1[i], macroName, 32);
			fKeyTable1[i].Data = strMacroNames1[i];
		}
	}
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, 32) != 32)
			goto Exit;
		fKeyTable2[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(strMacroNames2[i], macroName, 32);
			fKeyTable2[i].Data = strMacroNames2[i];
		}
	}
	success = TRUE;
/*
	Set macro menu
*/
	for (i = 0; i < 10; i++)
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), strMacroNames2[i]);
/*
	All done
*/
Exit:
	if (file)
		Close(file);
}

/*
 *	Get custom macro names and insert in menu
 */

static BOOL DoCustomizeMacro(WindowPtr window)
{
	WORD i, item;
	DialogPtr dlg;
	TextChar newNames[10][32];

/*
	Get macro name from user
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_CUSTOMMACRO], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON && item != SAVE_BUTTON);
	for (i = 0; i < 10; i++) {
		GetEditItemText(dlg->FirstGadget, NAME1_TEXT + i, strBuff);
		strncpy(newNames[i], strBuff, 32);
		newNames[i][31] = '\0';
	}
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Insert macro names into menu
*/
	for (i = 0; i < 10; i++) {
		strncpy(strMacroNames2[i], newNames[i], 32);
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), strMacroNames2[i]);
	}
	if (item == SAVE_BUTTON) {
		SetStdPointer(window, POINTER_WAIT);
		SaveFKeys();
	}
	return (TRUE);
}

/*
 *	Handle macro menu
 */

BOOL DoMacroMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL success;
	DocDataPtr docData = GetWRefCon(window);

	if (RexxSysBase == NULL || inMacro)
		return (FALSE);

	if (IsDocWindow(window) && docData->DirLock) {
		SetCurrentDir(docData->DirLock);
	}

	if (item >= 0 && item <= 9) {
		DoMacro(strMacroNames2[item], NULL);
		success = TRUE;
	}
	else if (item == OTHERMACRO_ITEM)
		success = DoOtherMacro();
	else if (item == CUSTOMMACRO_ITEM)
		success = DoCustomizeMacro(window);
	else
		success = FALSE;
		
	return (success);
}
