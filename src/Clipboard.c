/*
 *	ProWrite
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Clipboard routines
 */

#include <exec/types.h>
#include <devices/clipboard.h>
#include <libraries/iffparse.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/iffparse.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>

#include <IFF/IFF.h>

#include <string.h> 

#include "Flow.h"
#include "Proto.h"
#include "HEAD.h"

#define ID_FTXT	MakeID('F','T','X','T')
#define ID_CHRS	MakeID('C','H','R','S') 
#define ID_CLIP MakeID('C','L','I','P')

extern WORD		pasteType;
extern Ptr		pasteBuff;

extern UBYTE	strBuff[];

extern Options 	options;

extern struct IFFParseBase *IFFParseBase;

struct 	ClipboardHandle *clipboard;
static struct 	IFFHandle *iff;
static WORD		ignoreDepth;
static LONG		flowprops[] = 
	{ ID_HEAD, ID_NEST, ID_HEAD, ID_HSTL, ID_HEAD, ID_HINF, ID_HEAD, ID_TEXT };
		
static LONG		lastID;

void InitClipboard()
{
	if (IFFParseBase == NULL && !options.FullClipboard) 
		return;
	if (IFFParseBase == NULL && options.FullClipboard) {
		Error(ERR_NO_PARSE);
		return;
	}
	if ((clipboard = OpenClipboard(PRIMARY_CLIP)) == NULL) {
		Error(ERR_NO_CLIP);
		return;
	}
		
	iff = AllocIFF();
	if (iff)
		iff->iff_Stream = (LONG)clipboard;
	InitIFFasClip(iff);

}

void ShutDownClipboard()
{
	if (IFFParseBase == NULL) 
		return;
		
	if (clipboard)
		CloseClipboard(clipboard);
	if (iff)
		FreeIFF(iff);
}

static HeadPtr AttachHead(HeadPtr prev,HeadPtr next,WORD depth)
{
	if (depth == 0) {			/* same level */
		if (prev)
			prev->Next = next;
		next->Prev = prev;
		return (next);
	}
	if (prev) { 
		while ((depth > 0) && prev->Sub) { 
			prev = prev->Sub;
			depth--;
		}
		if (depth == 0) {			/* right level reached */
			while (prev->Next)
				prev = prev->Next;
			prev->Next = next;
			next->Prev = prev;
			return (next);
		}			
		else {						/* level does not exist yet */
			prev->Sub = next;
			next->Super = prev;
			ignoreDepth++;
		}
		if ((prev->Flags & HD_EXPANDED) != HD_EXPANDED)
			prev->Flags |= HD_EXPANDED;
		return (next);
	}
	else 
		return (next);
}

/*
 *  alter paste buff to headPtr if text has tabs or returns in it
 */
 
static Ptr AdjustPasteBuff(TextPtr text,WORD length)
{
	WORD	i,j,last=0,depth=0,lastTab = 0;
	HeadPtr nexthead,prevhead=NULL;
	BOOL	isHeading = FALSE;
	Ptr		ptr;
	
	ClearPaste();
	ignoreDepth = 0;
	for (i=0; i < length; i++) {
		switch (text[i]) {
			case TAB :
				depth++; 
				last = i+1;
				break;
			case LF  :
				if ((nexthead = AllocHead()) == NULL) {
					text[i] = ' ';
					break;
				}
				if (!AdjustBuffer(nexthead,i-last)) {
					DisposeHead(nexthead,NULL);
					text[i] = ' ';
					break;
				}
				for (j=last; j<i;j++)
					nexthead->Text[j-last] = text[j];
				nexthead->Len = i-last;
				if (!isHeading) {
					ptr = (Ptr)nexthead;
					isHeading = TRUE;
				}
				if (lastTab > depth) {   /* backing up */
					ignoreDepth = depth;
					while (prevhead->Prev) {
						prevhead = prevhead->Prev;
					}
					prevhead = prevhead->Super;
				}
				lastTab = depth;
				prevhead = AttachHead(prevhead,nexthead,depth-ignoreDepth);
				depth = 0;
				last = i+1;
				break;
			default:
				break;
		}
	}
	
	if (!isHeading) {
		ptr = NewPtr(length);
		BlockMove(text,ptr,length);
		pasteType = PASTE_TEXT;
	}
	else {
		pasteType = PASTE_HEAD;
	}
	
	MemFree(text,length);	
	return (ptr);
}

/*
 *  write text string to clipboard
 */
 
static LONG WriteFTXT(TextPtr text)
{
	LONG	retval;
	LONG	length = strlen(text);
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);
/*
	while (text[length-1] != '\0')
		length--;
*/		
	if ((retval = OpenIFF(iff,IFFF_WRITE)) == 0) {
		if ((retval = PushChunk(iff,ID_FTXT,ID_FORM, -1L)) == 0) {
			
			/* write text */
			if (PushChunk(iff,0,ID_CHRS,length) == 0L) {
				if (WriteChunkBytes(iff,text,length) >= 0L)
					PopChunk(iff);
			}
			PopChunk(iff);
			retval = clipIO->io_ClipID;
		}
		CloseIFF(iff);
	}
			
	return (retval);		
}

/*
 *  recursive procedure to write subheading text to clipboard
 */
 
static LONG WriteSubChunks(HeadPtr head,WORD depth)
{
	TextPtr 	text;
	TextChar 	tab=TAB;
	WORD		i;
	
	while (head) {
		text = MemAlloc(head->Len+depth+1,MEMF_CLEAR);
		for (i=0;i<depth;i++)		/* insert tabs for formatting */
			text[i] = tab;
		for (i=depth;i<head->Len+depth;i++)
			text[i] = head->Text[i-depth];
		text[head->Len+depth] = '\n';
		if (WriteChunkBytes(iff,text,head->Len+depth+1) == 0)
			return (0L);
		MemFree(text,head->Len+depth+1);
		if (head->Sub && ((head->Flags & HD_EXPANDED) == HD_EXPANDED))
			WriteSubChunks(head->Sub,depth+1);
		head = head->Next;	
	}
}

/*
 *  write list of headings to clipboard 
 */
 
static LONG WriteText(HeadPtr head)
{
	LONG	retval;
	TextPtr	text;
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);
	
	if ((retval = PushChunk(iff,ID_FTXT,ID_FORM, -1L)) == 0) {
			
		/* write text */
		if ((retval = PushChunk(iff,0,ID_CHRS,-1L)) == 0L) {
			while (head) {
				text = MemAlloc(head->Len+1,MEMF_CLEAR);
				BlockMove(head->Text,text,head->Len);
				text[head->Len] = '\n';
				if (WriteChunkBytes(iff,text,head->Len+1) == 0)
					return (0L);
				MemFree(text,head->Len+1);
				if (head->Sub && ((head->Flags & HD_EXPANDED) == HD_EXPANDED))
					WriteSubChunks(head->Sub,1);
				head = head->Next;
			}
			PopChunk(iff);
		}
		PopChunk(iff);
	}
	if (options.FullClipboard)
		return (retval);	
	else
		return (clipIO->io_ClipID);
		
}

/*
 *	Put NEST chunk
 */

static LONG PutNEST(WORD nest)
{
	NestLevel nestLevel;
	LONG	retval;
	
	nestLevel.Nest = nest;
	if ((retval = PushChunk(iff, 0,ID_NEST,sizeof(NestLevel))) == 0) {
		if (WriteChunkBytes(iff,(BYTE *) &nestLevel,sizeof(NestLevel)) >= 0L) {
				PopChunk(iff);
		}
	}
	
	return (retval);
}

/*
 *	Put HSTL chunk
 */

static LONG PutHSTL(WORD style)
{
	HeadStyle headStyle;
	LONG	retval;
	
	headStyle.Style = style;
	if ((retval = PushChunk(iff, 0,ID_HSTL,sizeof(HeadStyle))) == 0) {
		if (WriteChunkBytes(iff,(BYTE *) &headStyle,sizeof(HeadStyle)) >= 0L) {
				PopChunk(iff);
		}
	}
	
	return (retval);
}

/*
 *	Put HINF chunk
 */

static LONG PutHINF(HeadInfo *headInfo)
{
	LONG	retval;
	
	if ((retval = PushChunk(iff, 0,ID_HINF,sizeof(HeadInfo))) == 0) {
		if (WriteChunkBytes(iff,(BYTE *) headInfo,sizeof(HeadInfo)) >= 0L) {
				PopChunk(iff);
		}
	}
	
	return (retval);
}

/*
 *	Put heading chunks
 */

static LONG PutHeadings(register HeadPtr head)
{
	register WORD newNest;
	register WORD start;
	HeadInfo headInfo;
	register BOOL newInfo;
			 LONG retval;
			 	
	BlockClear( (Ptr) &headInfo, sizeof(HeadInfo));	
	headInfo.Style = FS_NORMAL;
	while( head != NULL ) {
		NextBusyPointer();
		newNest = HeadDepth(head);
		newInfo = FALSE;
		if( headInfo.Nest != newNest ) {
			newInfo = TRUE;
			headInfo.Nest = newNest;
			if( (retval = PutNEST(headInfo.Nest)) != 0)
				break;
		}
		if( headInfo.Style != head->Style ) {
			newInfo = TRUE;
			headInfo.Style = head->Style;
			if( (retval = PutHSTL(headInfo.Style)) != 0)
				break;
		}
		if( ( headInfo.Flags != head->Flags ) || newInfo ) {
			headInfo.Flags = head->Flags;
			headInfo.Reserved1 = 0;
			headInfo.Reserved2 = 0;
			if( (retval = PutHINF(&headInfo)) != 0)
				break;
		}
		start = head->BodyStart;
		if ((retval = PushChunk(iff,0,ID_TEXT,-1L)) == 0L) {
			if (WriteChunkBytes(iff,&head->Text[start],head->Len-start) >= 0L)
				PopChunk(iff);
		}
		head = NextHeadAll(head);
	}
	return (retval);
}

/*
 *  write HEAD format to clipboard 
 */
 
static LONG WriteHEAD(HeadPtr head)
{
	LONG	retval;

	if ((retval = PushChunk(iff,ID_HEAD,ID_FORM, -1L)) == 0) {
		if ((retval = PutHeadings(head)) == 0)
			PopChunk(iff);
	}
			
	return (retval);	
}

/*
 *  write headings to clipboard 
 */
 
static LONG WriteHeadings(HeadPtr head)
{
	LONG	retval;
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);
		
	if ((retval = OpenIFF(iff,IFFF_WRITE)) == 0) {
		if ((retval = PushChunk(iff,ID_CLIP,ID_CAT, -1L)) == 0) {
			if ((retval = WriteHEAD(head)) == 0) /* HEAD format has top priority */
				retval = WriteText(head);
			PopChunk(iff);
			retval = clipIO->io_ClipID;
		}
		CloseIFF(iff);
	}
			
	return (retval);	
}

/*
 *	Get NEST chunk
 */

static LONG GetNEST(WORD *pNest)
{
	NestLevel nestLevel;
	UBYTE *nestPtr = ((UBYTE *)pNest)+1;
	
	if (ReadChunkBytes(iff, (BYTE *) &nestLevel, sizeof(NestLevel)) == 0)
		return (1);
	*pNest = nestLevel.Nest;
	if( *nestPtr > MAX_DEPTH )
		*nestPtr = MAX_DEPTH;
	return (0);
}

/*
 *	Get HSTL chunk
 */

static LONG GetHSTL(WORD *pStyle)
{
	HeadStyle headStyle;

	if (ReadChunkBytes(iff, (BYTE *) &headStyle, sizeof(HeadStyle)) > 0) {
		*pStyle = headStyle.Style;
		return (0);
	}
	return (1);
}

/*
 * Get HINF chunk - new for V3.0
 */
 
static LONG GetHINF(HeadInfo *pHeadInfo)
{
	HeadInfo headInfo;
	
	if( ReadChunkBytes(iff, (BYTE *) &headInfo, sizeof(HeadInfo)) > 0 ) {
		*pHeadInfo = headInfo;
		return(0);
	}
	return (1);
}

/*
 *	Make new heading at given depth with given text
 *	Return pointer to heading or NULL
 */

static HeadPtr MakeHead(HeadPtr prevHead, HeadInfo *headInfo, TextPtr text, WORD len)
{
	register WORD depth;
	register HeadPtr head;
/*
	Create heading and attach text
*/
	NextBusyPointer();
	if ((head = AllocHead()) == NULL)
		return (NULL);
	if (!AdjustBuffer(head, len)) {
		DisposeHead(head,NULL);
		return (NULL);
	}
	if (text && len) {
		CopyMem(text, head->Text, len);
		DisposePtr(text);
	}
	head->Len = len;
/*
	Link heading at proper level
*/
	if (prevHead) {
		depth = HeadDepth(prevHead);
		if (headInfo->Nest > depth) {
			prevHead->Sub = head;
			head->Super = prevHead;
		}
		else {
			while (depth && depth-- > headInfo->Nest)
				prevHead = prevHead->Super;
			prevHead->Next = head;
			head->Prev = prevHead;
			head->Super = prevHead->Super;
		}
	}
	head->Num = (head->Prev != NULL) ? ((head->Prev->Num)+1) : 1;
	head->Style = headInfo->Style;
	head->Flags = headInfo->Flags;
	return (head);
}

/*
 *	Get TEXT chunk
 *	Create new heading after pHead (at current nesting) and attach text to it
 *	Return pointer to new heading in pHead
 */

static LONG GetTEXT(HeadPtr *pHead, HeadInfo *headInfo,LONG len)
{
	register TextPtr text;

	if (len == 0)
		text = NULL;
	else if ((text = NewPtr(len)) == NULL)
		return (1);
	else if (ReadChunkBytes(iff, text, len) == 0) {
		DisposePtr(text);
		return (1);
	}
	if ((*pHead = MakeHead(*pHead, headInfo, text, len)) == NULL) {
		if (text)
			DisposePtr(text);
		return (1);
	}
	return (0);
}

/*
 *	check current clipboard ID to see if flow ID or if clipboard is empty
 */
 
LONG CurrentClipID()
{
	struct IOClipReq *clipIO;

	if ((IFFParseBase == NULL) || (clipboard == NULL))
		return (0);
			
	clipIO = &(clipboard->cbh_Req);
	clipIO->io_Command = CBD_CURRENTREADID;

	DoIO((IOReqPtr)clipIO);
	
	return (clipIO->io_ClipID);
}

/*
 *  if last clip to clipboard was not from this Flow the get FTXT format
 */
 
void GetClipboard(WindowPtr window)
{
	LONG 	result;
	LONG	length=0L;
	TextPtr	buff=NULL;
	HeadPtr head=NULL,prevHead;
	WORD	nest=0,
			style=FS_NORMAL;
	HeadInfo	headInfo;
	struct 	ContextNode *context;
	
	if (IFFParseBase == NULL || clipboard == NULL) 
		return;
	
	if (CurrentClipID() == lastID)
		goto Exit;
	
	SetBusyPointer(window);
	BlockClear( (Ptr) &headInfo, sizeof(HeadInfo));
	if (iff) {
		StopChunk(iff,ID_FTXT,ID_CHRS);
		StopChunks(iff,flowprops,4);
		if (OpenIFF(iff,IFFF_READ) == 0) {
			do {
				result = ParseIFF(iff,IFFPARSE_SCAN);
				if (result == IFFERR_EOF || result == IFFERR_NOTIFF) {
					CloseIFF(iff);
					goto Exit;
				}
				if ((context = CurrentChunk(iff)) && ((length = context->cn_Size) > 0L || (context->cn_ID == ID_TEXT))) {
					if (context->cn_Type == ID_HEAD) {
						switch (context->cn_ID) {
							case ID_NEST :
								GetNEST(&nest);
								headInfo.Nest = nest;
								break;
							case ID_HSTL :
								GetHSTL(&style);
								headInfo.Style = style;
								break;
							case ID_HINF :
								GetHINF(&headInfo);
								break;
							case ID_TEXT :
								prevHead = head;
								if ((GetTEXT(&head,&headInfo,length) == 0) && prevHead == NULL)
									pasteBuff = (Ptr) head;
								break;
						}
					}	
					else {
						buff = MemAlloc(length,MEMF_CLEAR);
						ReadChunkBytes(iff,buff,length);
						CloseIFF(iff);
						goto Exit;
					}
				}
			} while (TRUE);
		}
	}
Exit:
	if (head) {
		pasteType = PASTE_HEAD;
	}
	else if (buff) {
		pasteBuff = AdjustPasteBuff(buff,length);
	}
	
}

/*
 *  write pasteBuff to clipboard in FTXT format
 */
 
void PutClipboard()
{
	if (IFFParseBase == NULL && !options.FullClipboard) 
		return;
	if (IFFParseBase == NULL && options.FullClipboard) {
		if ((IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4)) == NULL) {
			Error(ERR_NO_PARSE);
			return;
		}
	}
	if (clipboard == NULL && options.FullClipboard) {
		InitClipboard(); 
	}
	if (clipboard == NULL)
		return;
	
	switch (pasteType) {
		case PASTE_NONE:
			break;
		case PASTE_HEAD:
			if (options.FullClipboard)
				lastID = WriteHeadings(pasteBuff);
			else {
				if (OpenIFF(iff,IFFF_WRITE) == 0) {
					lastID = WriteText(pasteBuff);
					CloseIFF(iff);
				}
			}
			break;
		case PASTE_TEXT:
			lastID = WriteFTXT(pasteBuff);
			break;		
	}		
}
