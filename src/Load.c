/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	File load routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>
#include <workbench/workbench.h>		/* Don't load .info files! */
#include <workbench/startup.h>

#include <Toolbox/Gadget.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <IFF/GIO.h>
#include <IFF/IFF.h>

#include <Toolbox/StdFile.h>
#include <Toolbox/Screen.h>

#include <string.h>

#include "Flow.h"
#include "HEAD.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;
extern UBYTE	fileBuff[];
extern LONG		iffError;

extern UWORD	levelArray[];

/*
 *	Local variables and definitions
 */

#define FILETYPE_HEAD		0
#define FILETYPE_TEXT		1
#define FILETYPE_BAD			3

#define ID_BINARY	 (0x000003F3L)

typedef struct {
	ClientFrame	ClientFrame;
	HeadPtr		FirstHead;
	TextPtr		Header, Footer;
	WORD			IndentSpace;
	FlowPageInfo PageInfo;
	DocDataPtr	DocData;
	TextPtr		CustomLabel;
	UBYTE			LabelFlags, LabelStyle;
} HEADFrame;

/*
 *	Local prototypes
 */

void	InitDocData(DocDataPtr);
void	SetupDocData(DocDataPtr);

void  CalcPageWidthHeight(DocDataPtr, PrintRecPtr);

HeadPtr	MakeHead(DocDataPtr,HeadPtr, HeadInfo *, TextPtr, WORD);

IFFP	GetPAGE(GroupContext *, HEADFrame *);
IFFP	GetOPTS(GroupContext *, HEADFrame *);
IFFP	GetTEXT(GroupContext *, DocDataPtr,HeadPtr *, HeadInfo *);
IFFP	GetNEST(GroupContext *, WORD *);
IFFP	GetHSTL(GroupContext *, WORD *);
IFFP	GetHEDR(GroupContext *, TextPtr *);
IFFP	GetHINF(GroupContext *, HeadInfo *);
IFFP	GetFormHEAD(GroupContext *);

/*
 * Calculate page width and height in characters
 */

static void CalcPageWidthHeight(DocDataPtr docData, PrintRecPtr printRec)
{
	docData->PageWidth = (((long)DOTS_TO_DECIPOINTS(printRec->PageRect.MaxX - printRec->PageRect.MinX, printRec->xDPI))
									* docData->CPI) / 720 ;
	docData->PageHeight = ((((long)DOTS_TO_DECIPOINTS(printRec->PageRect.MaxY - printRec->PageRect.MinY, printRec->yDPI))
									* docData->LPI) / 720)
									- ((strlen(docData->Header) ? 2 : 0) +
										(strlen(docData->Footer) ? 2 : 0)) ;
}

/*
 *	Initialize DocData structure to default values
 *	These values could be overriden by loaded values
 */

static void InitDocData(DocDataPtr docData)
{
	register WORD i;
	
	*(docData->PrintRec) = defaults.PrintRec;
	docData->Indent = defaults.Indent;
	docData->Header[0] = docData->Footer[0] = '\0';
	docData->CPI = defaults.CPI;
	docData->LeftMargin = defaults.LeftMargin;
	docData->RightMargin = defaults.RightMargin;
			
	docData->LPI = (defaults.PrintRec.PrintSpacing == EIGHT_LPI) ? 8 : 6; 
	CalcPageWidthHeight(docData, &defaults.PrintRec);
	docData->StartPage = 1;
	for( i = 0 ; i < GADG_MAX_STRING / 4 ; i++ ) {
		levelArray[i] = 1;
	}
	docData->LabelRepeat = defaults.LabelRepeat;
	docData->LabelRange = defaults.LabelRange;
	docData->LabelStyle = defaults.LabelStyle;
	docData->LabelFlags = defaults.LabelFlags;
	for (i=0;i<MAX_LABEL_LEN+1;i++)
		docData->CustomLabel[i] = defaults.CustomLabel[i];
		
	docData->TimeStyle = defaults.TimeStyle;
	docData->DateStyle = defaults.DateStyle;
	docData->PageNumStyle = defaults.PageNumStyle;
	docData->PenColor = defaults.PenColor;
	docData->PaperColor = defaults.PaperColor;
}


/*
 *	Setup Page variables for given page size and orientation
 */

void SetPageParams(docData)
register DocDataPtr docData;
{
	register PrintRecPtr printRec = docData->PrintRec;
	
	PrValidate(printRec);

	CalcPageWidthHeight(docData, printRec);
	docData->MaxXDots = printRec->PaperRect.MaxX - printRec->PaperRect.MinX;
	docData->MaxYDots = printRec->PaperRect.MaxY - printRec->PaperRect.MinY;

/*
	If invalid parameters then get default parameters
	This should never happen!
*/
	if (docData->CPI == 0 || docData->LPI == 0 || docData->MaxXDots == 0 || docData->MaxYDots == 0 ) {
		*printRec = defaults.PrintRec;
		PrValidate(printRec);
		CalcPageWidthHeight(docData, printRec);
		docData->MaxXDots = printRec->PaperRect.MaxX - printRec->PaperRect.MinX;
		docData->MaxYDots = printRec->PaperRect.MaxY - printRec->PaperRect.MinY;
	}
}

/*
 *	Setup DocData to begin editing
 */

static void SetupDocData(DocDataPtr docData)
{
	docData->SelHead = docData->FirstHead;
	docData->SelStart = docData->SelEnd = docData->DragStart = docData->SelHead->BodyStart;
	docData->TopLine = 0;
	docData->LeftOffset = 0;
	docData->PageOffset = 0;
	docData->Mark.Head = NULL;
	CountLines(docData);
}


/*
 *	Allocate structures for new outline
 *	Return success status
 */

BOOL NewOutline(DocDataPtr docData)
{
	if( (docData->FirstHead = AllocHead()) != NULL ) {
		InitDocData(docData);
		SetupDocData(docData);
		ReformatOutline(docData);
		docData->FirstHead->Num = levelArray[0];
		(void) CreateLabel(docData, docData->FirstHead);
		docData->SelStart = docData->SelEnd = docData->DragStart = docData->SelHead->BodyStart;
	}
	return( docData->FirstHead != NULL ) ;
}

/*
 * Dispose of all document data
 */

void DisposeAll(DocDataPtr docData)
{
	if( docData->FirstHead ) {
		DisposeAllHeads(docData->FirstHead,docData);
		docData->FirstHead = NULL;
	}
}


/*
 *	Make new heading at given depth with given text
 *	Return pointer to heading or NULL
 */

static HeadPtr MakeHead(DocDataPtr docData,HeadPtr prevHead, HeadInfo *headInfo, TextPtr text, WORD len)
{
	register WORD depth;
	register HeadPtr head;
/*
	Create heading and attach text
*/
	NextBusyPointer();
	if ((head = AllocHead()) == NULL)
		return (NULL);
	if (!AdjustBuffer(head, len)) {
		DisposeHead(head,docData);
		return (NULL);
	}
	if (text && len) {
		CopyMem(text, head->Text, len);
		DisposePtr(text);
	}
	head->Len = len;
/*
	Link heading at proper level
*/
	if (prevHead) {
		depth = HeadDepth(prevHead);
		if (headInfo->Nest > depth) {
			prevHead->Sub = head;
			head->Super = prevHead;
		}
		else {
			while (depth && depth-- > headInfo->Nest)
				prevHead = prevHead->Super;
			prevHead->Next = head;
			head->Prev = prevHead;
			head->Super = prevHead->Super;
		}
	}
	head->Num = (head->Prev != NULL) ? ((head->Prev->Num)+1) : 1;
	head->Style = headInfo->Style;
	head->Flags = headInfo->Flags;
	return (head);
}

/*
 *	Get PAGE chunk
 */

static IFFP GetPAGE(GroupContext *context, HEADFrame *headFrame)
{
	register IFFP iffp;
	register FlowPageInfo *pageInfo = &headFrame->PageInfo;
	register UBYTE result;
	register DocDataPtr docData = headFrame->DocData;
	register WORD width, height, cpi, lpi;
	
	iffp = IFFReadBytes(context, (BYTE *) pageInfo, sizeof(FlowPageInfo));
	if (iffp == IFF_OKAY) {
		if (pageInfo->LeftMargin >= pageInfo->PageWidth - pageInfo->RightMargin)
			pageInfo->LeftMargin = pageInfo->RightMargin = 0;
		
		cpi = pageInfo->PitchCPI;
		lpi = pageInfo->SpacingLPI;
		if( cpi == 17 )
			result = PRT_CONDENSED; 
		else
			result = ( cpi == 12 ) ? PRT_ELITE : PRT_PICA;	
		
		docData->PrintRec->PrintPitch = result;
		docData->PrintRec->PrintSpacing = lpi == 6 ? PRT_SIXLPI : PRT_EIGHTLPI;
		result = PRT_CUSTOM;
		width = pageInfo->PageWidth;
		height = pageInfo->PageHeight;
		if( width == cpi << 3 ) {
			if( height == ( 11 * lpi ) )
				result = PRT_USLETTER;
			else if( height == ( 14 * lpi ) )
				result = PRT_USLEGAL;
		} else {
			if( ( width == (136*cpi)/10 ) &&
				( height == ( 11 * lpi ) ) )
				result = PRT_WIDECARRIAGE;
			else if( ( width == (776*cpi)/100 ) &&
				( height == (1169*lpi)/100 ) )
				result = PRT_A4LETTER;
		}			
		docData->PrintRec->PageSize = result;
		docData->PrintRec->PaperWidth = (((long)width) * 720 / cpi) + 360;
		docData->PrintRec->PaperHeight = ((long)height) * 720 / lpi;
		PrValidate(docData->PrintRec);
	}	
	return(iffp);
}

/*
 *	Get OPTS chunk
 */

static IFFP GetOPTS(GroupContext *context, HEADFrame *headFrame)
{
	register IFFP iffp;
	FileOptions opts;

	iffp = IFFReadBytes(context, (BYTE *) &opts, sizeof(FileOptions));
	if (iffp == IFF_OKAY) {
		if ((headFrame->IndentSpace = opts.IndentSpace) > 5)
			headFrame->IndentSpace = 5;
		if ((headFrame->LabelStyle = opts.LabelStyle) > 4)
			headFrame->LabelStyle = 4;
		headFrame->LabelFlags = opts.LabelFlags ;
	}
	return (iffp);
}

/*
 *	Get DOC chunk - new for V3.0
 */

static IFFP GetDOC(GroupContext *context,register DocDataPtr docData)
{
	register IFFP iffp;
	DocHdr docHdr;

	if ((iffp = IFFReadBytes(context, (BYTE *) &docHdr, sizeof(DocHdr))) != IFF_OKAY)
		return (iffp);
	docData->StartPage = docHdr.StartPage;
	if (docHdr.DateStyle >= DATESTYLE_SHORT && docHdr.DateStyle <= DATESTYLE_MILITARY)
		docData->DateStyle = docHdr.DateStyle;
	if (docHdr.TimeStyle >= TIMESTYLE_12HR && docHdr.TimeStyle <= TIMESTYLE_24HR)
		docData->TimeStyle = docHdr.TimeStyle;
	if (docHdr.PageNumStyle >= PAGESTYLE_1 && docHdr.PageNumStyle <= PAGESTYLE_a)
		docData->PageNumStyle = docHdr.PageNumStyle;
	return (IFF_OKAY);
}
		
/*
 *	Get TEXT chunk
 *	Create new heading after pHead (at current nesting) and attach text to it
 *	Return pointer to new heading in pHead
 */

static IFFP GetTEXT(GroupContext *context, DocDataPtr docData,HeadPtr *pHead, HeadInfo *headInfo)
{
	register IFFP iffp = IFF_OKAY;
	register TextPtr text;
	register WORD len;

	if ((len = context->ckHdr.ckSize) == 0)
		text = NULL;
	else if ((text = NewPtr(len)) == NULL)
		return (CLIENT_ERROR);
	else if ((iffp = IFFReadBytes(context, text, len)) != IFF_OKAY) {
		DisposePtr(text);
		return (iffp);
	}
	if ((*pHead = MakeHead(docData,*pHead, headInfo, text, len)) == NULL) {
		if (text)
			DisposePtr(text);
		iffp = CLIENT_ERROR;
	}
	return (iffp);
}

/*
 *	Get NEST chunk
 */

static IFFP GetNEST(GroupContext *context, WORD *pNest)
{
	register IFFP iffp;
	NestLevel nestLevel;
	UBYTE *nestPtr = ((UBYTE *)pNest)+1;
	
	if ((iffp = IFFReadBytes(context, (BYTE *) &nestLevel, sizeof(NestLevel)))
		!= IFF_OKAY)
		return (iffp);
	*pNest = nestLevel.Nest;
	if( *nestPtr > MAX_DEPTH )
		*nestPtr = MAX_DEPTH;
	return (IFF_OKAY);
}

/*
 *	Get HSTL chunk
 */

static IFFP GetHSTL(GroupContext *context, WORD *pStyle)
{
	register IFFP iffp;
	HeadStyle headStyle;

	if ((iffp = IFFReadBytes(context, (BYTE *) &headStyle, sizeof(HeadStyle))) == IFF_OKAY)
		*pStyle = headStyle.Style;
	return (iffp);
}

/*
 * Get HINF chunk - new for V3.0
 */
 
static IFFP GetHINF(GroupContext *context, HeadInfo *pHeadInfo)
{
	register IFFP iffp;
	HeadInfo headInfo;
	
	if( (iffp = IFFReadBytes(context, (BYTE *) &headInfo, sizeof(HeadInfo))) == IFF_OKAY )
		*pHeadInfo = headInfo;
	return(iffp);
}

/*
 *	Get PREC chunk
 */

static IFFP GetPREC(GroupContext *context, DocDataPtr docData)
{
	register IFFP iffp;
	PrintRecInfo printRecInfo;

	iffp = IFFReadBytes(context, (BYTE *) &printRecInfo, (LONG) sizeof(PrintRecInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	BlockMove(&printRecInfo.PrintRec, docData->PrintRec, sizeof(PrintRecord));
	SetPageParams(docData);
	return (IFF_OKAY);
}

/*
 *	Get HEDR/FOTR chunk
 */

static IFFP GetHEDR(GroupContext *context, TextPtr *pHeader)
{
	register IFFP iffp;
	register TextPtr text;
	register WORD len;

	if ((len = context->ckHdr.ckSize) == 0) {
		*pHeader = NULL;
		iffp = IFF_OKAY;
	}
	else if ((text = NewPtr(len + 1)) == NULL)
		iffp = CLIENT_ERROR;
	else if ((iffp = IFFReadBytes(context, text, len)) != IFF_OKAY)
		DisposePtr(text);
	else {
		text[len] = '\0';
		*pHeader = text;
	}
	return (iffp);
}

/*
 *	Get contents of IFF HEAD form
 */

static IFFP GetFormHEAD(GroupContext *parent)
{
	register IFFP iffp;
	WORD nest, style;				/* Need to take address of these */
	HeadPtr head;					/* Need to take address of this */
	register HeadPtr prevHead;
	HEADFrame *headFrame;
	GroupContext formContext;
	DocDataPtr docData;
	HeadInfo headInfo;
	
	if (parent->subtype != ID_HEAD)
		return (IFF_OKAY);
	headFrame = (HEADFrame *) parent->clientFrame;
	if ((iffp = OpenRGroup(parent, &formContext)) != IFF_OKAY)
		return (iffp);
	docData = headFrame->DocData;
	head = NULL;
	nest = 0;
	style = FS_NORMAL;
	BlockClear( (Ptr) &headInfo, sizeof(HeadInfo));
	
	do {
		switch (iffp = GetFChunkHdr(&formContext)) {
/*
	Get PAGE chunk
*/
		case ID_PAGE:
			iffp = GetPAGE(&formContext, headFrame);
			break;
/*
	Get OPTS chunk
*/
		case ID_OPTS:
			iffp = GetOPTS(&formContext, headFrame);
			break;
			
/*
	Get new DOC chunk as well
*/
		case ID_DOC:
			iffp = GetDOC(&formContext, docData);
			break;
			
/*
	Get NEST chunk
*/
		case ID_NEST:
			iffp = GetNEST(&formContext, &nest);
			headInfo.Nest = nest;
			break;
/*
	Get HINF chunk
*/
		case ID_HINF:
			iffp = GetHINF(&formContext, &headInfo);
			break;
/*
	Get TEXT chunk
*/
		case ID_TEXT:
			prevHead = head;
			iffp = GetTEXT(&formContext, docData,&head, &headInfo);
			if (iffp == IFF_OKAY && prevHead == NULL) {
				headFrame->FirstHead = head;
			}
			break;
/*
	Get HSTL chunk
*/
		case ID_HSTL:
			iffp = GetHSTL(&formContext, &style);
			headInfo.Style = style;
			break;
/*
	Get HEDR chunk
*/
		case ID_HEDR:
			if (headFrame->Header)
				iffp = CLIENT_ERROR;
			else
				iffp = GetHEDR(&formContext, &headFrame->Header);
			break;
/*
	Get FOTR chunk
*/
		case ID_FOTR:
			if (headFrame->Footer)
				iffp = CLIENT_ERROR;
			else
				iffp = GetHEDR(&formContext, &headFrame->Footer);
			break;
/*
	Get PREC chunk
*/
		case ID_PREC:
			iffp = (formContext.ckHdr.ckSize != sizeof(PrintRecInfo)) ?
					BAD_FORM : GetPREC(&formContext, docData);
			break;
/*
	Get CLAB chunk
*/
		case ID_CLAB:
			if( headFrame->CustomLabel)
				iffp = CLIENT_ERROR;
			else
				iffp = GetHEDR(&formContext, &headFrame->CustomLabel);
			break;	
/*
	End of file
*/
		case END_MARK:
			iffp = IFF_DONE;
			break;
		}
	} while (iffp >= IFF_OKAY);
	if (iffp != IFF_DONE)
		return (iffp);
	CloseRGroup(&formContext);
	return (iffp);
}

/*
 *	Load HEAD file
 *	Return success status
 */

BOOL LoadHEADFile(DocDataPtr docData, File file)
{
	HEADFrame headFrame;

	InitDocData(docData);
/*
	Initialize wordFrame and read file
*/
	headFrame.ClientFrame.getList = headFrame.ClientFrame.getProp = SkipGroup;
	headFrame.ClientFrame.getForm = GetFormHEAD;
	headFrame.ClientFrame.getCat = ReadICat;
	headFrame.FirstHead = NULL;
	headFrame.Header = headFrame.Footer = headFrame.CustomLabel = NULL;
	headFrame.IndentSpace = docData->Indent;
	headFrame.PageInfo.PageWidth = docData->PageWidth ;
	headFrame.PageInfo.PageHeight = docData->PageHeight ;
	headFrame.PageInfo.PitchCPI = docData->CPI;
	headFrame.PageInfo.SpacingLPI = docData->LPI;
	headFrame.PageInfo.LeftMargin = docData->LeftMargin;
	headFrame.PageInfo.RightMargin = docData->RightMargin;
	headFrame.DocData = docData;
	iffError = ReadIFF(file, (ClientFrame *) &headFrame);
/*
	If error then dispose of headings and return
*/
	if (iffError != IFF_DONE)
		return (FALSE);
	if (headFrame.FirstHead == NULL &&
		(headFrame.FirstHead = AllocHead()) == NULL) {
		iffError = CLIENT_ERROR;
		return (FALSE);
	}
/*
	Set up window data
*/
	DisposeHead(docData->FirstHead,docData);
	docData->FirstHead = headFrame.FirstHead;
	if( headFrame.Header != NULL ) {
		strncpy(docData->Header, headFrame.Header, MAX_HEADER_LEN);
		docData->Header[MAX_HEADER_LEN] = '\0';
		DisposePtr(headFrame.Header);
	}
	if( headFrame.Footer != NULL ) {
		strncpy(docData->Footer, headFrame.Footer, MAX_FOOTER_LEN);
		docData->Footer[MAX_FOOTER_LEN] = '\0';
		DisposePtr(headFrame.Footer);
	}
	if( headFrame.CustomLabel != NULL ) {
		strncpy(docData->CustomLabel, headFrame.CustomLabel, MAX_LABEL_LEN);
		docData->CustomLabel[MAX_LABEL_LEN] = '\0';
		DisposePtr(headFrame.CustomLabel);
	}
	docData->Indent = headFrame.IndentSpace;
	docData->PageWidth = headFrame.PageInfo.PageWidth;
	docData->PageHeight = headFrame.PageInfo.PageHeight;
	docData->CPI = headFrame.PageInfo.PitchCPI;
	docData->LPI = headFrame.PageInfo.SpacingLPI;
	docData->LeftMargin = headFrame.PageInfo.LeftMargin;
	docData->RightMargin = headFrame.PageInfo.RightMargin;
	docData->LabelFlags = headFrame.LabelFlags;
	docData->LabelStyle = headFrame.LabelStyle;
	return (TRUE);
}

/*
 *	Load text file
 *	Return success status
 */

BOOL LoadTextFile(DocDataPtr docData, File file)
{
	register WORD i, start, buffLen;
	register BOOL success, needHead;
	register HeadPtr head;
	HeadInfo headInfo;

	InitDocData(docData);
/*
	Read until end-of-file
	Count tags, discard all control characters, treat CR the same as LF
*/
	success = TRUE;
	head = docData->FirstHead;
	needHead = FALSE;
	headInfo.Nest = 0;
	headInfo.Style = FS_NORMAL;
	headInfo.Flags = 0;
	while (success && (buffLen = Read(file, fileBuff, FILEBUFF_SIZE)) > 0) {
		start = 0;
		for (i = 0; i < buffLen; i++) {
			if (fileBuff[i] == '\t') {
				if (needHead && start == i) {
					headInfo.Nest++;
					start++;
				}
				else
					fileBuff[i] = ' ';
			}
			else if ((fileBuff[i] & 0x7F) < ' ') {
				if (needHead) {
					if ((head = MakeHead(docData,head, &headInfo, NULL, 0))
						== NULL) {
						success = FALSE;
						break;
					}
					needHead = FALSE;
				}
				if (i > start && !AddText(head, &fileBuff[start], (WORD) (i - start))) {
					success = FALSE;
					break;
				}
				if (fileBuff[i] == '\n' || fileBuff[i] == '\r') {
					needHead = TRUE;
					headInfo.Nest = 0;
				}
				start = i + 1;
			}
		}
		if (i > start) {
			if (needHead) {
				if ((head = MakeHead(docData,head, &headInfo, NULL, 0)) == NULL)
					success = FALSE;
				needHead = FALSE;
			}
			if (!AddText(head, &fileBuff[start], (WORD) (i - start)))
				success = FALSE;
		}
	}
	if( success )
		iffError = IFF_DONE;
	else {
		iffError = CLIENT_ERROR;
		DisposeAll(docData);
	}
	return (success);
}

/*
 *	Determine what type of file to load and load it
 *	Set NAMED flag if file is HEAD type
 *	Return success status
 */

BOOL LoadFile(WindowPtr window, DocDataPtr docData, TextPtr fileName, Dir dir)
{
	register WORD fileType, len;
	register BOOL success = FALSE;
	register File file;
	GroupHeader grpHeader;
	
	iffError = CLIENT_ERROR;
	if( NewOutline(docData) ) {
		iffError = DOS_ERROR;
/*
	Open the file and determine what type of file it is
*/
		SetBusyPointer(window);
		SetCurrentDir(dir);
		if ((file = GOpen(fileName, MODE_OLDFILE)) != NULL) {
			len = GRead(file, (BYTE *) &grpHeader, sizeof(GroupHeader));
			if( len == sizeof(GroupHeader)  && grpHeader.ckID == FORM ) {
				if( grpHeader.grpSubID == ID_HEAD ) 
					fileType = FILETYPE_HEAD;
				else
					fileType = FILETYPE_BAD;
			} else if( len >= 4 && ( ( (((ULONG)grpHeader.ckID) >> 16) == WB_DISKMAGIC ) ||
				grpHeader.ckID == ID_BINARY))
				fileType = FILETYPE_BAD;
			else 
				fileType = FILETYPE_TEXT;
/*
	Load the document
*/
			/* if( fileType == FILETYPE_TEXT)
				GetTextType(window, TRUE) ;
			*/
			BeginWait();
			if( fileType != FILETYPE_BAD && ( GSeek(file, 0, OFFSET_BEGINNING) >= 0 ) ) {
				if (fileType == FILETYPE_HEAD) {
					docData->Flags |= WD_NAMED;
					docData->DirLock = dir;
					success = LoadHEADFile(docData, file);
				} else {								/* fileType == FILETYPE_TEXT */
					success = LoadTextFile(docData, file);
					UnLock(dir);
					docData->DirLock = dir = NULL;
				}
				if( success ) {
/*
	Tell BumpLevel() to traverse entire tree constructing labels
*/
					docData->LabelFlags |= LABEL_CHANGED_MASK;
					RenumberHeads(docData);
					BumpLevel(docData, docData->FirstHead, 0);
				}
			}
			GClose(file);
			if( success ) {
				strcpy(docData->FileName, fileName);
				SetupDocData(docData);
				ReformatOutline(docData);
			} else {
				DisposeAll(docData);
				if( dir )
					UnLock(dir);
				docData->DirLock = NULL;	/* In case this was Revert operation */
			}
			docData->LabelFlags &= (~LABEL_CHANGED_MASK);
			EndWait();
		}
	}	
	return(success) ;
}
