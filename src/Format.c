/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Style menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/dos.h>

#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Screen.h>

#include <string.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern DlgTemplPtr	dlgList[];

extern TextChar	strBuff[], strHeader[], strFooter[], strChars[], strWords[];
extern UWORD		levelArray[];
extern DialogPtr	dlgWindow;
extern TextPtr		headingStrs[];

extern TextChar		strBuff[];

extern UBYTE		_tbPenBlack;

#define MAX_INDENT	5

#define LFTMARG_TEXT	2
#define RGTMARG_TEXT	3
#define HEADER_TEXT  4
#define FOOTER_TEXT  5
#define COLMUP_ARROW	6
#define COLMDN_ARROW 7
#define COLUMNS_TEXT 8

#define NO_STYLE_RADBTN		 	2
#define CUSTOM_STYLE_RADBTN	3
#define SECTION_STYLE_RADBTN	4
#define HARVARD_STYLE_RADBTN	5
#define CHICAGO_STYLE_RADBTN	6
#define CUSTOM_TEXT				7
#define CUMULATIVE_CHECKBOX	8
#define APPEND_CHECKBOX			9
#define ALIGN_CHECKBOX			10
 
#define CHARS_TEXT				1
#define WORDS_TEXT				2
#define LINES_TEXT				3
#define SENS_TEXT				4
#define HEADS_TEXT				5
#define PAGES_TEXT				6
#define WORDLEN_TEXT				7
#define SENLEN_TEXT				8
#define GRADE_TEXT				9

#define PENCOLORS_USERITEM		2
#define PAPERCOLORS_USERITEM	3
#define PAPER_STRING			6

static UBYTE numColors;

/*
 *	Local prototypes
 */

BOOL	LayoutDialogFilter(IntuiMsgPtr, WORD *);
BOOL  LabelDialogFilter(IntuiMsgPtr, WORD *);
BOOL	ColorsDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Set selected headings to specified style
 */

BOOL DoStyle(WindowPtr window, UBYTE style)
{
	BOOL success = TRUE ;
	register HeadPtr selHead, lastHead, head;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	CursorOff(window);
	for (head = selHead; head && head != lastHead->Next; head = head->Next) {
		if (style == FS_NORMAL)
			head->Style = FS_NORMAL;
		else if (head->Style & style)
			head->Style &= ~style;
		else
			head->Style |= style;
	}
	DrawWindow(window);
	CursorOn(window);
	DocumentModified(docData);
	SetFormatMenu();
	return(success);
}

/*
 *	Set indent spaces to given value
 */

void DoIndentSpace(WindowPtr window, UWORD spaces)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CursorOff(window);
	docData->Indent = spaces;
	ReformatOutline(docData);
	DrawWindow(window);
	CursorOn(window);
	AdjustScrollBars(window);
	SetFormatMenu();
}

/*
 * Insert or delete page breaks for each line selected
 */
 
BOOL DoPageBreak(WindowPtr window)
{
	BOOL success = TRUE;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register HeadPtr head, selHead, endHead;

	selHead = docData->SelHead;	
	endHead = LastSelHead(selHead)->Next;
	if( selHead->Flags & HD_PAGEBREAK ) {
		for (head = selHead; head && head != endHead; head = head->Next) {
			head->Flags &= (~HD_PAGEBREAK);
		}
	} else {
		for (head = selHead; head && head != endHead; head = head->Next) {
			head->Flags |= HD_PAGEBREAK;
		}
	}
	DrawWindow(window);
	CountLines(docData);					/* This calculates number of pages */
	DocumentModified(docData);
	return(success);
}

/*
 *	Layout request filter
 */

static BOOL LayoutDialogFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register UWORD qualifier;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETUP || class == GADGETDOWN ) ) {
		qualifier = intuiMsg->Qualifier;
		itemHit = GadgetNumber((GadgetPtr)intuiMsg->IAddress);
		if (class == GADGETUP && !(qualifier & IEQUALIFIER_NUMERICPAD) &&
			itemHit >= LFTMARG_TEXT && itemHit <= FOOTER_TEXT) {
			if (qualifier & SHIFTKEYS)
				itemHit--;
			else
				itemHit++;
			if (itemHit < LFTMARG_TEXT || itemHit > FOOTER_TEXT)
				return (FALSE);
			ReplyMsg((MsgPtr) intuiMsg);
			ActivateGadget(GadgetItem(dlgWindow->FirstGadget,itemHit), dlgWindow, NULL);
			Delay(5);						/* Wait for gadget to become active */
			*item = -1;
			return (TRUE);
		}
		if ((class == GADGETDOWN || class == GADGETUP) &&
			(itemHit == COLMUP_ARROW || itemHit == COLMDN_ARROW) ) {
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Do the layout dialog
 */

BOOL DoLayout(WindowPtr window)
{
	register DialogPtr dlg;
	register WORD item;	
	register BOOL success = FALSE;
	register BOOL valid = TRUE;
	register WORD leftMarg, rightMarg ;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD docIndent;
	BOOL done = FALSE;
	GadgetPtr gadgList, gadget;
	WORD strItem;	
			
	BeginWait();	
	if( (dlg = GetDialog( dlgList[DLG_LAYOUT], screen, mainMsgPort)) != NULL ) {

		success = TRUE;
		dlgWindow = dlg;
		docIndent = docData->Indent;
		OutlineOKButton(dlg);
		gadgList = dlg->FirstGadget;
		SetEditItemText( dlg->FirstGadget,HEADER_TEXT,dlg,NULL, docData->Header);
		SetEditItemText( dlg->FirstGadget, FOOTER_TEXT,dlg,NULL, docData->Footer);
		NumToString((LONG) docIndent, strBuff);
		SetGadgetItemText(gadgList, COLUMNS_TEXT, dlg, NULL, strBuff);
		if( docIndent == 1 ) 
			EnableGadgetItem(gadgList,COLMDN_ARROW, dlg, NULL,FALSE);
		if( docIndent == MAX_INDENT ) 
			EnableGadgetItem(gadgList,COLMUP_ARROW,dlg,NULL,FALSE);
	
		leftMarg = rightMarg = -1;
		do {
			if( leftMarg < 0 ) {
				leftMarg = docData->LeftMargin;
				NumToString(leftMarg, strBuff);
				SetEditItemText(dlg->FirstGadget, LFTMARG_TEXT, dlg,NULL,strBuff);
			}
			if( rightMarg < 0 ) {
				rightMarg = docData->RightMargin;
				NumToString(rightMarg, strBuff);
				SetEditItemText(dlg->FirstGadget, RGTMARG_TEXT, dlg, NULL,strBuff);
			}
			do {
				strItem = LFTMARG_TEXT;
				item = ModalDialog( mainMsgPort, dlg, LayoutDialogFilter );
				switch( item ) {
				case CANCEL_BUTTON:
					success = FALSE;
				case OK_BUTTON:
					done = TRUE;
					break;
				case COLMUP_ARROW:
				case COLMDN_ARROW:
					if( item == COLMUP_ARROW) {
						if( docIndent < MAX_INDENT )
							docIndent++;		
					} else {
						if( docIndent > 1 )
							docIndent--;
					}
					NumToString((LONG) docIndent, strBuff);
					SetGadgetItemText(gadgList, COLUMNS_TEXT, dlg, NULL, strBuff);
					gadget = GadgetItem(gadgList, COLMDN_ARROW);
					if( docIndent == 1 )
						OffGList(gadget, dlg, NULL, 1);
					else
						OnGList(gadget, dlg, NULL, 1);
					gadget = GadgetItem(gadgList, COLMUP_ARROW);
					if( docIndent == MAX_INDENT )
						OffGList(gadget, dlg, NULL, 1);
					else 
						OnGList(gadget, dlg, NULL, 1);
					strItem = LFTMARG_TEXT;	
					break;
				default:
					break;
				}
				if( done && success ) {	 /* Was OK button hit? */
					GetEditItemText(gadgList, LFTMARG_TEXT, strBuff);
					valid = CheckNumber(strBuff);
					if( valid ) {
						leftMarg = StringToNum(strBuff);
						valid = (leftMarg >= 0) && (leftMarg < 256);
					}
					if( !valid ) {
						leftMarg = -1;
						strItem = LFTMARG_TEXT;
					} else {
						strItem = RGTMARG_TEXT;
						GetEditItemText(gadgList, RGTMARG_TEXT, strBuff);
						valid = CheckNumber(strBuff);
						if( valid ) {
							rightMarg = StringToNum(strBuff);
							valid = (rightMarg >= 0) && (rightMarg < 256);
						}
						if( !valid ) {
							rightMarg = -1;
						}
					}
					if( !valid )
						Error(ERR_BAD_MARGIN);
				}
				if( !valid || (!done && item != -1 ) ) {
					gadget = GadgetItem(gadgList, strItem);
					ActivateGadget(gadget, dlg, NULL);
				}
			} while( !done );
		} while( success && (!valid ) );
		if( success ) {
			docData->LeftMargin = leftMarg;
			docData->RightMargin = rightMarg;
			docData->Indent = docIndent;
			GetEditItemText(gadgList, HEADER_TEXT, docData->Header);
			GetEditItemText(gadgList, FOOTER_TEXT, docData->Footer);
			ReformatOutline(docData);
			DocumentModified(docData);
			CauseUpdate(window);
			AdjustScrollBars(window);
		}
		DisposeDialog( dlg ) ;
	}
	EndWait();
	return(success);
}

static BOOL LabelDialogFilter(intuiMsg, item)
register IntuiMsgPtr intuiMsg;
WORD *item;
{
	register WORD itemHit;
	register ULONG class;
	
	class = intuiMsg->Class;
	if( intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETUP || class == GADGETDOWN ) ) {
		itemHit = GadgetNumber((GadgetPtr)intuiMsg->IAddress);
		if( class == GADGETDOWN && itemHit == CUSTOM_TEXT ) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = CUSTOM_STYLE_RADBTN;
			return(TRUE);
		}
	}
	return(DialogFilter(intuiMsg, item));
}

BOOL DoLabel(WindowPtr window)
{
	register WORD item;
	register DialogPtr dlg;	
	register BOOL success = FALSE;
	UBYTE style;
	BOOL format, append, align;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL done = FALSE;
	register WORD offItem, onItem;
	register GadgetPtr gadgList;
	WORD oldBodyStart, displacement;
	
	BeginWait();

	AutoActivateEnable(FALSE);		
	dlgWindow = GetDialog( dlgList[DLG_LABEL], screen, mainMsgPort) ;
	AutoActivateEnable(TRUE);
 
	if( dlgWindow != NULL ) {
		dlg = dlgWindow;
		style = docData->LabelStyle + NO_STYLE_RADBTN;
		format = ( docData->LabelFlags & LABEL_FORMAT_MASK ) != 0 ;
		append = ( docData->LabelFlags & LABEL_APPEND_MASK ) != 0 ;
		align = ( docData->LabelFlags & LABEL_ALIGN_MASK ) != 0 ;
		OutlineOKButton(dlg);
		gadgList = dlg->FirstGadget;
		SetGadgetItemValue(gadgList, style, dlg, NULL, 1);
		SetGadgetItemValue(gadgList, CUMULATIVE_CHECKBOX, dlg, NULL, format);
		SetGadgetItemValue(gadgList, APPEND_CHECKBOX, dlg, NULL, append);
		SetGadgetItemValue(gadgList, ALIGN_CHECKBOX, dlg, NULL, align);
		strcpy(strBuff, docData->CustomLabel);
		headingStrs[CUSTOM_STYLE_RADBTN-NO_STYLE_RADBTN] = docData->CustomLabel;
		SetEditItemText(dlg->FirstGadget, CUSTOM_TEXT, dlg, NULL,headingStrs[style-NO_STYLE_RADBTN]);
		do {
			offItem = onItem = -1;
			item = ModalDialog( mainMsgPort, dlg, LabelDialogFilter );
			switch( item ) {
			case OK_BUTTON:
				success = TRUE;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case CUSTOM_STYLE_RADBTN:
				ActivateGadget(GadgetItem(gadgList, CUSTOM_TEXT), dlg, NULL);
			case NO_STYLE_RADBTN:
			case SECTION_STYLE_RADBTN:
			case HARVARD_STYLE_RADBTN:
			case CHICAGO_STYLE_RADBTN:
				offItem = style;
				style = onItem = item;
				break;
			case CUMULATIVE_CHECKBOX:
				ToggleCheckbox(&format, item, dlg);
				break;
			case APPEND_CHECKBOX:
				ToggleCheckbox(&append, item, dlg);
				break;
			case ALIGN_CHECKBOX:
				ToggleCheckbox(&align, item, dlg);
				break;
			default:
				break;
			}
			if( !done && item != -1 ) {
				if( style != CUSTOM_STYLE_RADBTN ) {
					SetEditItemText(dlg->FirstGadget,CUSTOM_TEXT,dlg,NULL, headingStrs[style-NO_STYLE_RADBTN]);
				}
				if( offItem != -1 )
					SetGadgetItemValue(gadgList, offItem, dlg, NULL, 0);
				if( onItem != -1 )
					SetGadgetItemValue(gadgList, onItem, dlg, NULL, 1);
			}
		} while( !done );
		if( success ) {
			docData->LabelStyle = style - NO_STYLE_RADBTN;
			docData->LabelFlags &= (255 - ( LABEL_FORMAT_MASK + LABEL_APPEND_MASK +
				LABEL_ALIGN_MASK + LABEL_CHANGED_MASK));
			if( format )
				docData->LabelFlags |= LABEL_FORMAT_MASK;
			if( append )
				docData->LabelFlags |= LABEL_APPEND_MASK;
			if( align )
				docData->LabelFlags |= LABEL_ALIGN_MASK;
			docData->LabelFlags |= LABEL_CHANGED_MASK;
			oldBodyStart = docData->SelHead->BodyStart;
			GetEditItemText(gadgList, CUSTOM_TEXT, docData->CustomLabel);
			DisposeDialog(dlg);					/* DO ASAP to be user-friendly */
			RenumberHeads(docData);				/* Heads may have different origins now */
			BumpLevel(docData, docData->FirstHead, 0);
			ReformatOutline(docData);			/* Recalculate line starts & counts */
			DocumentModified(docData);			/* Assume label has changed */
			docData->LabelFlags &= ~LABEL_CHANGED_MASK;
/*
	Adjust cursor to an equivalent position
*/
			displacement = docData->SelHead->BodyStart - oldBodyStart;
			docData->SelStart += displacement;
			docData->SelEnd += displacement;
			docData->DragStart += displacement;

			AdjustScrollBars(window);
			CauseUpdate(window);
		} else 
			DisposeDialog( dlg ) ;
	}
	EndWait();
	return(success);
}	

/*
 *	Pen/paper colors dialog filter
 */

static BOOL ColorsDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETUP || class == GADGETDOWN) ) {
		itemHit = GadgetNumber((GadgetPtr)intuiMsg->IAddress);
		if ((class == GADGETDOWN || class == GADGETUP) &&
			( itemHit == PENCOLORS_USERITEM || itemHit == PAPERCOLORS_USERITEM) ) {
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Choose pen and paper colors
 */
 
BOOL DoColors(WindowPtr window)
{
	BOOL success = FALSE;
	WORD item;
	WORD 	numAcross, numDown;
	WORD	penWidth,penHeight;
	LONG	height;
	WORD	paperWidth,paperHeight;
	BOOL done, doubleClick;
	ULONG secs, micros, prevSecs, prevMicros;
	register GadgetPtr colorsGadg;
	register DialogPtr dlg;
	register UWORD 	i;
	register WORD 	x, y;
	WORD 			newPenColor, newPaperColor;
	WORD 			*selColor;
	DocDataPtr 		docData;
	WORD 			lastItem = 0;
	WORD 			numColors;
	Rectangle		penRect,paperRect;
	RGBColor		colorTable[256];
	
/*
	Bring up dialog
*/

	if( IsDocWindow(window) ) {
		docData = (DocDataPtr) GetWRefCon(window);
		numColors = GetColorTable(screen,&colorTable);
		GetNumAcrossDown(numColors, &numAcross, &numDown);
		i = (numDown < 4) ? 20 : 15;
		height = i*numDown + 60;
		if (height < 95)
			height = 95;
		dlgList[DLG_COLORS]->Height = height;
	
		BeginWait();
		if ((dlgWindow = GetDialog(dlgList[DLG_COLORS], screen, mainMsgPort)) == NULL) {
			Error(ERR_NO_MEM);
		} else {
			newPenColor = docData->PenColor;
			newPaperColor = docData->PaperColor;
			dlg = dlgWindow;
			OutlineOKButton(dlg);
			colorsGadg = GadgetItem(dlg->FirstGadget, PENCOLORS_USERITEM);
			colorsGadg->Activation |= GADGIMMEDIATE;
			GetGadgetRect(colorsGadg, dlg, NULL, &penRect);
			penWidth = penRect.MaxX - penRect.MinX + 1;
			penHeight = penRect.MaxY - penRect.MinY + 1;
			DrawColorBorder(dlg, colorsGadg);
			
			colorsGadg = GadgetItem(dlg->FirstGadget, PAPERCOLORS_USERITEM);
			colorsGadg->Activation |= GADGIMMEDIATE;
			GetGadgetRect(colorsGadg, dlg, NULL, &paperRect);
			paperWidth = paperRect.MaxX - paperRect.MinX + 1;
			paperHeight = paperRect.MaxY - paperRect.MinY + 1;
			DrawColorBorder(dlg, colorsGadg);

			for (i = 0; i < numColors; i++) {
				DrawColorItem(dlg, i, (i == newPenColor),PENCOLORS_USERITEM);
				DrawColorItem(dlg, i, (i == newPaperColor),PAPERCOLORS_USERITEM);
			}
		
/*
	Handle dialog
*/
			prevSecs = prevMicros = 0;
			done = FALSE;
			do {
				item = ModalDialog(mainMsgPort, dlg, ColorsDialogFilter);
				CurrentTime(&secs, &micros);
				switch (item) {
				case OK_BUTTON:
				case CANCEL_BUTTON:
					done = TRUE;
					break;
				case PENCOLORS_USERITEM:
					x = (dlg->MouseX - penRect.MinX)*numAcross/penWidth;
					y = (dlg->MouseY - penRect.MinY)*numDown/penHeight;
					if (x < 0)
						x = 0;
					else if (x >= numAcross)
						x = numAcross - 1;
					if (y < 0)
						y = 0;
					else if (y >= numDown)
						y = numDown - 1;
					i = (UWORD) x + y*numAcross;
					selColor = &newPenColor;
					if (i != *selColor || (item != lastItem) ) {
						DrawColorItem(dlg, (UWORD) *selColor, FALSE,item);
						*selColor = i;
						DrawColorItem(dlg, (UWORD) *selColor, TRUE,item);
						doubleClick = FALSE;
					} else {
						doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
					}
					
					lastItem = item;
					if(doubleClick) {
						item = OK_BUTTON;
						done = TRUE;
					} else {
						prevSecs = secs;
						prevMicros = micros;
					}
					break;
				case PAPERCOLORS_USERITEM:
					x = (dlg->MouseX - paperRect.MinX)*numAcross/paperWidth;
					y = (dlg->MouseY - paperRect.MinY)*numDown/paperHeight;
					if (x < 0)
						x = 0;
					else if (x >= numAcross)
						x = numAcross - 1;
					if (y < 0)
						y = 0;
					else if (y >= numDown)
						y = numDown - 1;
					i = (UWORD) x + y*numAcross;
					selColor = &newPaperColor;
					if (i != *selColor || (item != lastItem) ) {
						DrawColorItem(dlg, (UWORD) *selColor, FALSE,item);
						*selColor = i;
						DrawColorItem(dlg, (UWORD) *selColor, TRUE,item);
						doubleClick = FALSE;
					} else {
						doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
					}
					
					lastItem = item;
					if(doubleClick) {
						item = OK_BUTTON;
						done = TRUE;
					} else {
						prevSecs = secs;
						prevMicros = micros;
					}
					break;
				default:
					break;
				}
			} while (!done);
			DisposeDialog(dlg);
			success = item == OK_BUTTON;
			if( success && ((docData->PenColor != newPenColor) || (docData->PaperColor != newPaperColor)) ) {
				DocumentModified(docData);
				docData->PenColor = newPenColor;
				docData->PaperColor = newPaperColor;
				CauseUpdate(window);		/* Redraw entire window w/ new color */
			}	
		}
		EndWait();
	}
	return(success);
}

/*
 *	Display document info
 */

BOOL DoDocumentInfo(WindowPtr window)
{
	register TextChar ch;
	register TextPtr text;
	register WORD loc, len, maxLoc;
	register LONG numChars, numWords, numLines;
	LONG numHeads, numSens;
	LONG wordChars, longWords, senChars;
	Fixed wordLen, senLen, gradeLevel;
	register HeadPtr head;
	DialogPtr dlg;
	GadgetPtr gadgList;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	BOOL success = FALSE;
	
	BeginWait();
/*
	Bring up dialog
*/
	if( (dlg = GetDialog( dlgList[DLG_DOCINFO], screen, mainMsgPort)) != NULL ) {
		success = TRUE;
		dlgWindow = dlg;
		gadgList = dlg->FirstGadget;
		OutlineOKButton(dlg);
		SetStdPointer(dlg, POINTER_WAIT);

/*
	Count items
*/
		numChars = numWords = numLines = numHeads = numSens = wordChars =
			longWords = senChars = 0;
		for( head = docData->FirstHead; head != NULL ; head = NextHeadAll(head) ) {
			numLines += head->NumLines;
			numHeads++;
			numChars += head->Len - head->BodyStart;
			text = head->Text;
			maxLoc = head->Len;

			loc = SkipSpace(head, head->BodyStart);
			while (loc < maxLoc) {
				len = WordLen(head, loc);
				wordChars += len;
				loc = SkipWord(head, (WORD) (loc + len));
				numWords++;
				if (len >= 9)
					longWords++;
			}
/*
	Count sentences
	(and check to see if paragraph ends without a sentence ending)
*/
			for (loc = head->BodyStart; loc < maxLoc; loc++) {
				ch = text[loc];
				if ((ch == '.' || ch == '?' || ch == '!') &&
					(loc == maxLoc - 1 || text[loc + 1] <= ' '))
					numSens++;
			}
			if (maxLoc) {
				loc = BackupSpaces(text, maxLoc, head->BodyStart);
				if (loc && (ch = text[loc - 1]) != '.' && ch != '?' && ch != '!')
					numSens++;
			}
		}
/*
	Calculate average word & sentence length, and grade level
*/
		wordLen = (numWords) ? FixDiv(wordChars, numWords) : 0;
		senLen = (numSens) ? FixDiv(numWords, numSens) : 0;
		if (numWords == 0)
			gradeLevel = 0;
		else {
			gradeLevel = FixDiv(longWords*100, numWords);
			gradeLevel = FixMul(gradeLevel + senLen, FixRatio(4, 10));
		}
/*
	Display items
*/
		NumToString(numChars, strBuff);
		SetGadgetItemText(gadgList, CHARS_TEXT, dlg, NULL, strBuff);
		NumToString(numWords, strBuff);
		SetGadgetItemText(gadgList, WORDS_TEXT, dlg, NULL, strBuff);
		NumToString(numLines, strBuff);
		SetGadgetItemText(gadgList, LINES_TEXT, dlg, NULL, strBuff);
		NumToString(numSens,  strBuff);
		SetGadgetItemText(gadgList, SENS_TEXT, dlg, NULL, strBuff);
		NumToString(numHeads, strBuff);
		SetGadgetItemText(gadgList, HEADS_TEXT, dlg, NULL, strBuff);
		NumToString(docData->Pages, strBuff);
		SetGadgetItemText(gadgList, PAGES_TEXT, dlg, NULL, strBuff);
		if (wordLen == 0)
			NumToString(0, strBuff);
		else if (wordLen < Long2Fix(10L)) {
			strBuff[0] = ' ';
			Fix2Ascii(wordLen, strBuff + 1, 1);
		} else
			Fix2Ascii(wordLen, strBuff, 1);
		strcat(strBuff, strChars);
		SetGadgetItemText(gadgList, WORDLEN_TEXT, dlg, NULL, strBuff);
		if (senLen == 0)
			NumToString(0, strBuff);
		else if (senLen < Long2Fix(10L)) {
			strBuff[0] = ' ';
			Fix2Ascii(senLen, strBuff + 1, 1);
		} else
			Fix2Ascii(senLen, strBuff, 1);
		strcat(strBuff, strWords);
		SetGadgetItemText(gadgList, SENLEN_TEXT, dlg, NULL, strBuff);
		if (gradeLevel == 0)
			NumToString(0, strBuff);
		else if (gradeLevel < Long2Fix(10L)) {
			strBuff[0] = ' ';
			Fix2Ascii(gradeLevel, strBuff + 1, 1);
		} else
			Fix2Ascii(gradeLevel, strBuff, 1);
		SetGadgetItemText(gadgList, GRADE_TEXT, dlg, NULL, strBuff);
/*
	Handle requester
*/
		SetStdPointer(dlg, POINTER_ARROW);
		while (ModalDialog(mainMsgPort, dlg, DialogFilter) != OK_BUTTON) ;
		DisposeDialog(dlg);
	} else {		
		Error(ERR_NO_MEM);
	}
	EndWait();
	return(success);
}


/*
 *	Handle style menu commands
 */

BOOL DoStyleMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL success ;
	switch (item) {
	case PLAIN_ITEM:
		success = DoStyle(window, FS_NORMAL);
		break;
	case BOLD_ITEM:
		success = DoStyle(window, FSF_BOLD);
		break;
	case ITALIC_ITEM:
		success = DoStyle(window, FSF_ITALIC);
		break;
	case UNDERLINE_ITEM:
		success = DoStyle(window, FSF_UNDERLINED);
		break;
	case PAGEBREAK_ITEM:
		success = DoPageBreak(window);
		break;
	case LAYOUT_ITEM:
		success = DoLayout(window);
		break;
	case LABEL_ITEM:
		success = DoLabel(window);
		break;
	case COLORS_ITEM:
		success = DoColors(window);
		break;
	case DOCUMENTINFO_ITEM:
		success = DoDocumentInfo(window);
		break;
	}
	return(success);
}
