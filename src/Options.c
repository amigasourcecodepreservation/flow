/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Options menu functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/exec.h>

#include <IFF/WORD.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Color.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Border.h>
#include <Toolbox/Screen.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern DlgTemplPtr	dlgList[];

extern TextChar	strScreenColor[];

extern DialogPtr	dlgWindow;	/* For color dialog */

extern Options		options;

extern TextChar	strBuff[];

/*
 *	Local variables and definitions
 */

enum {
	PREFRESET_BTN = 2,
	INCH_RADBTN,
	CM_RADBTN,
	INSERT_RADBTN,
	TYPEOVER_RADBTN,
	FLASH_RADBTN,
	SOUND_RADBTN,
	BOTH_RADBTN,
	FAST_RADBTN,
	SLOW_RADBTN,
	OFF_RADBTN,
	SHOWFLOW_RADBTN,
	SHOWALL_RADBTN,
	SAVEICONS_BOX,
	MAKEBACKUPS_BOX,
	AUTOSAVE_BOX,
	AUTOSAVE_TEXT,
	FULLCLIP_BOX
};

enum {
	SCREENCOLORS_USERITEM = 3,
	CHANGEPEN_BTN
};

static DialogPtr prefsDlg;

/*
 *	Local prototypes
 */

BOOL	ScreenColorsDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Set standard options
 *	If full is FALSE, then only set options that Options requester modifies
 */

void SetStdOptions(Options *opts, BOOL full)
{
	if (full)
		BlockClear(opts, sizeof(Options));
	opts->MeasureUnit	= MEASURE_DEFAULT;
	opts->ShowAllFiles	= FALSE;
	opts->SaveIcons		= TRUE;
	opts->BlinkPeriod	= BLINK_FAST;
	opts->BeepSound		= opts->BeepFlash = TRUE;
	opts->Typeover		= FALSE;
	opts->AutoSpell		= FALSE;
	opts->SpeakType		= FALSE;
	opts->MakeBackups	= FALSE;
	opts->AutoSave		= FALSE;
	opts->AutoSaveTime	= 30;
	opts->FullClipboard = TRUE;
}

/*
 *	Set options dialog buttons
 */

static void SetOptionsButtons(DialogPtr dlg, Options *opts)
{
	GadgetPtr gadgList = dlg->FirstGadget;

	SetGadgetItemValue(gadgList, INCH_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_INCH));
	SetGadgetItemValue(gadgList, CM_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_CM));
	SetGadgetItemValue(gadgList, INSERT_RADBTN, dlg, NULL, !opts->Typeover);
	SetGadgetItemValue(gadgList, TYPEOVER_RADBTN, dlg, NULL, opts->Typeover);
	SetGadgetItemValue(gadgList, FLASH_RADBTN, dlg, NULL, opts->BeepFlash && !opts->BeepSound);
	SetGadgetItemValue(gadgList, SOUND_RADBTN, dlg, NULL, !opts->BeepFlash && opts->BeepSound);
	SetGadgetItemValue(gadgList, BOTH_RADBTN, dlg, NULL, opts->BeepFlash && opts->BeepSound);
	SetGadgetItemValue(gadgList, FAST_RADBTN, dlg, NULL, opts->BlinkPeriod == BLINK_FAST);
	SetGadgetItemValue(gadgList, SLOW_RADBTN, dlg, NULL, opts->BlinkPeriod == BLINK_SLOW);
	SetGadgetItemValue(gadgList, OFF_RADBTN, dlg, NULL, opts->BlinkPeriod == BLINK_OFF);
	SetGadgetItemValue(gadgList, SHOWFLOW_RADBTN, dlg, NULL, !opts->ShowAllFiles);
	SetGadgetItemValue(gadgList, SHOWALL_RADBTN, dlg, NULL, opts->ShowAllFiles);
	SetGadgetItemValue(gadgList, SAVEICONS_BOX, dlg, NULL, opts->SaveIcons);
	SetGadgetItemValue(gadgList, MAKEBACKUPS_BOX, dlg, NULL, opts->MakeBackups);
	SetGadgetItemValue(gadgList, AUTOSAVE_BOX, dlg, NULL, opts->AutoSave);
	SetGadgetItemValue(gadgList, FULLCLIP_BOX, dlg, NULL, opts->FullClipboard);
}

/*
 *	Set auto-save text
 */

static void SetAutoSaveText(DialogPtr dlg, UBYTE autoSaveTime)
{
	NumToString(autoSaveTime, strBuff);
	SetEditItemText(dlg->FirstGadget, AUTOSAVE_TEXT, dlg, NULL, strBuff);
}

/*
 *	Preferences dialog filter
 */

static BOOL PrefsDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	if (intuiMsg->IDCMPWindow == prefsDlg && intuiMsg->Class == GADGETDOWN &&
		GadgetNumber((GadgetPtr) intuiMsg->IAddress) == AUTOSAVE_TEXT) {
		ReplyMsg((MsgPtr) intuiMsg);
		*item = AUTOSAVE_TEXT;
		return (TRUE);
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 * Adjust user options
 */
 
BOOL DoPreferences()
{
	WORD item;
	LONG autoSaveTime;
	Options opts;

	opts = options;
	if (!opts.BeepSound)
		opts.BeepFlash = TRUE;
/*
	Get dialog
*/
	BeginWait();
	AutoActivateEnable(FALSE);
	prefsDlg = GetDialog(dlgList[DLG_PREFERENCES], screen, mainMsgPort);
	AutoActivateEnable(TRUE);
	if (prefsDlg == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(prefsDlg);
	SetOptionsButtons(prefsDlg, &opts);
	SetAutoSaveText(prefsDlg, opts.AutoSaveTime);
/*
	Handle dialog
*/
	for (;;) {
		item = ModalDialog(mainMsgPort, prefsDlg, PrefsDialogFilter);
		if (item == OK_BUTTON || item == CANCEL_BUTTON)
			break;
		switch (item) {
		case PREFRESET_BTN:
			SetStdOptions(&opts, FALSE);
			SetAutoSaveText(prefsDlg, opts.AutoSaveTime);
			break;
		case SHOWFLOW_RADBTN:
			opts.ShowAllFiles = FALSE;
			break;
		case SHOWALL_RADBTN:
			opts.ShowAllFiles = TRUE;
			break;
		case SAVEICONS_BOX:
			opts.SaveIcons = !opts.SaveIcons;
			break;
		case FAST_RADBTN:
			opts.BlinkPeriod = BLINK_FAST;
			break;
		case SLOW_RADBTN:
			opts.BlinkPeriod = BLINK_SLOW;
			break;
		case OFF_RADBTN:
			opts.BlinkPeriod = BLINK_OFF;
			break;
		case INSERT_RADBTN:
			opts.Typeover = FALSE;
			break;
		case TYPEOVER_RADBTN:
			opts.Typeover = TRUE;
			break;
		case FLASH_RADBTN:
			opts.BeepFlash = TRUE;
			opts.BeepSound = FALSE;
			break;
		case SOUND_RADBTN:
			opts.BeepFlash = FALSE;
			opts.BeepSound = TRUE;
			break;
		case BOTH_RADBTN:
			opts.BeepFlash = opts.BeepSound = TRUE;
			break;
		case INCH_RADBTN:
			opts.MeasureUnit = MEASURE_INCH;
			break;
		case CM_RADBTN:
			opts.MeasureUnit = MEASURE_CM;
			break;
		case MAKEBACKUPS_BOX:
			opts.MakeBackups = !opts.MakeBackups;
			break;
		case AUTOSAVE_BOX:
			opts.AutoSave = !opts.AutoSave;
			if (opts.AutoSave)
				ActivateGadget(GadgetItem(prefsDlg->FirstGadget, AUTOSAVE_TEXT), prefsDlg, NULL);
			break;
		case AUTOSAVE_TEXT:
			opts.AutoSave = TRUE;
			break;
		case FULLCLIP_BOX:
			opts.FullClipboard = !opts.FullClipboard;
			break;
		}
		SetOptionsButtons(prefsDlg, &opts);
	}
	GetEditItemText(prefsDlg->FirstGadget, AUTOSAVE_TEXT, strBuff);
	autoSaveTime = StringToNum(strBuff);
	DisposeDialog(prefsDlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Set new values
*/
	options = opts;
	if (autoSaveTime < 0)
		autoSaveTime = 1;
	else if (autoSaveTime > 60)
		autoSaveTime = 60;
	options.AutoSaveTime = autoSaveTime;
	return (TRUE);
}

/*
 *	Screen colors dialog filter
 */

static BOOL ScreenColorsDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlgWindow && (class == GADGETUP || class == GADGETDOWN) ) {
		itemHit = GadgetNumber((GadgetPtr)intuiMsg->IAddress);
		if ((class == GADGETDOWN || class == GADGETUP) &&
			itemHit == SCREENCOLORS_USERITEM) {
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust screen colors
 */

BOOL DoScreenColors()
{
	WORD i, item, selColor;
	WORD x, y, numAcross, numDown;
	WORD	width,height;
	BOOL done, doubleClick;
	ULONG secs, micros, prevSecs, prevMicros;
	GadgetPtr colorsGadg;
	RGBColor 	newColor,prevColors[256];
	register DialogPtr dlg;
	WORD		numColors;
	RGBColor	screenColors[256];
	Rectangle	rect;
	RastPtr		rPort;
	
	numColors = GetColorTable(screen,&screenColors);	
	for (i = 0; i < numColors; i++)
		prevColors[i] = screenColors[i];
/*
	Bring up dialog
*/
	GetNumAcrossDown(numColors, &numAcross, &numDown);
	i = (numDown < 4) ? 20 : 15;
	height = i*numDown + 75;
	if (height < 100)
		height = 100;
	dlgList[DLG_SCREENCOLORS]->Height = height;

	BeginWait();
	if ((dlgWindow = GetDialog(dlgList[DLG_SCREENCOLORS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	dlg = dlgWindow;
	OutlineOKButton(dlg);
	colorsGadg = GadgetItem(dlg->FirstGadget, SCREENCOLORS_USERITEM);
	colorsGadg->Activation |= GADGIMMEDIATE;
	GetGadgetRect(colorsGadg, dlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	selColor = 0;
/*
	Draw dark border around pens and draw pen colors
*/
	DrawColorBorder(dlg,colorsGadg);
	for (i = 0; i < numColors; i++)
		DrawColorItem(dlg, i, (i == selColor), SCREENCOLORS_USERITEM);
/*
	Handle dialog
*/
	prevSecs = prevMicros = 0;
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, dlg, ScreenColorsDialogFilter);
		CurrentTime(&secs, &micros);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case PREFRESET_BTN:
			LoadRGB4(&screen->ViewPort, prevColors, numColors);
			UpdateWindows();	/* Since no refesh happens; calls CheckColorTable() */
			break;
		case SCREENCOLORS_USERITEM:
			x = (dlg->MouseX - rect.MinX)*numAcross/width;
			y = (dlg->MouseY - rect.MinY)*numDown/height;
			if (x < 0)
				x = 0;
			else if (x >= numAcross)
				x = numAcross - 1;
			if (y < 0)
				y = 0;
			else if (y >= numDown)
				y = numDown - 1;
			i = x + y*numAcross;
			if (i != selColor) {
				DrawColorItem(dlg, selColor, FALSE, SCREENCOLORS_USERITEM);
				selColor = i;
				DrawColorItem(dlg, selColor, TRUE, SCREENCOLORS_USERITEM);
				doubleClick = FALSE;
			}
			else
				doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
			if (!doubleClick) {
				prevSecs = secs;
				prevMicros = micros;
				break;
			}
			item = CHANGEPEN_BTN;		/* Fall through */
		case CHANGEPEN_BTN:
			if (GetColor(screen, mainMsgPort, DialogFilter, strScreenColor, NULL,
						 screenColors[selColor], &newColor, selColor)) {
				CheckColorTable();
			}
			break;
		}
	} while (!done);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON) {
		LoadRGB4(&screen->ViewPort, prevColors, numColors);
		CheckColorTable();
	}
	return( (BOOL) (item == OK_BUTTON));
}
