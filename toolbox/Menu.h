/*
 *	Toolbox library
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Menu template definitions
 */

#ifndef TOOLBOX_MENU_H
#define TOOLBOX_MENU_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#ifndef TOOLBOX_MEMORY_H
#include <Toolbox/Memory.h>
#endif

/*
 *	Menu Items template
 *
 *	Type field is menu item type from list below
 *	CommKey field is key equivalent or 0
 *	Style field is menu item style (for text items)
 *	Info field has following definition for each menu item type:
 *
 *	MENU_ITEM_TEXT	Pointer to text of menu item
 *	MENU_ITEM_IMAGE	Pointer to Image structure
 *
 *	SubItems field points to a list of menu subitems
 */

typedef struct _MenuItemTemplate {
	UBYTE						Type;
	UBYTE						Flags;
	BYTE						CommKey;
	UBYTE						Style;
	LONG						MutualExclude;
	Ptr							Info;
	struct _MenuItemTemplate	*SubItems;
} MenuItemTemplate, *MenuItemTemplPtr;

/*
 *	Menu template
 *
 *	Name field points to menu name
 *	Items field points to first item in menu item template list
 */

typedef struct {
	TextPtr				Name;
	MenuItemTemplPtr	Items;
} MenuTemplate, *MenuTemplPtr;

/*
 *	Menu Item types
 */

#define MENU_TEXT_ITEM		0
#define MENU_IMAGE_ITEM		1

#define MENU_NO_ITEM		0xFF

/*
 *	Flags for menu items
 */

#define MENU_ENABLED	0x01
#define MENU_CHECKABLE	0x02
#define MENU_CHECKED	0x04	/* Implies CHECKABLE */
#define MENU_TOGGLE		0x08	/* Implies CHECKABLE */

/*
 *	Prototypes
 */

MenuPtr		GetMenuStrip(MenuTemplPtr);
void		DisposeMenuStrip(MenuPtr);
void		InsertMenuStrip(WindowPtr, MenuPtr);
MenuPtr		GetMenu(MenuTemplPtr);
void		DisposeMenu(MenuPtr);
void		InsertMenu(WindowPtr, MenuPtr, UWORD);
void		InsertSubMenu(WindowPtr, UWORD, MenuItemPtr);
MenuItemPtr	GetMenuItems(MenuItemTemplPtr);
void		DisposeMenuItems(MenuItemPtr);
MenuPtr		MenuAddress(MenuPtr, UWORD);
void		SetMenuItem(WindowPtr, UWORD, TextPtr);
void		SetMenuItemFont(WindowPtr, UWORD, TextAttrPtr);
void		CheckMenu(WindowPtr, UWORD, BOOL);
void		InsertMenuItem(WindowPtr, UWORD, TextPtr);
void		DeleteMenuItem(WindowPtr, UWORD);

#endif
