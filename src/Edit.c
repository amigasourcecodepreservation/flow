/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Edit menu functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/layers.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Window.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>

#include <string.h>

#include "Flow.h"
#include "HEAD.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;

extern WindowPtr	windowList[];
extern WORD			numWindows;
extern WORD			pasteType;

extern Ptr			pasteBuff;				/* Inited to NULL */

extern BOOL			scrapValid;

extern TextChar	toUpper[];

extern DlgTemplPtr	dlgList[];

extern MsgPortPtr	mainMsgPort;

extern struct IFFParseBase *IFFParseBase;

extern WORD			charBaseline, charWidth, charHeight;

extern UWORD		levelArray[];

extern Options		options;

extern TextChar	strBuff[];

extern struct 		DateStamp dateStamp;

extern DialogPtr	insertDialog;	/* For insertDlg */

extern TextFontPtr	smallFont;	/* For insertLiteral's font */

extern UBYTE		_tbPenLight, _tbPenDark, _tbPenWhite, _tbPenBlack;

extern struct ClipboardHandle *clipboard;

/*
 *	Local variables
 *	pasteBuff is pointer to list of headings or NULL-terminated text string
 */

#define INSERT_ITEMS	0
#define NUM_ACROSS		24
#define NUM_DOWN		8

enum {
	PAGE1_RADBTN = 2,
	PAGEI_RADBTN,
	PAGEi_RADBTN,
	PAGEA_RADBTN,
	PAGEa_RADBTN,
	STARTPAGE_TEXT,
	TIME12HR_RADBTN,
	TIME24HR_RADBTN,
	DATESHORT_RADBTN,
	DATEABBR_RADBTN,
	DATELONG_RADBTN,
	DATEABBRDAY_RADBTN,
	DATELONGDAY_RADBTN,
	DATEMIL_RADBTN
};

enum {
	BLACK, WHITE, BEIGE, GREY
};

/*
 *	Local prototypes
 */

void	SetPasteMenuItems(void);
BOOL  DoInsertLiteral(WindowPtr);
BOOL	DoInsertDate(WindowPtr);
BOOL	DoInsertTime(WindowPtr);
BOOL  DoInsertSpace(WindowPtr);
BOOL  DoInsertMenu(WindowPtr, UWORD, UWORD);
BOOL  DoChangeCaseMenu(WindowPtr, UWORD);

/*
 *	Set paste menu items to on/off
 *	(Called only when something is put in clipboard)
 */

static void SetPasteMenuItems()
{
	register WORD i;
	register WindowPtr window;

	for (i = 0; i < numWindows; i++) {
		window = windowList[i];
		if (pasteType == PASTE_NONE)
			OffMenu(window, SHIFTMENU(EDIT_MENU) | SHIFTITEM(PASTE_ITEM));
		else
			OnMenu(window, SHIFTMENU(EDIT_MENU) | SHIFTITEM(PASTE_ITEM));
	}
}

/*
 *	Clear paste buffer
 */

void ClearPaste()
{
	if( pasteBuff != NULL ) {
		if (pasteType == PASTE_TEXT)
			DisposePtr(pasteBuff);
		else if (pasteType == PASTE_HEAD)
			DisposeAllHeads(pasteBuff,NULL);
		pasteBuff = NULL;
		pasteType = PASTE_NONE;
		scrapValid = FALSE;
	}
}

/*
 *	Return pointer to copy of selected heading(s)
 */

HeadPtr CopyHead(docData)
DocDataPtr docData;
{
	HeadPtr selHead, lastHead;

	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	return (DupHeadList(selHead, lastHead));
}

/*
 *	Paste heading(s) above currently selected heading
 *	Return success status
 */

BOOL PasteHead(DocDataPtr docData, HeadPtr pasteHead)
{
	register HeadPtr head, selHead;
	HeadPtr lastHead, nextHead, endHead;
	register WORD inc;
	HeadPtr saveHead;

	for(inc = 1, lastHead = pasteHead; lastHead->Next; inc++, lastHead = lastHead->Next) ;
	if ((head = DupHeadList(pasteHead, lastHead)) == NULL)
		return (FALSE);
	for (lastHead = head; lastHead->Next; lastHead = lastHead->Next) ;
	UnSelectHeads(docData);
	selHead = docData->SelHead;
/*
	Link heading list above currently selected heading
*/
	if (selHead == docData->FirstHead)
		docData->FirstHead = head;
	if (selHead->Super && selHead->Super->Sub == selHead)
		selHead->Super->Sub = head;
	if (selHead->Prev)
		selHead->Prev->Next = head;
	head->Prev = selHead->Prev;
	head->Num = selHead->Num;
	lastHead->Next = selHead;
	selHead->Prev = lastHead;
/*
	Set new selected heading and calculate line starts for new headings
*/
	head->Num -= inc;
	for( selHead = head ; selHead != lastHead->Next ; selHead = selHead->Next ) {
		if( selHead != NULL && selHead != head ) {
			selHead->Num = selHead->Prev->Num + 1 ;
		}
	}
	saveHead = head;
	endHead = lastHead->Next;

	while( head && head != endHead ) {  
		head->Super = selHead->Super;
		nextHead = head->Next;
		CalcLineStarts(docData, head);		/* May change head pointer */
		if( head == saveHead )
			saveHead = nextHead->Prev;
		head = nextHead;
	}
	BumpLevel(docData, saveHead, inc);
	ReformatRestOfLevel(docData, saveHead);
	CountLines(docData);
	docData->SelHead = PrevHead(endHead);
	docData->SelStart = docData->SelEnd = docData->DragStart = docData->SelHead->Len;
	UnSelectHeads(docData);
	return (TRUE);
}

/*
 *	Remove selected heading(s) from outline
 *	Return success status
 */

BOOL DeleteHead(DocDataPtr docData)
{
	BOOL expanded;
	HeadPtr head, lastHead, selHead;
	WORD oldNum ;
	HeadPtr prevHead;
	BOOL reformat;
	
	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	oldNum = selHead->Num;
	prevHead = selHead->Prev;
	reformat = ( lastHead->Next != NULL ) ;
/*
	Find which heading to be next selected heading
*/
	expanded = (lastHead->Flags & HD_EXPANDED);
	lastHead->Flags &= ~HD_EXPANDED;
	head = NextHead(lastHead);
	lastHead->Flags |= expanded;
	if( head == NULL ) {
		if( (head = PrevHead(selHead)) == NULL && (head = AllocHead()) == NULL )
			return (FALSE);
	}
	if (docData->FirstHead == selHead) {
		oldNum = levelArray[0];
		docData->FirstHead = head;
		reformat = TRUE;
	}
/*
	Adjust links to previous and next headings
*/
	if (prevHead != NULL)
		prevHead->Next = lastHead->Next;
		
	if (lastHead->Next)
		lastHead->Next->Prev = prevHead;
/*
	If super-head exists, check to see if this is its first sub-head
*/
	if (selHead->Super && selHead->Super->Sub == selHead) {
		selHead->Super->Sub = lastHead->Next;
		if (lastHead->Next == NULL)
			lastHead->Super->Flags &= ~HD_EXPANDED;
	}
/*
	Dispose of selected headings
*/
	selHead->Prev = lastHead->Next = NULL;
	DisposeAllHeads(selHead,docData);
		
/*
	If there are siblings, bump up their label
*/
	if( head != NULL && reformat ) {
		BumpLevel(docData, head, oldNum - head->Num);
		if( prevHead != NULL )
			ReformatRestOfLevel(docData, head);
		else
			ReformatSubs(docData, head);
	}
	CountLines(docData);
	docData->SelHead = head;
	docData->SelStart = docData->SelEnd = docData->DragStart = head->BodyStart;
	return (TRUE);
}

/*
 *	Return pointer to copy of selected text
 */

TextPtr CopyText(DocDataPtr docData)
{
	TextPtr text, buff;
	WORD selStart, len;

	selStart = docData->SelStart;
	len = docData->SelEnd - selStart;
	text = docData->SelHead->Text;
	if (len <= 0 || (buff = NewPtr(len + 1)) == NULL)
		return (NULL);
	BlockMove(text + selStart, buff, len);
	buff[len] = '\0';
	return (buff);
}

/*
 *	Paste text into selected heading
 *	Return success status
 */

BOOL PasteText(DocDataPtr docData,
				TextPtr pasteText)
{
	TextPtr text;
	WORD len;
	HeadPtr selHead;

	selHead = docData->SelHead;
	len = strlen(pasteBuff);
	if (selHead->Len + len > MAX_TEXTLEN || !AdjustBuffer(selHead, len))
		return (FALSE);
	text = selHead->Text + docData->SelStart;
	BlockMove(text, text + len, selHead->Len - docData->SelStart);
	BlockMove(pasteText, text, len);
	selHead->Len += len;
	docData->SelStart += len;
	docData->SelEnd = docData->DragStart = docData->SelStart;
	return (TRUE);
}

/*
 *	Remove selected text from heading
 *	Return success status
 */

BOOL DeleteText(DocDataPtr docData)
{
	TextPtr text;
	WORD selStart, selEnd;
	HeadPtr selHead;

	selStart = docData->SelStart;
	selEnd = docData->SelEnd;
	if (selStart != selEnd) {
		selHead = docData->SelHead;
		text = selHead->Text;
		BlockMove(text + selEnd, text + selStart, selHead->Len - selEnd);
		(void) AdjustBuffer(selHead, (WORD) (selStart - selEnd));
		selHead->Len -= selEnd - selStart;
	}
	docData->SelEnd = docData->DragStart = selStart;
	return (TRUE);
}

/*
 *	Cut selected item
 */

BOOL DoCut(WindowPtr window)
{
	register BOOL success = TRUE ;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	
	SetBusyPointer(window);
	ClearPaste();
	CursorOff(window);
	selHead = docData->SelHead;
/*
	Handle cut of heading(s)
*/
	if (selHead->Flags & HD_SELECTED) {
		if ((pasteBuff = (Ptr) CopyHead(docData)) == NULL || !DeleteHead(docData))
			success = FALSE;
		else {
			pasteType = PASTE_HEAD;
			/* selHead = docData->SelHead; */
		}	
	} else {
/*
	Handle cut of text
*/
		if ((pasteBuff = (Ptr) CopyText(docData)) == NULL || !DeleteText(docData))
			success = FALSE;
		else {
			CalcLineStarts(docData, selHead);		/* My change selHead pointer */
			pasteType = PASTE_TEXT;
		}
	}
	if(!success) {
		CursorOn(window);
		Error(ERR_NO_MEM);
	} else {
		PutClipboard();
		CountLines(docData);
		DrawWindow(window);						/* Draws cursor */
		AdjustScrollBars(window);
		ScrollToCursor(window);
		DocumentModified(docData);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	SetPasteMenuItems();
	return(success);
}

/*
 *	Copy selected item
 */

BOOL DoCopy(WindowPtr window)
{ 
	register BOOL success = TRUE;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	SetBusyPointer(window);
	ClearPaste();
	selHead = docData->SelHead;
/*
	Handle copy of heading(s)
*/
	if (selHead->Flags & HD_SELECTED) {
		if ((pasteBuff = (Ptr) CopyHead(docData)) == NULL)
			success = FALSE;
		else
			pasteType = PASTE_HEAD;
	}
/*
	Handle copy of text
*/
	else {
		if ((pasteBuff = (Ptr) CopyText(docData)) == NULL)
			success = FALSE;
		else
			pasteType = PASTE_TEXT;
	}
	if(!success)
		Error(ERR_NO_MEM);
	else {
		PutClipboard();
	/*	ScrollToCursor(window); */
		SetEditMenu();
	}
	SetPasteMenuItems();
	
	return(success);
}

/*
 *	Paste item to selected insertion point
 */

BOOL DoPaste(WindowPtr window)
{
	register BOOL success = FALSE;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
		
	if (IFFParseBase == NULL && options.FullClipboard) {
		if ((IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4)) == NULL) {
			Error(ERR_NO_PARSE);
		}
	}
	else if (clipboard == NULL && options.FullClipboard) {
		InitClipboard();
	}
	if (clipboard) 
		GetClipboard(window);
	
	SetBusyPointer(window);
	if (pasteType == PASTE_NONE) {
		ErrBeep();
	} else {
		success = TRUE;
		CursorOff(window);
/*
	Handle paste of heading(s)
*/
		if (pasteType == PASTE_HEAD) {
			if (!PasteHead(docData, pasteBuff))
				success = FALSE;
		} else {
/*
	Handle paste of text
*/	
			selHead = docData->SelHead;
			if (selHead->Flags & HD_SELECTED) {
				UnSelectHeads(docData);
				docData->SelStart = selHead->BodyStart;
				docData->SelEnd = selHead->Len;
			}
			if (!DeleteText(docData) || !PasteText(docData, pasteBuff))
				success = FALSE;
			else
				CalcLineStarts(docData, docData->SelHead); /* May change selHead ptr */
		}
		if(!success) {
			CursorOn(window);
			Error(ERR_NO_MEM);
		} else {
/*
	Tell BumpLevel() to traverse entire tree constructing labels
*/
			docData->LabelFlags |= LABEL_CHANGED_MASK;
			RenumberHeads(docData);
			BumpLevel(docData, docData->FirstHead, 0);
			docData->LabelFlags &= ~LABEL_CHANGED_MASK;
			CountLines(docData);
			DrawWindow(window);						/* Draws cursor */
			AdjustScrollBars(window);
			ScrollToCursor(window);
			DocumentModified(docData);
			SetEditMenu();
			SetSearchMenu();
			SetSubHeadMenu();
			SetFormatMenu();
		}
	}
	return(success);
}

/*
 *	Erase selected item
 */

BOOL DoErase(WindowPtr window)
{
	register BOOL success = TRUE;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CursorOff(window);
	selHead = docData->SelHead;
/*
	Handle erase of heading(s)
*/
	if (selHead->Flags & HD_SELECTED) {
		
		if (!DeleteHead(docData))
			success = FALSE;
	} else {
/*
	Handle erase of text
*/
		if (!DeleteText(docData))
			success = FALSE;
		else
			CalcLineStarts(docData, selHead);	/* May change selHead pointer */
	}
	if(!success) {
		CursorOn(window);
		Error(ERR_NO_MEM);
	} else {
		CountLines(docData);
		DrawWindow(window);					/* Draws cursor */
		AdjustScrollBars(window);
		ScrollToCursor(window);
		DocumentModified(docData);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

/*
 *	Handle Change Case menu
 */

static BOOL DoChangeCaseMenu(WindowPtr window, UWORD item)
{
	register TextChar ch;
	register TextPtr text;
	register WORD loc;
	register WORD selStart, selEnd;
	DocDataPtr docData;
	register BOOL success = FALSE;
	register HeadPtr head;
	HeadPtr endHead;
		
	if( IsDocWindow(window)) {
		docData = (DocDataPtr) GetWRefCon(window);
		
		head = docData->SelHead;
		endHead = LastSelHead(head);
		selStart = docData->SelStart;
		selEnd = docData->SelEnd;
		
		if( head->Flags & HD_SELECTED ) {
			selEnd = head->Len;
			endHead = endHead->Next;
		}
		if( (selStart == selEnd) && ( (head->Flags & HD_SELECTED) == 0 ) )
			ErrBeep();
		else {
/*
	Change selection range to specified case
*/
			do {
				text = &head->Text[selStart];
				for (loc = selStart; loc < selEnd; loc++) {
					ch = *text;
					switch (item) {
					case UPPERCASE_SUBITEM:
						*text++ = toUpper[ch];
						break;
					case LOWERCASE_SUBITEM:
						*text++ = toLower[ch];
						break;
					case MIXEDCASE_SUBITEM:
						if (loc == 0 || !wordChar[*(text-1)])
							*text++ = toUpper[ch];
						else
							*text++ = toLower[ch];
						break;
					}
				}
				if( head != endHead ) {
					head = NextHead(head);
					if( head != NULL ) {
						selStart = head->BodyStart;
						selEnd = head->Len;
					}
				}
			} while( head != endHead );
/*
	Draw changes
*/
			DrawWindowRange(window, (WORD) VertPosition(window, docData->SelHead, docData->SelStart),
					(WORD) (window->Height - window->BorderBottom - 1));
			AdjustScrollBars(window);
			DocumentModified(docData);
			ScrollToCursor(window);
			success = TRUE;
		}
	}
	return( success );
}

/*
 *	Return character in specified cell
 */

TextChar CellChar(WORD i, WORD j)
{
	register TextChar ch;

	ch = i*8 + j + 0x20;
	if (ch >= 0x80)
		ch += 0x20;
	return (ch);
}

/*
 *	Get cell rectangle at given position
 */

void GetCellRect(DialogPtr dlg, WORD i, WORD j, register RectPtr rect)
{
	WORD width, height;
	GadgetPtr itemsGadg;

	itemsGadg = GadgetItem(dlg->FirstGadget, INSERT_ITEMS);
	width = itemsGadg->Width;
	height = itemsGadg->Height;
	rect->MinX = itemsGadg->LeftEdge + (i*width)/NUM_ACROSS;
	rect->MaxX = rect->MinX + width/NUM_ACROSS - 1;
	rect->MinY = itemsGadg->TopEdge + (j*height)/NUM_DOWN;
	rect->MaxY = rect->MinY + height/NUM_DOWN - 1;
}

/*
 *	Draw specified character at given location into items array
 */

void DrawInsertItem(DialogPtr dlg, WORD i, WORD j, BOOL selected)
{
	TextChar ch;
	WORD cellWidth, cellHeight, charWidth, charHeight, baseline;
	RastPtr rPort = dlg->RPort;
	TextFontPtr font;
	RegionPtr clipRgn, oldRgn;
	Rectangle rect;

	if (i < 0 || i >= NUM_ACROSS || j < 0 || j >= NUM_DOWN)
		return;
	ch = CellChar(i, j);
	if (ch == NBSP)
		ch = ' ';
	if (ch == SHYPH)
		ch = '-';
	font = rPort->Font;
	charWidth = TextLength(rPort, &ch, 1);
	charHeight = font->tf_YSize;
/*
	Clear the cell contents
*/
	GetCellRect(dlg, i, j, &rect);
	if (ch == DEL) {
		SetAPen(rPort, _tbPenDark);
		FillRect(rPort, &rect);
		return;
	}
	SetAPen(rPort, (selected) ? _tbPenDark : _tbPenLight);
	FillRect(rPort, &rect);
/*
	Display the character, clipping if it is larger than cell rect
*/
	cellWidth  = rect.MaxX - rect.MinX + 1;
	cellHeight = rect.MaxY - rect.MinY + 1;
	baseline = (cellHeight - charHeight)/2 + font->tf_Baseline;
	if (baseline < font->tf_Baseline)
		baseline = font->tf_Baseline;
	if (baseline >= cellHeight)
		baseline = cellHeight - 1;
	if (baseline - font->tf_Baseline < 0 ||
		baseline - font->tf_Baseline + charHeight >= cellHeight ||
		charWidth >= cellWidth) {
		if ((clipRgn = NewRegion()) != NULL) {
			ClearRegion(clipRgn);
			(void) OrRectRegion(clipRgn, &rect);
			oldRgn = InstallClipRegion(dlg->WLayer, clipRgn);
		}
	}
	else
		clipRgn = NULL;
	SetAPen(rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	Move(rPort, rect.MinX + (cellWidth - charWidth)/2, rect.MinY + baseline);
	Text(rPort, &ch, 1);
	if (clipRgn) {
		clipRgn = InstallClipRegion(dlg->WLayer, oldRgn);
		DisposeRegion(clipRgn);
	}
	DrawShadowBox(rPort, rect.MinX, rect.MinY, cellWidth, cellHeight, 0, !selected);
}

/*
 *	Insert literal character
 */

static BOOL DoInsertLiteral(WindowPtr window)
{
	BOOL success = TRUE;
	register WORD i, j;
	
	if( insertDialog == NULL ) {
		if( (insertDialog = GetDialog(dlgList[DLG_INSERT], screen, mainMsgPort)) == NULL ) {
			Error(ERR_NO_MEM);
			success = FALSE;
		} else {
			for (i = 0; i <= NUM_ACROSS; i++) {
				for (j=0; j <= NUM_DOWN; j++) {
					DrawInsertItem(insertDialog,i,j,FALSE);
				}
			}
		}
		CursorOn(window);
	} else {
		SelectWindow(insertDialog);
	}
	
	return(success);
}

/*
 * Insert date or time
 */

static BOOL DoInsertDateTime(WindowPtr window, TextChar ch)
{
	BOOL success = TRUE;
	register WORD i, len;
	BOOL oldAutoSpell;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	TextChar buff[50];
	
	DateStamp(&dateStamp);
	if( ch == DATE_CHAR )
		DateString( &dateStamp, (WORD) docData->DateStyle, buff);
	else	
		TimeString( &dateStamp, FALSE, (BOOL) docData->TimeStyle == TIMESTYLE_12HR, buff);
	len = strlen(buff);
	oldAutoSpell = options.AutoSpell;
	options.AutoSpell = FALSE;
	for( i = 0 ; i < len ; i++ )
		AddChar(window, buff[i]);
	options.AutoSpell = oldAutoSpell;
	return(success);
}

/*
 *	Insert space
 */

static BOOL DoInsertSpace(WindowPtr window)
{
	BOOL oldMode;

	oldMode = options.Typeover;
	UpdateChars();
	options.Typeover = FALSE;
	AddChar(window, ' ');
	UpdateChars();
	/* UndoOff(); */
	DoCursorLeft(window, 0);
	options.Typeover = oldMode;
	return (TRUE);
}

/*
 *	Handle insert submenu
 */

static BOOL DoInsertMenu(WindowPtr window, UWORD item, UWORD modifiers)
{
	BOOL success = FALSE;

	switch (item) {
	case LITERAL_SUBITEM:
		success = DoInsertLiteral(window);
		break;
	case DATE_SUBITEM:
		success = DoInsertDateTime(window, DATE_CHAR);
		break;
	case TIME_SUBITEM:
		success = DoInsertDateTime(window, TIME_CHAR);
		break;
	case SPACE_SUBITEM:
		DoInsertSpace(window);
		success = TRUE;
	default:
		break;
	}
	return(success);
}

/*
 *	Get special item formats
 */

BOOL DoItemFormats(WindowPtr window)
{
	register WORD item, oldItem;
	UBYTE dateStyle, timeStyle, pageStyle;
	register GadgetPtr gadget, gadgList;
	register DialogPtr dlg;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register WORD pageNum;
	register BOOL valid, done;
	BOOL success = TRUE;
	
/*
	Set up page numbering requester
*/
	DateStamp(&dateStamp);
	dateStyle = docData->DateStyle;
	timeStyle = docData->TimeStyle;
	pageStyle = docData->PageNumStyle;
	pageNum = -1;						/* Force redraw immediately */
	if (dateStyle < DATESTYLE_SHORT || dateStyle > DATESTYLE_MILITARY)
		dateStyle = DATESTYLE_SHORT;
	if (timeStyle != TIMESTYLE_12HR && timeStyle != TIMESTYLE_24HR)
		timeStyle = TIMESTYLE_12HR;
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_ITEMFORMATS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return(FALSE);
	}
	OutlineOKButton(dlg);
	
	gadgList = dlg->FirstGadget;
	for (item = DATESHORT_RADBTN; item <= DATEMIL_RADBTN; item++) {
		DateString(&dateStamp, (WORD) (item - DATESHORT_RADBTN + DATE_SHORT), strBuff);
		SetGadgetItemText(gadgList, item, dlg, NULL, strBuff);
	}
	SetGadgetItemValue(gadgList, (WORD) (pageStyle + PAGE1_RADBTN), dlg, NULL, 1);
	SetGadgetItemValue(gadgList, (WORD) (dateStyle + DATESHORT_RADBTN), dlg, NULL, 1);
	item = (timeStyle == TIMESTYLE_12HR) ? TIME12HR_RADBTN : TIME24HR_RADBTN;
	SetGadgetItemValue(gadgList, item, dlg, NULL, 1);
/*
	Handle requester
*/
	done = FALSE;
	do {
		if( pageNum < 0 ) {
			NumToString(docData->StartPage, strBuff);
			SetEditItemText(dlg->FirstGadget,STARTPAGE_TEXT,dlg,NULL,strBuff);
		}
		do {
			item = ModalDialog(mainMsgPort, dlg, DialogFilter);
			switch (item) {
			case PAGE1_RADBTN:
			case PAGEI_RADBTN:
			case PAGEi_RADBTN:
			case PAGEA_RADBTN:
			case PAGEa_RADBTN:
				oldItem = pageStyle + PAGE1_RADBTN;
				SetGadgetItemValue(gadgList, oldItem, dlg, NULL, 0);
				SetGadgetItemValue(gadgList, item, dlg, NULL, 1);
				pageStyle = item - PAGE1_RADBTN;
				break;
			case DATESHORT_RADBTN:
			case DATEABBR_RADBTN:
			case DATELONG_RADBTN:
			case DATEABBRDAY_RADBTN:
			case DATELONGDAY_RADBTN:
			case DATEMIL_RADBTN:
				oldItem = dateStyle + DATESHORT_RADBTN;
				SetGadgetItemValue(gadgList, oldItem, dlg, NULL, 0);
				SetGadgetItemValue(gadgList, item, dlg, NULL, 1);
				dateStyle = item - DATESHORT_RADBTN;
				break;
			case TIME12HR_RADBTN:
			case TIME24HR_RADBTN:
				oldItem = (timeStyle == TIMESTYLE_12HR) ? TIME12HR_RADBTN : TIME24HR_RADBTN;
				SetGadgetItemValue(gadgList, oldItem, dlg, NULL, 0);
				SetGadgetItemValue(gadgList, item, dlg, NULL, 1);
				timeStyle = (item == TIME12HR_RADBTN) ? TIMESTYLE_12HR : TIMESTYLE_24HR;
				break;
			case CANCEL_BUTTON:
				success = FALSE;
			case OK_BUTTON:
				done = TRUE;
			default:
				break;
			}
			gadget = GadgetItem(gadgList, STARTPAGE_TEXT);
			ActivateGadget(gadget, dlg, NULL);
			if( done && success ) {			/* Was OK button hit? */
				GetEditItemText(gadgList, STARTPAGE_TEXT, strBuff);
				valid = CheckNumber(strBuff);
				if( valid ) {
					pageNum = StringToNum(strBuff);
					valid = (pageNum >= 0) && (pageNum + docData->Pages < 9999);
				}
				if( !valid ) {
					Error(ERR_PAGE_NUM);
					pageNum = -1;
				}
			}
		} while( !done );
	} while( success && !valid );
	DisposeDialog(dlg);
	EndWait();
	if( success ) {
/*
	Save new values and redraw window
*/		docData->PageNumStyle = pageStyle;
		docData->DateStyle = dateStyle;
		docData->TimeStyle = timeStyle;
		docData->StartPage = pageNum;
		AdjustScrollBars(window);
		DocumentModified(docData);
	}
	return (TRUE);
}

/*
 *	Select current heading
 */

void DoSelectHead(WindowPtr window)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CursorOff(window);
	docData->SelHead->Flags |= HD_SELECTED;
	docData->SelStart = docData->SelEnd = docData->DragStart = docData->SelHead->BodyStart;
	CursorOn(window);
	ScrollToCursor(window);
	SetEditMenu();
	SetSubHeadMenu();
}

/*
 *	Select all headings at current level
 */

BOOL DoSelectAll(WindowPtr window)
{
	register HeadPtr head;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CursorOff(window);
	for (head = docData->SelHead; head->Prev; head = head->Prev) ;
	docData->SelHead = head;
	while (head) {
		head->Flags |= HD_SELECTED;
		head = head->Next;
	}
	docData->SelStart = docData->DragStart = docData->SelEnd = 0;
	CursorOn(window);
/*	ScrollToCursor(window); */
	SetEditMenu();
	SetSubHeadMenu();
	SetFormatMenu();
	return(TRUE);
}

/*
 *	Process Edit menu selection
 */

BOOL DoEditMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifiers)
{
	BOOL success = FALSE;
	
	if( IsDocWindow(window) || (item == PREFERENCES_ITEM ) || ( item == SCREENCOLORS_ITEM ) ){
	
		switch (item) {
		case CUT_ITEM:
			success = DoCut(window);
			break;
		case COPY_ITEM:
			success = DoCopy(window);
			break;
		case PASTE_ITEM:
			success = DoPaste(window);
			break;
		case ERASE_ITEM:
			success = DoErase(window);
			break;
		case CHANGECASE_ITEM:
			success = DoChangeCaseMenu(window, sub);
			break;
		case INSERT_ITEM:
			success = DoInsertMenu(window, sub, modifiers);
			break;
		case ITEMFORMATS_ITEM:
			success = DoItemFormats(window);
			break;
		case SELECTALL_ITEM:
			success = DoSelectAll(window);
			break;
		case PREFERENCES_ITEM:
			success = DoPreferences();
			break;
		case SCREENCOLORS_ITEM:
			success = DoScreenColors();
			break;
		}
	}
	return(success);
}
