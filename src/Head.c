/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Heading manipulation routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Memory.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	Return pointer to next visible heading from given heading, or NULL
 */

HeadPtr NextHead(HeadPtr head)
{
	if (head) {
		if ((head->Flags & HD_EXPANDED) && head->Sub)
			return (head->Sub);
		if (head->Next)
			return (head->Next);
		while (head = head->Super) {
			if (head->Next)
				return (head->Next);
		}
	}
	return (NULL);
}

/*
 *	Return pointer to next heading from given heading, or NULL
 *	Returns pointer to heading even if it is not currently visible
 */

HeadPtr NextHeadAll(HeadPtr head)
{
	if (head) {
		if (head->Sub)
			return (head->Sub);
		if (head->Next)
			return (head->Next);
		while (head = head->Super) {
			if (head->Next)
				return (head->Next);
		}
	}
	return (NULL);
}

/*
 *	Return pointer to previous visible heading from given heading, or NULL
 */

HeadPtr PrevHead(HeadPtr head)
{
	if (head) {
		if (head->Prev == NULL)
			return (head->Super);		/* May be NULL */
		head = head->Prev;
		while (head->Sub && (head->Flags & HD_EXPANDED)) {
			head = head->Sub;
			while (head->Next)
				head = head->Next;
		}
	}
	return (head);
}

/*
 *	Get number of visible subheading lines (of all levels) for given heading
 */

WORD NumSubLines(HeadPtr head)
{
	register WORD num;

	num = 0;
	if (head->Sub && (head->Flags & HD_EXPANDED)) {
		for (head = head->Sub; head; head = head->Next)
			num += NumSubLines(head) + head->NumLines;
	}
	return (num);
}

/*
 *	Return depth of specified heading (0 to ...)
 */

WORD HeadDepth(HeadPtr head)
{
	register WORD depth;

	if (head == NULL)
		return (0);
	depth = 0;
	while (head = head->Super)
		depth++;
	return (depth);
}

/*
 *	Find deepest depth of subheadings of given heading
 */

WORD SubDepth(HeadPtr head)
{
	register WORD depth, maxDepth;

	maxDepth = 0;
	for (head = head->Sub; head; head = head->Next) {
		depth = SubDepth(head) + 1;
		if (depth > maxDepth)
			maxDepth = depth;
	}
	return (maxDepth);
}

/*
 *	Swap heading with the one immediately following it
 */

void SwapHeads(HeadPtr head)
{
	register HeadPtr swapHead;
   WORD temp;
   
	if ((swapHead = head->Next) != NULL) {
	   if (head->Super && head->Super->Sub == head)
		   head->Super->Sub = swapHead;
	   if (head->Prev)
		   head->Prev->Next = swapHead;
	   if (swapHead->Next)
		   swapHead->Next->Prev = head;
	   head->Next = swapHead->Next;
	   swapHead->Prev = head->Prev;
	   head->Prev = swapHead;
	   swapHead->Next = head;
      temp = head->Num;
      head->Num = swapHead->Num;
      swapHead->Num = temp;
   }
}

/*
 *	Allocate new heading record, return pointer to it or NULL
 */

HeadPtr AllocHead()
{
	register HeadPtr newHead;

	if ((newHead = NewPtr(sizeof(Head))) != NULL) {
		BlockClear(newHead, sizeof(Head));
		newHead->NumLines = 1;
		newHead->BodyStart = 0;
	}
	return (newHead);
}

/*
 *	Deallocate heading record and all sub-ordinate heading records
 *	Relink pointers to remaining headings
 */

void DisposeHead(HeadPtr head,DocDataPtr docData)
{
	register HeadPtr subHead;

	if (head == NULL)
		return;
	if (docData) {
		if (head == docData->Mark.Head)
			docData->Mark.Head = NULL;
	}
/*
	Deallocate all subheads
	(Don't bother unlinking, since we will de-allocate anyway)
*/
	while (subHead = head->Sub) {
		head->Sub = subHead->Next;
		NextBusyPointer();
		DisposeHead(subHead,docData);
	}
/*
	Free memory used by this heading
*/
	if (head->Text)
		DisposePtr(head->Text);
	DisposePtr((Ptr) head);
}

/*
 *	Deallocate all headings from the given one
 */

void DisposeAllHeads(HeadPtr firstHead,DocDataPtr docData)
{
	register HeadPtr head;

	while (head = firstHead) {
		firstHead = head->Next;
		DisposeHead(head,docData);
	}
}

/*
 *	Make a duplicate copy of given heading (including all subheadings)
 *	Return NULL if heading could not be duplicated
 */

HeadPtr DupHead(HeadPtr head)
{
	register HeadPtr newHead, subHead;
	HeadPtr prevHead;

	if (head == NULL)
		return (NULL);
/*
	First duplicate the heading itself
*/
	if ((newHead = AllocHead()) == NULL)
		return (NULL);
	newHead->Flags = head->Flags;
	newHead->Style = head->Style;
	newHead->BodyStart = head->BodyStart;
	newHead->Num = head->Num;
	if (!AdjustBuffer(newHead, head->Len)) {
		DisposeHead(newHead,NULL);
		return (NULL);
	}
	BlockMove(head->Text, newHead->Text, head->Len);
	newHead->Len = head->Len;
/*
	Now duplicate its sub-headings
*/
	prevHead = NULL;
	for (head = head->Sub; head; head = head->Next) {
		if ((subHead = DupHead(head)) == NULL) {
			DisposeHead(newHead,NULL);
			return (NULL);
		}
		if (prevHead == NULL)
			newHead->Sub = subHead;
		else {
			prevHead->Next = subHead;
			subHead->Prev = prevHead;
		}
		subHead->Super = newHead;
		prevHead = subHead;
		NextBusyPointer();
	}
	return (newHead);
}

/*
 *	Duplicate a list of headings
 */

HeadPtr DupHeadList(HeadPtr head1, HeadPtr head2)
{
	register HeadPtr newHead;
	HeadPtr headList, addHead;

	headList = addHead = NULL;
	while (head1 && head1 != head2->Next) {
		if ((newHead = DupHead(head1)) == NULL) {
			DisposeAllHeads(headList,NULL);
			return (NULL);
		}
		if (addHead == NULL)
			headList = newHead;
		else {
			addHead->Next = newHead;
			newHead->Prev = addHead;
		}
		addHead = newHead;
		head1 = head1->Next;
	}
	return (headList);
}

/*
 *	Return pointer to last visible heading in outline
 */

HeadPtr LastHead(DocDataPtr docData)
{
	register HeadPtr head, nextHead;

	head = docData->FirstHead;
	while (nextHead = NextHead(head))
		head = nextHead;
	return (head);
}

/*
 *	Unselect selected headings
 */

void UnSelectSubHeads(HeadPtr head)
{
	for (head; head;	head = head->Next) {
		if (head->Sub)
			UnSelectSubHeads(head->Sub);
		head->Flags &= ~HD_SELECTED;
	}
}

/*
 *	Unselect selected headings
 */

void UnSelectHeads(DocDataPtr docData)
{
	register HeadPtr head;

	for (head = docData->FirstHead; head;	head = head->Next) {
		if (head->Sub)
			UnSelectSubHeads(head->Sub);
		head->Flags &= ~HD_SELECTED;
	}
	RefreshWindow(docData->Window);
}

/*
 *	Return pointer to last selected heading in a selection range
 */

HeadPtr LastSelHead(HeadPtr selHead)
{
	if (selHead->Flags & HD_SELECTED) {
		while (selHead->Next && (selHead->Next->Flags & HD_SELECTED))
			selHead = selHead->Next;
	}
	return (selHead);
}

/*
	Get first head in current level
*/

HeadPtr GetFirstHead( HeadPtr selHead )
{
	register HeadPtr head;
	
	for( head = selHead ; head->Prev != NULL ; head = head->Prev ) ;
	return( head );
}
		