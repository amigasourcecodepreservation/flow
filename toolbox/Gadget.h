/*
 *	Toolbox library
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Gadget template definitions
 */

#ifndef TOOLBOX_GADGET_H
#define TOOLBOX_GADGET_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TOOLBOX_MEMORY_H
#include <ToolBox/Memory.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Gadget template
 *
 *	LeftEdge and TopEdge are relative to upper left of containing item
 *	Type field is gadget type from list below (NOT intuition defines)
 *	Info field has following definition for each gadget type:
 *
 *	GADG_USER_ITEM			Pointer to Gadget structure
 *	GADG_PUSH_BUTTON		Pointer to text of item
 *	GADG_CHECK_BOX			"
 *	GADG_RADIO_BUTTON		"
 *	GADG_STAT_TEXT			"
 *	GADG_EDIT_TEXT			Pointer to initial text of item
 *	GADG_STAT_BORDER		Pointer to Border structure
 *	GADG_ACTIVE_BORDER		"
 *	GADG_STAT_IMAGE			Pointer to Image structure
 *	GADG_ACTIVE_IMAGE		"
 *	GADG_STAT_STDIMAGE		Item number of standard image
 *	GADG_ACTIVE_STDIMAGE	"
 *	GADG_PROP_HORIZ			Ignored
 *	GADG_PROP_VERT			"
 *
 *	GetGadgets does not make a copy of these data fields, but uses them directly
 *
 *	LeftEdge, TopEdge, Width, and Height are relative to current font x & y size,
 *		normalized to 10 pixels for an 8 by 11 pixel font
 *
 *	Offsets are absolute values that are added to the relative values
 *
 *	If LeftEdge and/or TopEdge are negative, then gadget is specified to be
 *		GRELRIGHT and/or GRELBOTTOM respectively
 *
 *	If Width and/or Height are negative, then gadget is specified to be
 *		GRELWIDTH and/or GRELHEIGHT respectively
 *
 *	The actual template is an array of items, with the last item having
 *		a type of GADG_ITEM_NONE
 */

typedef struct {
	UWORD		Type;
	WORD		LeftEdge, TopEdge, LeftOffset, TopOffset;
	WORD		Width, Height, WidthOffset, HeightOffset;
	TextChar	KeyEquiv;
	UBYTE		pad;
	Ptr			Info;
} GadgetTemplate, *GadgTemplPtr;

/*
 *	Gadget types
 *	(Note: these values are octal)
 */

#define GADG_TYPEBITS			0xFF

#define GADG_USER_ITEM			000

#define GADG_PUSH_BUTTON		010
#define GADG_CHECK_BOX			011
#define GADG_RADIO_BUTTON		012

#define GADG_STAT_TEXT			020

#define GADG_EDIT_TEXT			030

#define GADG_STAT_BORDER		040
#define GADG_ACTIVE_BORDER		041
#define GADG_STAT_IMAGE			042
#define GADG_ACTIVE_IMAGE		043
#define GADG_STAT_STDBORDER		044
#define GADG_ACTIVE_STDBORDER	045
#define GADG_STAT_STDIMAGE		046
#define GADG_ACTIVE_STDIMAGE	047

#define GADG_PROP_VERT			050
#define GADG_PROP_HORIZ			051

/*
 *	Gadget flags (contained in the Type field)
 */

#define GADG_FLAGBITS			0xFF00

#define GADG_PROP_NOBORDER		0x100		/* Flags for PROP gadgets */
#define GADG_PROP_NEWLOOK		0x200

#define GADG_EDIT_NUMONLY		0x100		/* Flags for EDIT_TEXT gadgets */
#define GADG_EDIT_RETURNCYCLE	0x200

/*
 *	This item type identifies the end of the gadget template array
 */

#define GADG_ITEM_NONE		0xFFFF

/*
 *	Size of buffer needed for all string gadgets (type GADG_EDIT_TEXT)
 */

#define GADG_MAX_STRING		100

/*
 *	Prototypes
 */

GadgetPtr	GetGadgets(GadgTemplPtr);
void		DisposeGadgets(GadgetPtr);

GadgetPtr	GadgetItem(GadgetPtr, WORD);
WORD	GadgetNumber(GadgetPtr);
WORD	GadgetType(GadgetPtr);

WORD	GetGadgetValue(GadgetPtr);
void	SetGadgetValue(GadgetPtr, WindowPtr, RequestPtr, BOOL);
void	SetGadgetItemValue(GadgetPtr, WORD, WindowPtr, RequestPtr, BOOL);

void	HiliteGadget(GadgetPtr, WindowPtr, RequestPtr, BOOL);
BOOL	GadgetSelected(GadgetPtr, WORD);

void	OnOffGList(GadgetPtr, WindowPtr, RequestPtr, WORD, BOOL);
void	OnGList(GadgetPtr, WindowPtr, RequestPtr, WORD);
void	OffGList(GadgetPtr, WindowPtr, RequestPtr, WORD);
void	EnableGadgetItem(GadgetPtr, WORD, WindowPtr, RequestPtr, WORD);

void	OutlineButton(GadgetPtr, WindowPtr, RequestPtr, BOOL);

void	GetEditItemText(GadgetPtr, WORD, TextPtr);
void	SetEditItemText(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr);

void	GetGadgetItemText(GadgetPtr, WORD, TextPtr);
void	SetGadgetText(GadgetPtr, WindowPtr, RequestPtr, TextPtr);
void	SetGadgetItemText(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr);
void	SetButtonItem(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr, TextChar);

void	GetGadgetRect(GadgetPtr, WindowPtr, RequestPtr, RectPtr);

void	TrackGadget(MsgPortPtr, WindowPtr, GadgetPtr, void (*)(WindowPtr, WORD));

BOOL	IsEditReturnCycle(GadgetPtr);
BOOL	IsGadgetKey(GadgetPtr, TextChar);

#endif
