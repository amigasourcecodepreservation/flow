/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Rexx/rxslib.h>

#include <IFF/WORD.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct RexxLib	*RexxSysBase;

extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPortPtr	appIconMsgPort;
extern MsgPort		monitorMsgPort;		/* Not a pointer! */
extern MsgPort		rexxMsgPort;

extern WindowPtr	windowList[];
extern WORD			numWindows;

extern WindowPtr	closeWindow;
extern BOOL			closeFlag, closeAllFlag, quitFlag;

extern DialogPtr	spellDialog;

extern struct DateStamp	dateStamp;

extern BOOL	inMacro;

extern BOOL	cursorRepeat;

extern BOOL	busy, titleChanged, doubleClick, tripleClick;

extern FKey	fKeyTable1[], fKeyTable2[];

extern BOOL	_tbOnPubScreen;

extern LONG	isClip;

/*
 *	Local variables and definitions
 */

#define RAWKEY_TAB	0x42
#define RAWKEY_F1	0x50
#define RAWKEY_F10	0x59
#define RAWKEY_HELP	0x5F

static BOOL	newPrefs;		/* Inited to FALSE */

#define MOUSEBUTTON(q)	\
	((q & (IEQUALIFIER_LEFTBUTTON | IEQUALIFIER_RBUTTON)) || ((q & AMIGAKEYS) && (q & (IEQUALIFIER_LALT | IEQUALIFIER_RALT))))

/*
 *	Local prototypes
 */

void	DoNewPrefs(void);

void	DoFunctionKey(WindowPtr, UWORD, UWORD);
void	DoRawKey(WindowPtr, UWORD, UWORD);
void	DoVanillaKey(WindowPtr, UWORD, UWORD);
void	DoDialogMessage(IntuiMsgPtr);

/*
 *	Main program
 */

void main(int argc, char *argv[])
{
	WORD mouseX, mouseY;
	UWORD modifier;
	LONG waitSigs;
	ULONG secs, micros, userSecs;
	BOOL isTick;
	register IntuiMsgPtr intuiMsg;
	struct RexxMsg *rexxMsg;
	struct AppMessage *appMsg;

/*
	Initialize, open document window, and start new document
*/
	Init(argc, argv);
	DateStamp(&dateStamp);	/* Need this in case new documents use date/time */
	SetUp(argc, argv);
	SetViewMenu();
/*
	Do auto exec macro
*/
	DoAutoExec(); 
/*
	Get and respond to messages
*/
	quitFlag = FALSE;
	mouseX = mouseY = -1;
	CurrentTime(&userSecs, &micros);
	for (;;) {
/*
	Process intuition messages
*/
		while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
			isTick = (intuiMsg->Class == INTUITICKS);
			if (!isTick || intuiVersion >= OSVERSION_2_0)
				modifier = intuiMsg->Qualifier;
			if (intuiMsg->Class == SIZEVERIFY) {
				UpdateChars();
				CursorOn(intuiMsg->IDCMPWindow);
				ReplyMsg((MsgPtr) intuiMsg);
			}
			 else {
				if (IsDialogMsg(intuiMsg))
					DoDialogMessage(intuiMsg);
				else	
					DoIntuiMessage(intuiMsg);
			}
			if (!isTick)	
				CurrentTime(&userSecs, &micros);
		}
/*
	Process REXX messages
	Only do one Rexx message per pass through main loop, so that all windows
		are opened/closed and program is in correct state
*/
		if (RexxSysBase &&
			(rexxMsg = (struct RexxMsg *) GetMsg(&rexxMsgPort)) != NULL)
			DoRexxMsg(rexxMsg);
/*
	Background handling
*/
		UpdateChars();
		UpdateWindows();
/*
	Check for mouse move
*/
		if ((screen->MouseX != mouseX || screen->MouseY != mouseY) &&
			!closeFlag && !closeAllFlag && !quitFlag &&
			(intuiVersion < OSVERSION_2_0 || !MOUSEBUTTON(modifier))) {
			mouseX = screen->MouseX;
			mouseY = screen->MouseY;
			SetPointerShape();
		}
/*
	Respond to NEWPREFS message (don't want to do this in dialogs)
*/
		if (newPrefs) {
			DoNewPrefs();
			newPrefs = FALSE;
		}
/*
	Respond to close window message
*/
		if (closeFlag) {
			if (GetWKind(closeWindow) == WKIND_DIALOG)
				DoDialogClose(closeWindow);
			else
				(void) DoClose(closeWindow);
			closeFlag = FALSE;
		}
		if (closeAllFlag)	{
			DoDialogCloseAll();
			closeAllFlag = (numWindows) ?
						   DoClose(windowList[numWindows - 1]) : FALSE;
		}
/*
	For quitting, close one window then respond to any messages
*/
		if (quitFlag) {
			if (numWindows)
				quitFlag = DoClose(windowList[numWindows - 1]);
			else	{ 
				DoDialogCloseAll();
				if (!inMacro)		/* Wait for macro reply msg before quitting */
				break;
			}
		}
/*
	If all windows are closed, and we are on a public screen, then quit
*/
		if (numWindows == 0 && _tbOnPubScreen)
			break;
/*
	Continue with main loop if closing all windows or quitting
*/
		if (closeAllFlag || quitFlag)
			continue;
/*
	Handle AppIcon messages
*/
		if (!inMacro && appIconMsgPort) {
			while ((appMsg = (struct AppMessage *) GetMsg(appIconMsgPort)) != NULL)
				DoAppMsg(appMsg);
		}
/*
	Check for auto-saves (only after 15 or more seconds of no user activity)
*/
		CurrentTime(&secs, &micros);
		if (secs > userSecs + 15) {
			CheckAutoSave();
			userSecs = secs;		/* In case they cancel a "Save As" requester */
		}
/*
	Check for clipboard ID
*/
	isClip = CurrentClipID();
	SetEditMenu();
/*
	If wait for messages
*/
		waitSigs = 1 << mainMsgPort->mp_SigBit;
		if (RexxSysBase)
			waitSigs |= 1 << rexxMsgPort.mp_SigBit;
		if (appIconMsgPort)
			waitSigs |= 1 << appIconMsgPort->mp_SigBit;
		Wait(waitSigs);
	}
/*
	Quitting
*/
	SetStdPointer(backWindow, POINTER_WAIT);
	ClearPaste();
	ShutDown();
}

/*
 *	Handle change in system preferences
 */

static void DoNewPrefs()
{
	WORD i;
	DocDataPtr docData;
	WindowPtr window;
	Rectangle rect;

	if (GetSysPrefs()) {
		BeginWait();
		for (i = 0; i < numWindows; i++) {
			window = windowList[i];
			docData = GetWRefCon(window);
			PrValidate(docData->PrintRec);
			GetWindowRect(window, &rect);
			InvalRect(window, &rect);
			AdjustScrollBars(window);
		}
		EndWait();
	}
}

/*
 *	Process intuition messages
 */

void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG class;
	register UWORD code;
	register APTR  iAddress;
	register WORD mouseX, mouseY;
	ULONG seconds, micros;
	register UWORD modifier;
	register WindowPtr window;

	if (intuiMsg->Class == RAWKEY)
		ConvertKeyMsg(intuiMsg);	/* Convert some RAWKEY to VANILLAKEY */
	class = intuiMsg->Class;
	code = intuiMsg->Code;
	iAddress = intuiMsg->IAddress;
	mouseX = intuiMsg->MouseX;
	mouseY = intuiMsg->MouseY;
	seconds = intuiMsg->Seconds;
	micros = intuiMsg->Micros;
	modifier = intuiMsg->Qualifier;
	window = intuiMsg->IDCMPWindow;
	ReplyMsg((MsgPtr) intuiMsg);
/*
	Ignore key up messages
*/
	if (class == RAWKEY && (code & IECODE_UP_PREFIX))
		return;
/*
	Ignore user input if processing macro
*/
	if (inMacro && class != NEWSIZE && class != REFRESHWINDOW &&
		class != ACTIVEWINDOW && class != INACTIVEWINDOW && class != NEWPREFS)
		return;
/*
	If not a key message then update typed character display
*/
	if (class != VANILLAKEY && class != INTUITICKS)
		UpdateChars();
/*
	If not a cursor key then turn off cursor repeat
*/
	if (class != INTUITICKS &&
		(class != RAWKEY || (code != CURSORUP && code != CURSORDOWN)))
		cursorRepeat = FALSE;
/*
	Process the message
*/
	switch (class) {
	case INTUITICKS:
		if (!inMacro)
			CursorBlink(window);
		break;
	case MOUSEBUTTONS:
		if (code == SELECTDOWN)
			DoMouseDown(window, modifier, mouseX, mouseY, seconds, micros);
		break;
	case GADGETDOWN:
		DoGadgetDown(window, (GadgetPtr) iAddress, modifier);
		break;
	case GADGETUP:
		DoGadgetUp(window, (GadgetPtr) iAddress);
		break;
	case NEWSIZE:
		DoNewSize(window);
		break;
	case REFRESHWINDOW:
		DoWindowRefresh(window);
		break;
	case ACTIVEWINDOW:
		DoWindowActivate(window, TRUE);
		break;
	case INACTIVEWINDOW:
		DoWindowActivate(window, FALSE);
		break;
	case CLOSEWINDOW:
		DoGoAwayWindow(window, modifier);
		break;
	case MENUPICK:
		(void) DoMenu(window, code, modifier, TRUE);
		break;
	case RAWKEY:
		DoRawKey(window, code, modifier);
		break;
	case VANILLAKEY:
		DoVanillaKey(window, code, modifier);
		break;
	case NEWPREFS:
		newPrefs = TRUE;
		break;
	}
}

/*
 *	Process dialog messages
 */

void DoDialogMessage(IntuiMsgPtr intuiMsg)
{
	register ULONG class;
	register UWORD code;
	register UWORD modifier;
	register WindowPtr window;
	DialogPtr dlg;
	WORD item;
		
	if (intuiMsg->Class == RAWKEY)
		ConvertKeyMsg(intuiMsg);		/* Convert some RAWKEY to VANILLAKEY */
	class	= intuiMsg->Class;
	code	 = intuiMsg->Code;
	if (class != INTUITICKS)
		modifier = intuiMsg->Qualifier;
	window	= intuiMsg->IDCMPWindow;
	
	if( inMacro )
		ReplyMsg((MsgPtr) intuiMsg);
	else {
		if( DialogSelect(intuiMsg, &dlg, &item) ) {
			DoDialogItem(dlg, intuiMsg, item);
		} else {
			if( class == GADGETUP ) {
				DoDialogGadgetUp(window, intuiMsg);
			} else if( class == GADGETDOWN ) {
				DoDialogGadgetDown(window, intuiMsg);
			} else {
				ReplyMsg((MsgPtr) intuiMsg);
			}
		}
	}
/*
	Process the message
*/
	if( class == RAWKEY && (code & IECODE_UP_PREFIX))
		return;
	
/*
	Ignore user input if processing macro
*/
	if( inMacro && class != NEWSIZE && class != REFRESHWINDOW &&
		class != ACTIVEWINDOW && class != INACTIVEWINDOW && class != NEWPREFS)
		return;
/*
	If not a key message then update typed character display
*/
	if( class != VANILLAKEY && class != INTUITICKS)
		UpdateChars();
/*
	If not a cursor key then turn off cursor repeat
*/
	if( window != NULL ) {
/*
	Process the message
*/
		
		switch (class) {
		case INTUITICKS:
			DoDialogIdle(window);	/* Periodic check, usually for empty string gadget */
			break;
		case ACTIVEWINDOW:
			DoDialogActivate(window, TRUE);
			break;
		case INACTIVEWINDOW:
			DoDialogActivate(window, FALSE);
			break;
		case CLOSEWINDOW:
			(void) DoGoAwayWindow(window, modifier); 
			break;
		case NEWPREFS:
			newPrefs = TRUE;
			break;
		case MENUPICK:
			DoDialogActivate(window, TRUE);	/* Reactivate string gadget */
			(void) DoMenu(window, code, modifier, TRUE);
			break;
		case RAWKEY:
			(void) DoRawKey(window, code, modifier);
			break;
		case VANILLAKEY:
		case MOUSEBUTTONS:
		case NEWSIZE:
		case REFRESHWINDOW:
		default:
			break;
		}
	}
}	

/*
 *	Handle menu events
 */

BOOL DoMenu(WindowPtr window, UWORD menuNumber, UWORD qualifier, BOOL multiSel )
{
	register UWORD menu, item, sub;
	BOOL success;
	
	if (titleChanged)
		FixTitle();
	success = FALSE;	 
	while (menuNumber != MENUNULL) {
		menu = MENUNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		sub = SUBNUM(menuNumber);
		switch (menu) {
		case PROJECT_MENU:
			success = DoProjectMenu(window, item, sub, qualifier);
			break;
		case EDIT_MENU:
			success = DoEditMenu(window, item, sub, qualifier );
			break;
		case SEARCH_MENU:
			success = DoSearchMenu(window, item, sub, qualifier);
			break;
		case HEADING_MENU:
			success = DoSubHeadMenu(window, item, sub);
			break;
		case FORMAT_MENU:
			success = DoStyleMenu(window, item, sub);
			break;
		case VIEW_MENU:
			success = DoViewMenu(window, item, sub);
			break;
		case MACRO_MENU:
			success = DoMacroMenu(window, item, sub);
			break;
		}
		menuNumber = multiSel ? ItemAddress(window->MenuStrip, menuNumber)->NextSelect : MENUNULL;
	}
/*
	Set pointer appearance
*/
	if(IsDocWindow(window)) {
		if (qualifier & AMIGARIGHT)
			ObscurePointer(window);
		else
			SetPointerShape();
	}
	return(success);
}

/*
 *	Process cursor key
 */

BOOL DoCursorKey(WindowPtr window, UWORD code, UWORD modifier)
{
	BOOL success;
	
	if (IsDocWindow(window))	{
		ObscurePointer(window);
		success = FALSE;
		switch (code) {
			case CURSORUP:
			success = DoCursorUp(window, modifier);
			break;
		case CURSORDOWN:
			success = DoCursorDown(window, modifier);
			break;
		case CURSORLEFT:
			success = DoCursorLeft(window, modifier);
			break;
		case CURSORRIGHT:
			success = DoCursorRight(window, modifier);
			break;
		}
	}
	return (success);
}

/*
 *	Process function key
 */

static void DoFunctionKey(WindowPtr window, UWORD code, UWORD modifier)
{
	FKey *fKeyTable;
	
	if( IsDocWindow(window) || (window == backWindow) ) {
		if( window != backWindow )
			ObscurePointer(window);
		code -= RAWKEY_F1;
		fKeyTable = (modifier & SHIFTKEYS) ? fKeyTable2 : fKeyTable1;
/*
 * For backwards flow compatibility, keep F10 as calling DoSelectHead()
 */
		if( code != (RAWKEY_F10-RAWKEY_F1) || modifier & SHIFTKEYS ) {
			if( fKeyTable[code].Type == FKEY_MENU) {
				(void) DoMenu(window, (UWORD) fKeyTable[code].Data, modifier & ~SHIFTKEYS, FALSE);
			} else if( (fKeyTable[code].Type == FKEY_MACRO) && RexxSysBase ) {
				DoMacro( fKeyTable[code].Data, NULL);
			}
		} else {
			DoSelectHead(window);
		}
	}
}

/*
 *	Handle RAWKEY messages
 *	ASCII keys will have already been stripped by ConvertKeyMsg()
 */

static void DoRawKey(WindowPtr window, UWORD code, UWORD modifier)
{
	ResetMultiClick();
	if ((code & IECODE_UP_PREFIX) || (modifier & AMIGAKEYS))
		return;
	busy = TRUE;
/*
	Check for function keys
*/
	if (code >= RAWKEY_F1 && code <= RAWKEY_F10) {
		if ((modifier & IEQUALIFIER_REPEAT) == 0)
			DoFunctionKey(window, code, modifier);
	}
/*
	Check for HELP key
*/
	else if (code == RAWKEY_HELP) {
		if (modifier & SHIFTKEYS)
			DoPreferences();
		else
			DoHelp(window);
		SetPointerShape();
	}
/*
	Ignore all others for non-document windows
*/
	else if (IsDocWindow(window)) {
/*
	Check for TAB key
*/
		if (code == RAWKEY_TAB && (modifier & SHIFTKEYS) ) {
			DoUnIndent(window);
		}
/*
	Check for cursor keys
*/
		else if (code >= CURSORUP && code <= CURSORLEFT)
			(void) DoCursorKey(window, code, modifier);
/*
	Otherwise is invalid key
*/
		else
			ErrBeep();
	} else if (window == spellDialog && (code == CURSORUP || CURSORDOWN))
		DoSpellDialogKey(window,code);
	busy = FALSE;
}

/*
 *	Handle VANILLAKEY messages
 *	These messages are synthesized from RAWKEY messages by ConvertKeyMsg()
 */

static void DoVanillaKey(WindowPtr window, UWORD code, UWORD modifier)
{
	doubleClick = FALSE;
	tripleClick = TRUE;
	if( IsDocWindow(window) ) {
		busy = TRUE;
		ObscurePointer(window);
		
/*
	Check for RETURN key
*/
		switch(code) {
		case CR:
			(void) NewHead(window, modifier);
			break;
		case TAB:
			DoIndent(window);
			break;
		case BS:
		case DEL:
			UpdateChars();
			(void) DoDelete(window, (TextChar) code, modifier);
			break;
		default:
			if( code == '-' && ( modifier & ALTKEYS ) ) {
				AddChar(window, (TextChar) SHYPH);
			} else if ((code >= 0x20 && code < 0x7F) || (code >= 0xA0 && code <= 0xFF)) {
				AddChar(window, (TextChar) code);
			} else {
				UpdateChars();
				ErrBeep();
			}
		}
		busy = FALSE;
	}
}
