/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Color picker routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight;

extern DialogTemplate	_colorDlgTempl;

/*
 *	Local variables and definitions
 */

enum {
	SAMPLE_USERITEM = 2,
	HUE_SLIDER,		BRIGHT_SLIDER,		SATURATE_SLIDER,
	REDARROW_UP,	REDARROW_DOWN,		RED_TEXT,
	GREENARROW_UP,	GREENARROW_DOWN,	GREEN_TEXT,
	BLUEARROW_UP,	BLUEARROW_DOWN,		BLUE_TEXT,
	PROMPT_TEXT
};

static DialogPtr	colorDlg;
static BOOL			(*dialogFilter)(IntuiMsgPtr, WORD *);

/*
 *	Local prototypes
 */

WORD	Max3(WORD, WORD, WORD);
WORD	Min3(WORD, WORD, WORD);
void	RGBtoHSV(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);
void	HSVtoRGB(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);

void	AdjustColorSliders(DialogPtr, WORD, WORD, WORD);
void	GetColorSliders(DialogPtr, WORD *, WORD *, WORD *);
void	ShowColorValues(DialogPtr, WORD, WORD, WORD);
void	SetRGBColor(ScreenPtr, WORD, RGBColor, RGBColor (*)(RastPtr, RGBColor));
BOOL	ColorDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Return maximum of three values
 */

static WORD Max3(register WORD a, register WORD b, register WORD c)
{
	register WORD max;

	max = a;
	if (max < b) max = b;
	if (max < c) max = c;
	return (max);
}

/*
 *	Return minimum of three values
 */

static WORD Min3(register WORD a, register WORD b, register WORD c)
{
	register WORD min;

	min = a;
	if (min > b) min = b;
	if (min > c) min = c;
	return (min);
}

/*
 *	Convert from rgb values (in range 0-15)
 *		to hsv (h in range 0-0xFFFF; s, v in range 0-15)
 */

static void RGBtoHSV(UWORD r, UWORD g, UWORD b, UWORD *pH, UWORD *pS, UWORD *pV)
{
	register UWORD v, s, m;
	register LONG r1, g1, b1;
	LONG h;

	v = Max3(r, g, b);
	m = Min3(r, g, b);
	s = (v - m)*15; s = (v) ? s/v : 0;
	if (s) {
		r1 = (v - r)*0x10000L; r1 /= (v - m);
		g1 = (v - g)*0x10000L; g1 /= (v - m);
		b1 = (v - b)*0x10000L; b1 /= (v - m);
		if (v == r)
			h = (m == g) ? 0x50000L + b1 : 0x10000L - g1;
		else if (v == g)
			h = (m == b) ? 0x10000L + r1 : 0x30000L - b1;
		else		/* v == b */
			h = (m == r) ? 0x30000L + g1 : 0x50000L - r1;
	}
	else
		h = 0;				/* Undefined hue */
	*pH = (h + 3)/6; *pS = s; *pV = v;
}

/*
 *	Convert hsv to rgb values
 */

static void HSVtoRGB(UWORD h, UWORD s, UWORD v, UWORD *pR, UWORD *pG, UWORD *pB)
{
	register UWORD r, g, b;
	register LONG p1, p2, p3;
	LONG i, f;

	i = h*6; i /= 0x10000L;		/* Don't round */
	f = h*6 - i*0x10000L;
	p1 = v*(15 - s);						p1 = (p1 + 7)/15;
	p2 = v*(15*0x10000L - s*f);				p2 = (p2 + 15*0x8000L)/(15*0x10000L);
	p3 = v*(15*0x10000L - s*(0x10000L - f));p3 = (p3 + 15*0x8000L)/(15*0x10000L);
	switch (i) {
	case 0:
		r = v;  g = p3; b = p1;
		break;
	case 1:
		r = p2; g = v;  b = p1;
		break;
	case 2:
		r = p1; g = v;  b = p3;
		break;
	case 3:
		r = p1; g = p2; b = v;
		break;
	case 4:
		r = p3; g = p1; b = v;
		break;
	case 5:
		r = v;  g = p1; b = p2;
		break;
	}
	*pR = r; *pG = g; *pB = b;
}

/*
 *	Adjust Color and Brightness sliders to actual color
 */

static void AdjustColorSliders(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	WORD hue, sat, value; /* Need to take address of these */
	LONG newPot, newBody;
	GadgetPtr gadget, gadgList;
	PropInfoPtr propInfo;

	gadgList = dlg->FirstGadget;
	RGBtoHSV(red, green, blue, &hue, &sat, &value);
/*
	Adjust color slider
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	if (sat == 0) {
		newPot = 0xFFFF;
		newBody = 0xFFFF;
	}
	else {
		newPot = hue;
		newBody = 0x1111;
	}
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust brightness slider
*/
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = value*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust saturation slider
*/
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = sat*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
 	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
}

/*
 *	Get color slider settings
 */

static void GetColorSliders(DialogPtr dlg, WORD *red, WORD *green, WORD *blue)
{
	WORD hue, sat, value;
	GadgetPtr gadget, gadgList;
	PropInfoPtr propInfo;

	gadgList = dlg->FirstGadget;
/*
	Get new color values
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	hue = propInfo->HorizPot;
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	sat = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	value = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	HSVtoRGB(hue, sat, value, red, green, blue);
}

/*
 *	Show new rgb values (and enable/disable arrows)
 */

static void ShowColorValues(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	register GadgetPtr gadget, gadgList;
	TextChar text[10];

	gadgList = dlg->FirstGadget;
	NumToString(red, text);
	gadget = GadgetItem(gadgList, RED_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(green, text);
	gadget = GadgetItem(gadgList, GREEN_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(blue, text);
	gadget = GadgetItem(gadgList, BLUE_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	EnableGadgetItem(gadgList, REDARROW_UP, dlg, NULL, (red < 15));
	EnableGadgetItem(gadgList, REDARROW_DOWN, dlg, NULL, (red > 0));
	EnableGadgetItem(gadgList, GREENARROW_UP, dlg, NULL, (green < 15));
	EnableGadgetItem(gadgList, GREENARROW_DOWN, dlg, NULL, (green > 0));
	EnableGadgetItem(gadgList, BLUEARROW_UP, dlg, NULL, (blue < 15));
	EnableGadgetItem(gadgList, BLUEARROW_DOWN, dlg, NULL, (blue > 0));
}

/*
 *	Set specified color register to given RGB color
 *	Color given is color corrected
 */

static void SetRGBColor(ScreenPtr scrn, WORD colorNum, RGBColor rgbColor,
						RGBColor (*colorCorrect)(RastPtr, RGBColor))
{
	if (colorCorrect)
		rgbColor = (*colorCorrect)(colorDlg->RPort, rgbColor);
	SetRGB4(&scrn->ViewPort, colorNum, RED(rgbColor), GREEN(rgbColor), BLUE(rgbColor));
}

/*
 *	Color dialog filter
 */

static BOOL ColorDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == colorDlg &&
		(class == IDCMP_GADGETDOWN || class == IDCMP_GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == REDARROW_UP || itemHit == REDARROW_DOWN ||
			itemHit == GREENARROW_UP || itemHit == GREENARROW_DOWN ||
			itemHit == BLUEARROW_UP || itemHit == BLUEARROW_DOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == IDCMP_GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return ((*dialogFilter)(intuiMsg, item));
}

/*
 *	Get new color
 *	A penNum of -1 means to choose one
 *	If an actual penNum is specified, don't reset color on exit
 */

BOOL GetColor(ScreenPtr scrn, MsgPortPtr msgPort,
			  BOOL (*dlgFilter)(IntuiMsgPtr, WORD *), TextPtr prompt,
			  RGBColor (*colorCorrect)(RastPtr, RGBColor),
			  RGBColor inColor, RGBColor *outColor, WORD penNum)
{
	WORD item, numColors, red, green, blue, width, height;
	BOOL done, hasPen;
	RGBColor origColor, newColor;
	GadgetPtr gadget, gadgList, hueGadg, briGadg, satGadg;
	RastPtr rPort;
	Rectangle rect;

	dialogFilter = dlgFilter;
	red = RED(inColor);
	green = GREEN(inColor);
	blue = BLUE(inColor);
	numColors = 1 << scrn->BitMap.Depth;
	if (penNum == -1) {
		hasPen = FALSE;
		penNum = (numColors >= 8) ? numColors - 3 : numColors - 1;
	}
	else
		hasPen = TRUE;
	origColor = GetRGB4(scrn->ViewPort.ColorMap, penNum);
/*
	Get dialog
*/
	if ((colorDlg = GetDialog(&_colorDlgTempl, scrn, msgPort)) == NULL)
		return (FALSE);
	OutlineButton(GadgetItem(colorDlg->FirstGadget, 0), colorDlg, NULL, TRUE);
	SetRGBColor(scrn, penNum, inColor, colorCorrect);
/*
	Draw dialog contents
*/
	rPort = colorDlg->RPort;
	gadgList = colorDlg->FirstGadget;
	gadget = GadgetItem(gadgList, PROMPT_TEXT);
	SetGadgetText(gadget, colorDlg, NULL, prompt);
	gadget = GadgetItem(gadgList, SAMPLE_USERITEM);
	GetGadgetRect(gadget, colorDlg, NULL, &rect);
	InsetRect(&rect, 2, 2);
	SetAPen(rPort, penNum);
	FillRect(rPort, &rect);
	ShowColorValues(colorDlg, red, green, blue);
	AdjustColorSliders(colorDlg, red, green, blue);
	hueGadg = GadgetItem(gadgList, HUE_SLIDER);
	briGadg = GadgetItem(gadgList, BRIGHT_SLIDER);
	satGadg = GadgetItem(gadgList, SATURATE_SLIDER);
	width 	= hueGadg->Width;
	height	= hueGadg->Height;
	DrawShadowBox(rPort, hueGadg->LeftEdge, hueGadg->TopEdge, width, height, -2, TRUE);
	DrawShadowBox(rPort, briGadg->LeftEdge, briGadg->TopEdge, width, height, -2, TRUE);
	DrawShadowBox(rPort, satGadg->LeftEdge, satGadg->TopEdge, width, height, -2, TRUE);
/*
	Handle requester
*/
	done = FALSE;
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, colorDlg, ColorDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case HUE_SLIDER:
		case BRIGHT_SLIDER:
		case SATURATE_SLIDER:
			GetColorSliders(colorDlg, &red, &green, &blue);
			break;
		case REDARROW_UP:
		case REDARROW_DOWN:
			if (item == REDARROW_UP) {
				if (red < 15)
					red++;
			}
			else {
				if (red > 0)
					red--;
			}
			break;
		case GREENARROW_UP:
		case GREENARROW_DOWN:
			if (item == GREENARROW_UP) {
				if (green < 15)
					green++;
			}
			else {
				if (green > 0)
					green--;
			}
			break;
		case BLUEARROW_UP:
		case BLUEARROW_DOWN:
			if (item == BLUEARROW_UP) {
				if (blue < 15)
					blue++;
			}
			else {
				if (blue > 0)
					blue--;
			}
			break;
		}
		*outColor = RGBCOLOR(red, green, blue);
		if (item != -1) {
			SetRGBColor(scrn, penNum, *outColor, colorCorrect);
			ShowColorValues(colorDlg, red, green, blue);
			AdjustColorSliders(colorDlg, red, green, blue);
		}
		else if ((hueGadg->Flags & GFLG_SELECTED) || (briGadg->Flags & GFLG_SELECTED) ||
				 (satGadg->Flags & GFLG_SELECTED)) {
			GetColorSliders(colorDlg, &red, &green, &blue);
			if ((newColor = RGBCOLOR(red, green, blue)) != *outColor) {
				*outColor = newColor;
				SetRGBColor(scrn, penNum, *outColor, colorCorrect);
				ShowColorValues(colorDlg, red, green, blue);
			}
		}
	} while (!done);
	if (item == CANCEL_BUTTON || !hasPen)
		SetRGB4(&scrn->ViewPort, penNum, RED(origColor), GREEN(origColor), BLUE(origColor));
	DisposeDialog(colorDlg);
	if (item == CANCEL_BUTTON)
		return (FALSE);
	return (TRUE);
}

/*
 *	Return pen number of closest matching color
 */

UBYTE PenNumber(RGBColor rgbColor, ColorMapPtr colorMap, WORD numColors)
{
	register WORD red, green, blue, redDiff, greenDiff, blueDiff;
	WORD entry, diff, newDiff, penNum;
	LONG color;

	red   = RED(rgbColor);
	green = GREEN(rgbColor);
	blue  = BLUE(rgbColor);
	diff = 0x7FFF;
	penNum = 0;
	for (entry = 0; entry < numColors; entry++) {
		color = GetRGB4(colorMap, entry);
		if ((redDiff = red - RED(color)) < 0)
			redDiff = -redDiff;
		if ((greenDiff = green - GREEN(color)) < 0)
			greenDiff = -greenDiff;
		if ((blueDiff = blue - BLUE(color)) < 0)
			blueDiff = -blueDiff;
		if ((newDiff = redDiff + greenDiff + blueDiff) < diff) {
			penNum = entry;
			diff = newDiff;
			if (diff == 0)
				break;
		}
	}
	return ((UBYTE) penNum);
}
