/*
 *	Toolbox library
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Fixed point math routines
 */

#include <Toolbox/Memory.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>

#include <string.h>

#include <Typedefs.h>

/*
 *	Local definitions
 */

#define HIWORD(num)	(((num) >> 16) & 0xFFFF)
#define LOWORD(num)	((num) & 0xFFFF)

/*
 *	Return the fixed point ratio of two word values
 */

Fixed FixRatio(WORD num1, WORD num2)
{
	return (FixDiv(num1 << 16, num2 << 16));
}

/*
 *	Convert ASCII text to fixed point number
 */

Fixed Ascii2Fix(register TextPtr s)
{
	register WORD sign;
	register LONG divisor, frac;
	register Fixed num;

	while (*s == ' ')
		s++;
	sign = 1;
	if (*s == '-') {
		sign = -1;
		s++;
	}
	num = 0;
	while (*s && *s >= '0' && *s <= '9') {
		num *= 10;
		num += (*s++ & 0x0F);
	}
	num <<= 16;
	if (*s++ == DECIMAL_CHAR) {
		frac = 0;
		divisor = 1;
		while (*s >= '0' && *s <= '9' && divisor <= (9 << 16)) {
			frac *= 10;
			frac += (*s++ & 0x0F);
			divisor *= 10;
		}
		frac = (frac << 16)/divisor;
		if (*s >= '5' && *s <= '9')
			frac++;
		num += frac;
	}
	if (sign < 0)
		num = -num;
	return (num);
}

/*
 *	Convert fixed point number to ASCII text with specified decimal places
 *	String will be zero-padded to number of places
 *	Maximum number of decimal places is four
 */

void Fix2Ascii(register Fixed num, register TextPtr s, register WORD places)
{
	register WORD i, divisor, extra;

	if (places < 0)
		places = 0;
	else if (places > 4)
		places = 4;
	if (num < 0) {
		*s++ = '-';
		num = -num;
	}
	divisor = 1;
	for (i = 0; i < places; i++)
		divisor *= 10;
	num += (0x00008000L + divisor/2)/divisor;
	NumToString(HIWORD(num), s);
	if (places > 0) {
		s += strlen(s);
		*s++ = DECIMAL_CHAR;
		num = LOWORD(num);
		for (i = 0; i < places; i++)
			num *= 10;
		NumToString(HIWORD(num), s);
		extra = places - strlen(s);
		if (extra > 0) {
			BlockMove(s, s + extra, strlen(s) + 1);
			while (extra--)
				*s++ = '0';
		}
	}
}

/*
 *	Multiply two fixed point numbers
 */

Fixed FixMul(register Fixed num1, register Fixed num2)
{
	register WORD sign;
	register LONG result;
	register ULONG lo1, lo2, hi1, hi2;

	sign = 1;
	if (num1 < 0) {
		sign = -sign;
		num1 = -num1;
	}
	if (num2 < 0) {
		sign = -sign;
		num2 = -num2;
	}
	lo1 = LOWORD(num1);	lo2 = LOWORD(num2);
	hi1 = HIWORD(num1);	hi2 = HIWORD(num2);
	result = (hi1*hi2 << 16) + lo1*hi2 + lo2*hi1 + (lo1*lo2 >> 16);
	if (sign < 0)
		result = -result;
	return ((Fixed) result);
}

/*
 *	Divide two fixed point numbers
 *	Can also divide two LONG numbers, returning a Fixed result
 */

Fixed FixDiv(register Fixed num1, register Fixed num2)
{
	register WORD i, sign;
	register LONG result;

	sign = 1;
	if (num1 < 0) {
		sign = -sign;
		num1 = -num1;
	}
	if (num2 < 0) {
		sign = -sign;
		num2 = -num2;
	}
	if (num2 == 0)
		result = (sign < 0) ? 0x80000000L : 0x7FFFFFFFL;
	else {
		result = 0;
		for (i = 0; i < 16; i++) {
			result += num1/num2;
			num1 = (num1 % num2) << 1;
			result <<= 1;
		}
		if (sign < 0)
			result = -result;
	}
	return ((Fixed) result);
}
