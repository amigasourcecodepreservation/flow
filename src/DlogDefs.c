/*
 * Flow
 * Copyright (c) 1990 New Horizons Software, Inc.
 *
 * Dialog/Gadget templates
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Image.h>

#include "Flow.h"

/*
 * External variables
 */

extern TextChar	strDone[], strCancel[], strSave[], strPrint[], strFind[], strStart[];
extern TextChar strMacroNames2[10][32];

/*
	 Repeated and general purpose text strings
*/

static TextChar strOK[]			= "OK";
static TextChar strYes[]		= "Yes";
static TextChar strNo[]			= "No";
static TextChar strChange[]	= "Change";
static TextChar strFindTitle[]= "Find:";
static TextChar strChangeTo[] = "Change to:";
static TextChar strWholeWord[]= "Whole Word";
static TextChar strMatchCase[]= "Match Case";
static TextChar strOpenOnly[] = "Skip Hidden";
static TextChar strStopSearch[] = "Stop Search At End";
static TextChar strAvailMem[] = "Available Memory:";
static TextChar strGraphics[] = "Graphics";
static TextChar strExpansion[]= "Expansion";
static TextChar strEnter[]		= "Enter";
static TextChar strBack[]		= "Back";
static TextChar strDisk[]		= "Disk";
static TextChar strWord[]		= "Word:";
static TextChar strReset[]		= "Reset";
static TextChar strShowAll[]	= "Show all files";
static TextChar strCustom[]	= "Custom:";
static TextChar strUse[]		= "Use";

#define SL_GADG_BOX(left, top, width, num)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width-ARROW_WIDTH, (num)*11, -1, num, NULL }

#define SL_GADG_UPARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-2*ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_UP }

#define SL_GADG_DOWNARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define SL_GADG_SLIDER(left, top, width, num)	\
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,		\
		(left)+(width)-ARROW_WIDTH, top, 1, 0,	\
		ARROW_WIDTH, (num)*11-2*ARROW_HEIGHT, -2, num, NULL }

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
	Init info
*/

static GadgetTemplate initGadgets[] = {
	{ GADG_STAT_TEXT , 20, 16, 0, 0, 0, 0, 0, 0, 0, 0, "Flow" },
	{ GADG_STAT_TEXT , 54, 10, 0, 0, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT , 20, 40, 0, 0, 0, 0, 0, 0, 0, 0, 
		"Copyright \251 1986-92\nNew Horizons Software, Inc.\nAll Rights Reserved" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate initDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 250, 95, &initGadgets[0], NULL
};

/*
 *	Structures for SFPGetFile()
 */

#define LIST_WIDTH	(24*8)
#define OPEN_NUM	10


static GadgetTemplate openFileGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'O', 0, "Open" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, OPEN_NUM),

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX, 20, -20, 0, 0, 130,  12, 0, 0, 'S', 0, "Show All Files" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate openFileDialogT = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 200, &openFileGadgets[0], NULL
};

/*
 *	Structures for SFPPutFile()
 */
 
#define SAVE_NUM	7
#define PUT_PROMPT			14

static GadgetTemplate saveAsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'S', 0, "Save" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT, 20, -60, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -80, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,  20, -40, 0, 0,  70,  12, 0, 0, 'N', 0, "Normal" },
	{ GADG_RADIO_BUTTON, 110, -40, 0, 0,  90,  12, 0, 0, 'T', 0, "Text Only" },
	{ GADG_RADIO_BUTTON,  20, -20, 0, 0,  200, 12, 0, 0, 'L', 0, "Text with Heading Labels" },
	
	{ GADG_ITEM_NONE }
};

static DialogTemplate saveAsDialogT = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 200, &saveAsGadgets[0], NULL
};

/*
	Save Changes
*/

static GadgetTemplate saveGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, -90, 90, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON,  30, 90, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },

	{ GADG_STAT_TEXT,     55, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Save changes to" },
	{ GADG_STAT_TEXT,     55, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,     55, 40, 0, 0, 0, 0, 0, 0, 0, 0, "before closing?" },

	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static DialogTemplate saveDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 120, &saveGadgets[0], NULL
};

/*
	 Revert dialog
*/

static GadgetTemplate revertGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, -90, 60, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },

	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Revert to last\nversion saved?" },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

DialogTemplate revertDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 90, &revertGadgets[0], NULL
};

/*
	 Page Setup dialog
*/

static GadgetTemplate pageSetupGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,  10, 0, 0,  60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,  -80,  40, 0, 0,  60, 20, 0, 0, 'C', 0, &strCancel[0]},

	{ GADG_RADIO_BUTTON,  80,  35, 0, 0,  90, 12, 0, 0, 'L', 0, "US Letter" },
	{ GADG_RADIO_BUTTON,  80,  55, 0, 0,  80, 12, 0, 0, 'e', 0, "US Legal" },
	{ GADG_RADIO_BUTTON, 180,  35, 0, 0,  90, 12, 0, 0, '4', 0, "A4 Letter" },
	{ GADG_RADIO_BUTTON, 180,  55, 0, 0, 120, 12, 0, 0, 'W', 0, "Wide Carriage" },
	{ GADG_RADIO_BUTTON,  80,  75, 0, 0,  80, 12, 0, 0, 'u', 0, "Custom:" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 220, 75, 0, 0, 48, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 340, 75, 0, 0, 48, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON,  90,  95, 0, 0,  50, 12, 0, 0, 'P', 0, "Pica" },
	{ GADG_RADIO_BUTTON, 170,  95, 0, 0,  60, 12, 0, 0, 'i', 0, "Elite" },
	{ GADG_RADIO_BUTTON, 250,  95, 0, 0,  90, 12, 0, 0, 'd', 0, "Condensed" },

	{ GADG_RADIO_BUTTON,  90, 115, 0, 0,  60, 12, 0, 0, '6', 0, "6 LPI" },
	{ GADG_RADIO_BUTTON, 170, 115, 0, 0,  60, 12, 0, 0, '8', 0, "8 LPI" },

	{ GADG_CHECK_BOX,     90, 135, 0, 0, 190, 12, 0, 0, 'G', 0, "No Gaps Between Pages" },

	{ GADG_STAT_TEXT, 130,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 410 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Page Setup" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Paper" },
	{ GADG_STAT_TEXT,  28,  48, 0, 0, 0, 0, 0, 0, 0, 0, "size:" },
	{ GADG_STAT_TEXT, 170,  75, 0, 0, 0, 0, 0, 0, 0, 0, "width" },
	{ GADG_STAT_TEXT, 285,  75, 0, 0, 0, 0, 0, 0, 0, 0, "height" },
	{ GADG_STAT_TEXT,  20,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Pitch:" },
	{ GADG_STAT_TEXT,  20, 115, 0, 0, 0, 0, 0, 0, 0, 0, "Spacing:" },
	{ GADG_STAT_TEXT,  20, 135, 0, 0, 0, 0, 0, 0, 0, 0, "Options:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate pageSetupDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 410, 155, &pageSetupGadgets[0], NULL
};

/*
	 Print dialog
*/

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,  -90,  10, 0, 0,  70, 20, 0, 0, 'P', 0, "Print" },
	{ GADG_PUSH_BUTTON,  -90,  40, 0, 0,  70, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON,  -90,  70, 0, 0,  70, 20, 0, 0, 'O', 0, "Options" },

	{ GADG_RADIO_BUTTON, 100,  35, 0, 0, 100, 12, 0, 0, 't', 0, "PostScript" },
	{ GADG_RADIO_BUTTON, 210,  35, 0, 0,  40, 12, 0, 0, 'Q', 0, "NLQ" },
	{ GADG_RADIO_BUTTON, 275,  35, 0, 0,  60, 12, 0, 0, 'D', 0, "Draft" },

	{ GADG_RADIO_BUTTON, 100,  55, 0, 0,  40, 12, 0, 0, 'A', 0, "All" },
	{ GADG_RADIO_BUTTON, 160,  55, 0, 0,  50, 12, 0, 0, 'r', 0, "From" },

	{ GADG_RADIO_BUTTON, 130,  95, 0, 0,  90, 12, 0, 0, 'u', 0, "Automatic" },
	{ GADG_RADIO_BUTTON, 260,  95, 0, 0,  90, 12, 0, 0, 'H', 0, "Hand Feed" },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 80, 75, 0, 0, 40, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 220, 55, 0, 0, 32, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 285, 55, 0, 0, 32, 11, 0, 0, 0, 0, NULL },

	GT_ADJUST_UPARROW(190, 135),
	GT_ADJUST_DOWNARROW(190,135),
	GT_ADJUST_TEXT(190, 135),

	{ GADG_CHECK_BOX,    150, 160, 0, 0,  70, 12, 0, 0, 'l', 0, "Collate" },
	{ GADG_CHECK_BOX,    150, 175, 0, 0, 170, 12, 0, 0, 'B', 0, "Print Back to Front" },

	{ GADG_RADIO_BUTTON, 130, 115, 0, 0, 80, 12, 0, 0, 'n', 0, "Printer" },
	{ GADG_RADIO_BUTTON, 260, 115, 0, 0, 50, 12, 0, 0, 'F', 0, "File" },
		
	{ GADG_STAT_TEXT,  90,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 420 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Print" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Quality:" },
	{ GADG_STAT_TEXT,  20,  55, 0, 0, 0, 0, 0, 0, 0, 0, "Pages:" },
	{ GADG_STAT_TEXT, 260,  55, 0, 0, 0, 0, 0, 0, 0, 0, "to" },
	{ GADG_STAT_TEXT,  20,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Copies:" },
	{ GADG_STAT_TEXT,  20,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Paper feed:" },
	{ GADG_STAT_TEXT,  20, 135, 0, 0, 0, 0, 0, 0, 0, 0, "Printer font number:" },
	{ GADG_STAT_TEXT,  20, 160, 0, 0, 0, 0, 0, 0, 0, 0, "Paper handling:" },
	{ GADG_STAT_TEXT,  20, 115, 0, 0, 0, 0, 0, 0, 0, 0, "Destination:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate printDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 430, 200, &printGadgets[0], NULL
};

/*
	PostScript options
*/

static GadgetTemplate postScriptGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,  -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_RADIO_BUTTON, 100, 35, 0, 0, 110, 12, 0, 0, 'S', 0, "Serial Port" },
	{ GADG_RADIO_BUTTON, 220, 35, 0, 0, 120, 12, 0, 0, 'P', 0, "Parallel Port" },
	{ GADG_RADIO_BUTTON, 100, 55, 0, 0,  70, 12, 0, 0, 'u', 0, "Custom:" },

	{ GADG_EDIT_TEXT, 180, 55, 0, 0, 19*8, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 440 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "PostScript Options" },
	{ GADG_STAT_TEXT, 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Send to:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate postScriptDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 440, 80, &postScriptGadgets[0], NULL
};

/*
	 Cancel print dialog
*/

static GadgetTemplate cancelPrintGadgets[] = {
	{ GADG_STAT_TEXT , 	 60, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Printing in progress." },
	{ GADG_PUSH_BUTTON, -80, 50, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_STAT_TEXT,	 60, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

DialogTemplate cancelPrintDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, &cancelPrintGadgets[0], NULL
};

/*
	 Next Page dialog
*/

static GadgetTemplate nextPageGadgets[] = {
	{ GADG_PUSH_BUTTON,290, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,290, 40, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_STAT_TEXT , 60, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Please insert the next" },
	{ GADG_STAT_TEXT , 60, 35, 0, 0, 0, 0, 0, 0, 0, 0, "sheet of paper" },
	{ GADG_STAT_STDIMAGE , 10, 15, 0, 0, 0, 0, 0, 0,0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

DialogTemplate nextPageDialogT = {
	DLG_TYPE_ALERT, 0, -1, 30, 370, 70, &nextPageGadgets[0], NULL
};

/*
	 Find dialog
*/

static GadgetTemplate findGadgets[] = {
	{ GADG_PUSH_BUTTON, 154,-30, 0, 0, 60, 20, 0, 0, 'F', 0, &strFind[0] },
	{ GADG_EDIT_TEXT,	70, 10, 0, 0,288, 11, 0, 0, 0, 0, NULL },
	{ GADG_CHECK_BOX,	20, 30, 0, 0,118, 12, 0, 0, 'W', 0, &strWholeWord[0] },
	{ GADG_CHECK_BOX,  138, 30, 0, 0,118, 12, 0, 0, 'M', 0, &strMatchCase[0] },
	{ GADG_CHECK_BOX,  256, 30, 0, 0,118, 12, 0, 0, 'O', 0, &strOpenOnly[0] },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0, 'T', 0, &strFindTitle[0] },
	{ GADG_ITEM_NONE }
};

DialogTemplate findDialogT = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH,
		-1, 30, 378, 80, &findGadgets[0], "Find Text"
};

/*
	Change
*/

static GadgetTemplate changeGadgets[] = {
	{ GADG_PUSH_BUTTON, 20,-30, 0, 0, 60, 20, 0, 0, 'F', 0, &strFind[0] },

	{ GADG_EDIT_TEXT,  108, 10, 0, 0,320, 11, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,	20, 50, 0, 0,118, 12, 0, 0, 'W', 0, &strWholeWord[0]},
	{ GADG_CHECK_BOX,  150, 50, 0, 0,118, 12, 0, 0, 'M', 0, &strMatchCase[0]},
	{ GADG_CHECK_BOX,  280, 50, 0, 0,118, 12, 0, 0, 'O', 0, &strOpenOnly[0]},
		
	{ GADG_EDIT_TEXT,  108, 30, 0, 0,320, 11, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON, 90,-30, 0, 0,150, 20, 0, 0, 'T', 0, "Change, Then Find" },
	{ GADG_PUSH_BUTTON,250,-30, 0, 0, 70, 20, 0, 0, 'C', 0, &strChange[0] },
	{ GADG_PUSH_BUTTON,330,-30, 0, 0,100, 20, 0, 0, 'A', 0, "Change All" },

	{ GADG_STAT_TEXT , 20,10, 0, 0, 0, 0, 0, 0, 'i', 0, &strFindTitle[0] },
	{ GADG_STAT_TEXT , 20,30, 0, 0, 0, 0, 0, 0, 'h', 0, &strChangeTo[0] },
	{ GADG_ITEM_NONE }
};

static DialogTemplate changeDialogT = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH,
		-1, 30, 450, 100, &changeGadgets[0], "Change Text"
};

/*
	Continue search from beginning
*/

static GadgetTemplate findContinueGadgets[] = {
	{ GADG_PUSH_BUTTON, 45, 48, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON,145, 48, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },
	{ GADG_STAT_TEXT , 60, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Continue search from" },
	{ GADG_STAT_TEXT , 60, 25, 0, 0, 0, 0, 0, 0, 0, 0, "the beginning?" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0, 0,  (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate findContinueDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 250, 80, &findContinueGadgets[0], NULL
};

/*
	Go To
*/

static GadgetTemplate goToGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, 40, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 100, 40, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0,  0, 0, "Go to page:" },
	{ GADG_EDIT_TEXT,	120, 10, 0, 0, 40, 11, 0, 0, 0, 0, NULL },
	{ GADG_ITEM_NONE }
};

static DialogTemplate goToDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 180, 70, &goToGadgets[0], NULL
};

/*
 * Macro name dialog
 */

static GadgetTemplate macroNameGadgets[] = {
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0, 'E', 0, "Execute" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,  80, 10, 0, 0, 200, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 112, 30, 0, 0, 168, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Parameter:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate macroNameDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &macroNameGadgets[0], NULL
};

/*
	 Error dialog
*/

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, 220, 50, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_STAT_TEXT , 60, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 60, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

DialogTemplate errorDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 310, 80, &errorGadgets[0], NULL
};

/*
	 Help (and memory) dialog
*/

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },

	{ GADG_STAT_TEXT, 150, 165, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 180, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 200, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "Flow" },
	{ GADG_STAT_TEXT,  53,	10, 0, 0, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT,  70,  16, 0, 0, 0, 0, 0, 0, 0, 0, " 3.1" },

	{ GADG_STAT_TEXT,  20,  40, 0, 0, 0, 0, 0, 0, 0, 0,
		"Designed and developed\n  by James Bayless,\n  Glen Merriman, &\n  Beth Henry" },

	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1986-92\nNew Horizons Software, Inc.\nAll Rights Reserved" },
	{ GADG_STAT_TEXT,  20, 150, 0, 0, 0, 0, 0, 0, 0, 0, &strAvailMem[0]},
	{ GADG_STAT_TEXT,  40, 165, 0, 0, 0, 0, 0, 0, 0, 0, &strGraphics[0]},
	{ GADG_STAT_TEXT,  40, 180, 0, 0, 0, 0, 0, 0, 0, 0, &strExpansion[0]},
	{ GADG_STAT_TEXT,  20, 200, 0, 0, 0, 0, 0, 0, 0, 0, "AREXX port:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 225, &aboutGadgets[0], NULL
};

static GadgetTemplate memoryGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_STAT_TEXT ,150, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT ,150, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, &strAvailMem[0]},
	{ GADG_STAT_TEXT , 40, 25, 0, 0, 0, 0, 0, 0, 0, 0, &strGraphics[0]},
	{ GADG_STAT_TEXT , 40, 40, 0, 0, 0, 0, 0, 0, 0, 0, &strExpansion[0]},
	{ GADG_ITEM_NONE }
};

DialogTemplate memoryDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 65, &memoryGadgets[0], NULL
};

/*
	Preferences
*/

static GadgetTemplate preferencesGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,  10, 0, 0,	 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,  -80,  40, 0, 0,	 60, 20, 0, 0, 'C', 0, &strCancel[0]},
	{ GADG_PUSH_BUTTON,  -80,  70, 0, 0,	 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_RADIO_BUTTON,  30,  50, 0, 0,  70, 12, 0, 0, 'I', 0, "Inches" },
	{ GADG_RADIO_BUTTON,  30,  65, 0, 0, 100, 12, 0, 0, 'e', 0, "Centimeters" },

	{ GADG_RADIO_BUTTON, 210,  50, 0, 0,  70, 12, 0, 0, 'n', 0, "Insert" },
	{ GADG_RADIO_BUTTON, 210,  65, 0, 0,  80, 12, 0, 0, 'T', 0, "Typeover" },

	{ GADG_RADIO_BUTTON,  30, 105, 0, 0, 110, 12, 0, 0, 'h', 0, "Flash Screen" },
	{ GADG_RADIO_BUTTON,  30, 120, 0, 0, 100, 12, 0, 0, 'S', 0, "Sound Beep" },
	{ GADG_RADIO_BUTTON,  30, 135, 0, 0,  50, 12, 0, 0, 'B', 0, "Both" },

	{ GADG_RADIO_BUTTON, 210, 105, 0, 0,  50, 12, 0, 0, 'a', 0, "Fast" },
	{ GADG_RADIO_BUTTON, 210, 120, 0, 0,  50, 12, 0, 0, 'l', 0, "Slow" },
	{ GADG_RADIO_BUTTON, 210, 135, 0, 0,  40, 12, 0, 0, 'f', 0, "Off" },

	{ GADG_RADIO_BUTTON, 210, 175, 0, 0, 210, 12, 0, 0, 'y', 0, "Show Flow Files Only" },
	{ GADG_RADIO_BUTTON, 210, 190, 0, 0, 130, 12, 0, 0, 'w', 0, "Show All Files" },
		
	{ GADG_CHECK_BOX,   30, 175, 0, 0, 100, 12, 0, 0, 'v', 0, "Save Icons" },
	{ GADG_CHECK_BOX,   30, 190, 0, 0, 110, 12, 0, 0, 'M', 0, "Make Backups" },
	{ GADG_CHECK_BOX,   30, 210, 0, 0, 150, 12, 0, 0, 'u', 0, "Auto-save every:" },
	
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 185, 210, 0, 0, 24, 11, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX,  	30, 250, 0, 0, 200, 12, 0, 0, 'p', 0, "Full clipboard support" },
	
	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Unit of measurement:" },
	{ GADG_STAT_TEXT, 200,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Typing mode:" },
	{ GADG_STAT_TEXT,  20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "Error notification:" },
	{ GADG_STAT_TEXT, 200,  90, 0, 0, 0, 0, 0, 0, 0, 0, "Insertion point blinking:" },
	{ GADG_STAT_TEXT, 200, 160, 0, 0, 0, 0, 0, 0, 0, 0, "Open requester:" },
	{ GADG_STAT_TEXT,  20, 160, 0, 0, 0, 0, 0, 0, 0, 0, "Saving documents:" },
	{ GADG_STAT_TEXT, 220, 210, 0, 0, 0, 0, 0, 0, 0, 0, "minutes" },
	{ GADG_STAT_TEXT,  20, 235, 0, 0, 0, 0, 0, 0, 0, 0, "Clipboard: "},

	{ GADG_ITEM_NONE }
};

static DialogTemplate preferencesDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 460, 270, &preferencesGadgets[0], NULL
};


/*
	Save prefs dialog
*/
 
static GadgetTemplate savePrefsGadgets[] = {
	{ GADG_PUSH_BUTTON, 60, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON,180, 60, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },
	{ GADG_STAT_TEXT , 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Save current format settings" },
	{ GADG_STAT_TEXT , 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, "as default values?" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0,0,(Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate savePrefsDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &savePrefsGadgets[0], NULL
};

static GadgetTemplate layoutGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 12, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 42, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
		
	{ GADG_EDIT_TEXT,	 84, 50, 0, 0, 24, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	 84, 70, 0, 0, 24, 11, 0, 0, 0, 0, NULL },
	
	{ GADG_EDIT_TEXT,	 84,100, 0, 0,272, 11, 0 ,0, 0, 0, NULL },
	{ GADG_EDIT_TEXT,	 84,120, 0, 0,272, 11, 0, 0, 0, 0, NULL },
		 
	GT_ADJUST_UPARROW(244, 50),
	GT_ADJUST_DOWNARROW(244, 50),
	GT_ADJUST_TEXT(244, 50),
	
	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 400 -120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Document Layout" },
	{ GADG_STAT_TEXT , 20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Margins" },
	{ GADG_STAT_TEXT ,148, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Auto-Indent" },
	{ GADG_STAT_TEXT , 36, 50, 0, 0, 0, 0, 0, 0, 0, 0, "Left:" },
	{ GADG_STAT_TEXT , 28, 70, 0, 0, 0, 0, 0, 0, 0, 0, "Right:" },
	{ GADG_STAT_TEXT ,180, 50, 0, 0, 0, 0, 0, 0, 0, 0, "Spaces:" },
	{ GADG_STAT_TEXT , 20,100, 0, 0, 0, 0, 0, 0, 0, 0, "Header:" },
	{ GADG_STAT_TEXT , 20,120, 0, 0, 0, 0, 0, 0, 0, 0, "Footer:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate layoutDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 400, 150, &layoutGadgets[0], NULL
};

static GadgetTemplate insertGadgets[] = {
	{ GADG_ACTIVE_BORDER, 20, 10, 0, 0, 360, 160, 0, 0, 0, 0, NULL },
/*	{ GADG_PUSH_BUTTON,-80,-28, 0, 0,  60,  20, 0, 0, 'C', 0, &strCancel[0] }, */
	{ GADG_ITEM_NONE }
};

static DialogTemplate insertDialogT = {
	DLG_TYPE_WINDOW, DLG_FLAG_DEPTH | DLG_FLAG_CLOSE, -1, -1, 400, 180, &insertGadgets[0], "Insert Literal"
};

static GadgetTemplate labelGadgets[] = {
	{ GADG_PUSH_BUTTON,-80, 12, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,-80, 42, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_RADIO_BUTTON,20, 26, 0, 0, 50, 12, 0, 0, 'N', 0, "None" },
	{ GADG_RADIO_BUTTON,20, 98, 0, 0, 78, 12, 0, 0, 'u', 0, &strCustom[0] },
	{ GADG_RADIO_BUTTON,20, 44, 0, 0,120, 12, 0, 0, 'm', 0, "Numbers Only" },
	{ GADG_RADIO_BUTTON,20, 62, 0, 0,100, 12, 0, 0, 'I', 0, "I.A.1.a)i)"},
	{ GADG_RADIO_BUTTON,20, 80, 0, 0,150, 12, 0, 0, 'A', 0, "I.A.1.a)(1)(a)i)" },
	{ GADG_EDIT_TEXT, 96, 98, 0, 0,200, 11, 0, 0, 0, 0, NULL },
	{ GADG_CHECK_BOX,	20,126, 0, 0,170, 12, 0, 0, 'P', 0, "Append Parent Label" },
	{ GADG_CHECK_BOX,	20,143, 0, 0,170, 12, 0, 0, 'S', 0, "Append Space To End" },
	{ GADG_CHECK_BOX,	20,160, 0, 0,200, 12, 0, 0, 'L', 0, "Indent Secondary Lines" }, 
	{ GADG_STAT_TEXT,	20, 10, 0, 0,  0,  0, 0, 0, 'H', 0, "Heading Labels" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate labelDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 316, 184, &labelGadgets[0], NULL
};

/*
	Check Spelling
*/

#define SPELL_WIDTH	(17*8+20)
#define SPELL_NUM	9

static GadgetTemplate spellGadgets[] = {
	{ GADG_PUSH_BUTTON,-72, 50, 0, 0, 60, 20, 0, 0, 'S', 0, &strStart[0] },
	{ GADG_PUSH_BUTTON,-72, 77, 0, 0, 60, 20, 0, 0, 'C', 0, &strChange[0] },
	{ GADG_PUSH_BUTTON,-72,104, 0, 0, 60, 20, 0, 0, 'L', 0, "Learn" },
	{ GADG_PUSH_BUTTON,-72,131, 0, 0, 60, 20, 0, 0, 'G', 0, "Guess" },

	{ GADG_STAT_TEXT , 64, 8, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT, 103, 24, 0, 0, 144, 11, 0, 0, 0, 0, NULL },

	SL_GADG_BOX(16, 49, SPELL_WIDTH, SPELL_NUM),
	SL_GADG_UPARROW(16, 49, SPELL_WIDTH, SPELL_NUM),
	SL_GADG_DOWNARROW(16, 49, SPELL_WIDTH, SPELL_NUM),
	SL_GADG_SLIDER(16, 49, SPELL_WIDTH, SPELL_NUM),
	
	{ GADG_STAT_TEXT , 14,  8, 0, 0, 0, 0, 0, 0, 0, 0, &strWord[0] },
	{ GADG_STAT_TEXT , 14, 24, 0, 0, 0, 0, 0, 0, 0, 0, &strChangeTo[0] },
	{ GADG_ITEM_NONE }
};
	
static DialogTemplate spellDialogT = {
	DLG_TYPE_WINDOW, DLG_FLAG_CLOSE | DLG_FLAG_DEPTH, -1, 50, 106 + SPELL_WIDTH, 170, &spellGadgets[0], "Check Spelling"
};

/*
	Item Formats
*/

static GadgetTemplate formatsGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON,  -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_RADIO_BUTTON,  30,  50, 0, 0,  80, 12, 0, 0, '1', 0, "1, 2, 3" },
	{ GADG_RADIO_BUTTON,  30,  65, 0, 0,  90, 12, 0, 0, 'I', 0, "I, II, III" },
	{ GADG_RADIO_BUTTON,  30,  80, 0, 0,  90, 12, 0, 0, ',', 0, "i, ii, iii" },
	{ GADG_RADIO_BUTTON,  30,  95, 0, 0,  80, 12, 0, 0, 'A', 0, "A, B, C" },
	{ GADG_RADIO_BUTTON,  30, 110, 0, 0,  80, 12, 0, 0, 'b', 0, "a, b, c" },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 200, 130, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON, 150,  50, 0, 0,  80, 12, 0, 0, '2', 0, "12 Hour" },
	{ GADG_RADIO_BUTTON, 150,  65, 0, 0,  80, 12, 0, 0, '4', 0, "24 Hour" },

	{ GADG_RADIO_BUTTON, 260,  50, 0, 0,  80, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON, 260,  65, 0, 0, 110, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON, 260,  80, 0, 0, 120, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON, 260,  95, 0, 0, 150, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON, 260, 110, 0, 0, 190, 12, 0, 0, 0, 0, NULL },
	{ GADG_RADIO_BUTTON, 260, 125, 0, 0,  90, 12, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 500 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Special Item Formats" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Page number:" },
	{ GADG_STAT_TEXT, 140,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Time:" },
	{ GADG_STAT_TEXT, 250,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Date:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Starting page number:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate formatsDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 500, 150, &formatsGadgets[0], NULL
};

/*
	Screen colors
*/

static GadgetTemplate screenColorsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 38, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON, -80, 66, 0, 0, 60, 20, 0, 0, 'R', 0, &strReset[0] },
		
	{ GADG_ACTIVE_BORDER,  20, 35, 0, 0, -120, -75, 0, 0,  0, 0, NULL },
	{ GADG_PUSH_BUTTON,    70,-30, 0, 0,   60,   20, 0, 0, 'h', 0, &strChange[0] },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 280 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Screen Colors" },
	
	{ GADG_ITEM_NONE }
};

static DialogTemplate screenColorsDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 110, &screenColorsGadgets[0], NULL
};

static GadgetTemplate stopChangeGadgets[] = {
	{ GADG_STAT_TEXT , 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Change All in progress." },
	{ GADG_PUSH_BUTTON, -80, 50, 0, 0, 60, 20, 0, 0, 'S', 0, "Stop" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0,0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

static DialogTemplate stopChangeDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 250, 80, &stopChangeGadgets[0], NULL
};

static GadgetTemplate saveDictGadgets[] = {
	{ GADG_PUSH_BUTTON,  60, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, 180, 60, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },
	{ GADG_STAT_TEXT , 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Save changes to user dict?" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0,0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate saveDictDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &saveDictGadgets[0], NULL
};

static GadgetTemplate spellChangeGadgets[] = {
	{ GADG_PUSH_BUTTON, 50, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON,150, 60, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },
	{ GADG_STAT_TEXT , 60, 10, 0, 0, 0, 0, 0, 0, 0, 0, "That word is misspelled" },
	{ GADG_STAT_TEXT , 60, 30, 0, 0, 0, 0, 0, 0, 0, 0, "OK to use it anyway?" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0,0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate spellChangeDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 90, &spellChangeGadgets[0], NULL
};

/*
	Continue spell check from beginning
*/

static GadgetTemplate spellContinueGadgets[] = {
	{ GADG_PUSH_BUTTON, 40, 60, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON,140, 60, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },
	{ GADG_STAT_TEXT , 60, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Continue spell check" },
	{ GADG_STAT_TEXT , 60, 30, 0, 0, 0, 0, 0, 0, 0, 0, "from the beginning?" },
	{ GADG_STAT_STDIMAGE , 10, 10, 0, 0, 0, 0, 0, 0,0,0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate spellContinueDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 240, 90, &spellContinueGadgets[0], NULL
};

static GadgetTemplate docInfoGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_STAT_TEXT , 130, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 130, 55, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 130, 70, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 300, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 300, 55, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 300, 70, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 240, 95, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 240,110, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT , 240,125, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
		
	{ GADG_STAT_TEXT ,  30, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Characters" },
	{ GADG_STAT_TEXT ,  30, 55, 0, 0, 0, 0, 0, 0, 0, 0, "Words" },
	{ GADG_STAT_TEXT ,  30, 70, 0, 0, 0, 0, 0, 0, 0, 0, "Lines" },
	{ GADG_STAT_TEXT , 200, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Sentences" },
	{ GADG_STAT_TEXT , 200, 55, 0, 0, 0, 0, 0, 0, 0, 0, "Headings" },
	{ GADG_STAT_TEXT , 200, 70, 0, 0, 0, 0, 0, 0, 0, 0, "Pages" },
	{ GADG_STAT_TEXT ,  40, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Average word length" },
	{ GADG_STAT_TEXT ,  40,110, 0, 0, 0, 0, 0, 0, 0, 0, "Average sentence length" },
	{ GADG_STAT_TEXT ,  40,125, 0, 0, 0, 0, 0, 0, 0, 0, "Grade level readability" },
	
	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 370 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT ,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Document Information" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate docInfoDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 370, 145, &docInfoGadgets[0], NULL
};

static GadgetTemplate colorsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
		
	{ GADG_ACTIVE_BORDER,  	 20,   50, 0, 0, 155, -60, 0, 0, 0, 0, NULL },
	{ GADG_ACTIVE_BORDER,	200,   50, 0, 0, 155, -60, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 450 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT , 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Document Colors" },
	{ GADG_STAT_TEXT , 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Pen Color" },
	{ GADG_STAT_TEXT ,200, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Paper Color" },
	
	{ GADG_ITEM_NONE }
};

static DialogTemplate colorsDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 450, 95, &colorsGadgets[0], NULL
};

/*
	Customize macro menu dialog
*/

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 105, -30, 0, 0,  60, 20, 0, 0, 'U', 0, &strUse[0] },
	{ GADG_PUSH_BUTTON, 185, -30, 0, 0,  60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  60, 20, 0, 0, 'S', 0, "Save" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[0][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[1][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[2][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[3][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[4][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[5][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[6][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[7][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[8][0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, &strMacroNames2[9][0] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro names:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 270, 260, &customMacroGadgets[0], NULL
};

/*
	User request dialogs
*/

static GadgetTemplate userReq1Gadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq1DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, &userReq1Gadgets[0], NULL
};

static GadgetTemplate userReq2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq2DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, &userReq2Gadgets[0], NULL
};

static GadgetTemplate userReq3Gadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, 60, 20, 0, 0, 'Y', 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, 210, -30, 0, 0, 60, 20, 0, 0, 'C', 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0, 60, 20, 0, 0, 'N', 0, &strNo[0] },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq3DialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, &userReq3Gadgets[0], NULL
};

static GadgetTemplate userReqTextGadgets[] = {
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0, 'O', 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, 'C', 0, &strCancel[0] },

	{ GADG_EDIT_TEXT, 22, 55, 0, 0, 256, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReqTextDialogT = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 110, &userReqTextGadgets[0], NULL
};


/*
 * Dialog list
 */

DlgTemplPtr dlgList[] = {
	&initDialogT,
	&openFileDialogT, &saveAsDialogT,
	&preferencesDialogT, &macroNameDialogT,
	&errorDialogT, &aboutDialogT, &memoryDialogT,
	&layoutDialogT, &pageSetupDialogT, &printDialogT,
	&saveDialogT, &revertDialogT, &savePrefsDialogT, &postScriptDialogT,
	&cancelPrintDialogT, &nextPageDialogT,
	&findDialogT, &changeDialogT, &findContinueDialogT, &goToDialogT,
	&insertDialogT, &labelDialogT, &spellDialogT, &formatsDialogT,
	&screenColorsDialogT, &stopChangeDialogT, &saveDictDialogT,
	&spellChangeDialogT, &spellContinueDialogT, &docInfoDialogT, &colorsDialogT,
	&customMacroDialogT,
	&userReq1DialogT,
	&userReq2DialogT,
	&userReq3DialogT,
	&userReqTextDialogT
}; 
