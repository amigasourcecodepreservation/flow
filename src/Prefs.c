/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Flow preferences routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>
#include <devices/narrator.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>

#include <IFF/WORD.h>		/* Includes IFF.h and ILBM.h */

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/LocalData.h>	/* For default decimal char */
#include <Toolbox/Window.h>
#include <Toolbox/Color.h>
#include <Toolbox/Screen.h>

#include "Flow.h"

#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;

extern TextChar	progPathName[], strPrefsName[];

extern Defaults		defaults;
extern Options		options;

extern BOOL			_tbOnPubScreen;

extern UBYTE		_tbPenDark, _tbPenLight, _tbPenWhite, _tbPenBlack;

extern PostScriptOptions postScriptOptions;

/*
 *	Prefs file for version 3.1
 */

#define FLAG_SHOWINVISIBLES		0x01
#define FLAG_SHOWPICTURES		0x02
#define FLAG_SHOWRULER			0x04
#define FLAG_SHOWRICON			0x08
#define FLAG_SHOWSHOWGUIDES		0x10
#define FLAG_BEEPSOUND			0x20
#define FLAG_BEEPFLASH			0x40

#define SPEAKFLAG_PHONETIC		0x01

#define PREFS_VERSION 1

typedef struct {
	ULONG		FileID;		/* Prefs file ID */
	UWORD		Version;	/* Version number of Flow that created this file */
	TextChar	FontName[32];			/* NULL terminated, without ".font" */
	UWORD		FontSize;
	Defaults	Defaults;
	WORD		ObsoleteColor[8];
	TextChar	MenuFontName[32];	/* Without trailing ".font" */
	UWORD		MenuFontSize;
	WORD		NumColors;
	RGBColor 	ColorTable[256];
	Options		Options;
	PostScriptOptions PostScriptOptions;
} ProgPrefs;

/*
 *	Local prototypes
 */

void	SetStdDefaults(void);
void	SetDefaults(ProgPrefs *);

/*
 *	Create path name to given file at same directory as program
 *	If abbrev is TRUE, then it's OK to use short-cut in AmigaDOS 2.0
 */

void SetPathName(TextPtr pathNameBuff, TextPtr fileName, BOOL abbrev)
{
	register WORD i;

	if (SystemVersion() >= OSVERSION_2_0 && abbrev)
		strcpy(pathNameBuff, "ProgDir:");
	else {
		strcpy(pathNameBuff, progPathName);
		for (i = strlen(pathNameBuff); i; i--) {
			if (pathNameBuff[i - 1] == '/' || pathNameBuff[i - 1] == ':')
				break;
		}
		pathNameBuff[i] = '\0';
	}
	strcat(pathNameBuff, fileName);
}

/*
 *	Set built-in default
 *	defaultFont and default font num will already be set to normal default
 */

static void SetStdDefaults()
{
	PrintDefault(&defaults.PrintRec);
	defaults.PageNumStyle	= PAGESTYLE_1;
	defaults.DateStyle		= DATESTYLE_SHORT;
	defaults.TimeStyle		= TIMESTYLE_12HR;
	defaults.CPI		= 10;
	defaults.Indent		= 5;
	defaults.LeftMargin = defaults.RightMargin = 5;
	defaults.DocFlags	= WD_SHOWRULER | WD_SHOWRICON | WD_SHOWPICTS;

	defaults.LabelStyle = 0;
	defaults.LabelFlags = 0;
	defaults.LabelRange = 1;
	defaults.LabelRepeat = 0;
	defaults.PenColor	= _tbPenBlack;
	defaults.PaperColor	= _tbPenWhite;
	
	SetStdOptions(&options, TRUE);
}

/*
 *	Set defaults from prefs structure
 */

static void SetDefaults(progPrefs)
register ProgPrefs *progPrefs;
{
	register WORD i ;
	WORD entries, numColors;
	RGBColor colors[256];
	
	if (!_tbOnPubScreen) {
		numColors = GetColorTable(screen, &colors);		/* In case screen has more colors */
		entries = MIN(numColors, progPrefs->NumColors);
		for( i = 0 ; i < entries/2 ; i++ ) {
			colors[i] = progPrefs->ColorTable[i];
			colors[numColors - i - 1] = progPrefs->ColorTable[progPrefs->NumColors - i - 1];
		}
		LoadRGB4(&screen->ViewPort, colors, numColors);
		CheckColorTable(); 
	}

	defaults = progPrefs->Defaults;
	
	BlockMove(&progPrefs->Defaults.PrintRec, &defaults.PrintRec, sizeof(PrintRecord));
	PrValidate(&defaults.PrintRec);
	
	postScriptOptions = progPrefs->PostScriptOptions;
	options = progPrefs->Options;

	if (_tbOnPubScreen)	{
		defaults.PenColor = _tbPenBlack;
		defaults.PaperColor = _tbPenWhite;
	}
}

/*
 *	Read and set defaults from specified file
 *	Return success status
 */

BOOL ReadProgPrefs(TextPtr fileName)
{
	register BOOL success;
	register File file;
	ProgPrefs progPrefs;

	success = FALSE;
	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		goto Exit2;
	if (Read(file, (BYTE *) &progPrefs, sizeof(ProgPrefs)) != sizeof(ProgPrefs) ||
		progPrefs.FileID != ID_PREFSFILE)
		goto Exit1;
	SetDefaults(&progPrefs);
/*
	Get version 3.1 info
	If we couldn't get it, then stick with what we have
*/
	success = TRUE;

Exit1:
	Close(file);
Exit2:
	return (success);
}


/*
 *	Return TRUE if file is a defaults file
 */

BOOL IsPrefsFile(TextPtr fileName)
{
	ULONG fileID;
	UWORD version;
	BOOL success;
	File file;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &fileID, sizeof(ULONG)) == sizeof(ULONG) &&
		fileID == ID_PREFSFILE &&
		Read(file, &version, sizeof(UWORD)) == sizeof(UWORD) &&
		version == PREFS_VERSION)
		success = TRUE;
	Close(file);
	return (success);
}

/*
 *	Load ProWrite preference settings
 *	If no prefs file found, set to standard defaults
 */

void GetProgPrefs()
{
	TextChar pathNameBuff[150];

	SetPathName(pathNameBuff, strPrefsName, TRUE);
	if (!ReadProgPrefs(pathNameBuff)) {
		SetStdDefaults();
	}
}

/*
 *	Save ProWrite preferences settings from given document
 *	Return success status
 */

BOOL SaveProgPrefs(WindowPtr window,TextPtr fileName)
{
	register WORD i;
	register File file;
	register BOOL success;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	UBYTE pathNameBuff[150];
	ProgPrefs progPrefs;
	RGBColor colors[256];
	WORD numColors;
	WORD entries;
	
	BlockClear(&progPrefs, sizeof(ProgPrefs));
	progPrefs.FileID = ID_PREFSFILE;
	progPrefs.Version = PREFS_VERSION;

/*
	Save color palette
	Save as a minimum the standard color palette
*/
	numColors = GetColorTable(screen, &colors);
	if( numColors < NUM_STDCOLORS) 
		progPrefs.NumColors = NUM_STDCOLORS;
	else
		progPrefs.NumColors = numColors;
	entries = (MIN(numColors, progPrefs.NumColors)) >> 1;
	for( i = 0 ; i < entries ; i++ ) {
 		progPrefs.ColorTable[i] = colors[i];
		progPrefs.ColorTable[progPrefs.NumColors - i - 1] = colors[numColors - i - 1];
	}
	
	BlockMove(docData->PrintRec, &defaults.PrintRec, sizeof(PrintRecord));
	defaults.DocFlags = docData->Flags;
	defaults.LabelStyle = docData->LabelStyle;
	defaults.LabelFlags = docData->LabelFlags;
	defaults.LabelRepeat = docData->LabelRepeat;
	defaults.LabelRange = docData->LabelRange;
	for (i=0;i<MAX_LABEL_LEN+2;i++)
		defaults.CustomLabel[i] = docData->CustomLabel[i];

	defaults.PenColor = docData->PenColor;
	defaults.PaperColor = docData->PaperColor;
	defaults.Indent = docData->Indent;
	defaults.LeftMargin = docData->LeftMargin;
	defaults.RightMargin = docData->RightMargin;
	defaults.CPI = docData->CPI;
	defaults.PageNumStyle = docData->PageNumStyle;
	defaults.DateStyle = docData->DateStyle;
	defaults.TimeStyle = docData->TimeStyle;
	
	progPrefs.Defaults = defaults;
	progPrefs.Options = options;
	progPrefs.PostScriptOptions = postScriptOptions;
	
	success = FALSE;
	if (fileName)
		strcpy(pathNameBuff, fileName);
	else
		SetPathName(pathNameBuff, strPrefsName, TRUE);
	if ((file = Open(pathNameBuff, MODE_NEWFILE)) == NULL)
		goto Exit2;

	if (Write(file, (BYTE *) &progPrefs, sizeof(ProgPrefs)) != sizeof(ProgPrefs)) 
		goto Exit1;

	success = TRUE;
Exit1:
	Close(file);
	if (success)
		SaveIcon(pathNameBuff, ICON_PREFS);
Exit2:
	SetDefaults(&progPrefs);
	return (success);
}
