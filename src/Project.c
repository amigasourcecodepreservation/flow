/*
 * Flow
 * Copyright (c) 1989 New Horizons Software, Inc.
 *
 * Project menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Gadget.h>

#include <IFF/IFF.h>

#include "Flow.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WindowPtr	closeWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;
extern BOOL			closeFlag, closeAllFlag, quitFlag ;

extern Options		options;
		 
extern BOOL			titleChanged;

extern LONG			iffError;

extern UBYTE		printerName[];

extern DlgTemplPtr	dlgList[];

extern TextChar	strUntitled[];
extern TextChar	strOpenFile[], strSaveAs[];
extern TextChar strLoadDefaults[],strSaveAsDefaults[];
extern TextChar strPrefsName[];

extern TextChar	strBuff[];

/*
 * Local variables
 */

#define NUM_TOOLTYPES	1

static TextPtr toolTypes[] = { "text" };  /* All others automatically found */

#define SHOWALL_RADBTN  SFP_GET_USER


#define NUM_SUFFIX		2

static TextPtr suffixList[] = {".txt", ".text" };
	
#define NORMAL_RADBTN	SFP_PUT_USER
#define TEXT_RADBTN		(SFP_PUT_USER + 1)
#define HEAD_RADBTN		(SFP_PUT_USER + 2)

#define ID_HEAD		MakeID('H','E','A','D')

#define SAVE_NO_BUTTON	2
#define SAVE_NAME_TEXT	4

#define SAVE_NORMAL		0
#define SAVE_TEXT		1
#define SAVE_HEADINGS	2

static WORD newCount = 1;		 /* Number of "Untitled" windows */
static BOOL showAll;				 /* Inited to FALSE */
static WORD saveMode;

static TextChar		emergencyName[] = "Flow.Recover";

/*
 * Local prototypes
 */

void  SetWindowOffsets(WindowPtr);
BOOL  OpenFileFilter(TextPtr);
BOOL  SaveFile(WindowPtr, TextPtr, Dir, WORD);
WORD  OpenDlgHook(WORD, DialogPtr);
WORD  SaveDlgHook(WORD, DialogPtr);


/*
 *	Set current save time to current time
 */

static void UpdateSaveTime(DocDataPtr docData)
{
	ULONG micros;

	CurrentTime(&docData->LastSaveTime, &micros);
}

/*
 *	Check for and handle auto-saves
 */

void CheckAutoSave()
{
	WORD i;
	ULONG secs, micros;
	DocDataPtr docData;
	WindowPtr window;

	if (options.AutoSave == 0)
		return;
	CurrentTime(&secs, &micros);
	for (i = 0; i < numWindows; i++) {
		window = windowList[i];
		docData = GetWRefCon(window);
		if (secs - docData->LastSaveTime > options.AutoSaveTime*60 &&
			(docData->Flags & WD_MODIFIED))
			DoSave(window, 0);
	}
}
/*
 *	Set left and top offsets for window
 */

static void SetWindowOffsets(WindowPtr window)
{
	register WORD leftOffset, leftIndent, leftMargin;
	WORD maxPage, maxTop, maxLeft;
	register HeadPtr head;
	register DocDataPtr docData = GetWRefCon(window);

	MaxWindowOffsets(window, &maxPage, &maxTop, &maxLeft);
	docData->PageOffset = docData->TopLine = 0;
	head = docData->SelHead;
	leftIndent = leftMargin = LeftOffset(window);
	leftOffset = MIN(leftIndent, leftMargin) - docData->CPI/2;
	if (leftOffset < 0)
		leftOffset = 0;
	if (leftOffset > maxLeft)
		leftOffset = maxLeft;
	docData->LeftOffset = leftOffset;
}

/*
 * Check for file loadability
 * Returns TRUE if file should be shown in SFPGetFile() list
 */

static BOOL OpenFileFilter(TextPtr fileName)
{
	register WORD i, len, len1;
	register BOOL success = TRUE ;
	register File file;
	GroupHeader grpHeader;

	if(!showAll) {
/*
	Check if file has a "text" suffix
*/
		len = strlen(fileName);
		for( i = 0 ; i < NUM_SUFFIX ; i++ ) {
			len1 = strlen(suffixList[i]);
			if( len > len1 && 
				CmpString(fileName + len - len1, suffixList[i], len1, len1, FALSE) == 0)
				return(success);
		}
/*
	Check if this is an IFF file we understand
*/
		success = FALSE;
		if((file = Open(fileName, MODE_OLDFILE)) != NULL) {
			if (Read(file, (BYTE *) &grpHeader, sizeof(GroupHeader)) == sizeof(GroupHeader) &&
				grpHeader.ckID == FORM && grpHeader.grpSubID == ID_HEAD)
				success = TRUE;
			Close(file);
		}
	}
	return(success);
}

/*
 * Create new document window
 */

BOOL DoNew()
{
	register WindowPtr window;
	register DocDataPtr docData;
	register BOOL success = FALSE;
	
	if( numWindows == MAX_WINDOWS ) {
		ErrBeep();
	} else {
		strcpy(strBuff, strUntitled);
		strcat(strBuff, " #");
		NumToString((LONG) newCount, strBuff + strlen(strBuff));
		if( (window = CreateWindow(strBuff)) == NULL) {
			Error(ERR_NO_MEM);
		} else {
			docData = (DocDataPtr) GetWRefCon(window);
			docData->DirLock = NULL;
/*
	Create new outline
*/
			if (!NewOutline(docData)) {
				RemoveWindow(window);
				Error(ERR_NO_MEM);
			} else {
				newCount++;
				
/*
	Finish window setup
*/
				SetWindowOffsets(window);
				DoNewSize(window);
				CauseUpdate(window);
				DoWindowActivate(window, TRUE);
				UpdateSaveTime(docData);
				success = TRUE;
			}
		}
	}
	return(success);
}

/*
 * Open new window and load file
 * Return success status
 */

BOOL OpenFile(TextPtr fileName, Dir dir)
{
	register BOOL success = FALSE;
	LONG error;
	WORD len;
	BOOL newWindow;
	register WindowPtr window;
	register DocDataPtr docData;
	TextChar oldTitle[100];
/*
	Open new window
	If active window (or last window opened) is untitled with nothing in it,
		then use it instead
*/
	window = NULL;
	if( numWindows) {
		window = ActiveWindow();
		if( !IsDocWindow(window))
			window = windowList[numWindows - 1];
		docData = (DocDataPtr) GetWRefCon(window);
		len = strlen(strUntitled);
		GetWTitle(window, oldTitle);
		if( strlen(oldTitle) > len &&
			CmpString(oldTitle, strUntitled, len, len, TRUE) == 0 &&
			docData->DirLock == NULL && (docData->Flags & WD_MODIFIED) == 0 ) {
			DisposeAll(docData);
			SetWTitle(window, fileName);
			ChangeWindowItem(window);
			ActivateWindow(window);
		} else {
			window = NULL;
		}
	}
	newWindow = FALSE;
	if( window == NULL ) {
		if( numWindows != MAX_WINDOWS) {
			if( (window = CreateWindow(fileName)) == NULL ) {
				Error(ERR_NO_MEM);
			} else {
				newWindow = TRUE;
			}
		}
	}
	if( window != NULL ) {
		docData = (DocDataPtr) GetWRefCon(window);
/*
	Load the file
*/
		success = LoadFile(window, docData, fileName, dir);
/*
	Finish window setup
*/
		if( !success ) {
			if( iffError == DOS_ERROR) {
				error = IoErr();
			} else if( iffError == CLIENT_ERROR) {
				error = 0;							/* No memory */
			} else {
				error = -1;
			}
			if(docData->FirstHead) {			/* Necessary to thwart update attempt */
				DisposeAllHeads(docData->FirstHead,docData);
				docData->FirstHead = NULL;
			}
			DOSError(ERR_OPEN, error);			/* An update of the window occurs here! */
			if( newWindow ) {
				RemoveWindow(window);
				return(FALSE);
			}
			(void) NewOutline(docData);		/* This better not fail */
			SetWTitle(window, oldTitle);
			ChangeWindowItem(window);
		}
		SetWindowOffsets(window);
		DoNewSize(window);
		CauseUpdate(window);
		DoWindowActivate(window, TRUE);
	}
	if (success)
		UpdateSaveTime(docData);
	return(success);
}

/*
 *	Open file dialog hook
 */

static WORD OpenDlgHook(WORD item, DialogPtr dlg)
{
	GadgetPtr gadgList = dlg->FirstGadget;

	switch( item ) {
	case SHOWALL_RADBTN:
		showAll = !showAll ;
		item = SFPMSG_RELIST ;
	case SFPMSG_INIT:
		SetGadgetItemValue(gadgList, SHOWALL_RADBTN, dlg, NULL, showAll);
		break;
	default:
		break;
	}
	return (item);
}

/*
 * Save file dialog hook
 */
 
static WORD SaveDlgHook(WORD item, DialogPtr dlg)
{
	GadgetPtr gadgList = dlg->FirstGadget;
	
	if( item == SFPMSG_INIT || item == NORMAL_RADBTN || item == TEXT_RADBTN ||
		item == HEAD_RADBTN ) {
		if( item == NORMAL_RADBTN ) 
			saveMode = SAVE_NORMAL;
		else if( item == TEXT_RADBTN ) {
			saveMode = SAVE_TEXT;
		}
		else if (item == HEAD_RADBTN) {
			saveMode = SAVE_HEADINGS;
		}
		SetGadgetItemValue( gadgList, NORMAL_RADBTN, dlg, NULL, (BOOL)
				saveMode == SAVE_NORMAL ) ;
		SetGadgetItemValue( gadgList, TEXT_RADBTN, dlg, NULL, (BOOL)
				saveMode == SAVE_TEXT ) ;
		SetGadgetItemValue( gadgList, HEAD_RADBTN, dlg, NULL, (BOOL)
				saveMode == SAVE_HEADINGS );
	}
	return( item ) ;
}

/*
 * Open document
 * If fileName is not NULL, open specified file, otherwise put up "Open" dialog
 */

BOOL DoOpen(WindowPtr window, UWORD modifier, TextPtr fileName )
{
	WORD len;
/*	BOOL isDocWindow; */
	SFReply sfReply;
	register BOOL success = FALSE;
	
	if( numWindows == MAX_WINDOWS ) {
		ErrBeep();
	} else {
		if( fileName ) {
			sfReply.DirLock = ConvertFileName(fileName);
			len = strlen(fileName);
			if( len == 0 || len > MAX_FILENAME_LEN) {
				UnLock(sfReply.DirLock);	/* FAILED! */
			} else {
				success = TRUE;
				strcpy(sfReply.Name, fileName);
			}
		} else {
		/*
			isDocWindow = ((WindowNum(window) != -1 &&
					((DocDataPtr) GetWRefCon(window))->FirstHead));
		*/
/*
	If ALT key pressed then show list of ALL files
*/
			showAll = (modifier & ALTKEYS) || options.ShowAllFiles ;
/*
	Get file name to load
*/
			DoWindowActivate(window,FALSE);  /* Since INACTIVE event will be lost */
			BeginWait();
			SFPGetFile(screen, mainMsgPort, strOpenFile, DialogFilter, OpenDlgHook,
					dlgList[DLG_OPENFILE], NUM_TOOLTYPES, toolTypes, OpenFileFilter, &sfReply);
			EndWait();
			DoWindowActivate(window,TRUE);
			if (sfReply.Result != SFP_OK) {
				if (sfReply.Result == SFP_NOMEM)
					Error(ERR_NO_MEM);
			} else {
				success = TRUE;
			}
			RefreshWindows();
		}
	}
	if( success ) {
		
/*
	Open new window and load file
*/
		success = OpenFile( sfReply.Name, sfReply.DirLock );
		if( !success && IsDocWindow(window))
			CursorOn(window);
	}
	return(success);
}

/*
 * Save file to specified fileName, dirLock, and mode (Normal or Text Only)
 * Return success status
 */

BOOL SaveFile(WindowPtr window, TextPtr fileName, Dir dirLock, WORD mode)
{
#ifdef DEMO
	DOSError(ERR_SAVE, -2);
	return (FALSE);
#else
	register LONG error;
	register BOOL success;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	BeginWait();

	SetCurrentDir(dirLock);
	SetStdPointer(window, POINTER_WAIT);
	SetBusyPointer(window);
	
	success = (options.MakeBackups) ? MakeBackup(fileName) : TRUE;
	if (success) {
		switch (mode) {
			case SAVE_NORMAL :
				success = SaveHEADFile(docData, fileName);
				break;
			case SAVE_TEXT :
				success = SaveTextFile(docData, fileName,FALSE);
				break;
			case SAVE_HEADINGS :
				success = SaveTextFile(docData, fileName,TRUE);
				break;
			}
	}
/*
	If successful then save file name and set window to new title
*/
	EndWait();
	if (!success) {
		if (iffError == DOS_ERROR) {
			error = IoErr();
			if (error != ERROR_DISK_WRITE_PROTECTED) 
				DeleteFile(fileName);
		}
		else
			error = 0;							  /* No memory (?) */
		DOSError(ERR_SAVE, error);
		if (dirLock != docData->DirLock)
			UnLock(dirLock);
	} else {
		SaveIcon(fileName, (mode == SAVE_NORMAL ) ? ICON_DOC : ICON_TEXT);
		if (fileName != docData->FileName) {
			strcpy(docData->FileName, fileName);
			SetWTitle(window, docData->FileName);
			ChangeWindowItem(window);
		}
/*
	A text save is not considered a full save
*/

		if( docData->DirLock && dirLock != docData->DirLock ) {
			UnLock( docData->DirLock );
			docData->DirLock = NULL;
		}
		if( mode == SAVE_NORMAL ) 
			docData->DirLock = dirLock;
		else
			UnLock( dirLock );
		docData->Flags |= WD_NAMED;
		docData->Flags &= ~WD_MODIFIED;
		SetProjectMenu();
		if (success)
			UpdateSaveTime(docData);
	}
	return (success);
#endif
}

/*
 * Close outline window
 * If close is successful return TRUE, else return FALSE
 */

BOOL DoClose(WindowPtr window)
{
	register WORD item;
	Dir lock;
	register DialogPtr dlg;
	register WindowPtr activeWindow;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = FALSE;
	
	if (titleChanged)
		FixTitle();
	if( IsDocWindow(window) ) {	
		if (docData->Flags & WD_MODIFIED) {
			WindowToFront(window);
			Delay(5);
			activeWindow = ActiveWindow();
			if( window != activeWindow ) {
				if( IsDocWindow(activeWindow))
					DoWindowActivate(activeWindow, FALSE);
				DoWindowActivate(window, TRUE);
				CursorOff(window);
			}
			BeginWait();
			GetWTitle(window, strBuff);
			dlgList[DLG_SAVECHANGES]->Gadgets[SAVE_NAME_TEXT].Info = strBuff;
			if ((dlg = GetDialog(dlgList[DLG_SAVECHANGES], screen, mainMsgPort)) == NULL) {
				EndWait();
				Error(ERR_SAVE_NO_MEM);
				lock = Lock("DF0:", ACCESS_READ);
				if (!SaveFile(window, emergencyName, lock, NORMAL_RADBTN))
					return(success);
			} else {
				OutlineOKButton(dlg);
				StdBeep();
				do {
					item = ModalDialog(mainMsgPort, dlg, DialogFilter);
				} while (item != OK_BUTTON && item != CANCEL_BUTTON &&
					 item != SAVE_NO_BUTTON);
				DisposeDialog(dlg);
				EndWait();
				if (item == CANCEL_BUTTON ||
					(item == OK_BUTTON && !DoSave(window, 0)))
					return(success);
			}
		}
		DoWindowActivate(window, FALSE);
		SetStdPointer(window, POINTER_WAIT);
		if (docData->DirLock)
			UnLock(docData->DirLock);
		DisposeAll(docData);
		RemoveWindow(window);
		SetAllMenus();
		success = TRUE;
	}
	return(success);
}

/*
 * Save outline
 * Return success status
 */

BOOL DoSave(WindowPtr window, UWORD modifier)
{
	register BOOL success;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( ((modifier & SHIFTKEYS) && (modifier & CMDKEY)) ||
		docData->DirLock == NULL )
		success = DoSaveAs(window);
	else
		success = SaveFile(window, docData->FileName, docData->DirLock, SAVE_NORMAL);
	return (success);
}

/*
 * Save project with new name
 */

BOOL DoSaveAs(WindowPtr window)
{
	register BOOL success = FALSE;
	Dir lock;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	SFReply sfReply;

/*
	Get file name to load
*/
	BeginWait();
	if (docData->DirLock)
		SetCurrentDir(docData->DirLock);
/*
	Get new name for file
*/
	saveMode = SAVE_NORMAL;
	SFPPutFile(screen, mainMsgPort, strSaveAs, docData->FileName,
				DialogFilter, SaveDlgHook, dlgList[DLG_SAVEAS], &sfReply);
	EndWait();
/*
	If document is untitled and cannot get name, do emergency save
*/
	if( sfReply.Result != SFP_CANCEL ) {
		if (sfReply.Result == SFP_NOMEM) {
			if ((docData->Flags & WD_MODIFIED) == 0) {
				Error(ERR_NO_MEM);
			} else {
				Error(ERR_SAVE_NO_MEM);
				if( !(lock = Lock("DF0:", ACCESS_READ)) == NULL ||
							!SaveFile(window, emergencyName, lock, NORMAL_RADBTN))
					success = TRUE;
			}
		} else {
/*
	Save document to specified file
*/
			success = SaveFile(window, sfReply.Name, sfReply.DirLock, saveMode);
		}
	}
	return (success);
}

/*
 * Revert to last version saved
 */

BOOL DoRevert(WindowPtr window)
{
	register BOOL success = FALSE;
	register WORD item;
	register LONG error;
	register DialogPtr dlg;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
		
	if ((docData->DirLock) == NULL) {
		ErrBeep();
	} else {
/*
	Confirm the revert operation
*/
		BeginWait();
		if ((dlg = GetDialog(dlgList[DLG_REVERT], screen, mainMsgPort)) == NULL) {
			EndWait();
			Error(ERR_NO_MEM);
		} else {
			OutlineOKButton(dlg);
			StdBeep();
			do {
				item = ModalDialog(mainMsgPort, dlg, DialogFilter);
			} while (item != OK_BUTTON && item != CANCEL_BUTTON);
			DisposeDialog(dlg);
			EndWait();
			if (item != CANCEL_BUTTON) {
/*
	Dispose of current outline and load original one
*/
				SetStdPointer(window, POINTER_WAIT);
				DisposeAll(docData);
				success = LoadFile(window, docData, docData->FileName, docData->DirLock);
/*
	Finish window setup
*/
				if (!success) {
					if (iffError == DOS_ERROR)
						error = IoErr();
					else if (iffError == CLIENT_ERROR)
						error = 0;							  /* No memory */
					else
						error = -1;							 /* Bad file */
					DOSError(ERR_OPEN, error);
					(void) NewOutline(docData);				/* If this fails !!!! */
				} else
					docData->Flags &= ~WD_MODIFIED;
				SetWindowOffsets(window);
				DoNewSize(window);
				CauseUpdate(window);
				DoWindowActivate(window, TRUE);
				SetAllMenus();
				if (success)
					UpdateSaveTime(docData);
			}
		}
	}
	return(success);
}

/*
 * Page setup
 */

BOOL DoPageSetup(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success ;
	
	
	success = PageSetupDialog(window, docData->PrintRec) ;
	if( success ) {
		SetStdPointer(window, POINTER_WAIT);
		ReformatOutline(docData);
		RefreshWindow(window);
		CursorOn(window);
		AdjustScrollBars(window);
		DocumentModified(docData);
		success = TRUE;
	}
	return(success);
}

/*
 * Print project
 */

BOOL DoPrint(WindowPtr window, UWORD modifier, BOOL printOne)
{
	register PrintRecPtr printRec;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = FALSE;
	
	if( IsDocWindow( window ) ) {
		if( modifier & ALTKEYS )
			printOne = TRUE;
		UpdateWindows();
		printRec = docData->PrintRec;
		printRec->Copies = 1;
		printRec->FirstPage = docData->StartPage;
		printRec->LastPage = (docData->StartPage + docData->Pages) - 1;
		if( printOne )
			(void) PrValidate(printRec);
		else if( !PrintDialog(window, printRec))
			return(FALSE);
/*
 * Now print the pages
 */
#ifdef DEMO
		DOSError(ERR_PRINT, -2);
#else
		success = PrintOutline(window,docData);
		if( !success )
			Error(ERR_PRINT);
#endif
	}
	return(success);
}

/*
 * Save program preferences
 */

BOOL DoSavePrefs(WindowPtr window)
{
	WORD item;
	BOOL success;
		
	item = StdDialog(DLG_SAVEPREFS);
	if (item == CANCEL_BUTTON)
		return (FALSE);
		
/*
 * Save settings
 */
	SetStdPointer(window, POINTER_WAIT);
	success = SaveProgPrefs(window,NULL);
	if( !success )
		DOSError(ERR_SAVE, IoErr());
	return( success );
}

/*
 *	Save program defaults with specified file name
 */

BOOL DoSaveAsDefaults(WindowPtr window)
{
	BOOL success;
	SFReply sfReply;
/*
	Get file name to save
*/
	BeginWait();
	SFPPutFile(screen, mainMsgPort, strSaveAsDefaults, strPrefsName,
			   DialogFilter, NULL, NULL, &sfReply);
	EndWait();
	if (sfReply.Result == SFP_CANCEL)
		return (FALSE);
	UnLock(sfReply.DirLock);
/*
	Save settings
*/
	SetStdPointer(window, POINTER_WAIT);
	success = SaveProgPrefs(window, sfReply.Name);
	if (!success)
		DOSError(ERR_SAVE, IoErr());
	return (success);
}

/*
 *	Load defaults
 */

static BOOL DoLoadDefaults(void)
{
	SFReply sfReply;

/*
	Get file name of defaults file
*/
	BeginWait();
	SFPGetFile(screen, mainMsgPort, strLoadDefaults, DialogFilter, NULL,
			   NULL, 0, NULL, IsPrefsFile, &sfReply);
	EndWait();
	if (sfReply.Result != SFP_OK) {
		if (sfReply.Result == SFP_NOMEM)
			Error(ERR_NO_MEM);
		return (FALSE);
	}
	RefreshWindows();
	UnLock(sfReply.DirLock);
/*
	Load defaults
*/
	return (ReadProgPrefs(sfReply.Name));
}

/*
 *	Handle defaults submenu
 */

static BOOL DoDefaultsMenu(WindowPtr window, UWORD item, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window) && item != LOADDEFAULTS_SUBITEM)
		return (FALSE);
	success = FALSE;
	switch (item) {
	case SAVEDEFAULTS_SUBITEM:
		success = DoSavePrefs(window);
		break;
	case SAVEASDEFAULTS_SUBITEM:
		success = DoSaveAsDefaults(window);
		break;
	case LOADDEFAULTS_SUBITEM:
		success = DoLoadDefaults();
		break;
	}
	return (success);
}

/*
 * Process project menu selection
 */

BOOL DoProjectMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	register BOOL success = FALSE;
	register BOOL isDoc = IsDocWindow(window);
	
	if( isDoc || item <= SAVE_ITEM || item == QUIT_ITEM ) {
		switch (item) {
		case NEW_ITEM:
			success = DoNew();
			break;
		case OPEN_ITEM:
			success = DoOpen(window, modifier, NULL);
			break;
		case CLOSE_ITEM:
			if( isDoc || ( GetWKind(window) == WKIND_DIALOG) ) {
				if( modifier & ALTKEYS )
					closeAllFlag = TRUE;
				else {
					closeWindow = window;
					closeFlag = TRUE;
				}
			}
			success = TRUE;
			break;
		case SAVE_ITEM:
			success = DoSave(window, modifier);
			break;
		case SAVEAS_ITEM:
			success = DoSaveAs(window);
			break;
		case REVERT_ITEM:
			success = DoRevert(window);
			break;
		case PAGESETUP_ITEM:
			success = DoPageSetup(window);
			break;
		case PRINT_ITEM:
		case PRINTONE_ITEM:
			success = DoPrint(window, modifier, (BOOL) (item == PRINTONE_ITEM));
			break;
		case SAVEPREFS_ITEM:
			success = DoDefaultsMenu(window,sub,modifier);
			break;
		case QUIT_ITEM:
			quitFlag = TRUE;
			success = TRUE;
			break;
		}
	}
	return(success);
}
