/*
 *	Amiga Toolbox
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Window handler
 */

#ifndef TOOLBOX_WINDOW_H
#define TOOLBOX_WINDOW_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	Window kinds
 *	Stored in lower two bits of window UserData
 */

#define WKIND_DIALOG	3		/* Values 0 - 2 are for user */

/*
 *	Routines prototypes
 */

void	GetWindowRect(WindowPtr, RectPtr);
void	InvalRect(WindowPtr, RectPtr);
void	SetWTitle(WindowPtr, TextPtr);
void	GetWTitle(WindowPtr, TextPtr);
void	SetWRefCon(WindowPtr, Ptr);
Ptr		GetWRefCon(WindowPtr);
void	SetWKind(WindowPtr, WORD);
WORD	GetWKind(WindowPtr);
void	CloseWindowSafely(WindowPtr, MsgPortPtr);

#endif
