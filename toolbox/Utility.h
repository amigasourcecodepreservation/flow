/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Misc utility routines
 */

#ifndef TOOLBOX_UTILITY_H
#define TOOLBOX_UTILITY_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef LIBRARTES_DOS_H
#include <libraries/dos.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	System versions
 */

#define OSVERSION_1_2	33
#define OSVERSION_1_3	34
#define OSVERSION_2_0	36
#define OSVERSION_2_0_4	37

/*
 *	Pointer types
 */

enum {
	POINTER_ARROW = 0,
	POINTER_IBEAM,
	POINTER_CROSS,
	POINTER_PLUS,
	POINTER_WAIT
};

#define POINTER_MAX_TYPE	POINTER_WAIT

/*
 *	Pointer structure definition
 */

typedef struct {
    WORD	Width, Height;
    WORD	XOffset, YOffset;
    UWORD	*PointerData;
} Pointer, *PointerPtr;

/*
 *	Date formats
 */

enum {
	DATE_SHORT = 0,		/* 3/1/90 */
	DATE_ABBR,			/* Mar 1, 1990 */
	DATE_LONG,			/* March 1, 1990 */
	DATE_ABBRDAY,		/* Thu, Mar 1, 1990 */
	DATE_LONGDAY,		/* Thursday, March 1, 1990 */
	DATE_MILITARY		/* 1 Mar 90 */
};

#define DATE_CUSTOM		-1

/*
 *	Custom date characters
 */

enum {
	DAY_NUMBER = 0x01,
	DAY_NAME,
	DAY_NAMEABBREV,
	MONTH_NUMBER,
	MONTH_NAME,
	MONTH_NAMEABBREV,
	YEAR_LONG,
	YEAR_SHORT,
	DAY_NUMZERO,
	MONTH_NUMZERO
};

/*
 *	Arrays
 */

extern TextChar	toUpper[];		/* Convert character to upper case */
extern TextChar	toLower[];		/* Convert character to lower case */
extern BYTE		wordChar[];		/* 1 if char is valid word char, 0 otherwise */

/*
 *	Prototypes
 */

void	InitToolbox(ScreenPtr);
void	AutoActivateEnable(BOOL);

UWORD	LibraryVersion(struct Library *);
UWORD	SystemVersion(void);

void	SetStdPointer(WindowPtr, UWORD);
void	ObscurePointer(WindowPtr);

void	SysBeep(UWORD);
void	ConvertKeyMsg(IntuiMsgPtr);
BOOL	WaitMouseUp(MsgPortPtr, WindowPtr);

WindowPtr	ActiveWindow(void);

WORD	CmpString(TextPtr, TextPtr, WORD, WORD, BOOL);
void	NumToString(LONG, TextPtr);

void	MonthName(WORD, BOOL, TextPtr);
LONG	StringToNum(TextPtr);
void	DateString(struct DateStamp *, WORD, TextPtr);
void	TimeString(struct DateStamp *, BOOL, BOOL, TextPtr);

TextFontPtr	GetFont(TextAttrPtr);

#endif
