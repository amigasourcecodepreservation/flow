/*
 *	Amiga Toolbox
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Misc graphics routines
 */

#ifndef TOOLBOX_GRAPHICS_H
#define TOOLBOX_GRAPHICS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Maximum blitter blit width
 */

#define MAX_BLIT_SIZE	1008

/*
 *	Routine prototypes
 */

void		LocalToGlobal(WindowPtr, WORD *, WORD *);
ScreenPtr	WhichScreen(WORD, WORD);

void	ScrollRect(LayerPtr, WORD, WORD, WORD, WORD, WORD, WORD);

void	LoadRGB4CM(ColorMapPtr, UWORD *, WORD);
RastPtr	OpenPort(void);
void	ClosePort(RastPtr);

RegionPtr	GetClip(LayerPtr);
void		SetClip(LayerPtr, RegionPtr);
void		SetRectClip(LayerPtr, Rectangle *);

BOOL	LayerObscured(LayerPtr);

RastPtr	CreateRastPort(WORD, WORD, WORD);
void	DisposeRastPort(RastPtr);

BitMapPtr	CreateBitMap(WORD, WORD, WORD, BOOL);
void		DisposeBitMap(BitMapPtr);

void	SetRect(RectPtr, WORD, WORD, WORD, WORD);
void	OffsetRect(RectPtr, WORD, WORD);
void	InsetRect(RectPtr, WORD, WORD);
BOOL	SectRect(RectPtr, RectPtr, RectPtr);
void	UnionRect(RectPtr, RectPtr, RectPtr);
BOOL	EmptyRect(RectPtr);
BOOL	EqualRect(RectPtr, RectPtr);
BOOL	PtInRect(PointPtr, RectPtr);

void	FrameRect(RastPtr, RectPtr);
void	FillRect(RastPtr, RectPtr);

void	FramePoly(RastPtr, WORD, PointPtr);
void	FillPoly(RastPtr, WORD, PointPtr);

#endif
