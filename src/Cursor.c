/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Cursor control routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <Toolbox/Window.h>
#include <Toolbox/ColorPick.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern WORD	charHeight, charWidth;

extern WORD		dragType, dragDepth;
extern HeadPtr	dragHead;

extern Options	options;

extern BOOL cursorRepeat;

extern BOOL drawOn;

extern UBYTE _tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight;

/*
 *	Local variables
 */
 
static WORD cursorCol;		/* Cursor position for repeated up/down movement */
static BOOL dragOn;

/*
 * Prototypes
 */
 
void DrawDragLine(WindowPtr);
void CursorBeep(void);

/*
 *	Display/Remove heading drag line
 *	dragHead is heading to draw drag line under
 *	dragDepth is depth to draw drag depth marker
 *	If dragHead == NULL then drag drag line above first heading
 *	If dragDepth == -1 then do not draw drag depth marker
 *	Only one heading can be dragged at a time
 */

static void DrawDragLine(WindowPtr window)
{
	WORD dragLine, lastLine, linePos, depthPos, topPos, bottomPos;
	WORD minX = window->BorderLeft + LEFT_MARGIN;
	WORD minY = window->BorderTop;
	WORD maxY = window->Height - window->BorderBottom - 1;
	WORD maxX = window->Width - window->BorderRight - 1;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	SetDrMd(rPort, COMPLEMENT);
	SetAPen(rPort, _tbPenBlack);
	SetDrPt(rPort, 0xFFFF);
	if (dragType != DRAG_HEAD)
		return;
	dragLine = (dragHead) ? LineNumber(docData, dragHead, dragHead->Len) : -1;
	depthPos = (dragDepth*docData->Indent - docData->LeftOffset)*charWidth + minX;
/*
	If within sub-heads of currentHead then just draw depth marker
*/
	if (dragHead == docData->SelHead) {
		if (dragDepth == -1)
			return;
		lastLine = dragLine + NumSubLines(dragHead);
		topPos = (dragLine - dragHead->NumLines + 1 - docData->TopLine)
				 *charHeight + minY - (charHeight/2 + 1);
		bottomPos = (lastLine - docData->TopLine)*charHeight + minY
					+ (3*charHeight/2 - 1);
	}
/*
	General case:  draw drag line and depth marker
*/
	else {
		linePos = (dragLine - docData->TopLine + 1)*charHeight - 1 + minY;
		if (dragLine == -1)
			linePos++;						/* Special case */
		if (linePos >= minY && linePos <= maxY) {
			Move(rPort, window->BorderLeft, linePos);
			Draw(rPort, maxX, linePos);
		}
		if (dragDepth == -1)
			return;
		topPos = linePos - charHeight/2;
		bottomPos = linePos + charHeight/2;
	}
/*
	Draw vertical drag marker using parameters set-up above
*/
	if (depthPos >= minX && depthPos <= maxX &&
		topPos <= maxY && bottomPos >= minY) {
		Move(rPort, depthPos, topPos);
		Draw(rPort, depthPos, bottomPos);
		Move(rPort, depthPos + 1, topPos);
		Draw(rPort, depthPos + 1, bottomPos);
	}
}

/*
 *	Turn the drag line on
 */

void DragLineOn(WindowPtr window)
{
	if (dragOn)
		return;
	DrawDragLine(window);
	dragOn = TRUE;
}

/*
 *	Turn the drag line off
 */

void DragLineOff(WindowPtr window)
{
	if (!dragOn)
		return;
	DrawDragLine(window);
	dragOn = FALSE;
}

/*
 *	Invert colors of given rectangle
 */

void InvertRect(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;
	DocDataPtr	docData = GetWRefCon(window);
	
	GetTextRect(window, &rect);
	if (yStart > rect.MaxY || yEnd < rect.MinY ||
		xStart > rect.MaxX || xEnd < rect.MinX)
		return;
	if (yStart < rect.MinY)
		yStart = rect.MinY;
	if (yEnd > rect.MaxY)
		yEnd = rect.MaxY;
	if (xStart <= xEnd && yStart <= yEnd) {
		SetDrMd(rPort, COMPLEMENT);
		SetWrMsk(rPort,  docData->PenColor ^ docData->PaperColor);
		RectFill(rPort, xStart, yStart, xEnd, yEnd);
		SetWrMsk(rPort, 0xFF);
	}
}

/*
 *	Draw the selection range by inverting the specified selection range
 */

static void DrawSelRange(WindowPtr window, WORD selStart, WORD selEnd)
{
	register WORD line, startLine, endLine, startLoc, endLoc;
	WORD xStart, xEnd;
	LONG yStart, yEnd;
	register HeadPtr selHead;
	register RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	SetDrMd(rPort, COMPLEMENT);
	SetWrMsk(rPort, docData->PenColor ^ docData->PaperColor);
/*
	Get the starting and ending locations
*/
	selHead = docData->SelHead;
	startLine = WhichLine(selHead, selStart);
	endLine = WhichLine(selHead, selEnd);
/*
	Highlight the selected lines
*/
	yStart = VertPosition(window, selHead, selStart);
	for (line = startLine; line <= endLine; line++) {
		startLoc = (line == startLine) ? selStart : selHead->LineStarts[line];
		endLoc = (line == endLine) ? selEnd : selHead->LineStarts[line + 1] - 1;
		xStart = HorizPosition(window, selHead, startLoc);
		if (xStart < window->BorderLeft + LEFT_MARGIN)
			xStart = window->BorderLeft + LEFT_MARGIN;
		xEnd = (line == endLine) ?
				HorizPosition(window, selHead, endLoc) - 1 :
				(docData->PageWidth - docData->LeftMargin - docData->RightMargin
				- docData->LeftOffset)*charWidth + window->BorderLeft
				+ LEFT_MARGIN - 1;
		yEnd = yStart + (charHeight - 1);
		if (xStart < xEnd)				/* Don't draw if xStart == xEnd */
			InvertRect(window, xStart, (WORD) yStart, xEnd, (WORD) yEnd);
		yStart += charHeight;
	}
}

/*
 *	Draw the insertion point by inverting a vertical bar
 */

static void DrawInsPoint(WindowPtr window, WORD loc)
{
	register WORD xStart;
	register LONG yStart;
	register RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	SetDrMd(rPort,COMPLEMENT);
	SetWrMsk(rPort,_tbPenBlack ^ _tbPenWhite);

	xStart = HorizPosition(window, docData->SelHead, loc);
	if (xStart < window->BorderLeft + LEFT_MARGIN)
		return;
	yStart = VertPosition(window, docData->SelHead, loc);
	Move(rPort, xStart, yStart);
	Draw(rPort, xStart, yStart + (charHeight - 1));
	Move(rPort, xStart + 1, yStart);
	Draw(rPort, xStart + 1, yStart + (charHeight - 1));
	SetWrMsk(rPort, 0xFF);
}

/*
 *	Draw selected heading(s)
 *	If dragging in progress then draw drag line
 */

static void DrawSelHead(WindowPtr window)
{
	register WORD xStart, xEnd;
	register LONG yStart, yEnd, yTemp;
	register HeadPtr selHead;
	register RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	SetDrMd(rPort, COMPLEMENT);
	SetAPen(rPort, _tbPenBlack);
	for (selHead = docData->SelHead; selHead && (selHead->Flags & HD_SELECTED);
		 selHead = selHead->Next) {
		xStart = window->BorderLeft;
		xEnd = window->Width - window->BorderRight - 1;
		yStart = VertPosition(window, selHead, 0);
		yEnd = yStart + selHead->NumLines*charHeight - 1;
		InvertRect(window, xStart, (WORD) yStart,
					(WORD) (xStart + (SELECT_WIDTH - 1)), (WORD) yEnd);
		xStart += SELECT_WIDTH;
		yTemp = yEnd;
		yEnd += charHeight*NumSubLines(selHead);
		yEnd = MIN(window->Height - window->BorderBottom, yEnd);
		/*
		if (yEnd > yTemp) {
			Move(rPort, xStart, yTemp);
			Draw(rPort, xStart, yEnd - 1);
			Move(rPort, xStart + 1, yTemp);
			Draw(rPort, xStart + 1, yEnd - 1);
		}
		*/
		InvertRect(window,xStart,yStart,xEnd,yEnd);
		/*
		SetAPen(rPort, docData->PaperColor);
		SetDrMd(rPort, JAM1);
		SetDrPt(rPort, 0xFFFF);
		Move(rPort, xStart, yStart);
		Draw(rPort, xEnd, yStart);
		Move(rPort, xStart, yEnd);
		Draw(rPort, xEnd, yEnd);	
		*/ 
	}
	if (dragType == DRAG_HEAD && dragOn) {
		DrawDragLine(window);
		dragOn = TRUE;
	}
}

/*
 *	Draw the cursor (either insertion point or selection range)
 */

static void DrawCursor(WindowPtr window)
{
	WORD selStart, selEnd;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( drawOn ) {
		selStart = docData->SelStart;
		selEnd = docData->SelEnd;
		if (docData->SelHead->Flags & HD_SELECTED)
			DrawSelHead(window);
		else if (selStart != selEnd)
			DrawSelRange(window, selStart, selEnd);
		else
			DrawInsPoint(window, selStart);
	}
}

/*
 *	Turn cursor on (must have been previously erased or turned off)
 */

void CursorDrawOn(WindowPtr window)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	DrawCursor(window);
	docData->CursorOn = TRUE;
	docData->BlinkCount = options.BlinkPeriod;
}

/*
 *	Blink the cursor
 */

void CursorBlink(WindowPtr window)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( IsDocWindow(window) && options.BlinkPeriod ) {
		if ((docData->SelHead->Flags & HD_SELECTED) ||
			docData->SelStart != docData->SelEnd)
			return;
		if (docData->BlinkCount-- == 0) {
			docData->BlinkCount = options.BlinkPeriod;
			if( AttemptLockLayerRom(window->WLayer) ) {
				DrawInsPoint(window, docData->SelStart);
				docData->CursorOn = !docData->CursorOn;
				UnlockLayerRom(window->WLayer);
			}
		}
	}
}

/*
 *	If the cursor is blinked off, then turn it on
 */

void CursorOn(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (!docData->CursorOn) {
		DrawCursor(window);
		docData->CursorOn = TRUE;
		docData->BlinkCount = options.BlinkPeriod;
	}
}

/*
 *	If the cursor is blinked on, then turn it off
 */

void CursorOff(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->CursorOn) {
		DrawCursor(window);
		docData->CursorOn = FALSE;
	}
}

/*
 *	Draw changes to cursor
 */

void CursorChangeOn(WindowPtr window, WORD oldSelStart, WORD oldSelEnd)
{
	WORD selStart, selEnd;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	selStart = docData->SelStart;
	selEnd = docData->SelEnd;
	if (docData->SelHead->Flags & HD_SELECTED)
		return;
	if (!docData->CursorOn)
		CursorOn(window);
	else {
		if (oldSelStart == oldSelEnd)
			DrawInsPoint(window, oldSelStart);
		if (selStart < oldSelStart)
			DrawSelRange(window, selStart, oldSelStart);
		else if (selStart > oldSelStart)
				DrawSelRange(window, oldSelStart, selStart);
		if (oldSelEnd < selEnd)
			DrawSelRange(window, oldSelEnd, selEnd);
		else if (oldSelEnd > selEnd)
			DrawSelRange(window, selEnd, oldSelEnd);
		if (selStart == selEnd)
			DrawInsPoint(window, selStart);
	}
}

/*
 *	Move cursor up one line, window, or to top of outline
 */

BOOL DoCursorUp(WindowPtr window, UWORD qualifier)
{
	register WORD loc, line, lineLoc, headLine;
	WORD oldStart, oldEnd;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	BOOL success = TRUE;
	BOOL ctrlKey;
	BOOL align = docData->LabelFlags & LABEL_ALIGN_MASK;
	WORD start ;
	register BOOL oldCursorRepeat = cursorRepeat;
	
	oldStart = docData->SelStart;
	oldEnd = docData->SelEnd;
	selHead = docData->SelHead;
	start = selHead->BodyStart;
	if (oldStart == oldEnd)
		docData->DragStart = oldStart;
	loc = (docData->DragStart == oldStart) ? oldEnd : oldStart;
	line = LineNumber(docData, selHead, loc);
	headLine = WhichLine(selHead, loc);

	lineLoc = loc - selHead->LineStarts[headLine]
				+ docData->Indent*HeadDepth(selHead);

	if( align && (headLine == 0) )
		lineLoc -= start;

	if( !oldCursorRepeat ) {
		cursorCol = lineLoc;
		/*
		if( headLine == 0 )
			cursorCol -= start;
		*/
		cursorRepeat = TRUE;
	}
	
	if (selHead->Flags & HD_SELECTED) {
		qualifier &= ~MODIFIER_SEL;
		oldStart = selHead->BodyStart;
		oldEnd = oldStart + 1;	/* Any old value which causes below "if" to fail */
	}
	ctrlKey = (BOOL) ((qualifier & MODIFIER_SEL) == MODIFIER_SEL);
	if( !ctrlKey )
		CursorOff(window);
	UnSelectHeads(docData);
	if( ( oldStart == oldEnd ) || ctrlKey ) {
/*		
	If the top line, or the top line in a head while extending selection, beep!
*/
		if( ( line == 0 ) || ( ctrlKey && (headLine == 0) ) )
			success = FALSE;
		else {
/*
	Move to top of outline
*/
			if (qualifier & ALTKEYS)
				line = 0;
/*
	Move up one window
*/
			else if (qualifier & SHIFTKEYS)
				line -= WINDOW_LINES(window) - 1;
/*
	Move up one line
*/
			else /*if( oldStart == oldEnd )*/
				line--;
/*
	Set selection point
*/
			if (line < 0)
				line = 0;
			selHead = WhichHead(docData, line);
			if( ctrlKey && selHead != docData->SelHead) {
				selHead = docData->SelHead;
				headLine = 0;
			} else {
				docData->SelHead = selHead;
				headLine = line - LineNumber(docData, selHead, 0);
			}
			if( oldCursorRepeat ) {
				lineLoc = cursorCol;
			}
			lineLoc -= docData->Indent*HeadDepth(selHead);
			
			if( align && (headLine == 0) )
				lineLoc += start;
			
			if( lineLoc < 0 ) {
				lineLoc = 0;
			}	
			loc = selHead->LineStarts[headLine] + lineLoc;
			if( !headLine && (loc < (selHead->LineStarts[headLine] + selHead->BodyStart)))
				loc = selHead->LineStarts[headLine] + selHead->BodyStart;
			else if (headLine < selHead->NumLines - 1 &&
					loc >= selHead->LineStarts[headLine + 1])
				loc = selHead->LineStarts[headLine + 1] - 1;
			else if (loc > selHead->Len)
				loc = selHead->Len;
		}
	} else
		loc = oldStart;
/*
	If control key down, then make this a selection range
	Display new cursor position
*/
	if( !success )
		CursorBeep();
	else {
		if( !ctrlKey ) {
			docData->SelStart = docData->SelEnd = docData->DragStart = loc;
			CursorOn(window);
		} else {
			SetSelRange(docData, loc);
			CursorChangeOn(window, oldStart, oldEnd);
		}
		ScrollToLocation(window, selHead, loc);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

/*
 *	Move cursor down one line, window, or to end of outline
 */

BOOL DoCursorDown(WindowPtr window, UWORD qualifier)
{
	register WORD loc, line, lineLoc, headLine;
	WORD oldStart, oldEnd;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	BOOL success = TRUE;
	register BOOL ctrlKey;
	register BOOL oldCursorRepeat = cursorRepeat;
		
	oldStart = docData->SelStart;
	oldEnd = docData->SelEnd;
	selHead = docData->SelHead;
	if (oldStart == oldEnd)
		docData->DragStart = oldStart;
	loc = (docData->DragStart == oldStart) ? oldEnd : oldStart;
	line = LineNumber(docData, selHead, loc);
	headLine = WhichLine(selHead, loc);
	
	if( docData->LabelFlags & LABEL_ALIGN_MASK ) {
		if( headLine == 0 )
			loc -= selHead->BodyStart;
		if( headLine == (selHead->NumLines-1) )
			loc += selHead->BodyStart;
	}
	lineLoc = loc - selHead->LineStarts[headLine]
			+ docData->Indent*HeadDepth(selHead);

	if( !oldCursorRepeat ) {
		cursorCol = lineLoc;
		/*
		if( headLine == 0 )
			cursorCol -= selHead->BodyStart;
		*/
		cursorRepeat = TRUE;
	}
	
	if (selHead->Flags & HD_SELECTED) {
		qualifier &= ~IEQUALIFIER_CONTROL;
		selHead = LastSelHead(selHead);
		oldEnd = selHead->Len;
		oldStart = oldEnd - 1;
	}
	ctrlKey = (BOOL) qualifier & IEQUALIFIER_CONTROL;
	if( !ctrlKey )
		CursorOff(window);
	UnSelectHeads(docData);
	if( ( oldStart == oldEnd ) || ctrlKey ) {
/*
	If the bottom line, or if the last line in a head while extending selection, beep!
*/
		if( ( line == (docData->TotalLines-1) ) ||
				 ( ctrlKey && ( headLine == (selHead->NumLines-1) ) ) ) {
			success = FALSE;
		} else {
/*
	Move to top of outline
*/
			if (qualifier & ALTKEYS)
				line = docData->TotalLines - 1;
/*
	Move up one window
*/
			else if (qualifier & SHIFTKEYS)
				line += WINDOW_LINES(window) - 1;
/*
	Move up one line
*/
			else
				line++;
/*
	Set selection point
*/
			if (line >= docData->TotalLines - 1)
				line = docData->TotalLines - 1;
			selHead = WhichHead(docData, line);
			if( ctrlKey && selHead != docData->SelHead) {
				selHead = docData->SelHead;
				headLine = selHead->NumLines - 1;
			} else {
				docData->SelHead = selHead;
				headLine = line - LineNumber(docData, selHead, 0);
			}
			if( oldCursorRepeat )
				lineLoc = cursorCol;
				
			lineLoc -= docData->Indent*HeadDepth(selHead);
			if( lineLoc < 0 ) {
				lineLoc = 0;
			}
			loc = selHead->LineStarts[headLine] + lineLoc;
			if( !headLine && (loc < (selHead->LineStarts[headLine] + selHead->BodyStart)))
				loc = selHead->LineStarts[headLine] + selHead->BodyStart;
			else if (headLine < selHead->NumLines - 1 &&
					 loc >= selHead->LineStarts[headLine + 1])
				loc = selHead->LineStarts[headLine + 1] - 1;
			else if (loc > selHead->Len)
				loc = selHead->Len;
		}
	} else {
		docData->SelHead = selHead;
		loc = oldEnd;
	}
/*
	Draw new cusor position
*/
	if( !success )
		CursorBeep();
	else {
		if( !ctrlKey ) {
			docData->SelStart = docData->SelEnd = docData->DragStart = loc;
			CursorOn(window);
		} else {
			SetSelRange(docData, loc);
			CursorChangeOn(window, oldStart, oldEnd);
		}
		ScrollToLocation(window, selHead, loc);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

/*
 *	Move cursor left one character, one word, or to beginning of line
 */

BOOL DoCursorLeft(WindowPtr window, UWORD qualifier)
{
	register TextPtr text;
	register WORD loc, line, headLine;
	WORD oldStart, oldEnd;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL success = TRUE;
	register BOOL ctrlKey;
	WORD lines ;
	
	oldStart = docData->SelStart;
	oldEnd = docData->SelEnd;
	selHead = docData->SelHead;
	
	if (oldStart == oldEnd)
		docData->DragStart = oldStart;
	loc = (docData->DragStart == oldStart) ? oldEnd : oldStart;
	line = LineNumber(docData, selHead, loc);
	headLine = WhichLine(selHead, loc);
	if( selHead->Flags & HD_SELECTED ) {
		qualifier &= ~IEQUALIFIER_CONTROL;
		oldStart = selHead->BodyStart;
		oldEnd = oldStart + 1;
	}
	ctrlKey = (BOOL) qualifier & IEQUALIFIER_CONTROL;
	if( !ctrlKey ) {
		CursorOff(window);
		lines = line;
	} else
		lines = headLine;
		
	UnSelectHeads(docData);
	if( ( oldStart == oldEnd ) || ctrlKey ) {
/*
	Move to beginning of line
*/
		if( (lines == 0) && ( loc == selHead->BodyStart ) ) {
			success = FALSE;
		} else {
			if (qualifier & ALTKEYS) {
				if (loc == selHead->LineStarts[headLine] + selHead->BodyStart) {
					if (headLine)
						headLine--;
					else {
						if (line && ctrlKey == 0) {
							selHead = PrevHead(selHead);
							headLine = selHead->NumLines - 1;
						}
					}
				}
				loc = selHead->LineStarts[headLine] + (headLine ? 0 : selHead->BodyStart);
/*
	Move left one word
*/
			} else if (qualifier & SHIFTKEYS) {
				if( (loc == selHead->BodyStart) && line && ctrlKey == 0) {
					selHead = PrevHead(selHead);
					loc = selHead->Len;
				}
				text = selHead->Text;
				while( ( loc != selHead->BodyStart ) && text[loc - 1] == ' ')
					loc--;
				while( ( loc != selHead->BodyStart ) && text[loc - 1] != ' ')
					loc--;
/*
	Move left one character
*/
			} else {
				if( loc != selHead->BodyStart )
					loc--;
				else if (line && ctrlKey == 0) {
					selHead = PrevHead(selHead);
					loc = selHead->Len;
				}
			}
		}
	} else
		loc = oldStart;
/*
	Draw new cursor position
*/
	if( !success )
		CursorBeep();
	else {
		docData->SelHead = selHead;
		if( !ctrlKey ) {
			docData->SelStart = docData->SelEnd = docData->DragStart = loc;
			CursorOn(window);
		} else {
			SetSelRange(docData, loc);
			CursorChangeOn(window, oldStart, oldEnd);
		}
		ScrollToLocation(window, selHead, (loc == selHead->BodyStart) ? 0 : loc );
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

/*
 *	Move cursor right one character, word, or to end of line
 */

BOOL DoCursorRight(WindowPtr window, UWORD qualifier)
{
	register TextPtr text;
	register WORD loc, line, headLine, maxLineLoc;
	WORD oldStart, oldEnd;
	register HeadPtr selHead;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	BOOL success = TRUE;
	register BOOL ctrlKey;
	BOOL lastLine ;
	
	oldStart = docData->SelStart;
	oldEnd = docData->SelEnd;
	selHead = docData->SelHead;

	if (oldStart == oldEnd)
		docData->DragStart = oldStart;
	loc = (docData->DragStart == oldStart) ? oldEnd : oldStart;
	line = LineNumber(docData, selHead, loc);
	headLine = WhichLine(selHead, loc);
	maxLineLoc = (headLine < selHead->NumLines - 1) ?
		 selHead->LineStarts[headLine + 1] - 1 : selHead->Len;
	if (selHead->Flags & HD_SELECTED) {
	 	qualifier &= ~IEQUALIFIER_CONTROL;
		selHead = LastSelHead(selHead);
		oldEnd = selHead->Len;
		oldStart = oldEnd - 1;
	}
	ctrlKey = (BOOL) qualifier & IEQUALIFIER_CONTROL;
	if( !ctrlKey ) {
		CursorOff(window);
		lastLine = ( line == (docData->TotalLines-1) );
	} else
		lastLine = ( headLine == (selHead->NumLines-1) );
		
	UnSelectHeads(docData);
	if( ( oldStart == oldEnd ) || ctrlKey ) {
		if( lastLine && ( loc == maxLineLoc ) )
			success = FALSE;
		else {
/*
	Move to end of line
*/
			if (qualifier & ALTKEYS) {
				if (loc == maxLineLoc) {
					if (headLine < selHead->NumLines - 1)
						headLine++;
					else if (line < docData->TotalLines - 1 && ctrlKey == 0 ) {
						selHead = NextHead(selHead);
						headLine = 0;
					}
				}
				loc = (headLine < selHead->NumLines - 1) ?
					  selHead->LineStarts[headLine + 1] - 1 : selHead->Len;
/*
	Move right one word
*/
			} else if (qualifier & SHIFTKEYS) {
				text = selHead->Text;
				while (loc < selHead->Len && text[loc] != ' ')
					loc++;
				while (loc < selHead->Len && text[loc] == ' ')
					loc++;
				if( loc == maxLineLoc && line < docData->TotalLines - 1 && ctrlKey == 0 ) {
					selHead = NextHead(selHead);
					loc = selHead->BodyStart;
				}
/*
	Move right one character
*/
			} else {
				if (loc < selHead->Len)
					loc++;
				else if (line < docData->TotalLines - 1 && ctrlKey == 0 ) {
					selHead = NextHead(selHead);
					loc = selHead->BodyStart;
				}
			}
		}
	} else
		loc = oldEnd;
/* 	
	Draw new cursor position
*/
	if( !success )
		CursorBeep();
	else {
		docData->SelHead = selHead;
		if( !ctrlKey ) {
			docData->SelStart = docData->SelEnd = docData->DragStart = loc;
			CursorOn(window);
		} else {
			SetSelRange(docData, loc);
			CursorChangeOn(window, oldStart, oldEnd);
		}
		ScrollToLocation(window, selHead, loc);
		SetEditMenu();
		SetSearchMenu();
		SetSubHeadMenu();
		SetFormatMenu();
	}
	return(success);
}

/*
 * Beep if you don't mind the annoyance
 */
 
static void CursorBeep()
{
	ErrBeep();
}

BOOL DoCursorHeading(WindowPtr window, UBYTE code, UWORD modifier)
{
	DocDataPtr 	docData = GetWRefCon(window);
	WORD		level;
	HeadPtr		head;
	
	CursorOff(window);
	UnSelectHeads(docData);
	level = HeadDepth(docData->SelHead);
	
	head = docData->SelHead;
	
	switch (code) {
		case CURSORDOWN :
			while (((head = NextHead(head)) != NULL) && (HeadDepth(head) != level));
			break;
		case CURSORUP :
			while (((head = PrevHead(head)) != NULL) && (HeadDepth(head) != level));
			break;
	}
	
	if (head == NULL)
		return (FALSE);
		
	if (docData->SelStart > head->Len)
		docData->SelStart = docData->SelEnd = head->Len;
		
	docData->SelHead = head;	
	CursorOn(window);
	ScrollToLocation(window,docData->SelHead,docData->SelStart);
	return (TRUE);
}
