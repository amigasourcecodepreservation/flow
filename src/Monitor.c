/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	High-priority message monitor
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Utility.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow;
extern MsgPort		monitorMsgPort;
extern MsgPortPtr	mainMsgPort;

extern WORD	waitCount;
extern BOOL	inMacro;

extern BOOL busy, quitFlag;

/*
 *	Task to continuously monitor important events
 *	This task is started up with a higher priority than main program
 *	This routine and all routines called by it must NOT have stack checking
 */

void __saveds MonitorTask()
{
	register UWORD code, modifier;
	register IntuiMsgPtr intuiMsg;
	register WindowPtr window;

/*
	Set up monitor's message port
*/
	monitorMsgPort.mp_Node.ln_Name = NULL;
	monitorMsgPort.mp_Node.ln_Pri  = 0;
	monitorMsgPort.mp_Node.ln_Type = NT_MSGPORT;
	monitorMsgPort.mp_Flags			 = PA_SIGNAL;
	monitorMsgPort.mp_SigBit		 = AllocSignal(-1);
	monitorMsgPort.mp_SigTask		 = FindTask(NULL);
	NewList(&(monitorMsgPort.mp_MsgList));
/*
	Wait for intuition messages and process important ones
*/
	for (;;) {
		Wait(1 << monitorMsgPort.mp_SigBit);
		while (intuiMsg = (IntuiMsgPtr) GetMsg(&monitorMsgPort)) {
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			window = intuiMsg->IDCMPWindow;
			switch (intuiMsg->Class) {
			case MENUVERIFY:
				if (code == MENUHOT) {
					if (waitCount || inMacro)
						intuiMsg->Code = MENUCANCEL;
					else {
						if (modifier & IEQUALIFIER_RBUTTON)
							SetStdPointer(window, POINTER_ARROW);
					}
				}
				ReplyMsg((MsgPtr) intuiMsg);
				break;
			case RAWKEY:
				if ((modifier & IEQUALIFIER_REPEAT) && busy)
					ReplyMsg((MsgPtr) intuiMsg);
				else
					PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
			case REFRESHWINDOW:
				if (window == backWindow) {
					ReplyMsg((MsgPtr) intuiMsg);
					DoWindowRefresh(window);
				}
				else
					PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
			default:
/*
	Default case: let main loop handle message
*/
				PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
			}
		}
	}
}
