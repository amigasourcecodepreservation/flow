/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Display routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	charHeight, charWidth, charBaseline;

extern BYTE	penColor, paperColor;

extern ScreenPtr screen;

extern BOOL drawOn;

/*
 *	Local prototypes
 */

void	DrawPageBreak(WindowPtr);
void	DrawSelectBorder(DocDataPtr, WORD, WORD);
void	DrawSubMark(DocDataPtr, WORD, WORD, WORD);
void	FillPoly(RastPtr, WORD, PointPtr);
void	FramePoly1(RastPtr, WORD, PointPtr);

static struct tPoint arrowPts[4];

/*
 *	Calculate line start positions of specified heading
 *	Return TRUE if number of lines changes, FALSE otherwise
 */

BOOL CalcLineStarts(DocDataPtr docData, HeadPtr head)
{
	register WORD maxChars, i, loc, len, nLines;
	register TextPtr text;
	register BOOL diffLines;
	register HeadPtr newHead, subHead;
	WORD lineStarts[200];

	maxChars = docData->PageWidth - docData->LeftMargin
				- docData->RightMargin - HeadDepth(head)*docData->Indent-4;
	if (maxChars <= 0)
		maxChars = 0x7FFF;		/* So only one line start is created */
/*
	Find location of line starts
*/
	loc = nLines = 0;
	len = head->Len;
	text = head->Text;
	do {
		lineStarts[nLines++] = loc;
		if (loc + maxChars >= len)
			break;
		for (i = maxChars; i > 0; i--) {
			if (text[loc + i] == ' ')
				break;
		}
		loc += (i == 0) ? maxChars : i;
		while (loc < len && text[loc] == ' ') 
			loc++;
		if( nLines == 1 && (docData->LabelFlags & LABEL_ALIGN_MASK) ) {
			maxChars -= head->BodyStart;
			if( maxChars < 1 )
				maxChars = 1;
		}
	} while (loc < len);
/*
	If different number of line starts then create new head record
*/
	diffLines = (nLines != head->NumLines);
	if (diffLines) {
		newHead = NewPtr(sizeof(Head) + sizeof(WORD)*(nLines - 1));
		if (newHead == NULL)
			return (FALSE);
		CopyMem(head, newHead, sizeof(Head) - sizeof(WORD));
		if (newHead->Super && newHead->Super->Sub == head)
			newHead->Super->Sub = newHead;
		if (newHead->Prev)
			newHead->Prev->Next = newHead;
		if (newHead->Next)
			newHead->Next->Prev = newHead;
		for (subHead = newHead->Sub; subHead; subHead = subHead->Next)
			subHead->Super = newHead;
		if (docData->FirstHead == head)
			docData->FirstHead = newHead;
		if (docData->SelHead == head)
			docData->SelHead = newHead;
		DisposePtr((Ptr) head);
		newHead->NumLines = nLines;
		head = newHead;
	}
/*
	Copy line starts into head record
*/
	for (i = 0; i < nLines; i++)
		head->LineStarts[i] = lineStarts[i];
	return (diffLines);
}

/*
 *	Recalculate line breaks of all subheadings of given heading
 */

void ReformatSubs(DocDataPtr docData, HeadPtr mainHead)
{
	register HeadPtr head;
	HeadPtr nextHead;
	HeadPtr endHead;

 	endHead = mainHead->Next;
 	nextHead = mainHead->Sub;
	while( (nextHead != NULL) && (nextHead != endHead) ) {
		head = nextHead;
		nextHead = NextHeadAll(head);
		CalcLineStarts(docData, head);		/* May relocate head */
	}
}

/*
 * Recalculate the rest of the current level and all subheadings
 */
 
void ReformatRestOfLevel(DocDataPtr docData, register HeadPtr nextHead)
{
	register HeadPtr head;
	register HeadPtr endHead = NULL;
	
	if( nextHead != NULL )
		endHead = nextHead->Super;
	
	if( ( endHead != NULL ) && ( endHead->Super != NULL ) )
		endHead = endHead->Next;
	
	while( (nextHead != NULL) && (nextHead != endHead) ) {
		head = nextHead;
		nextHead = NextHeadAll(head);
		CalcLineStarts(docData, head);		/* May relocate head */
	}
}

/*
 *	Calculate all line breaks in outline, recalculate # of pages and lines.
 */

void ReformatOutline(DocDataPtr docData)
{
	HeadPtr head, nextHead;

	SetPageParams(docData);
	
	nextHead = docData->FirstHead;
	while (nextHead) {
		head = nextHead;
		nextHead = NextHeadAll(head);
		CalcLineStarts(docData, head);		/* May relocate head */
	}
	CountLines(docData);
}

/*
 *	Return pointer to heading on specified line
 */

HeadPtr WhichHead(DocDataPtr docData, WORD line)
{
	register HeadPtr head;

	for (head = docData->FirstHead; head && line >= head->NumLines;
		 head = NextHead(head))
		line -= head->NumLines;
	return (head);
}

/*
 *	Return heading line number (0..head->NumLines-1) of specified location
 *		in heading
 */

WORD WhichLine(HeadPtr head, WORD loc)
{
	register WORD line;

	for (line = 1; line < head->NumLines; line++) {
		if (loc < head->LineStarts[line])
			break;
	}
	return ((WORD) (line - 1));
}

/*
 *	Get line number of start of specified heading (or -1 if not visible)
 */

WORD LineNumber(DocDataPtr docData, HeadPtr whichHead, WORD loc)
{
	register WORD line;
	register HeadPtr head = docData->FirstHead;

	if (whichHead == NULL)
		return (-1);
	line = 0;
	if( head != NULL ) {
		while (head != whichHead) {
			line += head->NumLines;
			if ((head = NextHead(head)) == NULL)
				return (-1);
		}
		line += WhichLine(head, loc);
	}
	return (line);
}

/*
 *	Count number of visible lines in outline
 * This function also calculates number of pages in V3.0, to
 * avoid traversing the list twice.
 */

void CountLines(DocDataPtr docData)
{
	register WORD num, pages, line, pageHeight;
	register HeadPtr head = docData->FirstHead;

	num = 0;
	pages = 1;
	pageHeight = docData->PageHeight;
	if (head) {
		line = 0;
		do {
			num += head->NumLines;
			if( head->Flags & HD_PAGEBREAK ) {
				pages++;
				line = 0;
			} else {
				line += head->NumLines;
				while( line > pageHeight ) {
					line -= pageHeight;
					pages++;
				}
			}
		} while (head = NextHead(head));
	}
	docData->TotalLines = num;
	docData->Pages = pages;
}

/*
 *	Find vertical position of given heading
 */

LONG VertPosition(WindowPtr window, HeadPtr head, WORD loc)
{
	WORD line;
	LONG vertPos;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	line = LineNumber(docData, head, loc);
	vertPos = window->BorderTop + (line - docData->TopLine)*charHeight;
	return (vertPos);
}

/*
 *	Return horizontal position of given location within heading
 */

WORD HorizPosition(WindowPtr window, HeadPtr head, WORD loc)
{
	WORD 	horizPos;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD 	line;
	WORD 	rightMargin;
	
	line = WhichLine(head, loc);
	loc -= head->LineStarts[line];
	rightMargin = (docData->PageWidth - docData->RightMargin+docData->LeftOffset)*charWidth;
	if( line && (docData->LabelFlags & LABEL_ALIGN_MASK) )
		loc += head->BodyStart;
	  
	horizPos = window->BorderLeft + LEFT_MARGIN + 
				(loc + HeadDepth(head)*docData->Indent-docData->LeftOffset)*charWidth;
	if (horizPos > rightMargin)
		horizPos = rightMargin;
	return (horizPos);
}

/*
 *	Draw selection region vertical border
 */

static void DrawSelectBorder(DocDataPtr docData, WORD minY, WORD maxY)
{
	WindowPtr window = docData->Window;
	register WORD left = window->BorderLeft + SELECT_WIDTH;
	register RastPtr rPort = window->RPort;

 	SetAPen(rPort, docData->PenColor);
	SetDrMd(rPort, JAM1);
	SetDrPt(rPort, 0xFFFF);
	Move(rPort, left - 2, minY);
	Draw(rPort, left - 2, maxY);
	Move(rPort, left - 1, minY);
	Draw(rPort, left - 1, maxY);

}

/*
 *	Draw or erase subheading marker
 */

static void DrawSubMark(DocDataPtr docData, WORD x, WORD y, WORD drawIt)
{
	register WORD height = charHeight - 2;
	RastPtr rPort = docData->Window->RPort;
	
	SetAPen(rPort, docData->PaperColor);
	RectFill(rPort, x, y, x + (SELECT_WIDTH - 3), y + height);
	height &= 0xFFFE;			/* Make even number so arrow looks good */
	height -= 2 ;			/* Allow one line on top and bottom */
	x += 6;
	y += 1;
	if( drawIt ) {
		SetAPen(rPort, docData->PenColor);
		arrowPts[0].x = arrowPts[3].x = x;
		arrowPts[0].y = arrowPts[3].y = y;
		arrowPts[1].x = x + 9;
		arrowPts[1].y = y + ( height >> 1 );
		arrowPts[2].x = x;
		arrowPts[2].y = y + height;
		if( drawIt == 1 )
			FillPoly(rPort, 3, arrowPts);
		else
			FramePoly1(rPort, 4, arrowPts);
	}
}

/*
 *	Draw specified line of heading text
 *	Vertical position will already have been set up
 */

void DrawText(WindowPtr window, HeadPtr head, WORD line)
{
	WORD yPos, leftEdge, minX, maxX, len, indent, chOffset;
	TextPtr text;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD headSize;
	
	yPos = rPort->cp_y;
	if (yPos < window->BorderTop || !drawOn)
		return;
	minX = window->BorderLeft;
	maxX = window->Width - window->BorderRight - 1;
	leftEdge = minX + LEFT_MARGIN;
	text = head->Text + head->LineStarts[line];
	len = (line == head->NumLines - 1) ? head->Len : head->LineStarts[line + 1];
	len -= head->LineStarts[line];
	while (len && text[len - 1] <= ' ')
		len--;								/* Ignore trailing spaces */
/*
	Draw text line, erasing everything under it
*/
	indent = (HeadDepth(head)*docData->Indent - docData->LeftOffset)*charWidth;

	if( line && (docData->LabelFlags & LABEL_ALIGN_MASK) )
		indent += head->BodyStart * charWidth;
		
	if (indent > 0) {
		Move(rPort, leftEdge + indent, yPos + charBaseline);
		chOffset = 0;
	} else {
		Move(rPort, leftEdge, yPos + charBaseline);
		chOffset = -(indent/charWidth);
	}
	if( indent >= 0 ) {
		SetAPen(rPort, docData->PaperColor);
		SetDrMd(rPort, JAM1);
		RectFill( rPort, leftEdge-2, yPos, leftEdge + indent - 1, yPos + (charHeight-1) );
	}
/*
	Draw the text
*/
	SetAPen(rPort, docData->PenColor);
	SetBPen(rPort, docData->PaperColor);
	SetDrMd(rPort, JAM2);
	SetFont(rPort, GetFont(screen->Font));
	if( line == 0 ) {
		headSize = head->BodyStart - chOffset;
		if( (rPort->cp_x <= maxX) && head->BodyStart && (headSize > 0) ) {
			SetSoftStyle(rPort, FS_NORMAL, 0xFF);
			Text(rPort, text + chOffset, headSize);
			len -= headSize;
			text += headSize;
		}
	}
	SetSoftStyle(rPort, head->Style, 0xFF);
	if (rPort->cp_x <= maxX && len > chOffset)
		Text(rPort, text + chOffset, len - chOffset);
	if (rPort->cp_x <= maxX)
		ClearEOL(rPort);
/*
	Draw subhead indicator and/or selection box bottom
*/
	if (line == 0 && head->Sub) {
		DrawSubMark(docData, minX, yPos, head->Flags & HD_EXPANDED ? -1 : 1);
		if (indent > 0) {
			SetDrPt(rPort, 0x8080);
			Move(rPort, leftEdge, yPos + 3);
			Draw(rPort, leftEdge + indent - 1, yPos + 3);
		}
	}
	else
		DrawSubMark(docData, minX, yPos, 0);
	if (line == head->NumLines - 1)
		SetAPen(rPort, docData->PenColor);
	else
		SetAPen(rPort, docData->PaperColor);
	SetDrPt(rPort, 0xFFFF);
	yPos += charHeight-1;
	Move(rPort, minX, yPos);
	Draw(rPort, minX + (SELECT_WIDTH - 3), yPos);
}

/*
 *	Draw page break lines
 */

static void DrawPageBreak(WindowPtr window)
{
	register WORD line, windowLines, pageHeight, headLine, base;
	WORD yPos, startLine, endLine ;
	register RastPtr rPort = window->RPort;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register HeadPtr head;
	
	pageHeight = docData->PageHeight;
	if( pageHeight ) {
		windowLines = WINDOW_LINES(window);
		head = docData->FirstHead;
		SetAPen(rPort, docData->PenColor);
		SetDrMd(rPort, JAM1);
		SetDrPt(rPort, 0xAAAA);
		line = docData->TopLine;
/*
	First, we must calculate the value of "base", which will determine where
	the first displayable page break should go. Since non-artifical page
	breaks are not stored, we must traverse the list from the beginning until
	the start of the displayable area is reached.
*/
		base = 1;
		startLine = headLine = 0;
		while( startLine < line ) {
			if( base == pageHeight )
				base = 0;
				
			if( headLine == head->NumLines ) {
				head = NextHead(head);
				if( head == NULL ) {				/* End of doc reached */
					break;
				} else {
					headLine = 0;
					if( head->Flags & HD_PAGEBREAK ) {
						base = 1;				/* Page marker, reset */
					}
				}
			}
			startLine++;
			headLine++;
			base++;
		}
		head = WhichHead(docData, line);
		headLine = line - LineNumber(docData, head, 0);
		if( head->Flags & HD_PAGEBREAK )
			base = 1;
/*
	Second, we traverse the portion of the list which is displayable, and draw
	any page breaks where there are any artificial page breaks, or any lines
	which are exactly "pageHeight" lines after any page break.
*/
		endLine = MIN( docData->TopLine + windowLines, docData->TotalLines);
		for( ; line < endLine ; line++, base++ ) {
			if( ++headLine == head->NumLines ) {
				head = NextHead(head);
				if( head != NULL ) {
					headLine = 0;
					if( head->Flags & HD_PAGEBREAK )
						base = pageHeight;		/* Force a page break now! */
				}
			}
/*
	Regardless of PAGEBREAK flags, every pageHeight lines do a break!
*/
			if( base >= (pageHeight) ) {
				base = 0;		
  				yPos = window->BorderTop + (line - docData->TopLine + 1)*charHeight - 1;
				Move(rPort, window->BorderLeft, yPos);
				Draw(rPort, window->Width - window->BorderRight - 1, yPos);
			}
		}
		SetDrPt(rPort, 0xFFFF);						/* Just in case */
	}
}

/*
 *	Draw text line for given heading
 */

void DrawHead(WindowPtr window, HeadPtr head)
{
	WORD line;
	LONG yPos;

	if( drawOn ) {
		yPos = VertPosition(window, head, 0);
		for (line = 0; line < head->NumLines; line++) {
			Move(window->RPort, 0, yPos);
			DrawText(window, head, line);
			yPos += charHeight;
		}
		DrawPageBreak(window);
	}
}

/*
 *	Draw window contents within the specified range
 */

void DrawWindowRange(WindowPtr window, WORD minY, WORD maxY)
{
	WORD line, endLine, headLine;
	LONG yPos;
	HeadPtr head;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (minY < window->BorderTop)
		minY = window->BorderTop;
	if (maxY >= window->Height - window->BorderBottom)
		maxY = window->Height - window->BorderBottom - 1;
	if( minY >= maxY )
		return;
		
/*
	Draw select border
*/
	DrawSelectBorder(docData, minY, maxY);
	if( !drawOn )
		return;
/*
	Show heading text lines on screen
*/
	if (docData->TopLine > docData->TotalLines)
		yPos = window->BorderTop;
	else {
		line = docData->TopLine + (minY - window->BorderTop)/charHeight;
		endLine = docData->TopLine + (maxY - window->BorderTop)/charHeight;
		yPos = window->BorderTop + (line - docData->TopLine)*charHeight;
		if( docData->FirstHead != NULL ) {		/* Just in case */
			head = WhichHead(docData, line);
			headLine = line - LineNumber(docData, head, 0);		
			while (head && line <= endLine) {
				do {
					Move(rPort, 0, yPos);
					DrawText(window, head, headLine);
					yPos += charHeight;
					line++;
					headLine++;
				} while (line <= endLine && headLine < head->NumLines);
				head = NextHead(head);
				headLine = 0;		 
			}
		}
	}
/*
	Clear the remaining portion of the window
*/
	if (yPos < maxY) {
		SetAPen(rPort, docData->PaperColor);
		RectFill(rPort, window->BorderLeft, yPos,
				 window->Width - window->BorderRight - 1, maxY);
		DrawSelectBorder(docData, (WORD) yPos, maxY);
	}
	if( docData->FirstHead != NULL ) {			/* Just in case */
		DrawPageBreak(window);
		if (docData->CursorOn)
			CursorDrawOn(window);
	}
}

/*
 *	Draw window contents
 */

void DrawWindow(WindowPtr window)
{
	DrawWindowRange(window, (WORD) window->BorderTop,
					(WORD) (window->Height - window->BorderBottom - 1));
}

void FillPoly(RastPtr rPort, WORD numPoints, PointPtr pts)
{
	register WORD i;
	WORD width, height;
	UWORD *areaBuff;
	PLANEPTR planePtr;
	struct AreaInfo areaInfo;
	struct TmpRas tmpRas;
	Rectangle rect;
			
	if( numPoints > 2 ) {
		rect.MinX = rect.MaxX = pts[0].x;
		rect.MinY = rect.MinY = pts[0].y;
		
		for( i = 1; i < numPoints ; i++ ) {
			if( pts[i].x < rect.MinX )
				rect.MinX = pts[i].x;
			if( pts[i].x > rect.MaxX )
				rect.MaxX = pts[i].x;
			if( pts[i].y < rect.MinY )
				rect.MinY = pts[i].y;
			if( pts[i].y > rect.MaxY )
				rect.MaxY = pts[i].y;
		}
		width = rect.MaxX - rect.MinX + 1 + 16;
		height = rect.MaxY - rect.MinY + 1;
/*
	Do fill
*/
		SetDrMd(rPort, JAM2);
		BNDRYOFF(rPort);
		if ((areaBuff = MemAlloc((numPoints + 1)*5, MEMF_CLEAR)) != NULL) {
			if ((planePtr = AllocRaster(width, height)) != NULL) {
				InitArea(&areaInfo, areaBuff, numPoints + 1);
				InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
				rPort->AreaInfo = &areaInfo;
				rPort->TmpRas = &tmpRas;
				AreaMove(rPort, pts[0].x, pts[0].y);
				for (i = 1; i < numPoints; i++)
					AreaDraw(rPort, pts[i].x, pts[i].y);
				AreaEnd(rPort);
				WaitBlit();
				FreeRaster(planePtr, width, height);
				rPort->AreaInfo = NULL;
				rPort->TmpRas = NULL;
			}
			MemFree(areaBuff, (numPoints + 1)*5);
		}
	}
	return;
}

/*
 *	Draw single pixel wide poly
 */

static void FramePoly1(RastPtr rPort, WORD numPoints, PointPtr pts)
{
	Move(rPort, pts[0].x, pts[0].y);
	PolyDraw(rPort, numPoints - 1, (WORD *) &pts[1]);
}
