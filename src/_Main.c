/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	_main routine, slightly modified from Lattice source
 */

#include <exec/types.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>
 
#include <proto/exec.h>
#include <proto/dos.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ios1.h>
#include <string.h>

#define MAXARG 32	/* maximum command line arguments (for Flow) */
#define QUOTE  '"'

extern struct UFB _ufbs[];
extern int _fmode;

extern struct WBStartup *WBenchMsg;

int argc;		/* arg count */
char *argv[MAXARG];	/* arg pointers */

void	main(int, char **);

/*
 * Name		_main - process command line, open files, and call "main"
 *
 * Synopsis	_main(line);
 *		char *line;	ptr to command line that caused execution
 *
 * Description
 *	This function performs the standard pre-processing for
 *	the main module of a C program.  It accepts a command
 *	line of the form
 *
 *		pgmname arg1 arg2 ...
 *
 *	and builds a list of pointers to each argument.  The first
 *	pointer is to the program name.  For some environments, the
 *	standard I/O files are also opened, using file names that
 *	were set up by the OS interface module XCMAIN.
 */

void _main(line)
register char *line;
{
	register int x;
	struct Process *process;
	struct FileHandle *handle;
/*
	 Build argument pointer list
*/
	while (argc < MAXARG)  {
		while (*line == ' ')
			line++;
		if (*line == '\n' || *line == '\0')
			break;
		if (*line == QUOTE) {
			argv[argc++] = ++line;	/* ptr inside quoted string */
			while (*line && *line != QUOTE && *line != '\n')
				line++;
		}
		else {				/* non-quoted arg */
			argv[argc++] = line;
			while (*line && *line != ' ' && *line != '\n')
				line++;
		}
		if (*line == '\0')
			 break;
		else
			*line++ = '\0';		/* terminate arg */
	}
/*
	 Open standard files
*/
	if (argc == 0) {			/* running under workbench */
/* 	strncat(window, WBenchMsg->sm_ArgList->wa_Name, MAXWINDOW); */
		_ufbs[0].ufbfh = Open("NIL:", MODE_NEWFILE);
		_ufbs[1].ufbfh = _ufbs[0].ufbfh;
		_ufbs[1].ufbflg = UFB_NC;
		_ufbs[2].ufbfh = _ufbs[0].ufbfh;
		_ufbs[2].ufbflg = UFB_NC;
		handle = (struct FileHandle *) BADDR(_ufbs[0].ufbfh);
		process = (struct Process *) FindTask(NULL);
		process->pr_ConsoleTask = (APTR) handle->fh_Type;
		x = 0;
	}
	else {				/* running under CLI */
		_ufbs[0].ufbfh = Input();
		_ufbs[1].ufbfh = Output();
		_ufbs[2].ufbfh = Open("*", MODE_OLDFILE);
		x = UFB_NC;			/* do not close CLI defaults */
	}
	_ufbs[0].ufbflg |= UFB_RA | O_RAW | x;
	_ufbs[1].ufbflg |= UFB_WA | O_RAW | x;
	_ufbs[2].ufbflg |= UFB_RA | UFB_WA | O_RAW;
	x = (_fmode) ? 0 : _IOXLAT;
	stdin->_file  = 0;
	stdin->_flag  = _IOREAD | x;
	stdout->_file = 1;
	stdout->_flag = _IOWRT | x;
	stderr->_file = 2;
	stderr->_flag = _IORW | x;
/*
	 Call user's main program
*/
	main( argc, argc ? argv : (char **) WBenchMsg);	/* call main function */
	exit(0);
}
