/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Standard File Package routines
 */

#include <clib/macros.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/asl.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <dos/dosasl.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/asl.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/IntuiText.h>
#include <Toolbox/Request.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct DosLibrary	*DOSBase;
extern struct Library		*IconBase;

extern UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight;

extern DialogTemplate	_sfpGetFileDlgTempl, _sfpPutFileDlgTempl;
extern RequestTemplate	_sfpReq1Templ, _sfpReq2Templ;

extern TextChar	_sfpSystemText[];
extern TextChar	_sfpFileExistsText[], _sfpBadNameText[], _sfpNoFileText[], _sfpDiskLockText[];

/*
 *	Local variables and definitions
 */

struct Library		*AslBase;

typedef struct FileInfoBlock	FIB, *FIBPtr;
typedef struct FileLock			FileLock, *FileLockPtr;
typedef struct DosList			DosList, *DosListPtr;
typedef struct DosInfo			DosInfo, *DosInfoPtr;
typedef struct RootNode			RootNode, *RootNodePtr;
typedef struct DiskObject		DiskObject, *DiskObjPtr;

#define MAX_FILENAME_LEN	(30-5)			/* 5 chars for ".info" */

#define GET_DIALOG		0
#define PUT_DIALOG		1

static WORD	dlgType;

static DialogPtr	sfpDialog;
static MsgPortPtr	sfpMsgPort;
static BOOL			(*dialogFilter)(IntuiMsgPtr, WORD *);

static ScrollListPtr	scrollList;

static BOOL			atTop, useAslReq;

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)

/*
 *	Disk and drawer icons
 */

static UWORD iconColors[] = {
	0x00F, 0xFFF, 0x000, 0xF60
};

static ULONG chip systemIconData[] = {
	0xFC000000, 0x7E00FDFF, 0xFFFF7E00, 0xFD800003, 0x7E00FD80, 0x00037E00,
	0xFD800003, 0x7E00FD80, 0x00037E00, 0xFD800003, 0x7E00FD80, 0x00037E00,
	0xFD800003, 0x7E00FD80, 0x00037E00, 0xFD800003, 0x7E00FDFF, 0xFFFF7E00,
	0xFC000000, 0x7E00F000, 0x00001E00, 0xEFFFFFFF, 0xEE00D924, 0x92493600,
	0xB24C9264, 0x9A006499, 0x93324C00, 0x7FFFFFFF, 0xFC008000, 0x00000200,

	0x03FFFFFF, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x02000000, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x02000000, 0x80000200, 0x00008000, 0x02000000, 0x80000200, 0x00008000,
	0x03FFFFFF, 0x80000FFF, 0xFFFFE000, 0x10000000, 0x100026DB, 0x6DB6C800,
	0x4DB36D9B, 0x64009B66, 0x6CCDB200, 0x80000000, 0x02007FFF, 0xFFFFFC00,
};

static ULONG systemIconMask[] = {
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x800003FF, 0xFFFF8000, 0x03FFFFFF, 0x800003FF, 0xFFFF8000,
	0x03FFFFFF, 0x80000FFF, 0xFFFFE000, 0x1FFFFFFF, 0xF0003FFF, 0xFFFFF800,
	0x7FFFFFFF, 0xFC00FFFF, 0xFFFFFE00, 0xFFFFFFFF, 0xFFE07FFF, 0xFFFFFC00,
};

static ULONG chip diskIconData[] = {
	0x00030FF1, 0x0F900F90, 0x0F900FF0, 0x00000000,
	0x00003FFC, 0x3FFC3FFC, 0x3FFC3FFC, 0x3FFC0000,

	0xFFFCFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFC003, 0xC003C003, 0xC003C003, 0xC003FFFF,
};

static ULONG diskIconMask[] = {
	0xFFFCFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF
};

static ULONG chip drawerIconData[] = {
    0x00000000, 0x7FFFFFFE, 0x60000006, 0x6FFFFFF6,
    0x6FFFFFF6, 0x6FF7EFF6, 0x6FF00FF6, 0x6FFFFFF6,
    0x6FFFFFF6, 0x60000006, 0x7FFFFFFE, 0x00000000,

    0xFFFFFFFF, 0x80000001, 0x9FFFFFF9, 0x90000009,
    0x90000009, 0x90081009, 0x900FF009, 0x90000009,
    0x90000009, 0x9FFFFFF9, 0x80000001, 0xFFFFFFFF,
};

static Icon systemIcon = {
	39, 20, 2, &iconColors[0], (PLANEPTR) &systemIconData[0], (PLANEPTR) &systemIconMask[0]
};

static Icon diskIcon = {
	16, 16, 2, &iconColors[0], (PLANEPTR) &diskIconData[0], (PLANEPTR) &diskIconMask[0]
};

static Icon drawerIcon = {
	32, 12, 2, &iconColors[0], (PLANEPTR) &drawerIconData[0], NULL
};

/*
 *	Gadgets in common between get and put dialogs
 */

#define ENTER_BUTTON	2
#define BACK_BUTTON		3
#define DISKS_BUTTON	4
#define FILE_LIST		5
#define FILE_UP			6
#define FILE_DOWN		7
#define FILE_SCROLL		8
#define FILENAME_TEXT	9
#define DISK_NAME		10
#define DISK_ICON		11

#define PUT_PROMPT	12		/* In put dialog only */

/*
 *	Misc requesters
 */

#define REQ_FILEEXISTS	0
#define REQ_BADNAME		1
#define REQ_NOFILE		2
#define REQ_DISKLOCK	3

#define REQ1_STATTEXT	1
#define REQ2_STATTEXT	2

#define YES_BUTTON	OK_BUTTON
#define NO_BUTTON	CANCEL_BUTTON

/*
 *	Local routine prototypes
 */

DosListPtr	LockDosList1(ULONG);
void		UnLockDosList1(ULONG);
DosListPtr	NextDosEntry(DosListPtr, ULONG);

Dir		GetCurrentDir(void);
WORD	ShowDirName(void);
BOOL	ValidFile(FIBPtr);
void	GetFileList(BOOL (*)(TextPtr), WORD, TextPtr *);
void	ShowFileList(ListHeadPtr, ListHeadPtr);
void	ClearFileList(void);
BOOL	IsDirItem(WORD);
void	OffButtons(void);
void	SetBackButton(void);
void	SetButtons(void);
WORD	CheckButtons(void);
WORD	EnterDir(void);
WORD	ExitDir(BOOL);
Dir		CheckFileName(TextPtr);
BOOL	DiskIsLocked(void);
void	NewSelItem(WORD);
WORD	SFPDialogFilter(IntuiMsgPtr, WORD *);
WORD	SimpleRequest(MsgPortPtr, WindowPtr, WORD, BOOL (*)(IntuiMsgPtr, WORD *));
void	DoDirSwitch(WORD, BOOL (*)(TextPtr), WORD, TextPtr *);
WORD	DoFileList(void);

ULONG	AslFileFilter(ULONG, CPTR, CPTR);
BOOL	AslRequestFile(ScreenPtr, BOOL, TextPtr, TextPtr, DlgTemplPtr, SFReplyPtr);

/*
 *	Return pointer to lock of current directory
 *	Does not return a duplicate of the lock
 */

static Dir GetCurrentDir()
{
	return (((ProcessPtr) FindTask(NULL))->pr_CurrentDir);
}

/*
 *	Simulate LockDosList() for 1.3 and 2.0 OS
 */

static DosListPtr LockDosList1(ULONG flags)
{
	DosListPtr dosEntry;

	if (SystemVersion() >= OSVERSION_2_0_4)
		dosEntry = LockDosList(flags);
	else {
		Forbid();
		dosEntry = NULL;		/* So NextDosEntry1() will return first entry */
	}
	return (dosEntry);
}

/*
 *	Simulate UnLockDosList() for 1.3 and 2.0 OS
 */

static void UnLockDosList1(ULONG flags)
{
	if (SystemVersion() >= OSVERSION_2_0_4)
		UnLockDosList(flags);
	else
		Permit();
}

/*
 *	Simulate NextDosEntry for 1.3 and 2.0 OS
 *	Ignores flags under 1.3
 */

static DosListPtr NextDosEntry1(DosListPtr dosEntry, ULONG flags)
{
	DosInfoPtr dosInfo;
	RootNodePtr rootNode;

	if (SystemVersion() >= OSVERSION_2_0_4)
		dosEntry = NextDosEntry(dosEntry, flags);
	else if (dosEntry)
		dosEntry = (DosListPtr) BADDR(dosEntry->dol_Next);
	else {
		rootNode = (RootNodePtr) DOSBase->dl_Root;
		dosInfo = (DosInfoPtr) BADDR(rootNode->rn_Info);
		dosEntry = (DosListPtr) BADDR(dosInfo->di_DevInfo);
	}
	return (dosEntry);
}

/*
 *	Display the new directory/volume name and icon in dialog
 *	Return success status
 */

static BOOL ShowDirName()
{
	WORD position;
	BOOL success;
	Dir currentDir, parentDir;
	FIBPtr dirFIB;
	register GadgetPtr nameGadg, iconGadg;
	GadgetPtr gadgList = sfpDialog->FirstGadget;
	RastPtr rPort = sfpDialog->RPort;
	IconPtr icon;
	ImagePtr iconImage;
	TextPtr dirName;
	Rectangle rect;

	if ((dirFIB = AllocMem(sizeof(FIB), MEMF_CLEAR)) == NULL)
		return (FALSE);
	nameGadg = GadgetItem(gadgList, DISK_NAME);
	iconGadg = GadgetItem(gadgList, DISK_ICON);
	success = FALSE;
	currentDir = GetCurrentDir();
/*
	Display the dir/volume name
*/
	if (atTop)
		dirName = _sfpSystemText;
	else if (Examine(currentDir, dirFIB))
		dirName = dirFIB->fib_FileName;
	else
		goto Exit;
	SetGadgetText(nameGadg, sfpDialog, NULL, dirName);
/*
	Display the icon (erasing old one first)
*/
	if ((iconImage = iconGadg->GadgetRender) != NULL) {
		GetGadgetRect(iconGadg, sfpDialog, NULL, &rect);
		rect.MinX += iconImage->LeftEdge;
		rect.MinY += iconImage->TopEdge;
		rect.MaxX = rect.MinX + iconImage->Width;
		rect.MaxY = rect.MinY + iconImage->Height;
		SetAPen(rPort, _tbPenLight);
		RectFill(rPort, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
	}
	position = RemoveGadget(sfpDialog, iconGadg);
	FreeImage(iconImage);
	if (atTop)
		icon = &systemIcon;
	else if ((parentDir = ParentDir(currentDir)) != NULL) {
		UnLock(parentDir);
		icon = &drawerIcon;
	}
	else
		icon = &diskIcon;
	iconGadg->GadgetRender = iconImage = MakeIconImage(icon, 0, 0, ICON_BOX_NONE);
	iconImage->LeftEdge = 0;
	iconImage->TopEdge = -iconImage->Height/2;
	AddGList(sfpDialog, iconGadg, position, 1, NULL);
	RefreshGList(iconGadg, sfpDialog, NULL, 1);
	success = TRUE;
/*
	All done
*/
Exit:
	FreeMem(dirFIB, sizeof(FIB));
	return (success);
}

/*
 *	Return TRUE if fib entry is valid for listing
 */

static BOOL ValidFile(FIBPtr fib)
{
	WORD len;
	TextPtr fileName;

	fileName = fib->fib_FileName;
	len = strlen(fileName);
	if (fileName[0] == '.' ||
		(len > 4 && CmpString(fileName + len - 5, ".info", 5, 5, FALSE) == 0) ||
		fib->fib_Size == 0 ||
		(len == 1 && fileName[0] == '*'))
		return (FALSE);
	return (TRUE);
}

/*
 *	Get directory/file list for current directory and display
 *	If at top of tree, save list of mounted volumes in file list
 *		and list of path assignments in directory list
 */

static void GetFileList(BOOL (*fileFilter)(TextPtr), WORD numTypes, TextPtr typeList[])
{
	register TextPtr toolType, fileName;
	register WORD i, len;
	register BOOL addFile;
	DosListPtr dosEntry;
	Dir currentDir;
	DiskObjPtr diskObj;
	ListHeadPtr	dirList, fileList, list;
	FIBPtr fib;
	TextChar nameBuff[50];

/*
	Allocate needed items
*/
	if ((fib = AllocMem(sizeof(FIB), MEMF_CLEAR)) == NULL)
		goto Exit4;
	if ((dirList = CreateList()) == NULL)
		goto Exit3;
	if ((fileList = CreateList()) == NULL)
		goto Exit2;
/*
	If at top, get list of mounted volumes
*/
	if (atTop) {
		dosEntry = LockDosList1(LDF_VOLUMES | LDF_ASSIGNS | LDF_READ);
		while ((dosEntry = NextDosEntry1(dosEntry, LDF_VOLUMES | LDF_ASSIGNS | LDF_READ)) != NULL) {
			if ((dosEntry->dol_Type == DLT_VOLUME && dosEntry->dol_Task != NULL) ||
				dosEntry->dol_Type == DLT_DIRECTORY) {
				fileName = (TextPtr) BADDR(dosEntry->dol_Name);
				len = *fileName;
				BlockMove(fileName + 1, nameBuff, len);
				nameBuff[len] = '\0';
				list = (dosEntry->dol_Type == DLT_VOLUME) ? fileList : dirList;
				if (AddListItem(list, nameBuff, len, TRUE) == -1)
					break;
			}
		}
		UnLockDosList1(LDF_VOLUMES | LDF_ASSIGNS | LDF_READ);
	}
/*
	Otherwise, get directory list
*/
	else {
		currentDir = GetCurrentDir();
		if (!Examine(currentDir, fib) || fib->fib_DirEntryType <= 0)
			goto Exit1;
		fileName = fib->fib_FileName;
		while (ExNext(currentDir, fib)) {
			if (CheckButtons())
				goto Exit1;
			len = strlen(fileName);
/*
	If directory entry, add entry to directory list
*/
			if (fib->fib_DirEntryType > 0) {
				if (AddListItem(dirList, fileName, len, TRUE) == -1)
					break;
			}
/*
	Else if file, then add to list of files
*/
			else if (fib->fib_DirEntryType < 0) {
				if (!ValidFile(fib))
					continue;
/*
	Check to see if this file should be included in list
	(avoid reading icon if fileFilter says to list it)
*/
				addFile = FALSE;
				if (fileFilter)
					addFile = (*fileFilter)(fileName);
				else if (numTypes == 0)
					addFile = TRUE;
				if (!addFile && numTypes && IconBase &&
					(diskObj = GetDiskObject(fileName)) != NULL) {
					if ((toolType = FindToolType(diskObj->do_ToolTypes, "FILETYPE"))
						!= NULL) {
						for (i = 0; !addFile && i < numTypes; i++)
							addFile = MatchToolValue(toolType, typeList[i]);
					}
					FreeDiskObject(diskObj);
				}
/*
	Add file to list
*/
				if (addFile && AddListItem(fileList, fileName, len, TRUE) == -1)
					break;
			}
		}
	}
/*
	Show the file lists and dispose of items
*/
Exit0:
	ShowFileList(dirList, fileList);
Exit1:
	FreeMem(fib, sizeof(FIB));
Exit2:
	DisposeList(dirList);
Exit3:
	DisposeList(fileList);
Exit4:
	return;			/* Keeps compiler happy */
}

/*
 *	Display the new list of directories/files in dialog
 *	Mark directory names with a non-space character before name
 */

static void ShowFileList(ListHeadPtr dirList, ListHeadPtr fileList)
{
	WORD i, j, len, numItems, numScroll, dashWidth;
	TextPtr text;
	IntuiTextPtr intuiText;
	TextChar buff[50];

	SLDoDraw(scrollList, FALSE);
/*
	Add file names
*/
	numItems = NumListItems(fileList);
	buff[0] = ' ';
	for (i = 0; i < numItems; i++) {
		text = GetListItem(fileList, i);
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, (WORD) strlen(buff), i);
	}
/*
	If at top, put separator between volume and path lists
*/
	if (atTop) {
		intuiText = NewIntuiText(0, 0, "-", FS_NORMAL, 0);
		dashWidth = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
		len = (scrollList->ListBox->Width - 1)/dashWidth;
		memset(buff, '-', len);
		numScroll = SLNumItems(scrollList);
		SLAddItem(scrollList, buff, len, numScroll);
		SLEnableItem(scrollList, numScroll, FALSE);
	}
/*
	Sort directory names into file list
	Don't put flag in front of path assignments
*/
	numItems = NumListItems(dirList);
	for (i = 0; i < numItems; i++) {
		text = GetListItem(dirList, i);
		len = strlen(text);
		numScroll = SLNumItems(scrollList);
		if (atTop)
			j = numScroll;
		else {
			for (j = 0; j < numScroll; j++) {
				SLGetItem(scrollList, j, buff);
				if (CmpString(text, buff + 1, len, (WORD) strlen(buff + 1), FALSE) < 0)
					break;
			}
		}
		buff[0] = (atTop) ? ' ' : '-';
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, (WORD) strlen(buff), j);
		SLSetItemStyle(scrollList, j, FSF_ITALIC);
	}
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Clear the file list in dialog
 */

static void ClearFileList()
{
	SLRemoveAll(scrollList);
}

/*
 *	Return TRUE if specified list item is a directory (not a file name)
 */

static BOOL IsDirItem(WORD item)
{
	TextChar buff[50];

	SLGetItem(scrollList, item, buff);
	return ((BOOL) (atTop || buff[0] != ' '));
}

/*
 *	Disable all but "Disk" and "Cancel" buttons
 */

static void OffButtons()
{
	GadgetPtr gadgList = sfpDialog->FirstGadget;

	EnableGadgetItem(gadgList, OK_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, BACK_BUTTON, sfpDialog, NULL, FALSE);
}

/*
 *	Set "Back" button on or off
 */

static void SetBackButton()
{
	EnableGadgetItem(sfpDialog->FirstGadget, BACK_BUTTON, sfpDialog, NULL, !atTop);
}

/*
 *	Set dialog gadgets to enabled or disabled as appropriate
 */

static void SetButtons()
{
	WORD selItem;
	BOOL enable;
	GadgetPtr gadgList = sfpDialog->FirstGadget;
	TextChar fileName[GADG_MAX_STRING];

	SetBackButton();
/*
	Enable "Enter" button if a directory is selected
*/
	selItem = SLNextSelect(scrollList, -1);
	enable = (selItem >= 0 && IsDirItem(selItem));
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, enable);
/*
	Enable "Open"/"Save" button if there is a file name
*/
	GetEditItemText(gadgList, FILENAME_TEXT, fileName);
	enable = (strlen(fileName) != 0);
	EnableGadgetItem(gadgList, OK_BUTTON, sfpDialog, NULL, enable);
}

/*
 *	Check to see if "Disk", "Back", or "Cancel" button is pressed
 *	Return TRUE if yes, FALSE if not
 */

static BOOL CheckButtons()
{
	register WORD gadgNum;
	register BOOL found;
	register IntuiMsgPtr msg, nextMsg;
	GadgetPtr gadget;

	found = FALSE;
	Forbid();
	msg = (IntuiMsgPtr) sfpMsgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		if (msg->Class == GADGETUP && msg->IDCMPWindow == sfpDialog) {
			gadget = (GadgetPtr) msg->IAddress;
			if (gadget &&
				((gadgNum = GadgetNumber(gadget)) == CANCEL_BUTTON ||
				 gadgNum == DISKS_BUTTON ||gadgNum == BACK_BUTTON)) {
				found = TRUE;
				break;
			}
		}
		msg = nextMsg;
	}
	Permit();
	return (found);
}

/*
 *	Enter the currently selected subdirectory
 *	Return TRUE if entered subdirectory, FALSE if not
 */

static BOOL EnterDir()
{
	WORD selItem;
	register Dir dir;
	TextChar dirName[50];

	selItem = SLNextSelect(scrollList, -1);
	if (selItem < 0 || !IsDirItem(selItem))
		return (FALSE);
	SLGetItem(scrollList, selItem, dirName);
	if (atTop)
		strcat(dirName, ":");
	if ((dir = Lock(dirName + 1, ACCESS_READ)) == NULL)
		return (FALSE);
	SetCurrentDir(dir);
	UnLock(dir);
	atTop = FALSE;
	return (TRUE);
}

/*
 *	Back up one or all directory level(s)
 *	Return TRUE if backed up, FALSE if not
 */

static BOOL ExitDir(BOOL toTop)
{
	register Dir dir, prevDir, currentDir;

	if (atTop)
		return (FALSE);
	currentDir = GetCurrentDir();
	if (toTop) {
		prevDir = NULL;
		for (dir = DupLock(currentDir); dir; dir = ParentDir(dir)) {
			if (prevDir)
				UnLock(prevDir);
			prevDir = dir;
		}
		SetCurrentDir(prevDir);
		UnLock(prevDir);
		atTop = TRUE;
	}
	else if ((dir = ParentDir(currentDir)) == NULL)
		atTop = TRUE;
	else {
		SetCurrentDir(dir);
		UnLock(dir);
	}
	return (TRUE);
}

/*
 *	Check file name
 *	Change directory to path in file name and strip path
 *	Return directory lock if valid name, NULL if not
 *	Note:  If file name is a path only, then result is lock with empty file name
 */

static Dir CheckFileName(TextPtr fileName)
{
	register WORD i, len;
	Dir dir;

/*
	Check for illegal characters
*/
	len = strlen(fileName);
	for (i = 0; i < len; i++) {
		if ((fileName[i] & 0x7F) < ' ')
			return (NULL);
	}
/*
	Strip path from name
*/
	if ((dir = ConvertFileName(fileName)) == NULL)
		return (NULL);
	if (strlen(fileName) > MAX_FILENAME_LEN) {
		UnLock(dir);
		return (NULL);
	}
	return (dir);
}

/*
 *	Check to see if current directory is on locked disk
 *	Return TRUE if so
 */

static BOOL DiskIsLocked()
{
	BOOL locked;
	struct InfoData *infoData;

	if ((infoData = MemAlloc(sizeof(struct InfoData), 0)) == NULL)
		return (FALSE);
	Info(GetCurrentDir(), infoData);
	locked = (infoData->id_DiskState == ID_WRITE_PROTECTED);
	MemFree(infoData, sizeof(struct InfoData));
	return (locked);
}

/*
 *	Set new scroll list selection
 */

static void NewSelItem(register WORD newSelItem)
{
	WORD numItems;
	TextChar fileName[50];

	numItems = SLNumItems(scrollList);
	if (newSelItem < 0)
		newSelItem = 0;
	else if (newSelItem >= numItems)
		newSelItem = numItems - 1;
	SLSelectItem(scrollList, newSelItem, TRUE);
	SLAutoScroll(scrollList, newSelItem);
	if (!IsDirItem(newSelItem)) {
		SLGetItem(scrollList, newSelItem, fileName);
		SetEditItemText(sfpDialog->FirstGadget, FILENAME_TEXT, sfpDialog, NULL, fileName + 1);
	}
	SetButtons();
}

/*
 *	Handle GetFile and PutFile dialog intuiMsg
 */

static BOOL SFPDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG class = intuiMsg->Class;
	UWORD code = intuiMsg->Code;
	UWORD modifier = intuiMsg->Qualifier;
	WORD gadgNum;
	GadgetPtr gadgList = sfpDialog->FirstGadget;

	if (intuiMsg->IDCMPWindow == sfpDialog) {
		switch (class) {
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == FILE_LIST || gadgNum == FILE_SCROLL ||
				gadgNum == FILE_UP || gadgNum == FILE_DOWN) {
				SLGadgetMessage(scrollList, sfpMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle cursor keys
*/
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				SLCursorKey(scrollList, code);
				*item = FILE_LIST;
				return (TRUE);
			}
			else if (code == CURSORLEFT || code == CURSORRIGHT) {
				ReplyMsg((MsgPtr) intuiMsg);
				if (code == CURSORLEFT)
					gadgNum = (modifier & ALTKEYS) ? DISKS_BUTTON : BACK_BUTTON;
				else
					gadgNum = ENTER_BUTTON;
				if (DepressGadget(GadgetItem(gadgList, gadgNum), sfpDialog, NULL))
					*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle disk inserted or removed
*/
		case DISKINSERTED:
		case DISKREMOVED:
			ReplyMsg((MsgPtr) intuiMsg);
			*item = SFPMSG_DISK;
			return (TRUE);
		}
	}
	return ((dialogFilter) ? (*dialogFilter)(intuiMsg, item) : FALSE);
}

/*
 *	Handle simple request containing only buttons
 *	Return button number, or OK_BUTTON if not enough memory
 */

static WORD SimpleRequest(MsgPortPtr msgPort, WindowPtr window, WORD reqNum,
						  BOOL (*reqFilter)(IntuiMsgPtr, WORD *))
{
	WORD item;
	ReqTemplPtr reqTempl;
	RequestPtr request;

	switch (reqNum) {
	case REQ_FILEEXISTS:
		_sfpReq2Templ.Gadgets[REQ2_STATTEXT].Info = _sfpFileExistsText;
		reqTempl = &_sfpReq2Templ;
		break;
	case REQ_BADNAME:
		_sfpReq1Templ.Gadgets[REQ1_STATTEXT].Info = _sfpBadNameText;
		reqTempl = &_sfpReq1Templ;
		break;
	case REQ_NOFILE:
		_sfpReq1Templ.Gadgets[REQ1_STATTEXT].Info = _sfpNoFileText;
		reqTempl = &_sfpReq1Templ;
		break;
	case REQ_DISKLOCK:
		_sfpReq1Templ.Gadgets[REQ1_STATTEXT].Info = _sfpDiskLockText;
		reqTempl = &_sfpReq1Templ;
		break;
	default:
		return (OK_BUTTON);
	}
	if ((request = GetRequest(reqTempl, window, TRUE)) == NULL)
		return (OK_BUTTON);
	if (reqNum == REQ_FILEEXISTS) {
		SetDefaultButtons(NO_BUTTON, NO_BUTTON);
		item = NO_BUTTON;
	}
	else
		item = OK_BUTTON;
	OutlineButton(GadgetItem(request->ReqGadget, item), window, request, TRUE);
	SysBeep(5);
	item = ModalRequest(msgPort, window, reqFilter);
	SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);
	EndRequest(request, window);
	DisposeRequest(request);
	return (item);
}

/*
 *	Handle directory switch operations
 */

static void DoDirSwitch(WORD item, BOOL (*fileFilter)(TextPtr), WORD numTypes,
						TextPtr typeList[])
{
	BOOL success;

	OffButtons();
	if (dlgType == GET_DIALOG)
		SetEditItemText(sfpDialog->FirstGadget, FILENAME_TEXT, sfpDialog, NULL, NULL);
	SetStdPointer(sfpDialog, POINTER_WAIT);
	if (item == OK_BUTTON) {
		atTop = FALSE;
		success = TRUE;		/* Have already switched to directory */
	}
	else if (item == ENTER_BUTTON)
		success = EnterDir();
	else if (item == BACK_BUTTON || item == SFPMSG_DIRTOP)
		success = ExitDir((item == SFPMSG_DIRTOP));
	else if (item == DISKS_BUTTON)
		success = ExitDir(TRUE);
	else if ((item == SFPMSG_DISK && atTop) || item == SFPMSG_RELIST)
		success = TRUE;
	else
		success = FALSE;
	if (success) {
		ClearFileList();
		ShowDirName();
		SetBackButton();
		GetFileList(fileFilter, numTypes, typeList);
	}
	SetButtons();
	SetStdPointer(sfpDialog, POINTER_ARROW);
}

/*
 *	Handle click in file list
 *	Return FILE_LIST if single-click, OK_BUTTON if double-click on file,
 *		ENTER_BUTTON if double-click on directory
 */

static WORD DoFileList()
{
	WORD selItem;

	selItem = SLNextSelect(scrollList, -1);
	if (selItem == -1)
		return (FILE_LIST);
	NewSelItem(selItem);
	if (SLIsDoubleClick(scrollList)) {
		if (IsDirItem(selItem))
			return (ENTER_BUTTON);
		return (OK_BUTTON);
	}
	return (FILE_LIST);
}

/*
 *	Filter function for Asl requester
 *	Returns 0 to have file displayed in list
 */

static ULONG AslFileFilter(ULONG mask, CPTR object, CPTR aslReq)
{
	struct AnchorPath *anchorPath = (struct AnchorPath *) object;

	if (mask != FILF_DOWILDFUNC ||
		anchorPath->ap_Info.fib_DirEntryType > 0 ||
		ValidFile(&anchorPath->ap_Info))
		return (0);
	return (1);
}

/*
 *	Use Asl requester to get file/dir name
 *	Return FALSE if couldn't open asl library
 */

static BOOL AslRequestFile(ScreenPtr screen, BOOL saveReq, TextPtr prompt,
						   TextPtr origName, DlgTemplPtr dlgTempl, SFReplyPtr sfReply)
{
	BOOL gotLib, result;
	TextPtr okText, cancelText;
	struct Library *oldAslBase;
	struct FileRequester *aslReq;
	WindowPtr window;

	sfReply->Result = SFP_NOMEM;
	aslReq = NULL;
	window = NULL;
	oldAslBase = AslBase;
	gotLib = FALSE;
	if (SystemVersion() < OSVERSION_2_0 ||
		(AslBase = OpenLibrary("asl.library", OSVERSION_2_0)) == NULL)
		goto Exit;
	gotLib = TRUE;
/*
	Open small window on screen (to pass to file requester)
*/
	window = OpenWindowTags(NULL, WA_Left, 0, WA_Top, 0, WA_Width, 1, WA_Height, 1,
							WA_CustomScreen, screen);
	if (window == NULL)
		goto Exit;
/*
	Allocate and handle FileRequester
*/
	if ((aslReq = AllocFileRequest()) == NULL)
		goto Exit;
	okText = dlgTempl->Gadgets[OK_BUTTON].Info;
	cancelText = dlgTempl->Gadgets[CANCEL_BUTTON].Info;
	if (saveReq)
		result = AslRequestTags(aslReq, ASL_Hail, prompt, ASL_Window, window,
										ASL_OKText, okText, ASL_CancelText, cancelText,
										ASL_FuncFlags, FILF_SAVE | FILF_DOWILDFUNC,
										ASL_HookFunc, AslFileFilter,
										ASL_File, origName);
	else
		result = AslRequestTags(aslReq, ASL_Hail, prompt, ASL_Window, window,
										ASL_OKText, okText, ASL_CancelText, cancelText,
										ASL_FuncFlags, FILF_DOWILDFUNC,
										ASL_HookFunc, AslFileFilter);
	if (result) {
		strcpy(sfReply->Name, aslReq->rf_File);
		sfReply->DirLock = Lock(aslReq->rf_Dir, ACCESS_READ);
		SetCurrentDir(sfReply->DirLock);
		sfReply->Result = SFP_OK;
	}
	else
		sfReply->Result = SFP_CANCEL;
/*
	Clean up
*/
Exit:
	if (aslReq)
		FreeFileRequest(aslReq);
	if (window)
		CloseWindow(window);
	if (AslBase)
		CloseLibrary(AslBase);
	AslBase = oldAslBase;
	return (gotLib);
}

/*
 *	SFPGetFile
 *
 *	Display a dialog to get a file name for input
 *	Parameters:
 *		screen is the screen in which the dialog is to appear
 *		msgPort is the Message Port to for intuition messages
 *		prompt is the window title
 *		dlgFilter(intuiMsg) is called by ModalDialog during dialog
 *		dlgHook(item, Dialog) is called after ModalDialog with the
 *			item number and pointer to the dialog
 *			Should return with item number or -1
 *		dlgTempl is dialog template to be used instead of default one
 *		numTypes is number of types in typeList array
 *			If 0 then tool types are not checked
 *		typeList is array of pointers to NULL terminated tool types
 *		fileFilter(fileName) will be called for each file
 *			Should return TRUE if file should be shown in list
 *			(reverse of Macintosh convention)
 *		sfReply is pointer to reply record
 */

void SFPGetFile(ScreenPtr screen,
				MsgPortPtr msgPort,
				TextPtr prompt,
				BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				WORD (*dlgHook)(WORD, DialogPtr),
				DlgTemplPtr dlgTempl,
				WORD numTypes,
				TextPtr typeList[],
				BOOL (*fileFilter)(TextPtr),
				SFReplyPtr sfReply)
{
	register WORD item, result;
	register BOOL done, success;
	LONG lock;
	GadgetPtr gadgList;
	ImagePtr iconImage;
	Dir dir;
	TextChar fileName[GADG_MAX_STRING];

	if (dlgTempl == NULL)
		dlgTempl = &_sfpGetFileDlgTempl;
/*
	If user wants Asl requester, user gets Asl requester
*/
	if (useAslReq && AslRequestFile(screen, FALSE, prompt, NULL, dlgTempl, sfReply))
		return;
/*
	Initialize
*/
	dlgTempl->Title = prompt;
	dlgType = GET_DIALOG;
	atTop = FALSE;
	sfpMsgPort = msgPort;
	dialogFilter = dlgFilter;	/* So that SFPDialogFilter() can call them */
	result = SFP_NOMEM;
	if ((scrollList = NewScrollList(FALSE)) == NULL)
		goto Exit2;
/*
	Bring up dialog
*/
	AutoActivateEnable(FALSE);
	sfpDialog = GetDialog(dlgTempl, screen, msgPort);
	AutoActivateEnable(TRUE);
	if (sfpDialog == NULL)
		goto Exit1;
	SetStdPointer(sfpDialog, POINTER_WAIT);
	gadgList = sfpDialog->FirstGadget;
	InitScrollList(scrollList, GadgetItem(gadgList, FILE_LIST), sfpDialog, NULL);
	ModifyIDCMP(sfpDialog, sfpDialog->IDCMPFlags | (DISKINSERTED | DISKREMOVED));
	SLDrawBorder(scrollList);
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), sfpDialog, NULL, TRUE);
/*
	Read file names, and activate appropriate gadgets
*/
	OffButtons();
	if (dlgHook)
		(void) (*dlgHook)(SFPMSG_INIT, sfpDialog);
	success = ShowDirName();
	SetBackButton();
	if (success)
		GetFileList(fileFilter, numTypes, typeList);
	SetButtons();
/*
	Get and process dialog messages
*/
	done = FALSE;
	SetStdPointer(sfpDialog, POINTER_ARROW);
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, sfpDialog, SFPDialogFilter);
		if (item != -1 && dlgHook)
			item = (*dlgHook)(item, sfpDialog);
/*
	Process item
*/
		switch (item) {
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case CANCEL_BUTTON:
			result = SFP_CANCEL;
			done = TRUE;
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != OK_BUTTON)
				break;				/* Else fall through */
		case OK_BUTTON:
			GetEditItemText(gadgList, FILENAME_TEXT, fileName);
			if (strlen(fileName) == 0 || (dir = CheckFileName(fileName)) == NULL) {
				(void) SimpleRequest(msgPort, sfpDialog, REQ_BADNAME, dlgFilter);
				break;
			}
			SetCurrentDir(dir);
			if (strlen(fileName) && (lock = Lock(fileName, ACCESS_READ)) != NULL) {
				UnLock(lock);
				strcpy(sfReply->Name, fileName);
				sfReply->DirLock = dir;
				result = SFP_OK;
				done = TRUE;
				break;
			}
			if (strlen(fileName))
				(void) SimpleRequest(msgPort, sfpDialog, REQ_NOFILE, dlgFilter);
			SetEditItemText(gadgList, FILENAME_TEXT, sfpDialog, NULL, fileName);
			UnLock(dir);			/* Else fall through */
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case DISKS_BUTTON:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, fileFilter, numTypes, typeList);
			break;
		}
	} while (!done);
/*
	Dispose of dialog
*/
	iconImage = GadgetItem(gadgList, DISK_ICON)->GadgetRender;
	DisposeDialog(sfpDialog);
	FreeImage(iconImage);
/*
	Dispose of scroll list
*/
Exit1:
	DisposeScrollList(scrollList);
/*
	Return with specified result
*/
Exit2:
	sfReply->Result = result;
}

/*
 *	SFPPutFile
 *
 *	Display a dialog to get a file name for output
 *	Parameters:
 *		screen is the screen in which the dialog is to appear
 *		msgPort is the Message Port to for intuition messages
 *		prompt is NULL terminated prompt string
 *		origName is NULL terminated string for original file name
 *		dlgFilter(intuiMsg) is called by ModalDialog during dialog
 *		dlgHook(item, dialog) is called after ModalDialog with the
 *			item number and pointer to the dialog
 *			Should return with item number or -1
 *		dlgTempl is dialog template to be used instead of default one
 *		sfReply is pointer to reply record
 */

void SFPPutFile(ScreenPtr screen,
				MsgPortPtr msgPort,
				TextPtr prompt,
				TextPtr origName,
				BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				WORD (*dlgHook)(WORD, DialogPtr),
				DlgTemplPtr dlgTempl,
				SFReplyPtr sfReply)
{
	register WORD item, result;
	register BOOL done, success;
	WORD origLen, newLen;
	BPTR fileLock;
	GadgetPtr gadgList;
	ImagePtr iconImage;
	Dir dir;
	TextChar fileName[GADG_MAX_STRING];

	if (dlgTempl == NULL)
		dlgTempl = &_sfpPutFileDlgTempl;
/*
	If user wants Asl requester, user gets Asl requester
*/
	if (useAslReq && AslRequestFile(screen, TRUE, prompt, origName, dlgTempl, sfReply))
		return;
/*
	Initialize
*/
	dlgTempl->Title = dlgTempl->Gadgets[OK_BUTTON].Info;
	dlgTempl->Gadgets[PUT_PROMPT].Info = prompt;
	dlgTempl->Gadgets[FILENAME_TEXT].Info = origName;
	dlgType = PUT_DIALOG;
	atTop = FALSE;
	sfpMsgPort = msgPort;
	dialogFilter = dlgFilter;	/* So that SFPDialogFilter() can call them */
	result = SFP_NOMEM;
	if ((scrollList = NewScrollList(FALSE)) == NULL)
		goto Exit2;
/*
	Set up dialog
*/
	if ((sfpDialog = GetDialog(dlgTempl, screen, msgPort)) == NULL)
		goto Exit1;
	SetStdPointer(sfpDialog, POINTER_WAIT);
	gadgList = sfpDialog->FirstGadget;
	InitScrollList(scrollList, GadgetItem(gadgList, FILE_LIST), sfpDialog, NULL);
	ModifyIDCMP(sfpDialog, sfpDialog->IDCMPFlags | (DISKINSERTED | DISKREMOVED));
	SLDrawBorder(scrollList);
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), sfpDialog, NULL, TRUE);
/*
	Read file names, and activate appropriate gadgets
*/
	OffButtons();
	if (dlgHook)
		(void) (*dlgHook)(SFPMSG_INIT, sfpDialog);
	success = ShowDirName();
	SetBackButton();
	if (success)
		GetFileList(NULL, 0, NULL);
	SetButtons();
/*
	Get and process dialog messages
*/
	done = FALSE;
	SetStdPointer(sfpDialog, POINTER_ARROW);
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, sfpDialog, SFPDialogFilter);
		if (item != -1 && dlgHook)
			item = (*dlgHook)(item, sfpDialog);
/*
	Process item
*/
		switch (item) {
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case CANCEL_BUTTON:
			result = SFP_CANCEL;
			done = TRUE;
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != OK_BUTTON)
				break;				/* Else fall through */
		case OK_BUTTON:
			GetEditItemText(gadgList, FILENAME_TEXT, fileName);
			origLen = strlen(fileName);
			if (origLen == 0 || (dir = CheckFileName(fileName)) == NULL) {
				(void) SimpleRequest(msgPort, sfpDialog, REQ_BADNAME, dlgFilter);
				break;
			}
			SetCurrentDir(dir);
			newLen = strlen(fileName);
			if (newLen) {
				if (DiskIsLocked()) {
					(void) SimpleRequest(msgPort, sfpDialog, REQ_DISKLOCK, dlgFilter);
					item = CANCEL_BUTTON;
				}
				else if ((fileLock = Lock(fileName, ACCESS_READ)) != NULL) {
					UnLock(fileLock);
					item = SimpleRequest(msgPort, sfpDialog, REQ_FILEEXISTS, dlgFilter);
				}
				if (item == OK_BUTTON) {
					strcpy(sfReply->Name, fileName);
					sfReply->DirLock = dir;
					result = SFP_OK;
					done = TRUE;
					break;
				}
				else if (newLen == origLen)
					break;	/* Don't re-display directory if it didn't change */
				item = OK_BUTTON;
			}
			SetEditItemText(gadgList, FILENAME_TEXT, sfpDialog, NULL, fileName);
			UnLock(dir);			/* Else fall through */
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case DISKS_BUTTON:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, NULL, 0, NULL);
			break;
		}
	} while (!done);
/*
	Dispose of dialog
*/
	iconImage = GadgetItem(gadgList, DISK_ICON)->GadgetRender;
	DisposeDialog(sfpDialog);
	FreeImage(iconImage);
/*
	Dispose of scroll list
*/
Exit1:
	DisposeScrollList(scrollList);
/*
	Return with specified result
*/
Exit2:
	sfReply->Result = result;
}

/*
 *	Convert file name to be a pure file name (no path specifiers)
 *	Path name is relative to current directory
 *	Return directory lock of path to file, or NULL if no such path
 */

Dir ConvertFileName(register TextPtr fileName)
{
	register TextChar ch;
	register WORD i, start, len;
	Dir dir;
	FIBPtr fib;

	len = strlen(fileName);
/*
	Check to see if fileName is a path only
*/
	dir = NULL;
	if (*fileName && (fib = AllocMem(sizeof(FIB), MEMF_CLEAR)) != NULL) {
		if ((dir = Lock(fileName, ACCESS_READ)) != NULL) {
			if (Examine(dir, fib) && fib->fib_DirEntryType <= 0) {
				UnLock(dir);				/* Is a file lock */
				dir = NULL;
			}
		}
		FreeMem(fib, sizeof(FIB));
	}
	if (dir) {
		*fileName = '\0';
		return (dir);
	}
/*
	Scan for ':' or '/' from end of name back
	If found, get lock on directory portion and change text to simple file name
*/
	for (start = len; start > 0; start--) {
		if (fileName[start - 1] == ':' || fileName[start - 1] == '/') {
			ch = fileName[start];
			fileName[start] = '\0';
			dir = Lock(fileName, ACCESS_READ);
			fileName[start] = ch;
			for (i = start; i <= len; i++)
				fileName[i - start] = fileName[i];
			return (dir);
		}
	}
/*
	No path specifier found, so return duplicate of current directory lock
*/
	return (DupLock(GetCurrentDir()));
}

/*
 *	Set current directory to directory specified by given lock
 *	Uses a copy of the supplied lock
 */

void SetCurrentDir(Dir dir)
{
	register Dir oldLock, newLock;

	newLock = DupLock(dir);
	oldLock = CurrentDir(newLock);
	UnLock(oldLock);
}

/*
 *	Set flag to use Asl file requester
 */

void SFPUseAslRequest(BOOL asl)
{
	useAslReq = asl;
}
