/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Spell checking menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Window.h>
#include <Toolbox/Gadget.h>

#include "Flow.h"
#include "HEAD.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;

extern MsgPortPtr	mainMsgPort;

extern TextChar	strNoSpellError[], strNoSpellAlternate[];
extern TextChar	strStart[], strDone[], strSkip[];

extern TextChar	strOpenDict[];

extern TextChar	strBuff[];

extern WindowPtr	cmdWindow;

extern DlgTemplPtr	dlgList[];

extern DialogPtr	spellDialog;

extern ScrollListPtr	scrollList;

extern WORD			modelessStrItem;

extern BOOL			idleEvent;

/*
 *	Local variables and definitions
 */

#define IS_POSSESSIVE(word, len)	\
	((len)>=2 && (word)[(len)-1]=='s' && ((word)[(len)-2]==QUOTE))

#define MAX_WORD_LEN		50		/* Maximum length of any word */

#define MAX_CURR_LIST	50		/* Max number of words in each current list */

#define START_BUTTON		OK_BUTTON
#define SKIP_BUTTON		START_BUTTON
#define CH_SPELL_BUTTON	1
#define LEARN_BUTTON		2
#define GUESS_BUTTON		3
#define SUSPECT_STATTEXT 4
#define CHANGE_EDITTEXT	5
#define SPELL_LIST		6
#define SPELL_UP		7
#define SPELL_DOWN		8
#define SPELL_SCROLL		9

static BOOL			startSet = TRUE;		/* "Start" button set in dialog */
static WORD			defaultSpellButton = START_BUTTON;

static BOOL			showProgress;		/* Show check progress in window */

static BOOL			checkAll;		/* FALSE if range was selected for check */

static TextLoc		beginRange, startRange, endRange;

static TextChar		suspectWord[MAX_WORD_LEN +1];

/*
 *	Local prototypes
 */

BOOL	DoCheckSpelling(UWORD);

BOOL	OpenDictFilter(TextPtr);
WORD	SkipSpellSpace(HeadPtr, WORD);
WORD	SpellWordLen(HeadPtr, WORD);
BOOL	SuspectSelected(void);
void	ResetSpellDialog(void);

/*
 *	Save suspect word, and if spell requester active then display it
 */

void SetSuspectWord(TextPtr word, WORD len)
{
	GadgetPtr gadgList;
	
	if (len > MAX_WORD_LEN)
		len = MAX_WORD_LEN;
	if (len)
		BlockMove(word, suspectWord, len);
	suspectWord[len] = '\0';
	if (spellDialog != NULL )
		gadgList = spellDialog->FirstGadget;
		SetGadgetItemText(gadgList, SUSPECT_STATTEXT, spellDialog, NULL, suspectWord);
}

/*
 *	Set text for edit box
 */

void SetEditWord(TextPtr word, WORD len)
{
	TextChar editWord[MAX_WORD_LEN + 1];

	if (len > MAX_WORD_LEN - 2)		/* Room for possessive */
		len = MAX_WORD_LEN - 2;
	if (len) {
		BlockMove(word, editWord, len);
		word = suspectWord;
		if (IS_POSSESSIVE(word, strlen(word)) && !IS_POSSESSIVE(editWord, len)) {
			editWord[len++] = QUOTE;
			editWord[len++] = 's';
		}
		if (IS_UPPER(word[0]))
			editWord[0] = toUpper[editWord[0]];
	}
	editWord[len] = '\0';
	SetEditItemText(spellDialog->FirstGadget,CHANGE_EDITTEXT, spellDialog,NULL,editWord);
}

/*
 *	Display the new list of suggested words
 */

void ShowSuggestList()
{
	WORD i, numItems;
	TextPtr text;

	SLDoDraw(scrollList, FALSE);
	numItems = NumSuggestWords();
	for (i = 0; i < numItems; i++) {
		text = GetSuggestWord(i);
		SLAddItem(scrollList, text, (WORD) strlen(text), i);
	}
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Set new selected word in spell or look up requester
 */

void NewWord(WORD newWord)
{
	TextPtr word;
	WORD numWords;

	numWords = NumSuggestWords();
	if (numWords ) {
		if (newWord < 0)
			newWord = 0;
		else if (newWord >= numWords)
			newWord = numWords - 1;
		SLSelectItem(scrollList, newWord, TRUE);
		SLAutoScroll(scrollList, newWord);
/*
	Display new word in change box
*/
		word = GetSuggestWord(newWord);
		SetEditWord(word, (WORD) strlen(word));
	}
}

/*
 *	Enable/Disable Skip, Change, Learn, and Guess buttons
 */

void SetSpellButtons()
{
	WORD changeLen;
	register BOOL enable;
	register GadgetPtr gadget;
	register GadgetPtr gadgList = spellDialog->FirstGadget;
	register WORD newDefault;
	
	GetEditItemText(gadgList, CHANGE_EDITTEXT, strBuff);
	changeLen = strlen(strBuff);
/*
	If no suspect word then disable Learn and Suggest buttons
*/
	enable = SuspectSelected();
	if( !idleEvent ) {
		EnableGadgetItem(gadgList, LEARN_BUTTON, spellDialog, NULL, enable);
		EnableGadgetItem(gadgList, GUESS_BUTTON, spellDialog, NULL, enable);
		SetButtonItem(gadgList, START_BUTTON, spellDialog, NULL, enable ? strSkip : strStart,'S');
	}
	EnableGadgetItem(gadgList, START_BUTTON, spellDialog, NULL, TRUE);
/*
	If no suspect or change word then disable Change button
*/
	enable &= ( changeLen != 0 );
	EnableGadgetItem(gadgList, CH_SPELL_BUTTON, spellDialog, NULL, enable);
/*
	Outline and set default button
*/
	newDefault = (!enable || 
		CmpString(suspectWord, strBuff, strlen(suspectWord), changeLen, TRUE) == 0)
		? SKIP_BUTTON : CH_SPELL_BUTTON;

	SetDefaultButtons(newDefault, -1);
	if (newDefault != defaultSpellButton) {
		gadget = GadgetItem(gadgList, defaultSpellButton);
		OutlineButton(gadget, spellDialog, NULL, FALSE);
		gadget = GadgetItem(gadgList, newDefault);
		OutlineButton(gadget, spellDialog, NULL, TRUE);
		defaultSpellButton = newDefault;
	}
}

/*
 *	Disable spell buttons
 */

void OffSpellButtons()
{
	register GadgetPtr gadgList = spellDialog->FirstGadget;
	
	EnableGadgetItem(gadgList, SKIP_BUTTON, spellDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, CH_SPELL_BUTTON, spellDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, LEARN_BUTTON, spellDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, GUESS_BUTTON, spellDialog, NULL, FALSE);
}

/*
 *	Skip over non-word characters for current dictionary
 */

static WORD SkipSpellSpace(head, loc)
HeadPtr head;
register WORD loc;
{
	register TextPtr text = head->Text + loc;
	register WORD maxLoc = head->Len;

	while (loc < maxLoc && !IsSpellWordChar(*text++))
		loc++;
	return (loc);
}

/*
 *	Return length of word at given location for current dictionary
 */

static WORD SpellWordLen(head, loc)
HeadPtr head;
register WORD loc;
{
	register TextPtr text = head->Text + loc;
	register WORD len, maxLen = head->Len - loc;

	for (len = 0; len < maxLen && IsSpellWordChar(*text++); len++) ;
	return (len);
}

/*
 *	Advance to next word
 *	Return TRUE if word found, FALSE if not
 */

static BOOL NextWord(TextLocPtr selStart, TextLocPtr selEnd, TextLocPtr endRange)
{
	register BOOL foundWord;
	register WORD loc, len, maxLoc;
	register HeadPtr head;
/*
	Start from current selEnd and search forward
	Words of one character are ignored
*/
	head = selEnd->Head;
	loc = selEnd->Loc;
	len = 0;
	foundWord = FALSE;
	while (head && !foundWord) {
		maxLoc = head->Len;
		if (head == endRange->Head && maxLoc > endRange->Loc)
			maxLoc = endRange->Loc;
		if (head->Flags & HD_SPELL)
			loc = maxLoc;
		else {
			loc = SkipSpellSpace(head, loc);
			while (loc < maxLoc) {
				if ((len = SpellWordLen(head, loc)) >= 2 && len <= MAX_WORD_LEN) {
					foundWord = TRUE;
					break;
				}
				loc += len;
				loc = SkipSpellSpace(head, loc);
			}
		}
		if (!foundWord) {
			if( head == endRange->Head )
				break;
			head = NextHeadAll(head);
			endRange->Loc = head->Len;
			loc = head->BodyStart;
		}
	}
/*
	Set return values and return success status
*/
	selStart->Head = selEnd->Head = head;
	selStart->Loc = loc;
	selEnd->Loc = loc + len;
	return (foundWord);
}

/*
 *	Search for next bad word and return
 *	Return 1 if bad word found, 0 if not, -1 if Stop button pressed
 */

WORD CheckSpelling()
{
	register TextPtr word;
	register WORD len, result;
	BOOL done;
	TextLoc selStart, selEnd, checkLoc;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);

	SetBusyPointer(spellDialog);
	CursorOff(cmdWindow);
	done = FALSE;
/*
	Begin main loop and look for words
*/
	do {
		selStart = selEnd = checkLoc = startRange;
/*
	Check from selStart to end of range
*/
		result = 0;
		while (result == 0 && NextWord(&selStart, &selEnd, &endRange)) {
			word = selStart.Head->Text + selStart.Loc;
			len = selEnd.Loc - selStart.Loc;
			if (selStart.Head != checkLoc.Head) {
				if (checkLoc.Loc == checkLoc.Head->BodyStart)
					checkLoc.Head->Flags |= HD_SPELL;
				checkLoc.Head = selStart.Head;
				checkLoc.Loc = checkLoc.Head->BodyStart;
			}
/*
	Ignore initial and trailing ' and trailing 's
*/
			if (word[0] == QUOTE) {
				word++;
				len--;
				selStart.Loc++;
			}
			if (word[len - 1] == QUOTE) {
				len--;
				selEnd.Loc--;
			}
			if (showProgress) {
				docData->SelHead = selStart.Head;
				docData->SelStart = selStart.Loc;
				docData->SelEnd = selEnd.Loc;
				CursorOn(cmdWindow);
				ScrollToCursor(cmdWindow);
			}
			if (!LookUpWord(word, len, TRUE))
				result = 1;
			/*
			if (CheckDialog(mainMsgPort, cmdWindow, DialogFilter) == DONE_BUTTON)
				result = -1;
			*/
			if (showProgress)
				CursorOff(cmdWindow);
			NextBusyPointer();
		}
/*
	If nothing found and checking everything, ask to continue from top
*/
		if (result != 0 || !checkAll ||
			(beginRange.Head == docData->FirstHead && beginRange.Loc == docData->FirstHead->BodyStart) ||
			StdDialog(DLG_WRAPSPELL) == CANCEL_BUTTON)
			done = TRUE;
		else {
			ScrollToOffset(cmdWindow, 0, docData->LeftOffset);
			CursorOff(cmdWindow);
			SetBusyPointer(cmdWindow);
			endRange = beginRange;
			startRange.Head = docData->FirstHead;
			startRange.Loc = docData->FirstHead->BodyStart;
			beginRange = startRange;
		}
	} while (!done);
/*
	Set selStart and selEnd to word if found
*/
	if( result == 0 &&	
		((selStart.Head == checkLoc.Head && checkAll) ||
		 (selStart.Head != checkLoc.Head && checkLoc.Loc == checkLoc.Head->BodyStart))) {
		checkLoc.Head->Flags |= HD_SPELL;
	}
	else if (result == 1) {
		docData->SelHead = selStart.Head;
		docData->SelStart = selStart.Loc;
		startRange = selEnd;
		docData->SelEnd = selEnd.Loc;
		RevealHead(docData);
	}
	CursorOn(cmdWindow);
	SetStdPointer(cmdWindow, POINTER_ARROW);
	return (result);
}

/*
	Is current selection the same as the suspect word?
*/

static BOOL SuspectSelected()
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	
	WORD len = strlen(suspectWord);
	return( len && (CompareText( &docData->SelHead->Text[docData->SelStart], suspectWord, docData->SelEnd - docData->SelStart, len ) == 0 ) ) ;
}

/*
	Call upon activate event of spell dialog
*/

void ActivateSpellDialog()
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	
	if( !SuspectSelected() ) {
		ResetSpellDialog();
		SetSpellRange(docData);
	}
	SetSpellButtons();
}

/*
 *	Set spell check range
 */
 
void SetSpellRange(DocDataPtr docData)
{
	TextPtr text;
	register HeadPtr nextHead, lastSelHead;
	
	startRange.Head = docData->SelHead;
	startRange.Loc = docData->SelStart;
	text = startRange.Head->Text;
	beginRange = startRange;
	if( startRange.Loc < startRange.Head->Len && wordChar[text[startRange.Loc]]) {
		while( (startRange.Loc != startRange.Head->BodyStart ) && 
				wordChar[text[startRange.Loc - 1]])
			startRange.Loc--;
	}
	if( docData->SelStart == docData->SelEnd && !( docData->SelHead->Flags & HD_SELECTED ) ) {
		endRange.Head = LastHead(docData);
		endRange.Loc = endRange.Head->Len;
		checkAll = TRUE;
	} else {
		lastSelHead = endRange.Head = LastSelHead(docData->SelHead);
		while( lastSelHead && lastSelHead->Next == NULL ) {
			lastSelHead = lastSelHead->Super;
		}
		if( lastSelHead != NULL )
			lastSelHead = lastSelHead->Next;
		while( (nextHead = NextHeadAll(endRange.Head)) != lastSelHead )
			endRange.Head = nextHead;
		endRange.Loc = docData->SelEnd;
		checkAll = FALSE;
	}
	text = endRange.Head->Text;
	if( endRange.Loc && wordChar[text[endRange.Loc-1]]) {
			while( endRange.Loc < endRange.Head->Len && wordChar[text[endRange.Loc]])
				endRange.Loc++;
	}
}

/*
 *	Check spelling of document
 */

static BOOL DoCheckSpelling(UWORD modifier)
{
	register BOOL success = FALSE;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);

	if( spellDialog == NULL ) {
		showProgress = (BOOL) (modifier & ALTKEYS);

		SetStdPointer(cmdWindow, POINTER_WAIT);

		if(!InitSpell(NULL, NULL)) {
			Error(ERR_NO_DICT);
		} else if( (scrollList = NewScrollList(FALSE)) == NULL) {
			Error(ERR_NO_MEM);
		} else if ((spellDialog = GetDialog(dlgList[DLG_CHECKSPELL], screen, mainMsgPort)) == NULL) {
			DisposeScrollList(scrollList);
			Error(ERR_NO_MEM);
		} else {
/*
	Perform modeless dialog initalization
*/
			success = TRUE;
			DoDialogOpen(spellDialog);
			InitScrollList(scrollList, GadgetItem(spellDialog->FirstGadget, SPELL_LIST), spellDialog, NULL);
			SLDrawBorder(scrollList);
			modelessStrItem = CHANGE_EDITTEXT;
		}
	} else {
		SelectWindow(spellDialog);
		success = TRUE;
	}
/*
	If selection highlighted, automatically start spell checker.
*/
	if( spellDialog != NULL ) {
		defaultSpellButton = START_BUTTON;
		SetSuspectWord(NULL, 0);
		SetSpellButtons();
		SetSpellRange(docData);
		SetStdPointer(cmdWindow, POINTER_ARROW);
		if( docData->SelStart != docData->SelEnd ) {
			DoSpellDialogItem(defaultSpellButton);
		}
	}
	return (success);
}

/*
 *	Check file for dictionary id
 *	Returns TRUE if file should be shown in SFPGetFile() list
 */

static BOOL OpenDictFilter(TextPtr fileName)
{
	register BOOL success;
	File file;
	LONG id;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, (BYTE *) &id, sizeof(LONG)) == sizeof(LONG) && id == ID_DICTFILE)
		success = TRUE;
	Close(file);
	return (success);
}

/*
 *	Get new main dictionary and set to use it
 */

static BOOL DoChangeDict(void)
{
	register WindowPtr window = cmdWindow;
	BOOL success;
	SFReply sfReply;

/*
	Get new dictionary name and location
*/
	DoWindowActivate(window, FALSE);	/* Since INACTIVE event will be lost */
	BeginWait();
	SFPGetFile(screen, mainMsgPort, strOpenDict, DialogFilter, NULL, NULL,
			   0, NULL, OpenDictFilter, &sfReply);
	EndWait();
	DoWindowActivate(window, TRUE);
	if (sfReply.Result != SFP_OK) {
		if (sfReply.Result == SFP_NOMEM)
			Error(ERR_NO_MEM);
		return (FALSE);
	}
/*
	Switch to new dictionary
*/
	SetStdPointer(window, POINTER_WAIT);
	success = ChangeMainDict(sfReply.Name, sfReply.DirLock);
	if (!success)
		Error(ERR_BAD_DICT);
	SetSearchMenu();
	return (success);
}

/*
	Resets list and text box spelling dialog gadgets to default conditions
*/

static void ResetSpellDialog()
{
	SetSuspectWord(NULL, 0);
	SetEditWord(NULL, 0);
	SLRemoveAll(scrollList);
	DisposeSuggestWords();
}

/*
	Handle dialog item action
*/

void DoSpellDialogItem(WORD item )
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(cmdWindow);
	WORD diffLen, oldLen, check;
	register HeadPtr oldHead, selHead;
	TextChar changeText[MAX_WORD_LEN + 1];
	static TextPtr word;
	static WORD len;
	register GadgetPtr gadgList = spellDialog->FirstGadget;
	
	switch(item) {
	case CH_SPELL_BUTTON:
		SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);
		GetEditItemText(gadgList, CHANGE_EDITTEXT, changeText);
		if( !LookUpWord(changeText, (WORD) strlen(changeText), FALSE) &&
				StdDialog(DLG_SPELLCHANGE) == CANCEL_BUTTON) {
			break;
		}
		SetSpellButtons();
		oldLen = docData->SelEnd - docData->SelStart;
		oldHead = docData->SelHead;
		CursorOff(cmdWindow);
		if (!ChangeText(changeText)) {
			Error(ERR_NO_CHANGE);
		} else {
			DrawHead(cmdWindow, docData->SelHead);
			CountLines(docData);
			AdjustScrollBars(cmdWindow);
			DocumentModified(docData);
		}
		CursorOn(cmdWindow);
		selHead = docData->SelHead;
		diffLen = (docData->SelEnd - docData->SelStart) - oldLen;
/*
 * Because we are modeless, must check to see if ChangeText() changed selHead
 * and update our spell check ranges accordingly
 */
		if( oldHead == startRange.Head ) {
			startRange.Head = selHead;
		}
		if( oldHead == beginRange.Head ) {
			beginRange.Head = selHead;
		}
		if( oldHead == endRange.Head ) {
			endRange.Head = selHead;
		}
		startRange.Loc += diffLen;
		if (selHead == endRange.Head)
			endRange.Loc += diffLen;
		goto StartCheck;
	case LEARN_BUTTON:
		if (*suspectWord)
			AddUserWord(suspectWord, (WORD) strlen(suspectWord));
		goto StartCheck;
	case START_BUTTON:
		if (*suspectWord)
			AddSkipWord(suspectWord, (WORD) strlen(suspectWord));
		SetButtonItem(gadgList, START_BUTTON, spellDialog, NULL, strSkip,'S');
StartCheck:
/*
		SetButtonItem(gadgList, CANCEL_BUTTON, window, spellRequest, strStop,'S');
*/
		SetStdPointer(spellDialog, POINTER_WAIT);
		OffSpellButtons();
		ResetSpellDialog();
		check = CheckSpelling();
/*
		SetButtonItem(gadgList, CANCEL_BUTTON, window, spellRequest, strDone,'D');
*/
		SetStdPointer(spellDialog, POINTER_ARROW);
		switch(check) {
		case 0:
			SetDefaultButtons(OK_BUTTON, -1);
			InfoDialog(strNoSpellError, 0);
		/*	SelectWindow(spellDialog); */
			ActivateWindow(cmdWindow);
		case -1:
		default:
			break;
		case 1:
			word = docData->SelHead->Text + docData->SelStart;
			len = docData->SelEnd - docData->SelStart;
			SetSuspectWord(word, len);
			SetEditWord(word, len);
			ScrollToCursor(cmdWindow);
			ErrBeep();
			break;
		}
		SetSpellButtons();
		break;
	case GUESS_BUTTON:
		SetStdPointer(spellDialog, POINTER_WAIT);
		/*
		EnableCancelButton(spellDialog, FALSE);*/
		OffSpellButtons();
		SetEditWord(NULL, 0);
		SLRemoveAll(scrollList);
		DisposeSuggestWords();
		GetSuggestList(word, len);
		ShowSuggestList();
		NewWord(BestSuggestWord(word, len));
		/*
		EnableCancelButton(spellDialog, TRUE);*/
		SetStdPointer(spellDialog, POINTER_ARROW);
		if (NumSuggestWords() == 0) {
			SetDefaultButtons(OK_BUTTON, -1);
			InfoDialog(strNoSpellAlternate, 0);
		}
		SetSpellButtons();
		break;
	}
	return;
}

/*
	Handle cursor up/down in scroll list
*/

void DoSpellDialogKey(DialogPtr dlg, UWORD code)
{
	register WORD selWord;
	
	DoDialogActivate(dlg, TRUE);
	selWord = SLNextSelect(scrollList, -1);
	if( selWord == -1 )
		selWord = 0;
	else {
		if( code == CURSORUP )
			selWord--;
		else
			selWord++;
		NewWord(selWord);		/* Cause auto-scrolling if necessary */
	}
	SLSelectItem(scrollList, selWord, TRUE);
	return;
}

/*
 *	Handle spelling submenu
 */

BOOL DoSpellingMenu(WindowPtr window, UWORD sub, UWORD modifier)
{
	BOOL success;
	
	if (!IsDocWindow(window))
		return (FALSE);
	success = FALSE;
	switch (sub) {
	case CHECK_SUBITEM:
		success = DoCheckSpelling(modifier);
		break;
	case CHANGEDICT_SUBITEM:
		success = DoChangeDict();
		break;
	}
	return (success);
}
