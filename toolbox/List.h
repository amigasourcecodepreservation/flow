/*
 *	Amiga Toolbox routines
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Simple linked list defines
 */

#ifndef TOOLBOX_LIST_H
#define TOOLBOX_LIST_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	List entry
 */

typedef struct _ListItem {
	struct _ListItem	*Next;
	TextChar			Text[1];	/* NULL terminated text string */
} ListItem, *ListItemPtr;

/*
 *	List header
 */

typedef struct {
	ListItemPtr	First, Last;
} ListHead, *ListHeadPtr;

/*
 *	Routine prototypes
 */

ListHeadPtr	CreateList(void);
void		DisposeList(ListHeadPtr);
ListItemPtr	CreateListItem(TextPtr, WORD);
void		AddToList(ListHeadPtr, ListItemPtr, ListItemPtr);
void		RemoveFromList(ListHeadPtr, ListItemPtr, ListItemPtr);
LONG		AddListItem(ListHeadPtr, TextPtr, WORD, BOOL);
BOOL		InsertListItem(ListHeadPtr, TextPtr, WORD, LONG);
void		RemoveListItem(ListHeadPtr, LONG);
void		DisposeListItems(ListHeadPtr);
LONG		NumListItems(ListHeadPtr);
TextPtr		GetListItem(ListHeadPtr, LONG);
LONG		FindListItem(ListHeadPtr, TextPtr);

#endif
