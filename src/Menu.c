/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>

#include <string.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow, windowList[];
extern WORD			numWindows;

extern TextChar	findBuffer[];
extern struct RexxLib *RexxSysBase;

extern Ptr pasteBuff;

extern WORD pasteType;

extern LONG isClip;

/*
 *	Local prototypes
 */

void	OnOffMenu(WindowPtr, LONG, BOOL);

/*
 *	Turn on or off the specified menu or menu item
 */

static void OnOffMenu(WindowPtr window, LONG menuNum, BOOL on)
{
	if (on)
		OnMenu(window, menuNum);
	else
		OffMenu(window, menuNum);
}


/*
 *	Set up project menu
 */

void SetProjectMenu()
{
	register BOOL on;
	WindowPtr window;

	window = ActiveWindow();

	on = (numWindows < MAX_WINDOWS);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB), on);

	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, CLOSE_ITEM, NOSUB), on ||
		GetWKind(window) == WKIND_DIALOG );
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB), on);
	OnOffRevertMenuItem();
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PAGESETUP_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PRINTONE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PRINT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVEPREFS_ITEM, NOSUB), on) ;
}

/*
 * Update enable/disable status of "Revert" menu item
 */

void OnOffRevertMenuItem()
{
	WindowPtr window = ActiveWindow();
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register BOOL on;
	static BOOL prev_on = TRUE;	/* Trigger an immediate turn off */
		
	on = IsDocWindow(window);
	if( on ) {
		on = (docData->DirLock != NULL) && ( docData->Flags & WD_MODIFIED ) ;
	}
	if( prev_on != on ) {
		OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, REVERT_ITEM, NOSUB), on ) ;
		prev_on = on;
	}
	return;
}

/*
 *	Adjust Edit menu
 */

void SetEditMenu()
{
	HeadPtr selHead ;
	register BOOL on;
	WindowPtr window;
	DocDataPtr docData;

	window = ActiveWindow();
	on = IsDocWindow(window);
	
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, SELECTALL_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, INSERT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, ITEMFORMATS_ITEM, NOSUB), on);
	if (!on) {
		OffMenu(backWindow, MENUITEM(EDIT_MENU, CUT_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, COPY_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, PASTE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, ERASE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, CHANGECASE_ITEM, NOSUB));
	} else {
		docData = (DocDataPtr) GetWRefCon(window);	
		selHead = docData->SelHead ;

		on = (BOOL) selHead->Flags & HD_SELECTED;
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, INSERT_ITEM, NOSUB), !on );
		
		on |= (docData->SelStart != docData->SelEnd);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, CUT_ITEM, NOSUB), on);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, COPY_ITEM, NOSUB), on);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, ERASE_ITEM, NOSUB), on);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, PASTE_ITEM, NOSUB),(isClip != 1));

		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, CHANGECASE_ITEM, NOSUB), on);
	}
}

/*
 * Adjust Search menu
 */

void SetSearchMenu()
{
	register BOOL on;
	WindowPtr window;
	DocDataPtr docData;
	
	window = ActiveWindow();

	docData = GetWRefCon(window);
	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, NOITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, FIND_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, FINDNEXT_ITEM, NOSUB),
		(BOOL) strlen(findBuffer) );
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, CHANGE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, SETMARK_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(SEARCH_MENU, GOTOMARK_ITEM, NOSUB),on &&
		(docData->Mark.Head != NULL));
}

/*
 * Set up menus to correspond to allowable operations for this heading
 */

void SetSubHeadMenu()
{
	WindowPtr window;
	register HeadPtr selHead;
	HeadPtr lastHead, nextHead;
	register BOOL on;
	DocDataPtr docData;

	window = ActiveWindow();
	
	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(HEADING_MENU, NOITEM, NOSUB), on);
	if( on ) {
		docData = (DocDataPtr) GetWRefCon(window);
		selHead = docData->SelHead;
		lastHead = LastSelHead(selHead);
		if (lastHead != selHead) {
			OnMenu(backWindow, MENUITEM(HEADING_MENU, COLLAPSE_ITEM, NOSUB));
			OnMenu(backWindow, MENUITEM(HEADING_MENU, EXPAND_ITEM, NOSUB));
			OnMenu(backWindow, MENUITEM(HEADING_MENU, EXPANDALL_ITEM, NOSUB));
		} else {
			OnOffMenu(backWindow, MENUITEM(HEADING_MENU, EXPANDALL_ITEM, NOSUB), (BOOL) selHead->Sub);
			if (selHead->Sub) {
				on = selHead->Flags & HD_EXPANDED ;
				OnOffMenu( backWindow, MENUITEM(HEADING_MENU, COLLAPSE_ITEM, NOSUB), on);
				OnOffMenu( backWindow, MENUITEM(HEADING_MENU, EXPAND_ITEM, NOSUB), !on);
			} else {
				OffMenu(backWindow, MENUITEM(HEADING_MENU, COLLAPSE_ITEM, NOSUB));
				OffMenu(backWindow, MENUITEM(HEADING_MENU, EXPAND_ITEM, NOSUB));
			}
		}
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, MOVEUP_ITEM, NOSUB), (BOOL) selHead->Prev);
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, MOVEDOWN_ITEM, NOSUB), (BOOL) lastHead->Next);
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, INDENT_ITEM, NOSUB), (BOOL) selHead->Prev);
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, UNINDENT_ITEM, NOSUB), (BOOL) selHead->Super);
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, SORT_ITEM, NOSUB),
				selHead->Prev || selHead->Next);
		on = !(selHead->Flags & HD_SELECTED);
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, SPLIT_ITEM, NOSUB), on);
		nextHead = NextHead(selHead);
		on &= ( nextHead != NULL ) && ( nextHead->Sub == NULL );
		OnOffMenu(backWindow, MENUITEM(HEADING_MENU, JOIN_ITEM, NOSUB), on);
	}
}
 
/*
 *	Adjust Format menu
 */

void SetFormatMenu()
{
	WindowPtr window;
	DocDataPtr docData;
	register BOOL on ;
	HeadPtr selHead ;
	register WORD style ;
	
	window = ActiveWindow();
	 
	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(FORMAT_MENU, NOITEM, NOSUB), on);
	if( on ) {
		docData = (DocDataPtr) GetWRefCon(window);
		selHead = docData->SelHead;
		style = ((selHead->Flags & HD_SELECTED) && selHead->Next &&
					(selHead->Next->Flags & HD_SELECTED)) ? 0x100 : selHead->Style;
		CheckMenu(backWindow, MENUITEM(FORMAT_MENU, PLAIN_ITEM, NOSUB), style == FS_NORMAL);
		CheckMenu(backWindow, MENUITEM(FORMAT_MENU, BOLD_ITEM, NOSUB), style & FSF_BOLD);
		CheckMenu(backWindow, MENUITEM(FORMAT_MENU, ITALIC_ITEM, NOSUB), style & FSF_ITALIC);
		CheckMenu(backWindow, MENUITEM(FORMAT_MENU, UNDERLINE_ITEM, NOSUB), style & FSF_UNDERLINED);
		CheckMenu(backWindow, MENUITEM(FORMAT_MENU, PAGEBREAK_ITEM, NOSUB),
			(BOOL) (selHead->Flags & HD_PAGEBREAK) );
	}
}

/*
 *	Adjust View menu
 */

void SetViewMenu()
{
	register WORD i;
	WindowPtr window;

	window = ActiveWindow();
	if (numWindows) {
		for (i = 0; i < numWindows; i++)
			CheckMenu(backWindow, (UWORD) MENUITEM(VIEW_MENU, WINDOW_ITEM + i, NOSUB),
			  (BOOL) (window == windowList[i]));
	}
}

/*
 *	Adjust Macro menu
 *	Only needs to be called once at program start-up
 */

void SetMacroMenu()
{
	OnOffMenu(backWindow, MENUITEM(MACRO_MENU, NOITEM, NOSUB), (BOOL) RexxSysBase);
}

/*
 * Set up all menus
 */
	 
void SetAllMenus()
{
	SetProjectMenu();
	SetEditMenu();
	SetSearchMenu();
	SetSubHeadMenu();
	SetFormatMenu();
	SetViewMenu();
}
