/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Screen routines
 */

#include <exec/types.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/workbench.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Color.h>
#include <Toolbox/Screen.h>

#include <IFF/Draw.h>		/* For RGBCOLOR_--- definitions */
#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct WBStartup	*WBenchMsg;

extern TextChar	progPathName[];

extern ScreenPtr	screen;
extern WindowPtr	windowList[];
extern WORD			numWindows;
extern RGBColor 	screenColors[];

extern TextChar 	strScreenName[];

/*
 *	Local variables and definitions
 */

static ULONG	displayID;			/* OS 2.0 DisplayID */

static TextChar topazFontName[] = "topaz.font";
static TextChar systemFontName[] = "System.font";
static TextChar systemThinFontName[] = "SystemThin.font";

static UWORD	newLookPens[] = {
	COLOR_BLUE, COLOR_WHITE,
	COLOR_BLACK,COLOR_WHITE, COLOR_BLACK, COLOR_CYAN, COLOR_BLACK,
	COLOR_BLUE, COLOR_WHITE,
	~0
};

#define DARK(color)	RGBCOLOR((RED(color)+1)/2,(GREEN(color)+1)/2,(BLUE(color)+1)/2)

/*
 *	Actual colors for dot-matrix printer ribbon (to match screen colors)
 *	Screen colors for red, green, and blue are derived from these
 */

#define PRT_RGBCOLOR_CYAN		0x3AC
#define PRT_RGBCOLOR_MAGENTA	0xE07
#define PRT_RGBCOLOR_YELLOW	0xFE5

enum {
	BLACK, WHITE, GREY, BEIGE
};

/*
 *	Local prototypes
 */

void	BuildDefaultColorTable(ColorTablePtr);

RGBColor MixColors2( RGBColor, RGBColor);


/*
 *	Build default screen color tables
 */

void BuildDefaultColorTable(register ColorTablePtr colorTable)
{
	(*colorTable)[BLACK]		= RGBCOLOR(0,0,0);
	(*colorTable)[WHITE]		= RGBCOLOR(15,15,15);
	(*colorTable)[BEIGE]		= RGBCOLOR(11,10,9);
	(*colorTable)[GREY]			= RGBCOLOR(4,6,8);	
}

/*
 *	Check current screen color table and adapt to any changes
 *	Assumes screen color depth does change
 */

void CheckColorTable()
{
	WORD i, numColors;
	BOOL changed;
	Rectangle rect;
	RGBColor colorTable[256];

	if (screen->Flags & BEEPING)
		return;
/*
	Check to see if color table has changed, and copy new color table
*/
	numColors = GetColorTable(screen, &colorTable);
	changed = FALSE;
	for (i = 0; i < numColors; i++) {
		if (colorTable[i] != screenColors[i]) {
			screenColors[i] = colorTable[i];	/* Save new value */
			changed = TRUE;
		}
	}
/*
	If changed, redraw all windows
*/
	if (changed) {
		InitToolbox(screen);
		for (i = 0; i < numWindows; i++) {
			GetWindowRect(windowList[i], &rect);
			InvalRect(windowList[i], &rect);
		}
	}
}


/*
 *	Do subtractive mix of two colors, returning result
 */

static RGBColor MixColors2(register RGBColor color1, register RGBColor color2)
{
	register WORD red, green, blue;

	red		= (RED(color1)  *RED(color2)   + 7)/15;
	green	= (GREEN(color1)*GREEN(color2) + 7)/15;
	blue	= (BLUE(color1) *BLUE(color2)  + 7)/15;
	return (RGBCOLOR(red, green, blue));
}
