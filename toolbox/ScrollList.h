/*
 *	Amiga Toolbox
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Scroll List handler
 */

#ifndef TOOLBOX_SCROLLLIST_H
#define TOOLBOX_SCROLLLIST_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#ifndef TOOLBOX_MEMORY_H
#include <Toolbox/Memory.h>
#endif

#ifndef TOOLBOX_LIST_H
#include <Toolbox/List.h>
#endif

/*
 *	Scroll list structure
 */

typedef struct {
	GadgetPtr	ListBox;		/* Border gadget for list */
	GadgetPtr	UpArrow;		/* Up arrow */
	GadgetPtr	DownArrow;		/* Down arrow */
	GadgetPtr	Slider;			/* Scroll box */
	WindowPtr	Window;
	RequestPtr	Request;
	TextAttrPtr	Font;			/* Font for list elements */
	ListHeadPtr	List;			/* The actual list */
	WORD		XOffset, YOffset, Flags;	/* Private */
	void		(*DrawProc)(RastPtr, TextPtr, RectPtr);
	WORD		PrevItem;
	ULONG		PrevSeconds, PrevMicros;
	BOOL		DoubleClick;
} ScrollList, *ScrollListPtr;

/*
 *	Prototypes
 */

ScrollListPtr	NewScrollList(BOOL);

void	InitScrollList(ScrollListPtr, GadgetPtr, WindowPtr, RequestPtr);
void	DisposeScrollList(ScrollListPtr);

void	SLDoDraw(ScrollListPtr, BOOL);
void	SLDrawBorder(ScrollListPtr);
void	SLDrawItem(ScrollListPtr, WORD);
void	SLDrawList(ScrollListPtr);

void	SLAddItem(ScrollListPtr, TextPtr, WORD, WORD);
void	SLChangeItem(ScrollListPtr, WORD, TextPtr, WORD);
void	SLRemoveItem(ScrollListPtr, WORD);
void	SLRemoveAll(ScrollListPtr);

WORD	SLNumItems(ScrollListPtr);
void	SLGetItem(ScrollListPtr, WORD, TextPtr);

WORD	SLNextSelect(ScrollListPtr, WORD);
BOOL	SLIsSelected(ScrollListPtr, WORD);
void	SLSelectItem(ScrollListPtr, WORD, BOOL);
void	SLUnSelectAll(ScrollListPtr, WORD);

void	SLSetItemStyle(ScrollListPtr, WORD, WORD);
void	SLEnableItem(ScrollListPtr, WORD, BOOL);

void	SLGadgetMessage(ScrollListPtr, MsgPortPtr, IntuiMsgPtr);
void	SLCursorKey(ScrollListPtr, TextChar);

BOOL	SLIsDoubleClick(ScrollListPtr);
WORD	SLDoubleClickItem(ScrollListPtr);

void	SLAutoScroll(ScrollListPtr, WORD);

void	SLMultiSelectEnable(ScrollListPtr, BOOL);

void	SLHorizScroll(ScrollListPtr, WORD);

void	SLSetDrawProc(ScrollListPtr, void (*)(RastPtr, TextPtr, RectPtr));

#endif
