/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Border definitions
 */

#ifndef TOOLBOX_BORDER_H
#define TOOLBOX_BORDER_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	Standard border types
 */

#define BORDER_LINE			0		/* Black line */
#define BORDER_BOX			1		/* Black box */
#define BORDER_SHADOWLINE	2		/* Shadow line */
#define BORDER_SHADOWBOX	3		/* Shadow box */

/*
 *	Prototypes
 */

BorderPtr	NewBorder(WORD, WORD, WORD, WORD);
void		FreeBorder(BorderPtr);
void		AppendBorder(BorderPtr, BorderPtr);

BorderPtr	LineBorder(WORD, WORD, WORD);
BorderPtr	ShadowLine(WORD, WORD);

BorderPtr	BoxBorder(WORD, WORD, WORD, WORD);
BorderPtr	ShadowBoxBorder(WORD, WORD, WORD, BOOL);
void		DrawShadowBox(RastPtr, WORD, WORD, WORD, WORD, WORD, BOOL);

BorderPtr	GetStdBorder(WORD, WORD, WORD);
void		DrawStdBorder(RastPtr, WORD, WORD, WORD, WORD, WORD);

#endif
