/*
 *	IFF Form HEAD structures and defines
 *	Copyright (c) 1987 New Horizons Software, Inc.
 *
 *	Permission is hereby granted to use this file in any and all
 *	applications.  Modifying the structures or defines included
 *	in this file is not permitted without written consent of
 *	New Horizons Software, Inc.
 */

#define ID_HEAD		MakeID('H','E','A','D')		/* Form type */
#define ID_WORD		MakeID('W','O','R','D')		/* For dict error checking */

#define ID_PAGE		MakeID('P','A','G','E')		/* Chunks */
#define ID_OPTS		MakeID('O','P','T','S')
#define ID_TEXT		MakeID('T','E','X','T')
#define ID_NEST		MakeID('N','E','S','T')
#define ID_HSTL		MakeID('H','S','T','L')
#define ID_HEDR		MakeID('H','E','D','R')
#define ID_FOTR		MakeID('F','O','T','R')

#define ID_DOC		MakeID('D','O','C',' ')
#define ID_PREC		MakeID('P','R','E','C')
#define ID_CLAB		MakeID('C','L','A','B')
#define ID_HINF		MakeID('H','I','N','F')

/*
 *	Special text characters for page number, date, and time
 */

#define PAGENUM_CHAR		0x80
#define DATE_CHAR		0x81
#define TIME_CHAR		0x82

/*
 *  PAGE - Page setup information
 *  This chunk can occur anywhere in the form
 *  Default values are taken from Preferences
 */
 
typedef struct {
	 UBYTE	PageWidth;		/* Width in characters */
	 UBYTE	PageHeight;		/* Height in lines */
	 UBYTE	PitchCPI;		/* Pitch in characters per inch */
	 UBYTE	SpacingLPI;		/* Spacing in lines per inch */
	 UBYTE	LeftMargin;		/* Char's from left of page (0..) */
	 UBYTE	RightMargin;		/* Char's from right of page (0..) */
	 LONG	pad;
} FlowPageInfo;

/*
 *	PREC - Print record
 *	A ProWrite print record
 *	This information supercedes the PSET info, and should come after it
 *	(The PSET chunk should be saved only for backwards compatibility)
 */

typedef struct {
	UBYTE	PrintRec[120];
} PrintRecInfo;

/*
 *  OPTS - File options
 *  This chunk can occur anywhere in the form
 *  Default value is for IndentSpace = 5
 */

typedef struct {
	 UBYTE	IndentSpace;	/* Number of spaces per indent */
	 UBYTE	LabelStyle;		/* Enumerated type, default is 0 (no label) */
	 UBYTE	LabelFlags;		/* Default is 0 */
	 UBYTE	pad1;
} FileOptions;

/*
 *  TEXT - Heading text (one block per heading)
 *  Block is actual text, no need for separate structure
 *  If the heading is empty, this is an empty chunk -- there MUST be
 *  a TEXT block for every heading
 *  Note:  There must be no control characters in this chunk
 */

/*
 *  NEST - Depth of heading nesting
 *  This chunk appears whenever the nesting level changes
 *  The initial nesting level is assumed to be zero
 */

typedef struct {
	 WORD	Nest;		/* The nesting level */
} NestLevel;

/*
 *  HSTL - Heading style
 *  This chunk appears whenever the heading style changes
 *  The style applies to the entire heading
 *  The initial style is assumed to be FS_NORMAL
 */

typedef struct {
	 UBYTE	Style;		/* Style bits */
	 UBYTE	pad;
} HeadStyle;

/*
 *  HEDR - Print header
 *  Block is actual text, no need for separate structure
 *  Note:  There must be no control characters in this chunk, currently the
 *	maximum size is 80 characters
 */

/*
 *  FOTR - Print footer
 *  Block is actual text, no need for separate structure
 *  Note:  There must be no control characters in this chunk, currently the
 *	maximum size is 80 characters
 */

/*
 *  CLAB - Custom label for document
 *  Block is actual text, no need for separate structure
 *  Note:  Maximum size is GADG_MAX_STRING
 */

/*
 *  HINF - Head Info structure
 */
 
typedef struct {
	WORD	Nest;
	UBYTE Style;
	UBYTE	Reserved1;
	UWORD Flags;
	UWORD	Reserved2;
} HeadInfo;

/*
 *	DOC  - Begin document section
 *	All text and paragraph formatting following this chunk and up to a
 *	HEAD, FOOT, or PICT chunk belong to the document section
 */

#define PAGESTYLE_1	0		/* 1, 2, 3 */
#define PAGESTYLE_I	1		/* I, II, III */
#define PAGESTYLE_i	2		/* i, ii, iii */
#define PAGESTYLE_A	3		/* A, B, C */
#define PAGESTYLE_a	4		/* a, b, c */

#define DATESTYLE_SHORT		0
#define DATESTYLE_ABBR		1
#define DATESTYLE_LONG		2
#define DATESTYLE_ABBRDAY	3
#define DATESTYLE_LONGDAY	4
#define DATESTYLE_MILITARY	5

#define TIMESTYLE_12HR		0
#define TIMESTYLE_24HR		1

typedef struct {
	UWORD	StartPage;		/* Starting page number */
	UBYTE	PageNumStyle;	/* From defines above */
	UBYTE	DateStyle;
	UBYTE	TimeStyle;
	UBYTE	TabChar;
	WORD	pad;
} DocHdr;