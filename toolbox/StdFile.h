/*
 *	Toolbox library
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Standard File Package definitions
 */

#ifndef TOOLBOX_STDFILE_H
#define TOOLBOX_STDFILE_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#ifndef TOOLBOX_DIALOG_H
#include <Toolbox/Dialog.h>
#endif

/*
 *  Reply record
 */

typedef struct {
	BYTE		Result;		/* Result of function call */
	BYTE		pad;
	Dir			DirLock;	/* Lock of parent directory of file */
	TextChar	Name[32];	/* NULL terminated file name */
} SFReply, *SFReplyPtr;

/*
 *  SFReply result codes
 */

#define SFP_OK		0
#define SFP_CANCEL	1
#define SFP_NOMEM	2

/*
 *	User items in requester must start with these items
 */

#define SFP_GET_USER	12
#define SFP_PUT_USER	13

/*
 *	Special gadget numbers
 *	If there is a dlgHook, it will be sent an INIT message after requester is up
 *	Caller's hook will be sent, and can return other messages
 *		to force specific actions:
 *			DISK	Disk inserted/removed
 *			RELIST	Force relist of directory
 *			DIRTOP	Back up to top of directory tree (to volume list)
 */

#define SFPMSG_INIT		-1
#define SFPMSG_DISK		-2
#define SFPMSG_RELIST	-3
#define SFPMSG_DIRTOP	-10

/*
 *  Prototypes
 */

void	SFPGetFile(ScreenPtr, MsgPortPtr, TextPtr,
				   BOOL (*)(IntuiMsgPtr, WORD *), WORD (*)(WORD, DialogPtr),
				   DlgTemplPtr, WORD, TextPtr *, BOOL (*)(TextPtr), SFReplyPtr);
void	SFPPutFile(ScreenPtr, MsgPortPtr, TextPtr, TextPtr,
				   BOOL (*)(IntuiMsgPtr, WORD *), WORD (*)(WORD, DialogPtr),
				   DlgTemplPtr, SFReplyPtr);
Dir		ConvertFileName(TextPtr);
void	SetCurrentDir(Dir);

void	SFPUseAslRequest(BOOL);
#endif
