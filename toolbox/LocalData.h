/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Localization information
 */

#ifndef TOOLBOX_LANGUAGE_H
#include <Toolbox/Language.h>
#endif

/*
 *	Default time type
 */

#if AMERICAN

#define TIME_12HOUR	(TRUE)

#elif (BRITISH | GERMAN | FRENCH | SWEDISH)

#define TIME_24HOUR	(TRUE)

#endif

/*
 *	Decimal and thousand separator chars
 */

#if (AMERICAN | BRITISH)

#define DECIMAL_CHAR	('.')
#define THOUSAND_SEP	(',')

#elif (GERMAN | FRENCH | SWEDISH)

#define DECIMAL_CHAR	(',')
#define THOUSAND_SEP	('.')

#endif

/*
 *	Measurement systems
 */

enum {
	MEASURE_INCH,	MEASURE_CM
};

#if (AMERICAN | BRITISH)

#define MEASURE_DEFAULT	MEASURE_INCH

#elif (GERMAN | FRENCH | SWEDISH)

#define MEASURE_DEFAULT	MEASURE_CM

#endif
