/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Mouse routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Flow.h"
#include "Proto.h"

/*
 *  External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern WORD	charHeight, charWidth;

extern HeadPtr		dragHead;
extern WORD			dragType, dragDepth;
extern BOOL			titleChanged, doubleClick, tripleClick;

/*
 *  Local variables and definitions
 */

static ULONG	prevSecs, prevMicros;		/* Inited to 0 */
static WORD		prevMouseX, prevMouseY;		/* Inited to 0 */

/*
 *	Local prototypes
 */

void	DoDragText(WindowPtr, UWORD, WORD, WORD);
void	DoDragHead(WindowPtr, UWORD, WORD, WORD);
void	DoDragMouse(WindowPtr, UWORD, WORD, WORD, ULONG, ULONG);
void	AdjustSelRange(DocDataPtr, UWORD);

void ResetMultiClick()
{
	prevSecs = prevMicros = 0;
}
/*
 *	Set selection range from given loc and dragStart
 */

void SetSelRange(DocDataPtr docData, WORD loc)
{
	if (docData->DragStart < loc) {
		docData->SelStart = docData->DragStart;
		docData->SelEnd = loc;
	}
	else {
		docData->SelStart = loc;
		docData->SelEnd = docData->DragStart;
	}
}

/*
 * Adjust selection range for double clicks, triple clicks, and modifier keys
 */
 
static void AdjustSelRange(DocDataPtr docData, UWORD modifier)
{
	register WORD textLen;
	register WORD selStart = docData->SelStart;
	register WORD selEnd = docData->SelEnd;
	register HeadPtr head = docData->SelHead;
	register TextPtr text = head->Text;
	
	if (doubleClick) {
		if( modifier & ALTKEYS ) {
			head->Flags |= HD_SELECTED;
			selStart = selEnd = docData->DragStart = 0;
		} else {
			text = head->Text;
			textLen = head->Len;
			while( selStart && wordChar[text[selStart-1]])
				selStart--;
			if( selStart < textLen && text[selStart] == QUOTE_OPEN)
				selStart++;
			
			while( selEnd < textLen && wordChar[text[selEnd]])
				selEnd++;
			if( selEnd && text[selEnd-1] == QUOTE_CLOSE)
				selEnd--;
			while( selEnd < textLen && text[selEnd] == ' ')
				selEnd++;
		}
	} else if( tripleClick ) {
		if( modifier & ALTKEYS ) {
			docData->SelHead = head = docData->FirstHead;
			while( head != NULL ) {
				head->Flags |= HD_SELECTED;
				head = head->Next;
			}
			selStart = selEnd = docData->DragStart = 0;
		} else {
			selStart = BackupSentence(text, selStart, head->BodyStart, head->Len);
			selEnd = AdvanceSentence(text, selStart, head->Len);
		}
	}
	docData->SelStart = selStart;
	docData->SelEnd = selEnd;
	return;
}

/*
 *	Handle dragging in text area of window
 */

static void DoDragText(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	register WORD loc, prevLoc, line, firstLine, headLine, prevLine;
	WORD oldStart, oldEnd, maxLoc;
	HeadPtr head;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;
	register WORD start;
	
	GetTextRect( window, &rect ) ;
/*
	Find the text location of mouse down
*/
	line = (mouseY - window->BorderTop)/charHeight + docData->TopLine;
	loc = (mouseX - window->BorderLeft - LEFT_MARGIN + (charWidth >> 1))
		  /charWidth + docData->LeftOffset;
	if (line < docData->TotalLines) {
		head = WhichHead(docData, line);
		firstLine = LineNumber(docData, head, 0);
		headLine = line - firstLine;
		loc += head->LineStarts[headLine] - HeadDepth(head)*docData->Indent;

		if( headLine == 0 ) {
			start = head->BodyStart;
		} else {
			start = 0;
			if( docData->LabelFlags & LABEL_ALIGN_MASK ) {
				loc -= head->BodyStart;
			}
		}

		if (loc < head->LineStarts[headLine] + start)
			loc = head->LineStarts[headLine] + start;
		maxLoc = (headLine < head->NumLines - 1) ?
				 head->LineStarts[headLine + 1] - 1 : head->Len;
		if (loc > maxLoc)
			loc = maxLoc;
	} else {
		line = docData->TotalLines - 1;
		head = WhichHead(docData, line);
		firstLine = LineNumber(docData, head, 0);
/*		headLine = line - firstLine;*/
		loc = head->Len;
	}	
	CursorOff(window);
	UnSelectHeads(docData);
	if (head == docData->SelHead && (modifier & SHIFTKEYS))
		SetSelRange(docData, loc);
	else {
		docData->SelHead = head;
		docData->SelStart = docData->SelEnd = docData->DragStart = loc;
	}
	AdjustSelRange(docData, modifier);	/* Adjust for double/triple clicks */

	CursorOn(window);
/*
	Track mouse dragging
*/
	prevLine = line;
	prevLoc = loc;
	dragType = DRAG_TEXT;
	while (WaitMouseUp(mainMsgPort, window)) {
/*
	Scroll window if necessary
*/
		if (window->MouseY < window->BorderTop)
			ScrollUp(window);
		else if (window->MouseY >= window->Height - window->BorderBottom)
			ScrollDown(window);
		else if (window->MouseX < window->BorderLeft)
			ScrollLeft(window);
		else if (window->MouseX >= window->Width - window->BorderRight)
			ScrollRight(window);
		else if (window->MouseX == mouseX && window->MouseY == mouseY)
			continue;
		mouseX = window->MouseX;
		mouseY = window->MouseY;
/*
	Set new selection range
*/
		line = (mouseY >= window->BorderTop) ?
				docData->TopLine + (mouseY - window->BorderTop)/charHeight :
				docData->TopLine - (window->BorderTop - mouseY)/charHeight;
		loc = (mouseX - window->BorderLeft - LEFT_MARGIN + (charWidth >> 1))
			  /charWidth + docData->LeftOffset;
		if (line < firstLine) {
			line = firstLine;
			loc = head->BodyStart;
		}
		else if (line >= firstLine + head->NumLines) {
			line = firstLine + head->NumLines - 1;
			loc = head->Len;
		}
		else {
			headLine = line - firstLine;
			if( headLine == 0 ) {
				start = head->BodyStart;
			} else {
				start = 0;
				if( docData->LabelFlags & LABEL_ALIGN_MASK ) {
					loc -= head->BodyStart;
				}
			}
			loc += head->LineStarts[headLine] - HeadDepth(head)*docData->Indent;
			if (loc < head->LineStarts[headLine] + start)
				loc = head->LineStarts[headLine] + start;
			maxLoc = (headLine < head->NumLines - 1) ?
					 head->LineStarts[headLine + 1] - 1 : head->Len;
			if (loc > maxLoc)
				loc = maxLoc;
		}
		if (loc != prevLoc || line != prevLine) {
			oldStart = docData->SelStart;
			oldEnd = docData->SelEnd;
			SetSelRange(docData, loc);
			AdjustSelRange(docData, modifier);
			CursorChangeOn(window, oldStart, oldEnd);
			prevLine = line;
			prevLoc = loc;
		}
	}
	dragType = DRAG_NONE;
}

/*
 *	Handle selection and dragging headings
 *	Only one heading can be dragged at a time
 */

static void DoDragHead(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	register WORD leftEdge, line, firstLine, lastLine, depth, expanded;
	register HeadPtr head, selHead, tempHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	selHead = docData->SelHead;
	leftEdge = window->BorderLeft + LEFT_MARGIN;
/*
	Find heading of mouse down
*/
	line = (mouseY - window->BorderTop)/charHeight + docData->TopLine;
	if (line >= docData->TotalLines) {
		ErrBeep();
		return;
	} else {
		head = WhichHead(docData, line);
		firstLine = LineNumber(docData, head, 0);
		lastLine = firstLine + head->NumLines + NumSubLines(head) - 1;
		CursorOff(window);
		docData->SelStart = docData->DragStart = docData->SelEnd = head->BodyStart;
/*
	Check for shift-select, no dragging of extended selections
*/
		if (modifier & SHIFTKEYS) {
			depth = HeadDepth(selHead);
			if (firstLine < LineNumber(docData, selHead, 0)) {
				while (head && HeadDepth(head) > depth)
					head = NextHead(head);
				while (selHead && selHead != head->Prev) {
					selHead->Flags |= HD_SELECTED;
					docData->SelHead = selHead;		/* In case Prev is NULL */
					selHead = selHead->Prev;
				}
			} else {
				while (head && HeadDepth(head) > depth)
					head = PrevHead(head);
				while (selHead && selHead != head->Next) {
					selHead->Flags |= HD_SELECTED;
					selHead = selHead->Next;
				}
			}
			CursorOn(window);
			SetEditMenu();
			SetSearchMenu();
			SetSubHeadMenu();
			SetFormatMenu();
			return; 
		}
	}
/*
	Check for expand/collapse heading
*/
	if (docData->SelHead != head)
		doubleClick = tripleClick = FALSE;
	UnSelectHeads(docData); 
	docData->SelHead = selHead = head;
	selHead->Flags |= HD_SELECTED;
	CursorOn(window);
	SetEditMenu();
	SetSearchMenu();
	SetSubHeadMenu();
	SetFormatMenu();
	if (doubleClick) {
		if (head->Flags & HD_EXPANDED)
			DoCollapse(window);
		else if (head->Sub)
			DoExpand(window);
		doubleClick = FALSE;
		tripleClick = TRUE;				/* So next click will be single click */
		return;
	}
/*
	Drag heading
*/
	dragHead = head = selHead;
	dragDepth = depth = -1;
	dragType = DRAG_HEAD;
	DragLineOn(window);

	while (WaitMouseUp(mainMsgPort, window)) {
/*
	Scroll window if necessary
*/
		if (window->MouseY < window->BorderTop)
			ScrollUp(window);
		else if (window->MouseY >= window->Height - window->BorderBottom)
			ScrollDown(window);
		else if (window->MouseX < window->BorderLeft)
			ScrollLeft(window);
		else if (window->MouseX >= window->Width - window->BorderRight)
			ScrollRight(window);
		else if (window->MouseX == mouseX && window->MouseY == mouseY)
			continue;
		mouseX = window->MouseX;
		mouseY = window->MouseY;
/*
	Find new dragHead
*/
		line = (mouseY < window->BorderTop + charHeight/2) ?
				docData->TopLine - 1 :
				(mouseY - window->BorderTop - charHeight/2)/charHeight
				+ docData->TopLine;
		if (line < 0)
			line = -1;								/* Just in case */
		if (line >= docData->TotalLines)
			line = docData->TotalLines - 1;
		if (line >= firstLine - 1 && line <= lastLine)
			line = firstLine;
		if (line < 0)
			head = NULL;
		else
			head = WhichHead(docData, line);
/*
	Find new dragDepth
*/
		depth = (mouseX - leftEdge - docData->LeftOffset
				 + (charWidth*docData->Indent)/2)/(charWidth*docData->Indent);
		if (line < 0)
			depth = 0;
		else if (line >= firstLine - 1 && line <= lastLine) {
			if (mouseX > window->BorderLeft && mouseX < leftEdge)
				depth = -1;
			else {
				expanded = (selHead->Flags & HD_EXPANDED);
				selHead->Flags &= ~HD_EXPANDED;
				if (selHead == docData->FirstHead)
					depth = 0;
				else if (depth < HeadDepth(tempHead = NextHead(selHead)))
					depth = HeadDepth(tempHead);
				else if (depth > HeadDepth(tempHead = PrevHead(selHead))) {
					depth = HeadDepth(tempHead);
					if (tempHead->Sub == NULL ||
						(tempHead->Flags & HD_EXPANDED))
						depth++;
				}
				selHead->Flags |= expanded;
			}
		}
		else {
			if (depth < HeadDepth(tempHead = NextHead(head)))
				depth = HeadDepth(tempHead);
			else if (depth > HeadDepth(head)) {
				depth = HeadDepth(head);
				if (head->Sub == NULL || (head->Flags & HD_EXPANDED))
					depth++;
			}
		}
/*
	Draw new drag line
*/
		if (head != dragHead || depth != dragDepth) {
			DragLineOff(window);
			dragHead = head;
			dragDepth = depth;
			DragLineOn(window);
		}
	}
/*
	Move heading to selected location/depth
*/
	DragLineOff(window);
	dragType = DRAG_NONE;
	if (dragHead == selHead) {
		if (dragDepth == -1 || dragDepth == HeadDepth(selHead))
			return;
		dragHead = PrevHead(selHead);
	}
	CursorOff(window);
/*
	Unlink selHead from previous location
*/
	if (selHead->Prev)
		selHead->Prev->Next = selHead->Next;
	if (selHead->Next)
		selHead->Next->Prev = selHead->Prev;
	if (selHead->Super && selHead->Super->Sub == selHead) {
		selHead->Super->Sub = selHead->Next;
		if (selHead->Next == NULL)
			selHead->Super->Flags &= ~HD_EXPANDED;
	}
	if (selHead == docData->FirstHead)
		docData->FirstHead = selHead->Next;
	BumpLevel(docData, selHead->Next, -1);

	selHead->Prev = selHead->Next = selHead->Super = NULL;
/*
	Link selHead into new location
*/
	if (dragHead == NULL) {				/* Link as first head in outline */
		selHead->Next = docData->FirstHead;
		if (docData->FirstHead)
			docData->FirstHead->Prev = selHead;
		docData->FirstHead = selHead;
	} else {
		depth = HeadDepth(dragHead);
		if (dragDepth < 0)
			dragDepth = 0;				/* Just in case */
		if (dragDepth > depth) {		/* Link as subhead to dragHead */
			if (dragHead->Sub) {
				if (dragHead->Flags & HD_EXPANDED) {
					selHead->Next = dragHead->Sub;
					dragHead->Sub->Prev = selHead;
					dragHead->Sub = selHead;
				} else {
					for (head = dragHead->Sub; head->Next; head = head->Next) ;
					head->Next = selHead;
					selHead->Prev = head;
				}
			} else
				dragHead->Sub = selHead;
			selHead->Super = dragHead;
			dragHead->Flags |= HD_EXPANDED;
		} else {							/* Link as heading after dragHead */
			while (depth > dragDepth && dragHead->Super) {
				dragHead = dragHead->Super;
				depth--;
			}
			selHead->Next = dragHead->Next;
			selHead->Prev = dragHead;
			selHead->Super = dragHead->Super;
			if (dragHead->Next)
				dragHead->Next->Prev = selHead;
			dragHead->Next = selHead;
		}
	}
	selHead->Num = selHead->Prev ? selHead->Prev->Num : 0 ;
	BumpLevel(docData, selHead, 1);
/*
	Draw new window contents
*/
	if( dragHead != NULL && dragHead->BodyStart ) {
		ReformatOutline(docData);
	}	
	DrawWindow(window);
	CursorOn(window);
	ScrollToCursor(window);
	AdjustScrollBars(window);
	DocumentModified(docData);
}


/*
 *	Handle mouse down event
 */

static void DoDragMouse(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY,
						ULONG seconds, ULONG micros)
{
	if (titleChanged)
		FixTitle();
/*
	Determine if this was a double or triple click
*/
	if (ABS(prevMouseX - mouseX) > 2 || ABS(prevMouseY - mouseY) > 2)
		tripleClick = doubleClick = FALSE;
	else if (tripleClick)
		tripleClick = doubleClick = FALSE;
	else if (doubleClick) {
		doubleClick = FALSE;
		tripleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	}
	else
		doubleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	prevSecs = seconds;
	prevMicros = micros;
	prevMouseX = mouseX;
	prevMouseY = mouseY;
/*
	Find where mouse was pressed in window
*/
	if (mouseX < window->BorderLeft + SELECT_WIDTH)
		DoDragHead(window, modifier, mouseX, mouseY);
	else
		DoDragText(window, modifier, mouseX, mouseY);
/*
	Finish up
*/
	SetPointerShape();
	SetEditMenu();
	SetSearchMenu();
	SetSubHeadMenu();
	SetFormatMenu();
}

/*
 *	Handle mouse down events
 *	Does not return until mouse button is released
 */

void DoMouseDown(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY,
				 ULONG seconds, ULONG micros)
{
	Rectangle rect;
	
	if( IsDocWindow(window)) {
		GetWindowRect(window, &rect);
		if( mouseX >= rect.MinX && mouseY >= rect.MinY &&
			mouseX <= rect.MaxX && mouseY <= rect.MaxY )
			DoDragMouse(window, modifier, mouseX, mouseY, seconds, micros);
		else if( InPageIndic(window, mouseX, mouseY))
			DoInPageIndic(window);
	}
}
