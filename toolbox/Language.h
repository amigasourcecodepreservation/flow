/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *	All rights reserved
 *
 *	Language setting
 */

#ifndef TOOLBOX_LANGUAGE_H
#define TOOLBOX_LANGUAGE_H

#define AMERICAN	1
#define BRITISH		0
#define GERMAN		0
#define FRENCH		0
#define SWEDISH		0

#endif
