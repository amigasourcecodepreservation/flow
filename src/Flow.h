/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Flow include definitions
 */



#define FLOW_VERSION 310



#ifndef DEVICES_PRINTER_H
#include <devices/printer.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#include <Toolbox/Dialog.h>

#include "Print.h"


/* 
 * Tell Flow to use system.font
 */
 
#define HIRES_FONT		1
#define BRAIN_DEAD_KEYS_GONE 1

typedef UWORD		RGBColor;

typedef RGBColor	ColorTable[];
typedef ColorTable	*ColorTablePtr;

/*
 *	Misc items
 */

#define PGINDIC_WIDTH	(9*8 + 2)

#define MAX_WINDOWS		10		/* A nicer number */
#define MAX_DEPTH		20
#define MAX_TEXTLEN		0x7FFF
#define MAX_HEADER_LEN  144
#define MAX_FOOTER_LEN  MAX_HEADER_LEN
#define MAX_FILENAME_LEN 31
#define MAX_TABS		10
#define MAX_LABEL_LEN	100
#define MAX_WORD_LEN		50		/* For spell-checking */

#define FILEBUFF_SIZE	512

#define DECIPOINTS_TO_UNITS(a)	(((LONG)(a)<<4)/720)
#define UNITS_TO_DECIPOINTS(a)	(((LONG)(a)*720)>>4)

#define DOTS_TO_UNITS(a,dpi)		(((LONG)(a)<<4)/(dpi))
#define UNITS_TO_DOTS(a,dpi)		(((LONG)(a)*(dpi))>>4)

#define DOTS_TO_DECIPOINTS(a,dpi) (((LONG)(a)*720)/(dpi))
#define DECIPOINTS_TO_DOTS(a,dpi) (((LONG)(a)*(dpi))/720)

/*
 *	Window item widths
 */

#define SELECT_WIDTH	24
#define LEFT_MARGIN		(SELECT_WIDTH+2)

#define PAGE_GAP	0
#define PAGE_SEPARATION(docData) ((docData->PrintRec->PaperRect.MaxY - docData->PrintRec->PaperRect.MinY))

#define LABEL_FORMAT_MASK  128
#define LABEL_APPEND_MASK  64
#define LABEL_ALIGN_MASK	32
#define LABEL_CHANGED_MASK 1

#define FIND_TEXT			1
#define WHOLEWORD_BOX		2
#define MATCHCASE_BOX		3
#define OPENONLY_BOX			4
#define CHANGE_TEXT			5
#define CHANGEFIND_BUTTON  6
#define CHANGE_BUTTON		7
#define CHANGEALL_BUTTON	8

/*
 *	Icon numbers
 */

enum {
	ICON_DOC,	ICON_TEXT,	ICON_PREFS,	ICON_DICT, ICON_FKEYS
};

/*
 * Types of measurements used in requesters
 */

enum {							/* Column type */
	COLM_SNAKING,	  COLM_SIDEBYSIDE
};

/*
 *	Window gadget definitions
 */

enum {
	UP_ARROW,		DOWN_ARROW,		LEFT_ARROW,		RIGHT_ARROW,
	VERT_SCROLL,	HORIZ_SCROLL,	PAGEUP_ARROW,	PAGEDOWN_ARROW
};

/*
 * Dialog ID's
 */

enum {
	DLG_INIT,	
	DLG_OPENFILE,	DLG_SAVEAS,
	DLG_PREFERENCES,DLG_MACRONAME,
	DLG_ERROR,		DLG_ABOUT,			DLG_MEMORY,
	DLG_LAYOUT,		DLG_PAGESETUP,		DLG_PRINT,
	DLG_SAVECHANGES,  DLG_REVERT,		DLG_SAVEPREFS,		DLG_POSTSCRIPT,
	DLG_CANCELPRINT,  DLG_NEXTPAGE,
	DLG_FIND,		DLG_CHANGE,			DLG_FINDCONTINUE,	DLG_GOTO,
	DLG_INSERT,		DLG_LABEL,			DLG_CHECKSPELL,		DLG_ITEMFORMATS,
	DLG_SCREENCOLORS, DLG_STOPCHANGE,	DLG_SAVEDICT,		DLG_SPELLCHANGE,
	DLG_WRAPSPELL,	DLG_DOCINFO,		DLG_COLORS,			DLG_CUSTOMMACRO,
	DLG_USERREQ1,	DLG_USERREQ2,		DLG_USERREQ3,		DLG_USERREQTEXT
};

/*
 *	Color register numbers for possible colors
 *	Requirements are for:
 *		Blue is color 0
 *		White is color 1
 *		Black is inverse of white (ie color 6)
 *		Red is inverse of blue (ie color 7)
 */

enum {
	COLOR_BLUE,		COLOR_WHITE,		COLOR_GREEN,	COLOR_CYAN,
	COLOR_DKRED,	COLOR_DKYELLOW,	COLOR_DKGREEN,	COLOR_DKCYAN,
	COLOR_DKBLUE,	COLOR_DKMAGENTA,	COLOR_LTGRAY,	COLOR_DKGRAY,
	COLOR_YELLOW,	COLOR_MAGENTA,		COLOR_BLACK,	COLOR_RED
};

/*
 *	Types of dragging operations possible
 */

enum {
	DRAG_NONE,	DRAG_TEXT,	DRAG_HEAD
};

/*
 *	Error numbers
 */

enum {
	INIT_BADSYS,	INIT_NOICON,	INIT_NOFONT,	INIT_NOSCREEN, 
	INIT_NOMEM
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,			/* Not Enough Memory */
	ERR_OPEN,			/* Unable to load file */
	ERR_SAVE,			/* Unable to save file */
	ERR_SAVE_NO_MEM,  	/* Unable to put up "Save As" requester */
	ERR_PRINT,  		/* Unable to print file */
	ERR_BAD_COPIES,		/* Invalid number of copies to print */
	ERR_NO_PAGE,		/* No such page */
	ERR_PAGE_NUM,		/* Invalid page number */
	ERR_BAD_TEXT,		/* Bad text for find */
	ERR_NO_FIND,		/* Unable to find heading */
	ERR_BAD_MARGIN,		/* New ERR codes beginning with V3.0 -GM */
	ERR_NO_CHANGE,
	ERR_PAGE_SIZE,
	ERR_HEIGHT_NUM,
	ERR_NO_REXX,
	ERR_BAD_MACRO,
	ERR_MACRO_FAIL,
	ERR_SAVE_DICT,
	ERR_NO_DICT,
	ERR_BAD_DICT,
	ERR_NO_CLIP,
	ERR_NO_PARSE	/* !! IF YOU ADD TO THE BOTTOM OF THIS TABLE, UPDATE BELOW!! */
};

#define ERR_MAX_ERROR	ERR_NO_PARSE

/*
 *	Menu and menu item numbers
 */

enum {
	PROJECT_MENU,
	EDIT_MENU,
	SEARCH_MENU,
	HEADING_MENU,
	FORMAT_MENU,
	VIEW_MENU,
	MACRO_MENU
};

enum {						/* Project menu */
	NEW_ITEM,
	OPEN_ITEM,
	CLOSE_ITEM,
	SAVE_ITEM = 4,
	SAVEAS_ITEM,
	REVERT_ITEM,
	PAGESETUP_ITEM = 8,
	PRINTONE_ITEM,
	PRINT_ITEM,
	SAVEPREFS_ITEM = 12,
	QUIT_ITEM = 14
};

enum {						/* Edit menu */
	CUT_ITEM,
	COPY_ITEM,
	PASTE_ITEM,
	ERASE_ITEM,
	CHANGECASE_ITEM = 5,
	INSERT_ITEM,
	ITEMFORMATS_ITEM,
	SELECTALL_ITEM = 9,
	PREFERENCES_ITEM = 11,
	SCREENCOLORS_ITEM
};

enum {						/* Search menu */
	FIND_ITEM,
	FINDNEXT_ITEM,
	CHANGE_ITEM,
	GOTOPAGE_ITEM = 4,
	GOTOSELECTION_ITEM,
	SETMARK_ITEM = 7,
	GOTOMARK_ITEM,
	SPELLING_ITEM = 10
};

enum {						/* Heading menu */
	COLLAPSE_ITEM,
	EXPAND_ITEM,
	EXPANDALL_ITEM,
	INDENT_ITEM = 4,
	UNINDENT_ITEM,
	MOVEUP_ITEM,
	MOVEDOWN_ITEM,
	SPLIT_ITEM = 9,
	JOIN_ITEM,
	SORT_ITEM = 12
};

enum {						/* Format menu */
	PLAIN_ITEM,
	BOLD_ITEM,
	ITALIC_ITEM,
	UNDERLINE_ITEM,
	PAGEBREAK_ITEM = 5,
	LAYOUT_ITEM = 7,
	LABEL_ITEM,
	COLORS_ITEM,
	DOCUMENTINFO_ITEM = 11
};

enum {						/* View menu */
	ABOUT_ITEM = 0,
	WINDOW_ITEM = 2			/* First window in list */
};

enum {						/* Macro menu */
	OTHERMACRO_ITEM 	= 11,
	CUSTOMMACRO_ITEM	= 13
};

enum {						/* Defaults submenu */
	SAVEDEFAULTS_SUBITEM	= 0,
	SAVEASDEFAULTS_SUBITEM,
	LOADDEFAULTS_SUBITEM	= 3
};

enum {						/* Change case submenu */
	UPPERCASE_SUBITEM	= 0,
	LOWERCASE_SUBITEM,
	MIXEDCASE_SUBITEM
};

enum {
	DATE_SUBITEM = 0,
	TIME_SUBITEM,
	LITERAL_SUBITEM,
	SPACE_SUBITEM
};

enum {
	CHECK_SUBITEM = 0,
	CHANGEDICT_SUBITEM = 2
};

enum {				/* Sort submenu */
	ATOZ_SUBITEM,
	ZTOA_SUBITEM,
	RANDOM_SUBITEM
};

/*
 * Heading record
 */

#define HD_SELECTED	0x0001
#define HD_EXPANDED	0x0002
#define HD_PAGEBREAK 0x0004		/* Page break before this head */
#define HD_SPELL	0x0010		/* Head has been spell checked */

#define TEXT_CHUNK	8				/* Chunk size for text buffers */

#define PASTE_NONE	0x0000
#define PASTE_TEXT	0x0001
#define PASTE_HEAD	0x0002

typedef struct Head {
	struct Head	*Next, *Prev, *Super, *Sub;
	UWORD		Flags;
	TextPtr		Text;
	WORD		Len;
	WORD		BodyStart;				/* Beginning of non-header text - new for V3.0 */
	WORD		Num;					/* Number in heading label in this depth */
	UBYTE		Style, NumLines;
	WORD		LineStarts[1];			/* Extend for more lines */
} Head, *HeadPtr;

typedef struct {
	HeadPtr	Head;
	WORD		Loc;
} TextLoc, *TextLocPtr;

/*
 *	Document data record
 */

#define WD_MODIFIED		0x0001	/* Data has been modified */
#define WD_SHOWHEAD		0x0002	/* Show header */
#define WD_SHOWFOOT		0x0004	/* Show footer */
#define WD_DOUBLESIDED	0x0008	/* Separate odd/even headers & footers */
#define WD_TITLEPAGE 	0x0010	/* No headers/footers on first page */
#define WD_SHOWINVIS		0x0020	/* Show invisibles */
#define WD_SHOWPICTS		0x0040	/* Show pictures */
#define WD_SHOWRULER		0x0080	/* Show ruler */
#define WD_SHOWRICON		0x0100	/* Show ruler icons */
#define DD_SHOWGUIDES	0x0200	/* Show page guides */
#define WD_BUSY		0x8000	/* Busy modifying docData, don't use docData */
#define WD_NAMED		0x4000	/* Moved from 0x02, means it has a filename */

typedef struct {
	WindowPtr	Window;
	WORD		Flags;
	HeadPtr		FirstHead, SelHead;
	WORD		SelStart, SelEnd; /* Insertion point location */
	WORD		DragStart;			/* Location of start of mouse drag */
	WORD		TopLine, TotalLines;
	WORD		PageOffset;			/* Top page in window (0=first page) */
 	WORD		LeftOffset;			/* Character count of left offset */
	WORD		Pages;				/* Current page count */
	WORD		PageIndic;			/* Current page indicator (-1=header, -2=footer) */

	WORD		Indent;				/* Indent spaces */
	PrintRecPtr PrintRec;		/* Print Record-new for V3.0 */

	UWORD		StartPage;				/* Starting page number */
	UBYTE		PageWidth;			/* Page width in characters */
	UBYTE		PageHeight;			/* Page height in lines */
	UBYTE		LeftMargin, RightMargin;	/* Margins in characters */
	UBYTE		CPI;				/* Characters per inch horizontally */
	UBYTE		LPI;				/* Lines per inch vertically */
	UBYTE		Header[MAX_HEADER_LEN + 1], Footer[MAX_FOOTER_LEN + 1];
	
	WORD		MaxXDots, MaxYDots;	  /* Paper dimensions */
	BOOL		CursorOn;			/* Current cursor state */
	WORD		BlinkCount;			/* Countdown timer for cursor blink */
	
	TextChar	FileName[32];		/* Name of file */
	Dir			DirLock;				/* Lock of file directory */
	
	UBYTE		LabelStyle;			/* Auto-number heading style */
	UBYTE		LabelFlags;			/* Auto-number heading flags - format & append */
	UWORD		LabelRepeat;		/* What level semi-colon appears in custom label */
	UWORD		LabelRange;			/* Amount of levels between semi-colon and end */
	UBYTE		CustomLabel[MAX_LABEL_LEN+1];
	
	UBYTE		PageNumStyle;
	UBYTE		DateStyle;
	UBYTE		TimeStyle;
	
	UBYTE		PenColor;
	UBYTE		PaperColor;
	
	ULONG		LastSaveTime;
	TextLoc		Mark;				/* for set/goto mark */
} DocData, *DocDataPtr;

/*
 *	First long word of special files identifies file type
 */

#define ID_PREFSFILE		0x06660137L
#define ID_DICTFILE		0x06660138L
#define ID_THESFILE		0x06660139L
#define ID_FKEYSFILE	0x0666013AL

/*
 *	Misc definitions
 */

#define QUOTE			'\''
#define QUOTE_OPEN		QUOTE
#define QUOTE_CLOSE		QUOTE
#define DQUOTE			'\"'
#define DQUOTE_OPEN		DQUOTE
#define DQUOTE_CLOSE	DQUOTE

#define BS		0x08
#define TAB		'\t'
#define LF		'\n'
#define CR		0x0D
#define DEL		0x7F
#define NBSP	0xA0		/* Non-breaking space */
#define SHYPH	0xAD		/* Soft hyphen */

#define BLINK_FAST		3
#define BLINK_SLOW		6
#define BLINK_OFF		0

#define SHIFTKEYS		(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS			(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY			AMIGARIGHT

#define MODIFIER_WORD	SHIFTKEYS
#define MODIFIER_SENT	ALTKEYS
#define MODIFIER_SEL	IEQUALIFIER_CONTROL

#define LOWER_ROMAN_CHAR	'i'
#define UPPER_ROMAN_CHAR	'I'

#define LOWER_CASE_CHAR		'a'
#define UPPER_CASE_CHAR		'A'

#define LOWER_NUMERIC_CHAR 'n'
#define UPPER_NUMERIC_CHAR 'N'

#define LOWER_KILL_CHAR		'x'
#define UPPER_KILL_CHAR		'X'

/*
 *	Useful macros
 */

#define IS_UPPER(ch)	((ch) != toLower[(ch)])	/* Only alphabetics can be upper */

#define WINDOW_LINES(w)	\
	((w->Height-w->BorderTop-w->BorderBottom)/charHeight)
#define WINDOW_COLUMNS(w)	\
	((w->Width-w->BorderLeft-w->BorderRight-LEFT_MARGIN)/charWidth)

#define MENUBUTTON(q)	\
	((q&IEQUALIFIER_RBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_RALT)))
#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))
#define ABS(x)		((x)<0?(-(x)):(x))

#define MENUITEM(menu, item, sub)	\
	(SHIFTMENU(menu) | SHIFTITEM(item) | SHIFTSUB(sub))

/*
 *	Function key definition
 */

#define FKEY_MENU	0
#define FKEY_MACRO	1

typedef struct {
	WORD  	Type;		/* Menu equiv. or macro */
	Ptr		Data;		/* Menu item number or pointer to macro name */
} FKey;

#define NUM_STDCOLORS	4

/*
 *	Global user options
 */

#define BLINK_FAST	3
#define BLINK_SLOW	6
#define BLINK_OFF	0

typedef struct {
	UBYTE	MeasureUnit;	/* Inches or centimeters */
	UBYTE	ShowAllFiles;	/* Show all files */
	UBYTE	SaveIcons;		/* Save icons with files */
	UBYTE	BlinkPeriod;	/* Insertion point blinks 5/(n+1) per sec, 0 = off */
	UBYTE	BeepFlash;		/* Sound and/or flash screen on errors */
	UBYTE	BeepSound;
	UBYTE	Typeover;		/* Typeover mode */
	UBYTE	AutoSpell;		/* Auto spell check */
	UBYTE	SpeakType;		/* Speak while typing */
	UBYTE	MakeBackups;	/* Make backups when saving */
	UBYTE	AutoSave;		/* Auto-save */
	UBYTE	AutoSaveTime;	/* Minutes between auto-saves */
	UBYTE	FullClipboard;	/* clipboard support with CAT chunk */
	UBYTE	pad[100];
} Options;

/*
 *	Default values
 */

typedef struct {
	PrintRecord	PrintRec;
	UWORD		DocFlags;
	UBYTE		LabelStyle;
	UBYTE		LabelFlags;
	UWORD		LabelRepeat;
	UWORD		LabelRange;
	UBYTE		CustomLabel[MAX_LABEL_LEN+2];
	UBYTE		PenColor;
	UBYTE		PaperColor;
	UWORD		CPI;
	UWORD		LeftMargin,RightMargin;
	UWORD		Indent;
	UBYTE		PageNumStyle;
	UBYTE		DateStyle;
	UBYTE		TimeStyle;
	UBYTE		pad[100];
} Defaults;
