/*
 * Flow
 * Copyright (c) 1989 New Horizons Software
 *
 * Print routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <graphics/gfxbase.h>
#include <devices/printer.h>
#include <devices/prtbase.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Window.h>

#include "Flow.h"
#include "HEAD.h"		/* For TIME and DATE defines */
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr mainMsgPort;

extern BOOL graphicPrinter, colorPrinter, pagePrinter;

extern UBYTE	printerName[];

extern DlgTemplPtr dlgList[];

extern ScreenPtr screen;

extern TextChar strBuff[];

extern TextChar strPrtSetup[], strPrtPrint[], strPrtRender[], strPrtCancel[];

extern WORD		charHeight, charWidth, charBaseline;

extern PostScriptOptions postScriptOptions;

/*
 * Local prototypes
 */
 
PrintIOPtr	OpenPrinter(void);
void		ClosePrinter(PrintIOPtr);
void		SetCancelButton(BOOL);
BOOL		SetStyle(File, WORD);
WORD		PrintPage(File, DocDataPtr, WORD);
BOOL		NextPage(void);
BOOL		InitCharPrint(DocDataPtr, File);
BOOL		PrintHdrFtr(File, TextPtr, DocDataPtr, WORD,WORD,WORD);

WORD		PrintPSPage(PrintRecPtr, DocDataPtr, WORD);
WORD		PrintPostScript(WindowPtr, DocDataPtr);

void		SetInfoText(TextPtr);
void 		StartPSPage(PrintRecPtr,WORD *,WORD *,BOOL);
void		NextPSPage(PrintRecPtr,WORD *,WORD *);
/*
 * Local variables
 */

static struct Preferences  origPrefs;  /* For OpenPrinter() call */
static struct Preferences  prefs;		/* For GetSysPrefs() call */
  
static BOOL havePrinterType;	  		/* To avoid excessive calls to GetPrinterType() */

static DialogPtr cancelDialog = NULL;

static TextChar pageStyles[] = { LOWER_NUMERIC_CHAR, UPPER_ROMAN_CHAR,
		LOWER_ROMAN_CHAR, UPPER_CASE_CHAR, LOWER_CASE_CHAR };

#define INFO_TEXT		2

#define PAGECOUNT_CHAR '\%'

#define PRINT_OK		0
#define PRINT_ABORT		1
#define PRINT_ERROR		-1

#define PITCH_PICA		100
#define PITCH_PICA		100
#define PITCH_ELITE		120
#define PITCH_ELITEWIDE	60
#define PITCH_COND		150
#define PITCH_CONDWIDE	75
#define PITCH_ULTRA		172
#define PITCH_ULTRAWIDE	86

#define PITCH_TO_DECIPOINTS(pitch)	((7200 + (pitch - 1))/(pitch))	/* Must round up */

/*
 *	Get new preferences values
 *	Return TRUE if preferences changed since last call
 */

BOOL GetSysPrefs()
{
	register WORD i, len;
	struct Preferences newPrefs;

/*
	Check for changed preferences
*/
	GetPrefs(&newPrefs, sizeof(struct Preferences));
	if (memcmp((BYTE *) &newPrefs, (BYTE *) &prefs, sizeof(struct Preferences)) == 0)
		return (FALSE);
	BlockMove(&newPrefs, &prefs, sizeof(struct Preferences));
/*
	Get printer name here
	All other printer defaults are caught by PrintDefault()
*/
	*printerName = '\"';
	len = strlen(prefs.PrinterFilename);
	for (i = len; i; i--) {
		if (prefs.PrinterFilename[i - 1] == ':' ||
			prefs.PrinterFilename[i - 1] == '/')
			break;
	}
	len -= i;
	memcpy(printerName + 1, &prefs.PrinterFilename[i], len);
	printerName[len + 1] = '\"';
	printerName[len + 2] = '\0';
	for (i = 1; i < len + 1; i++) {
		if (printerName[i] == '_')
			printerName[i] = ' ';
	}
	if (printerName[1] >= 'a' && printerName[1] <= 'z')
		printerName[1] -= 'a' - 'A';
	havePrinterType = FALSE;
	return (TRUE);
}

/*
 * Request the next sheet of paper
 * Return success status
 */

static BOOL NextPage()
{
	WORD item;
	
	SetCancelButton(FALSE);	
	item = StdDialog(DLG_NEXTPAGE);
	SetCancelButton(TRUE);
	return( (BOOL) (item == OK_BUTTON) ) ;
}

/*
 * Advance the specified number of lines
 * Return success status
 */

static BOOL AdvanceLines(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, "\n", 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Print the specified number of spaces
 * Return success status
 */

static BOOL PrintSpaces(File printer, WORD num)
{
	if (num > 0) {
		while (num--) {
			if (Write(printer, " ", 1) != 1)
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 * Set print style to specified style
 * Return success status
 */

static BOOL SetStyle(File printer, WORD style)
{
	if ((style & FSF_BOLD) && Write(printer, "\033[1m", 4) != 4)
		return (FALSE);
	if ((style & FSF_ITALIC) && Write(printer, "\033[3m", 4) != 4)
		return (FALSE);
	if ((style & FSF_UNDERLINED) && Write(printer, "\033[4m", 4) != 4)
		return (FALSE);
	return (TRUE);
}

/*
 * Print one page of text
 * Assume we start one line down from the top
 * Return -1(PRINT_ERROR) if error, 1(PRINT_ABORT) if cancelled, 0 if OK
 */

static WORD PrintPage(File printer, DocDataPtr docData, WORD page)
{
	register TextPtr text;
	register WORD line, headLine, pageLine, spaces, len;
	register HeadPtr head;
	HeadPtr topHead;
	UWORD topLine, index;
	BOOL drawFooter;

/*
	Print header
*/
	SetPageIndic(docData->Window, page+1);
	if ((len = strlen(docData->Header)) != 0) {

		if (!PrintHdrFtr(printer, docData->Header, docData, page+1,0,0) )		
			return(PRINT_ERROR);
		if( !AdvanceLines(printer, 1))
			return(PRINT_ERROR);
			
	}
	drawFooter = FALSE;
/*
	Print page body. First, find the first line on this page.
*/
	topHead = head = FindFirstPageLine(docData, page, &index, &topLine);

	line = topLine;
	
	for (pageLine = 0; pageLine < docData->PageHeight && head ; pageLine++ ) {
		if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON)	{
			SetCancelButton(FALSE);
			SetInfoText(strPrtCancel);
			return(PRINT_ABORT);
		}

		headLine = line - LineNumber(docData, head, 0);
		spaces = docData->LeftMargin + docData->Indent*HeadDepth(head);
/*
	The only knowledge print code needs of labeling is here.
	Indents secondary lines if necessary.
*/
		if( ( docData->LabelFlags & LABEL_ALIGN_MASK ) && headLine )
			spaces += head->BodyStart;
		
		text = head->Text + head->LineStarts[headLine];
		len = (headLine < head->NumLines - 1) ? head->LineStarts[headLine + 1] : head->Len;
		len -= head->LineStarts[headLine];
		while (len > 0 && text[len - 1] <= ' ')
			len--;
		if (!PrintSpaces(printer, spaces) ||
			(head->Style != FS_NORMAL && !SetStyle(printer, (WORD) head->Style)) ||
			Write(printer, text, len) != len ||
			(head->Style != FS_NORMAL && Write(printer, "\033[0m", 4) != 4) ||
			!AdvanceLines(printer, 1))
			return(PRINT_ERROR);
		line++;
		head = WhichHead(docData, line);
		if( ( head != NULL ) && ( head != topHead ) && ( head->Flags & HD_PAGEBREAK ) ) {
			drawFooter = TRUE;			/* Page break done by AdvanceLines() below */
			head = NULL;					/* Cause termination condition */
		}
	}
/*
	Print footer
*/
	len = strlen(docData->Footer);
	if (drawFooter || len ) {
		if( !AdvanceLines(printer, (WORD) (docData->PageHeight - pageLine)))
			return( PRINT_ERROR);
		if( len ) {
			if( !PrintHdrFtr(printer, docData->Footer, docData, page+1,0,0))
				return(PRINT_ERROR);
		}	
	}
	return(PRINT_OK);
}

/*
	Perform parsing of header of footer text and write line to printer
*/

static BOOL PrintHdrFtr(File printer, register TextPtr srcPtr, DocDataPtr docData, WORD page,
							WORD penX, WORD penY)
{
	TextChar buff[MAX_HEADER_LEN+MAX_FILENAME_LEN+1];
	TextChar line[MAX_HEADER_LEN];
	TextChar tempStr[MAX_FILENAME_LEN+1];
	register TextPtr dstPtr = buff;
	register WORD len;
	struct DateStamp dateStamp;
	register WORD i;
	TextChar ch;
	register TextPtr firstTab = NULL;
	TextPtr secondTab = NULL;
	WORD	width;
	
	width = docData->PageWidth - docData->RightMargin - docData->CPI/4 - docData->LeftMargin;
		
/*
	First byte of srcPtr must not be zero
*/
	while( (dstPtr < &buff[MAX_HEADER_LEN]) && (ch = *srcPtr++) ) {
		switch(ch) {
		case '^':
			*((WORD *)tempStr) = 0;
			switch(*srcPtr){
			case '#':
				AppendNumberStyle( page, tempStr, pageStyles[docData->PageNumStyle]);
				break;
			case '%':
				AppendNumberStyle( docData->Pages, tempStr, pageStyles[docData->PageNumStyle]);
				break;
			case '&':
				GetWTitle(docData->Window, tempStr);
				break;
			case '@':
				DateStamp(&dateStamp);
				TimeString(&dateStamp, FALSE, (BOOL) docData->TimeStyle == TIMESTYLE_12HR, tempStr);
				break;
			case '!':
				DateStamp(&dateStamp);
				DateString(&dateStamp, (WORD) docData->DateStyle, tempStr);
				break;
			case 't':
				if( firstTab ) {
					if( secondTab == NULL )
						secondTab = dstPtr;
				} else {
					firstTab = dstPtr;
				}
				break;
			default:
				tempStr[0] = *srcPtr;
				break;
			}
			strcpy(dstPtr, tempStr);
			dstPtr += strlen(tempStr);
		 	srcPtr++;
			break;
		default:
			*dstPtr++ = ch;
			break;
		}
	}
/*
	Second pass - Parse tabs to construct final output "line".
	First, copy the left justified text and zap the rest with spaces.
*/
	len = (firstTab ? firstTab : dstPtr) - buff;
	BlockMove( buff, line, len);
	for( i = len ; i < width ; i++ ) {
		line[i] = ' ';
	}
/*
	Next, copy the center justified text into the line.
*/
	if( firstTab ) {
		len = (secondTab ? secondTab : dstPtr) - firstTab;
		i = (width - len) >> 1;
		BlockMove(firstTab, &line[i], len);
		len += i;
	}	
/*
	Now copy the right justfied text
*/			
	if( secondTab ) {
		len = MIN(width, dstPtr - secondTab);
		BlockMove(secondTab, &line[width - len], len);
		len = width;
	}	
	if (printer != NULL)	{
		if (Write(printer, line, len) == len )
			return (AdvanceLines(printer,1));
		else
			return (PRINT_ERROR);
	}
	else	{
		return ( PSText(docData->PrintRec, 
					penX, penY,docData->CPI, FS_NORMAL, FS_NORMAL, line, len,docData->LeftMargin));
	}
}

/*
 * Print outline
 */

BOOL PrintOutline(WindowPtr window,DocDataPtr docData)
{
	register WORD page;
	register WORD i;
	register BOOL success, abort;
	register File printer;
	register PrintRecPtr printRec = docData->PrintRec;
	WORD status;
	WORD copies = printRec->Copies;
	WORD startPage = printRec->FirstPage-1;
	WORD lastPage = printRec->LastPage-1;
	register WORD numPages;
	WORD direction, temp, iterations;
	WORD oldPage;
	
	BeginWait();
	if ((cancelDialog = GetDialog(dlgList[DLG_CANCELPRINT], screen, mainMsgPort)) == NULL) {
		EndWait();
		return (FALSE);
	}
	SetCancelButton(FALSE);
	SetInfoText(strPrtSetup);
	RefreshWindows();
	success = TRUE;
	
	if( lastPage > startPage + (docData->Pages-1) )
		lastPage = startPage + (docData->Pages-1);

	if( (startPage >= 0) && (lastPage >= startPage) ) {	
		
		if (printRec->Quality == PRT_POSTSCRIPT)	{
			 if ((status = PrintPostScript(window,docData)) == PRINT_ERROR)
			 	success = FALSE;
		}
		else {
/*
	Open the printer device for output
	Set for no margins and proper setup
*/
			if( (printer = Open(printRec->FileName, MODE_NEWFILE)) != NULL ) {
				if( InitCharPrint(docData, printer) ) {
/*
	Print the pages
*/
					abort = FALSE;
					success = TRUE;
					direction = 1;
					numPages = (1+lastPage - startPage) * copies;
					if( printRec->Flags & PRT_BACKTOFRONT ) {
						direction = -1;
						temp = startPage;
						startPage = lastPage;
						lastPage = temp;
					}
					lastPage += direction;
					SetCancelButton(TRUE);
					SetInfoText(strPrtPrint);
					oldPage = docData->PageIndic;
					while( (!abort) && (numPages > 0) ) {
						for (page = startPage; (!abort) && (page != lastPage); page += direction ) {
							iterations = (printRec->Flags & PRT_COLLATE) ? copies : 1;
							for( i = 0 ; (!abort) && (i < iterations) ; i++ ) {
								if( (!(printRec->Flags & PRT_TOFILE)) && printRec->PaperFeed == PRT_CUTSHEET && !NextPage()) {
									abort = TRUE;
								} else {
									--numPages;
									status = PrintPage(printer, docData, page);
									abort = status != PRINT_OK;
									if( status == PRINT_ERROR || Write(printer, "\f", 1) != 1) {
										success = FALSE;
									}
								}
							}
						}	  
					}
					SetPageIndic(docData->Window, oldPage);
				}
				Close(printer);
			}
		}
	}
	if( cancelDialog != NULL ) {
		DisposeDialog(cancelDialog);
		cancelDialog = NULL;
	}
	EndWait();

	return (success);
}

static BOOL InitCharPrint( register DocDataPtr docData, File printer )
{
	register BOOL success = FALSE;
	PrintRecPtr printRec = docData->PrintRec;
	register ULONG height;
	TextChar command[10];
	register WORD len;
			
	if (Write(printer, "\033#3", 3) == 3) { 
		if( !((printRec->Quality == PRT_NLQ &&
			Write(printer, "\033[2\"z", 5) != 5) ||
							(printRec->Quality == PRT_DRAFT &&
							Write(printer, "\033[1\"z", 5) != 5) ) ) {
			if(!((docData->CPI == 10 && Write(printer, "\033[0w", 4) != 4) ||
					 (docData->CPI == 12 && Write(printer, "\033[2w", 4) != 4) ||
					 (docData->CPI == 17 && Write(printer, "\033[4w", 4) != 4))){
				if(!((docData->LPI == 6 && Write(printer, "\033[1z", 4) != 4) ||
					  (docData->LPI == 8 && Write(printer, "\033[0z", 4) != 4))){

/*
	Set form height and turn off perf skip
*/
					success = TRUE;
					if( !pagePrinter ) {
						height = ((LONG) printRec->PaperHeight*6L)/720L;
						if( height > 255 )
							height = 255;
						strcpy(command, "\033[");
						NumToString(height, strBuff);
						strcat(command, strBuff);
						strcat(command, "t");
						len = strlen(command);
						if( ( Write(printer, command, len) != len ) ||
								( Write( printer, "\033[0q", 4 ) != 4 ) ) /* No perf skip */
							success = FALSE;
					}
				}
			}
		}
	}
	return(success);
}
								 
/*
 *	Enable/disable cancel button and set pointer
 */

static void SetCancelButton(BOOL on)
{
	EnableGadgetItem(cancelDialog->FirstGadget, CANCEL_BUTTON, cancelDialog, NULL, on);
	SetStdPointer(cancelDialog, (WORD) (on ? POINTER_ARROW : POINTER_WAIT));
}

/*
 *	Get the printer type
 */

void GetPrinterType()
{
	register WORD i;
	register PrintIOPtr printIO;
	register struct PrinterData *pData;
	register struct PrinterExtendedData *ped;

	if (havePrinterType)
		return;
/*
	Set default values
*/
	graphicPrinter = colorPrinter = pagePrinter = FALSE;
/*
	Get printer values
*/
	if ((printIO = OpenPrinter()) == NULL) {
		BeginWait();
		for (i = 0; i < 10; i++) {				/* Retry counter */
			if ((printIO = OpenPrinter()) != NULL)
				break;
			Delay(10L);
		}
		EndWait();
	}
	if (printIO == NULL)
		return;
	pData = (struct PrinterData *) printIO->Std.io_Device;
	if (pData->pd_SegmentData) {
		ped = &pData->pd_SegmentData->ps_PED;
		graphicPrinter = ((ped->ped_PrinterClass & PPCF_GFX) != 0 &&
						  ped->ped_XDotsInch != 0);
		colorPrinter = ((ped->ped_PrinterClass & PPCF_COLOR) != 0);
		pagePrinter = (ped->ped_MaxYDots != 0);
	}
	ClosePrinter(printIO);
	havePrinterType = TRUE;
}

/*
 *	Open the printer for direct output
 */

static PrintIOPtr OpenPrinter()
{
	register PrintIOPtr printIO;
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	if ((replyPort = CreatePort(NULL, 0)) == NULL)
		return (NULL);
	if ((printIO = (PrintIOPtr) CreateExtIO(replyPort, sizeof(PrintIO))) == NULL) {
		DeletePort(replyPort);
		return (NULL);
	}
	if (OpenDevice("printer.device", 0, (IOReqPtr) printIO, 0)) {
		DeleteExtIO((IOReqPtr) printIO);
		DeletePort(replyPort);
		return (NULL);
	}
	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&pData->pd_Preferences, &origPrefs, sizeof(struct Preferences));

	pData->pd_Preferences.PrintImage = IMAGE_POSITIVE;
	if (pData->pd_Preferences.PrintShade == SHADE_BW)
		pData->pd_Preferences.PrintShade = SHADE_GREYSCALE;
	pData->pd_Preferences.PrintLeftMargin = 1;
	pData->pd_Preferences.PrintRightMargin = 255;
	pData->pd_Preferences.PaperLength = 999;
	pData->pd_Preferences.PrintXOffset = 0;
	pData->pd_Preferences.PrintFlags &= ~(CORRECT_RGB_MASK | CENTER_IMAGE |
										  INTEGER_SCALING | DIMENSIONS_MASK |
										  DITHERING_MASK);
	pData->pd_Preferences.PrintFlags |= IGNORE_DIMENSIONS | ORDERED_DITHERING;
	return (printIO);
}

/*
 *	Close the printer device previously openned by OpenPrinter
 */

static void ClosePrinter(PrintIOPtr printIO)
{
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&origPrefs, &pData->pd_Preferences, sizeof(struct Preferences));

	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	CloseDevice((IOReqPtr) printIO);
	DeleteExtIO((IOReqPtr) printIO);
	DeletePort(replyPort);
}

/*
 *	Return rectangle of page boundaries, excluding headers and footers
 *	MaxX and MaxY are coordinates of first pixel outside page bounds
 */

void GetPageRect(register DocDataPtr docData, register Rectangle *rect)
{
	rect->MinX = 35;
	rect->MaxX = docData->MaxXDots-35;
	if (docData->PrintRec->Flags & PRT_NOGAPS) {
		rect->MinY = 18;
		rect->MaxY = docData->MaxYDots-18;
	}
	else {
		rect->MinY = 36;
		rect->MaxY = docData->MaxYDots - 36;
	}
	
	if (docData->Flags & WD_SHOWFOOT)
		rect->MaxY -= (2 * (72/docData->LPI));
}

/*
 *	Print given page in PostScript
 */

static WORD PrintPSPage(PrintRecPtr printRec, DocDataPtr docData, WORD page)
{
	WORD result;
	register TextPtr text;
	register WORD	penX, penY, prevStyle, len, line, pageLine, headLine;
	UBYTE			style;
	UWORD			topLine, index, spaces, baseline, maxLines;
	HeadPtr			topHead;
	register HeadPtr	head;
	Rectangle 		rect;
	BOOL			drawFooter = FALSE;
	
/*
	Set up page
*/
	if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON) {
		result = PRINT_ABORT;
		goto Exit;
	}
	if (!PSBeginPage(printRec, page)) {
		result = PRINT_ERROR;
		goto Exit;
	}
/*
	Set up
*/
	GetPageRect(docData,&rect);
	baseline =  (docData->LPI == 6) ? 12 : 9;
	penY = rect.MinY + baseline;
	penX = rect.MinX;
	result = PRINT_OK;
	prevStyle = style = FS_NORMAL;
	
	if (!PSSelectFont(printRec, 0, style, docData->LPI))
		return (FALSE);
/*
	Draw header
*/
	if ((len = strlen(docData->Header)) != 0) {
		if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON) {
			result = PRINT_ABORT;
			goto Exit;
		}
		if (!PrintHdrFtr(NULL,docData->Header,docData, page+1,penX,penY) ) {
			result = PRINT_ERROR;
			goto Exit;
		}
		penY += 2*baseline;
	}
/*
	Draw body
*/
	topHead = head = FindFirstPageLine(docData, page, &index, &topLine);

	line = topLine;
	maxLines = (rect.MaxY - rect.MinY)/baseline;
	maxLines -= (strlen(docData->Footer) != 0) ? 2 : 0;
	maxLines -= (strlen(docData->Header) != 0) ? 2 : 0;
	penX = rect.MinX;
	
	for (pageLine = 0; pageLine < maxLines && head ; pageLine++ ) {
		if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON)	{
			SetCancelButton(FALSE);
			SetInfoText(strPrtCancel);
			return(PRINT_ABORT);
		}
		style = head->Style;
		headLine = line - LineNumber(docData, head, 0);
		spaces = (docData->Indent*HeadDepth(head)) + docData->LeftMargin;
/*
	The only knowledge print code needs of labeling is here.
	Indents secondary lines if necessary.
*/
		if( ( docData->LabelFlags & LABEL_ALIGN_MASK ) && headLine )
			spaces += head->BodyStart;
		text = head->Text + head->LineStarts[headLine];
		len = (headLine < head->NumLines - 1) ? head->LineStarts[headLine + 1] : head->Len;
		len -= head->LineStarts[headLine];
		while (len > 0 && text[len - 1] <= ' ')
			len--;
		if (!PSSelectFont(printRec, 0, style, docData->LPI))
			return (FALSE);	
		if (!PSText(printRec, penX, penY,docData->CPI, prevStyle, style, text, len,spaces))
				return (FALSE);
		line++;
		penY += baseline;
		head = WhichHead(docData, line);
		if( ( head != NULL ) && ( head != topHead ) && ( head->Flags & HD_PAGEBREAK ) ) {
			drawFooter = TRUE;			/* Page break done by AdvanceLines() below */
			head = NULL;					/* Cause termination condition */
		}
	}
/*
	Draw footer
*/
	if (((len = strlen(docData->Footer)) != 0) || drawFooter) {
		penX = rect.MinX;
		penY = rect.MaxY;
		if (CheckDialog(mainMsgPort, cancelDialog, DialogFilter) == CANCEL_BUTTON) {
			result = PRINT_ABORT;
			goto Exit;
		}
		if (!PSSelectFont(printRec, 0, style, docData->LPI))
			return (FALSE);
		if (!PrintHdrFtr(NULL, docData->Footer, docData, page+1,penX,penY) ) {
			result = PRINT_ERROR;
			goto Exit;
		}
	}
/*
	End and print page
*/
Exit:
	if (result != PRINT_ERROR) {
		if (!PSEndPage(printRec))
			result = PRINT_ERROR;
	}
	return (result);
}

/*
 *	Print PostScript
 *	Save file to temp file on RAM disk and starts up ProScript
 */

static WORD PrintPostScript(WindowPtr window, DocDataPtr docData)
{
	WORD page, copy, result, i;
	register PrintRecPtr printRec = docData->PrintRec;

	result = PRINT_ERROR;				/* Assume error */
	
	if (printRec->Flags & PRT_TOFILE) {
		for (i=0; i<32; i++)
			postScriptOptions.CustomName[i] = printRec->FileName[i];
		postScriptOptions.Device = PS_CUSTOM;
	}
				 
/*
	Open device/file for output
*/
	if ((printRec->File = Open(PSDeviceName(), MODE_NEWFILE)) == NULL)
		goto Exit1;
	
	if( printRec->LastPage > printRec->FirstPage + (docData->Pages-1) )
		printRec->LastPage = printRec->FirstPage + (docData->Pages-1);
/*
	Begin job
*/
	GetWTitle(window, strBuff);
	if (!PSBeginJob(printRec, strBuff))
		goto Exit2;
		
/*
	Send the page contents
*/
	StartPSPage(printRec, &page, &copy, TRUE);
	while (copy > 0) {
		SetCancelButton(TRUE);
		SetInfoText(strPrtPrint);
		SetPageIndic(window, page);
		result = PrintPSPage(printRec, docData, page - 1); 
		if (result == PRINT_ABORT)
			break;
		if (result == PRINT_ERROR)
			goto Exit3;
		NextPSPage(printRec, &page, &copy);
	}
	result = PRINT_OK;
/*
	End job
*/
Exit3:
	if (!PSEndJob(printRec))
		goto Exit2;
/*
	Clean up
*/
Exit2:
	Close(printRec->File);
Exit1:
 
	return (result);
}

/*
 *	Set info text in cancel dialog
 */

static void SetInfoText(TextPtr text)
{
	SetGadgetItemText(cancelDialog->FirstGadget, INFO_TEXT, cancelDialog, NULL, text);
}

/*
 *	Get the starting page and copy from print record
 *	Return 0 for copy if no pages are to be printed
 *	Must be called first with doOdd = TRUE, then with doOdd = FALSE
 *	Note: when printing odd/even,
 *		first page will be rounded down to odd page
 *		and lastPage will be rounded up to even page
 *		(unless only printing one page)
 */

static void StartPSPage(PrintRecPtr printRec, WORD *page, WORD *copy, BOOL doOdd)
{
	register WORD firstPage = printRec->FirstPage;
	register WORD lastPage = printRec->LastPage;
	register UWORD flags = printRec->Flags;
	BOOL more;

	more = (firstPage != lastPage);
	if (more && (firstPage & 1) == 0)
		firstPage--;				/* Make firstPage odd */
	if (more && (lastPage & 1))
		lastPage++;					/* Make lastPage even */
	if (!doOdd && !more) {
		*copy = 0;
		return;
	}
	if (firstPage == lastPage)
		*page = firstPage;
	else if (flags & PRT_BACKTOFRONT) {
		*page = lastPage;
		if (more && doOdd)
			(*page)--;
	}
	else {
		*page = firstPage;
		if (more && !doOdd)
			(*page)++;
	}
	*copy = (flags & PRT_COLLATE) ? printRec->Copies : 1;
}

/*
 *	Get the next page and copy to print
 *	Return 0 for copy if no more pages to print
 *	Return -1 for copy if user needs to turn over paper for odd/even page printing
 */

static void NextPSPage(PrintRecPtr printRec, WORD *page, WORD *copy)
{
	register WORD firstPage = printRec->FirstPage;
	register WORD lastPage = printRec->LastPage;
	register UWORD flags = printRec->Flags;
	WORD currCopy;
	BOOL more;

	more = ((flags & PRT_ODDEVEN) && firstPage != lastPage);
	if (more && (firstPage & 1) == 0)
		firstPage--;				/* Make firstPage odd */
	if (more && (lastPage & 1))
		lastPage++;					/* Make lastPage even */
	if ((flags & PRT_COLLATE) == 0) {
		(*copy)--;
		if (*copy > 0)
			return;
		*copy = 1;
	}
	if (flags & PRT_BACKTOFRONT) {
		(*page)--;
		if (more)
			(*page)--;
	}
	else {
		(*page)++;
		if (more)
			(*page)++;
	}
	if (*page >= firstPage && *page <= lastPage)
		return;
	if ((flags & PRT_COLLATE) && *copy > 1) {
		currCopy = *copy;
		StartPSPage(printRec, page, copy, (BOOL) (!more || (*page & 1)));
		*copy = currCopy - 1;
	}
	else if (more && (*page & 1))
		*copy = -1;
	else
		*copy = 0;
	return;
}

