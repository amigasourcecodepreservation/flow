/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Dialog routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Window.h>
#include <Toolbox/Menu.h>

#include "Flow.h"
#include "Proto.h"

/*
	Local definitions 
*/

#define CH_SPELL_BUTTON	1
#define SPELL_LIST		6
#define SPELL_UP		7
#define SPELL_DOWN		8
#define SPELL_SCROLL		9

/*
 * External variables
 */

extern DialogPtr findDialog, changeDialog, spellDialog, insertDialog;
 
extern TextChar findBuffer[GADG_MAX_STRING];
extern TextChar changeBuffer[GADG_MAX_STRING];

extern TextChar strChangedNum[], strStop[], strDone[], strFind[];

extern BOOL wholeWord, matchCase, stopSearch, openOnly;

extern WindowPtr cmdWindow, backWindow;

extern MsgPortPtr mainMsgPort;

extern WORD modelessStrItem;

extern DlgTemplPtr dlgList[];

extern ScreenPtr screen;

extern ScrollListPtr scrollList;

extern MsgPort monitorMsgPort;
extern MenuPtr docMenuStrip;

extern BOOL	idleEvent;

/*
 * Call this on INTUITICKS messages for modeless dialogs
 */
 
void DoDialogIdle(DialogPtr dlg)
{
	idleEvent = TRUE;
	if( dlg == findDialog )
		SetFindButton();
	else if( dlg == changeDialog )
		SetChangeButtons();
	else if( dlg == spellDialog )
		SetSpellButtons();
	idleEvent = FALSE;
}

/*
	A non-button/checkbox/radio button gadget depressed in a modeless dialog
*/

void DoDialogGadgetDown(DialogPtr dlg, IntuiMsgPtr intuiMsg)
{
	register WORD item;
	GadgetPtr gadget = (GadgetPtr) intuiMsg->IAddress;
	BOOL reply = TRUE;
	register	WORD selWord;
	WORD xPos, yPos;
	WORD cellWidth, cellHeight, row, column;
	BOOL selected, select;
	Rectangle	rect;
	TextChar ch;
	
	if( cmdWindow != backWindow ) {
		item = GadgetNumber(gadget);
		if( dlg == changeDialog ) {
			if( item == FIND_TEXT || item == CHANGE_TEXT )
				 modelessStrItem = item;
		} else if( dlg == spellDialog ) {
			if( item == SPELL_LIST || item == SPELL_SCROLL || item == SPELL_UP || item == SPELL_DOWN ) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				reply = FALSE;
				if( item == SPELL_LIST ) {
					DoDialogActivate(dlg, TRUE);
					selWord = SLNextSelect(scrollList, -1);
					if( selWord != -1 ) {
						NewWord(selWord);
						if( SLIsDoubleClick(scrollList)) {
							DoSpellDialogItem(CH_SPELL_BUTTON);
						}
					}
				}
			}
		} else if( dlg == insertDialog ) {
/*
	Find selected cell and highlight it
*/
			xPos = intuiMsg->MouseX;
			yPos = intuiMsg->MouseY;
			ReplyMsg( (MsgPtr) intuiMsg);
			reply = FALSE;
			selected = FALSE;
			cellWidth = gadget->Width/24;
 			cellHeight = gadget->Height/8;
			if( (yPos - gadget->TopEdge) < (8*cellHeight) ) {
				column = (xPos - gadget->LeftEdge)/cellWidth;
				row = (yPos - gadget->TopEdge)/cellHeight;
				ch = CellChar(column,row);
				GetCellRect(insertDialog,column,row,&rect);
				if (ch != DEL) {		/* Not an insertable character */
			  		DrawInsertItem(insertDialog,column,row,TRUE);
					selected = TRUE;
/*
	Remove any messages which might WaitMouseUp() to return prematurely
*/
					intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort);
					while( intuiMsg != NULL && (intuiMsg->Class == ACTIVEWINDOW ||
						intuiMsg->Class == INACTIVEWINDOW ||
						intuiMsg->Class == INTUITICKS ) ) {
						ReplyMsg( (MsgPtr) intuiMsg);
						intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort);
					}
					if( intuiMsg != NULL )
						PutMsg( mainMsgPort, (MsgPtr) intuiMsg );
/*
	Track mouse and see if it stays in cell
*/
					while (WaitMouseUp(mainMsgPort, insertDialog)) {
						xPos = insertDialog->MouseX;
						yPos = insertDialog->MouseY;
						select = (xPos >= rect.MinX && xPos <= rect.MaxX &&
							  yPos >= rect.MinY && yPos <= rect.MaxY);
						if ((select && !selected) || (!select && selected)) {
							selected = select;
							DrawInsertItem(insertDialog,column,row,selected);
						}
					}
				}
			}
			if(selected) {
				AddChar(cmdWindow, ch);
				DrawInsertItem(insertDialog,column,row,FALSE);
				ActivateWindow(cmdWindow);
			}
		}
	}
	if( reply ) {
		ReplyMsg( (MsgPtr) intuiMsg);
	}
	return;
}

/*
	Gadget released for a modeless dialog; call DoDialogItem() to process it
*/

void DoDialogGadgetUp(DialogPtr dlg, IntuiMsgPtr intuiMsg)
{
	DoDialogItem( dlg, intuiMsg, GadgetNumber((GadgetPtr)intuiMsg->IAddress) );
}

/*
	Process the specified gadget item for this modeless dialog
*/

void DoDialogItem( register DialogPtr dlg, IntuiMsgPtr intuiMsg, register WORD item)
{
	DocDataPtr docData;
	register WORD changes;
	register GadgetPtr gadgList;
	DialogPtr abortDlg;
	register GadgetPtr gadget;
	BOOL reply = TRUE;
	
	if( cmdWindow != backWindow ) {  
		gadgList = dlg->FirstGadget;
		gadget = GadgetItem(gadgList, item);
		docData = (DocDataPtr) GetWRefCon(cmdWindow);
		if( dlg == findDialog || dlg == changeDialog ) {
			switch(item){
			case WHOLEWORD_BOX:
				ToggleCheckbox(&wholeWord, item, dlg);
				break;
			case MATCHCASE_BOX:
				ToggleCheckbox(&matchCase, item, dlg);
				break;
			case OPENONLY_BOX:
				ToggleCheckbox(&openOnly, item, dlg);
				break;
/*
			case STOPSEARCH_BOX:
				stopSearch = !stopSearch;
				SetGadgetValue(gadget, dlg, NULL, stopSearch);
				break;
*/
			case CHANGE_BUTTON:
			case CHANGEFIND_BUTTON:
				GetEditItemText(gadgList, CHANGE_TEXT, changeBuffer);
				CursorOff(cmdWindow);
				if (!ChangeText(changeBuffer)) {
					Error(ERR_NO_CHANGE);
					break;
				} else {
					DrawHead(cmdWindow, docData->SelHead);
				}
				CursorOn(cmdWindow);
				CountLines(docData);
				DocumentModified(docData);
				SetChangeButtons();
				if (item == CHANGE_BUTTON) {
					modelessStrItem = FIND_TEXT;
					break;								/* Else fall through */
				}
			case CHANGE_TEXT:
			case FIND_TEXT:
			case OK_BUTTON:
				modelessStrItem = ( dlg == changeDialog ) ? CHANGE_TEXT : FIND_TEXT ;  
				GetEditItemText(gadgList, FIND_TEXT, findBuffer);
				if( dlg == changeDialog ) {
					GetEditItemText(gadgList, CHANGE_TEXT, changeBuffer);
				}
				(void) DoFindNext(cmdWindow);
				break;
/*
	Change all occurances of text
*/
			case CHANGEALL_BUTTON:
				OffChangeButtons();
				GetEditItemText(gadgList, FIND_TEXT, findBuffer);
				GetEditItemText(gadgList, CHANGE_TEXT, changeBuffer);
				abortDlg = GetDialog( dlgList[DLG_STOPCHANGE], screen, mainMsgPort );
				if( abortDlg != NULL ) {
					BeginWait();
					SetBusyPointer(abortDlg);
					changes = 0;
					CursorOff(cmdWindow);
					if (WordSelected(docData, findBuffer))
						SetFindEnd(docData->SelHead, docData->SelStart);
					else
						SetFindEnd(docData->SelHead, docData->SelEnd);
					do {
						if (CheckDialog(mainMsgPort, abortDlg, DialogFilter) == CANCEL_BUTTON) {
							break;
						}
						if (!ChangeText(changeBuffer)) {
							Error(ERR_NO_CHANGE);
							break;
						}
						CursorOff(cmdWindow);
						changes++;
						NextBusyPointer();
					} while (FindText(findBuffer));
					EndWait();
					DisposeDialog(abortDlg);
					CursorOn(cmdWindow);
					CountLines(docData);
				}
				SetChangeButtons();
				ScrollToCursor(cmdWindow);
				SetStdPointer(dlg, POINTER_ARROW);
				DrawWindow(cmdWindow);
				InfoDialog(strChangedNum, changes);
				if (changes)
					DocumentModified(docData);
				break;
			default:
				break;
			}
		} else if( dlg == spellDialog ) {
			item = GadgetNumber(gadget);
/*
	Item can never be the scroll list here
*/
			if( item == SPELL_SCROLL || item == SPELL_UP || item == SPELL_DOWN ) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				reply = FALSE;
			}
			DoSpellDialogItem(item);
		} 
		DoDialogActivate(dlg, TRUE);
	}
	if( reply ) {
		ReplyMsg( (MsgPtr) intuiMsg);
	}
	return;
}
	
/*
	Perform necessary actions upon activate of event, mainly activating text box
*/

void DoDialogActivate(register DialogPtr dlg, BOOL activate)
{
	GadgetPtr gadget;
	
	if( activate ) {
		CursorOn(cmdWindow);
		if( dlg == findDialog || dlg == changeDialog || dlg == spellDialog ||
				dlg == insertDialog ) {
			gadget = GadgetItem(dlg->FirstGadget, modelessStrItem);
			ActivateGadget(gadget, dlg, NULL);
		}
		if( dlg == spellDialog ) {
			ActivateSpellDialog();
		}
	} else {
		if( dlg == spellDialog ) {
			SetDefaultButtons(OK_BUTTON, CANCEL_BUTTON);
		}
	}
	return;
}

/*
	Perform initialization required upon opening a modeless dialog
*/

void DoDialogOpen(DialogPtr dlg)
{
#ifdef BRAIN_DEAD_KEYS_GONE
	dlg->UserPort = &monitorMsgPort;
	InsertMenuStrip(dlg, docMenuStrip);
	ModifyIDCMP(dlg, dlg->IDCMPFlags | MENUPICK);
#endif
	OutlineOKButton(dlg);
}

/*
	Perform necessary actions upon closing of a modeless dialog
*/
	
void DoDialogClose(register DialogPtr dlg)
{
	GadgetPtr gadgList = dlg->FirstGadget;
	register WORD dlgNum;
	
	if( dlg == findDialog || dlg == changeDialog ) {
		GetEditItemText(gadgList, FIND_TEXT, findBuffer);
	}
	if( dlg == findDialog ) {
		findDialog = NULL;
		dlgNum = DLG_FIND;
	} else if( dlg == changeDialog ) {
		GetEditItemText(gadgList, CHANGE_TEXT, changeBuffer);
		changeDialog = NULL;
		dlgNum = DLG_CHANGE;
	} else if( dlg == spellDialog ) {
		DisposeSuggestWords();
		DisposeSkipWords();
		SaveUserDict(cmdWindow);
		DisposeScrollList(scrollList);
		scrollList = NULL;
		spellDialog = NULL;
		dlgNum = DLG_CHECKSPELL;
	} else if( dlg == insertDialog ) {
		insertDialog = NULL;
		dlgNum = DLG_INSERT;
	}
	dlgList[dlgNum]->LeftEdge = dlg->LeftEdge;
	dlgList[dlgNum]->TopEdge = dlg->TopEdge;
#ifdef BRAIN_DEAD_KEYS_GONE
	dlg->UserPort = mainMsgPort;
	ClearMenuStrip(dlg);
#endif
	DisposeDialog(dlg);
}

/*
	Close all modeless dialogs (quitting or alt-CLOSE done)
*/

void DoDialogCloseAll()
{
	if( findDialog != NULL )
		DoDialogClose(findDialog);
	if( changeDialog != NULL )
		DoDialogClose(changeDialog);
	if( spellDialog != NULL )
		DoDialogClose(spellDialog);
	if( insertDialog != NULL )
		DoDialogClose(insertDialog);
	return;
}
