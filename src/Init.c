/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>
#include <devices/console.h>			/* For ISO color values */
#include <devices/clipboard.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>
#include <stdlib.h>				/* For exit() prototype */

#include <Toolbox/Graphics.h>
#include <Toolbox/Image.h>
#include <Toolbox/IntuiText.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Screen.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase		*GfxBase;
extern struct LayersBase	*LayersBase;
extern struct Library		*IconBase;
extern struct Library		*DiskfontBase;
extern struct IFFParseBase	*IFFParseBase;
extern struct Device		*ConsoleDevice;

extern TextChar	progPathName[];

extern BOOL	fromCLI;

extern WORD	intuiVersion;

extern struct WBStartup	*WBenchMsg;

extern MenuTemplate		docWindMenus[], altWindMenus[];

extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPortPtr	clipboardMsgPort;
extern MsgPort		monitorMsgPort;

extern MenuPtr		docMenuStrip;

extern UWORD	colorList[];

extern RGBColor	screenColors[];

extern GadgetTemplate	windowGadgets[];

extern TextFontPtr	smallFont;

extern TextPtr	initErrors[];
extern TextChar	strCancel[];

extern TextChar	screenTitle[], strScreenName[];

extern UBYTE	isoColors[];
extern TextPtr	colorNames[];

extern DlgTemplPtr	dlgList[];

extern BOOL	_tbOnPubScreen;

extern WORD charHeight, charWidth, charBaseline;

/*
 *	Local variables and definitions
 */

#define MAX_FILENAME_LEN	31

static struct IOStdReq	consoleIOReq;

static TaskPtr	monitorTask;
static APTR		origWindowPtr;
static Dir		startupDir;

static struct NewScreen newScreen = {
	0, 0, 640, STDSCREENHEIGHT, 2, 0, 1,
	HIRES | LACE, CUSTOMSCREEN,
	NULL, &screenTitle[0], NULL, NULL
};

static TextAttr smallAttr = {
	NULL, 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextChar topazFontName[] = "topaz.font";
static TextChar	systemFontName[] = "System.font";

/*
 *	Local prototypes
 */

void	InitError(WORD);
void	BuildProgPathName(TextPtr, Dir);

BOOL	SetUpMenus(void);

void	SetColorNames(void);

BOOL	IsPubScreen(ScreenPtr);
void	WaitVisitor(void);

/*
 *	Abort initialization
 *	Display error message in workbench screen
 */

static void InitError(WORD errNum)
{
	IntuiText bodyText, negText;

	if (IntuitionBase) {
		bodyText.FrontPen	= negText.FrontPen  = AUTOFRONTPEN;
		bodyText.BackPen	= negText.BackPen   = AUTOBACKPEN;
		bodyText.DrawMode	= negText.DrawMode  = AUTODRAWMODE;
		bodyText.LeftEdge	= negText.LeftEdge  = AUTOLEFTEDGE;
		bodyText.TopEdge	= negText.TopEdge   = AUTOTOPEDGE;
		bodyText.ITextFont	= negText.ITextFont = AUTOITEXTFONT;
		bodyText.NextText	= negText.NextText  = AUTONEXTTEXT;
		bodyText.IText		= initErrors[errNum];
		negText.IText		= strCancel;
		SysBeep(5);
		DisplayBeep(NULL);
		AutoRequest(NULL, &bodyText, NULL, &negText, 0L, 0L, 250, 50);
	}
	ShutDown();
	exit(RETURN_FAIL);

}

/*
 *	Build path name to program from given parameters
 *	progPathName[] is 100 bytes in size
 *	Note: Cannot use ProgDir: in system 2.0, since this path & name is used for
 *		document icon default tools
 */

static void BuildProgPathName(TextPtr fileName, Dir dirLock)
{
	register TextPtr newName;
	register WORD nameLen, newLen;
	register Dir dir, parentDir;
	struct FileInfoBlock *fib;

	strcpy(progPathName, fileName);
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;
	newName = fib->fib_FileName;
	dir = DupLock(dirLock);
	while (dir) {
		if (!Examine(dir, fib))
			break;
		nameLen = strlen(progPathName);
		newLen = strlen(newName);
		if (nameLen + newLen > 98)
			break;
		BlockMove(progPathName, progPathName + newLen + 1, nameLen + 1);
		BlockMove(newName, progPathName, newLen);
		parentDir = ParentDir(dir);
		progPathName[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if (dir)
		UnLock(dir);
	MemFree(fib, sizeof(struct FileInfoBlock));
}

/*
 *	Set up menus
 */

static BOOL SetUpMenus()
{
	MenuTemplPtr menuTempl;

/*
	Get menu strip
*/
	menuTempl = (screen->ViewPort.Modes & HIRES) ? docWindMenus : altWindMenus;
	if ((docMenuStrip = GetMenuStrip(menuTempl)) == NULL)
		return (FALSE);
/*
	Attach to backWindow
*/
	InsertMenuStrip(backWindow, docMenuStrip);
	return (TRUE);
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	WORD i, barHeight, arrowWidth, arrowHeight, leftEdge;
	Dir dir;
	TextPtr progName;
	ProcessPtr process;
	DialogPtr initDlg;

	fromCLI = (argc > 0);
	process = (ProcessPtr) FindTask(NULL);
/*
	Build path name to program
	(Used to find program icon, prefs file and dictionary)
*/
	dir = DupLock(process->pr_CurrentDir);
	startupDir = CurrentDir(dir);		/* Workbench needs the original lock */
	if (argc)							/* Running under CLI */
		progName = argv[0];
	else								/* Running under Workbench */
		progName = WBenchMsg->sm_ArgList->wa_Name;
	dir = ConvertFileName(progName);
	BuildProgPathName(progName, dir);
	UnLock(dir);
/*
	Open needed libraries
*/
	IntuitionBase = (struct IntuitionBase *)
					OpenLibrary("intuition.library", OSVERSION_1_2);
	GfxBase = (struct GfxBase *)
			  OpenLibrary("graphics.library", OSVERSION_1_2);
	LayersBase = (struct LayersBase *)
				 OpenLibrary("layers.library", OSVERSION_1_2);
	if (IntuitionBase == NULL || GfxBase == NULL || LayersBase == NULL)
		InitError(INIT_BADSYS);
	IconBase = OpenLibrary("icon.library", OSVERSION_1_2);
	if (IconBase == NULL)
		InitError(INIT_NOICON);
	DiskfontBase = OpenLibrary("diskfont.library", OSVERSION_1_2);
	if (DiskfontBase == NULL)
		InitError(INIT_NOFONT);
	
	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Open console device and get base vector
	Needed for ConvertKeyMsg() function call
*/
	if (OpenDevice("console.device", -1, (IOReqPtr) &consoleIOReq, 0))
		InitError(INIT_BADSYS);
	ConsoleDevice = consoleIOReq.io_Device;
/*
	Open screen (or get pointer to public screen) and set colors
*/
	BuildDefaultColorTable(&screenColors);
	if ((screen = GetScreen(argc, argv, &newScreen, &screenColors, NUM_STDCOLORS,
							strScreenName)) == NULL)
		InitError(INIT_NOSCREEN);
	InitToolbox(screen);
	CheckColorTable();
	IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4);
/*
	Get 8 pixel high font (used in ruler and page indicator)
*/
	smallAttr.ta_Name  = systemFontName;
	smallAttr.ta_YSize = 8;
	smallFont = GetFont(&smallAttr);
	if (smallFont == NULL) {
		smallAttr.ta_Name  = topazFontName;
		smallFont = GetFont(&smallAttr);
	}
	
	
/*
	Adjust parameters for document windows and gadgets
*/
	barHeight = screen->WBorTop + screen->Font->ta_YSize + 1;
	arrowWidth  = ARROW_WIDTH;
	arrowHeight = ARROW_HEIGHT;
	leftEdge	= 3;
	if (intuiVersion < OSVERSION_2_0) {
		arrowWidth  -= 2;
		arrowHeight -= 1;
		leftEdge--;
	}

	for (i = UP_ARROW; i <= RIGHT_ARROW; i++) {
		windowGadgets[i].LeftOffset		= 1 - arrowWidth;
		windowGadgets[i].TopOffset		= 1 - arrowHeight;
		windowGadgets[i].WidthOffset	= arrowWidth;
		windowGadgets[i].HeightOffset	= arrowHeight;
	}
	windowGadgets[UP_ARROW].TopOffset		-= 2*arrowHeight;
	windowGadgets[DOWN_ARROW].TopOffset		-= arrowHeight;
	windowGadgets[LEFT_ARROW].LeftOffset	-= 2*arrowWidth;
	windowGadgets[RIGHT_ARROW].LeftOffset	-= arrowWidth;

	windowGadgets[VERT_SCROLL].LeftOffset	= 1 - arrowWidth;
	windowGadgets[VERT_SCROLL].TopOffset	= barHeight;
	windowGadgets[VERT_SCROLL].WidthOffset	= arrowWidth;
	windowGadgets[VERT_SCROLL].HeightOffset	= -3*arrowHeight - barHeight;
	windowGadgets[HORIZ_SCROLL].LeftOffset	= leftEdge + 2*arrowWidth + PGINDIC_WIDTH;
	windowGadgets[HORIZ_SCROLL].TopOffset	= 1 - arrowHeight;
	windowGadgets[HORIZ_SCROLL].WidthOffset	= -leftEdge - 5*arrowWidth - PGINDIC_WIDTH;
	windowGadgets[HORIZ_SCROLL].HeightOffset= arrowHeight;
	if (intuiVersion >= OSVERSION_2_0) {
		windowGadgets[VERT_SCROLL].LeftOffset	+= 4;
		windowGadgets[VERT_SCROLL].TopOffset	+= 1;
		windowGadgets[VERT_SCROLL].WidthOffset	-= 8;
		windowGadgets[VERT_SCROLL].HeightOffset	-= 2;
		windowGadgets[HORIZ_SCROLL].LeftOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].TopOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].WidthOffset	-= 4;
		windowGadgets[HORIZ_SCROLL].HeightOffset-= 4;
	}
	else {
		windowGadgets[VERT_SCROLL].TopOffset	-= 1;
		windowGadgets[VERT_SCROLL].HeightOffset	+= 1;
	}

	windowGadgets[PAGEUP_ARROW].LeftOffset		= leftEdge;
	windowGadgets[PAGEDOWN_ARROW].LeftOffset	= leftEdge + arrowWidth;
	for (i = PAGEUP_ARROW; i <= PAGEDOWN_ARROW; i++) {
		windowGadgets[i].TopOffset		= 1 - arrowHeight;
		windowGadgets[i].WidthOffset	= arrowWidth;
		windowGadgets[i].HeightOffset	= arrowHeight;
	}
/*
	Start the monitor task
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL)
		InitError(INIT_NOMEM);
	monitorTask = CreateTask("Flow.monitor",
							 (UBYTE) (process->pr_Task.tc_Node.ln_Pri + 1),
							 MonitorTask, 1000L);
	if (monitorTask == NULL)
		InitError(INIT_NOMEM);

/*
	Open background window and display init dialog
	(Monitor task will have set up monitorMsgPort)
*/
	if ((backWindow = OpenBackWindow()) == NULL)
		InitError(INIT_NOMEM);
	
/* set globals */
	SetFont(backWindow->RPort,GetFont(screen->Font));
	charWidth = backWindow->RPort->TxWidth;
	charHeight = screen->Font->ta_YSize;
	charBaseline = backWindow->RPort->TxBaseline;
	
	SetStdPointer(backWindow, POINTER_WAIT);
	if (_tbOnPubScreen && intuiVersion >= OSVERSION_2_0)
		UnlockPubScreen(NULL, screen);	/* Recommended procedure */
	if ((initDlg = GetDialog(dlgList[DLG_INIT], screen, mainMsgPort)) == NULL)
		InitError(INIT_NOMEM);
	SetStdPointer(initDlg, POINTER_WAIT);
/*
	Have system requesters appear in this screen
*/
	origWindowPtr = process->pr_WindowPtr;
	process->pr_WindowPtr = backWindow;

/*
	Set up menus
*/
	if (!SetUpMenus()) {
		DisposeDialog(initDlg);
		InitError(INIT_NOMEM);
	}
/*
	Misc initialization
*/
	SetAllMenus();		/* Must do before BeginWait() */
	BeginWait();
	GetProgPrefs();
	InitToolbox(screen);	/* In case color palette changed */
	GetSysPrefs();
	
	LoadFKeys();
	InitSpell(NULL, NULL);	/* If this fails, try again when spell check is requested */
	InitRexx();
	SetMacroMenu();
	InitIcons();
	Delay(25L);
	DisposeDialog(initDlg);
	InitClipboard();	
	SetStdPointer(backWindow, POINTER_ARROW);
	
	EndWait();
}

/*
 *	Wait until all visitor windows are closed on screen
 */

static void WaitVisitor()
{
	register WindowPtr window;
	register MsgPtr msg;

	if (_tbOnPubScreen || screen == NULL || backWindow == NULL)
		return;
/*
	Busy loop, since we won't receive INTUITICKS if backWindow is not active
*/
	ModifyIDCMP(backWindow, REFRESHWINDOW);
	for (;;) {
		while (msg = GetMsg(mainMsgPort))
			ReplyMsg(msg);
		for (window = screen->FirstWindow; window; window = window->NextWindow) {
			if (window != backWindow)
				break;
		}
		if (window == NULL) {			/* No visitor windows found */
			if (intuiVersion < OSVERSION_2_0 ||
				!IsPubScreen(screen) ||
				(PubScreenStatus(screen, PSNF_PRIVATE) & PSNF_PRIVATE))
				break;
		}
		Delay(10L);						/* Wait 1/5 second before checking again */
	}
}

/*
 *	Shut down program
 *	Close backWindow, close libraries, and quit
 */

void ShutDown()
{
	register Dir dir;
	register ProcessPtr process;
	MsgPtr msg;

	ShutDownRexx();
	ShutDownClipboard();
	ShutDownSpell();
	if (smallFont)
		CloseFont(smallFont);
	ShutDownIcons();			/* May need to open Workbench, so free memory first */
	if (backWindow) {
		process = (ProcessPtr) FindTask(NULL);
		process->pr_WindowPtr = origWindowPtr;
		ClearMenuStrip(backWindow);
		WaitVisitor();
		CloseWindowSafely(backWindow, mainMsgPort);
	}
	if (monitorTask) {		/* Need Forbid/Permit to work with MemMung */
		Forbid();
		DeleteTask(monitorTask);
		Permit();
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	if (docMenuStrip) {
		DisposeMenuStrip(docMenuStrip);
	}
	DisposeScreen(screen);
	if (ConsoleDevice) 
		CloseDevice((IOReqPtr) &consoleIOReq);
	if (DiskfontBase)
		CloseLibrary(DiskfontBase);
	if (IconBase)
		CloseLibrary(IconBase);
	if (LayersBase)
		CloseLibrary((struct Library *) LayersBase);
	if (GfxBase)
		CloseLibrary((struct Library *) GfxBase);
	if (IntuitionBase) {
		OpenWorkBench();
		CloseLibrary((struct Library *) IntuitionBase);
	}
	if (IFFParseBase)
		CloseLibrary(IFFParseBase);
	dir = CurrentDir(startupDir);		/* Keep Workbench happy */
	UnLock(dir);
}

/*
 *	Open initial files, or open new window if none given
 */

void SetUp(int argc, char *argv[])
{
	register TextPtr fileName;
	register WORD i, numFiles, len;
	register BOOL fileOpened;
	register Dir dir;
	register struct WBArg *wbArgList;

	fileOpened = FALSE;
/*
	Get number of files to open
*/
	if (argc)						/* Running under CLI */
		numFiles = argc;
	else							/* Running under Workbench */
		numFiles = WBenchMsg->sm_NumArgs;
/*
	Open files
	First check to see if this is a prefs file, if so then load prefs
*/
	for (i = 1; i < numFiles; i++) {
		if (argc) {						/* Running under CLI */
			fileName = argv[i];
			if (*fileName == '-')		/* Ignore program options */
				continue;
			SetCurrentDir(startupDir);	/* Path is relative to startupDir */
			dir = ConvertFileName(fileName);
		}
		else {						/* Running under Workbench */
			wbArgList = WBenchMsg->sm_ArgList;
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN)
			UnLock(dir);
		else {
			SetCurrentDir(dir);
			if (ReadProgPrefs(fileName))
				UnLock(dir);
			else if (OpenFile(fileName, dir))
				fileOpened = TRUE;
		}
	}
/*
	If no file opened, then open new window
*/
	if (!fileOpened)
		DoNew();
}
