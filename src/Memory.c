/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Memory handling routines
 */

#include <Toolbox/Memory.h>

#include <proto/exec.h>

/*
 *	Local variables and definitions
 */

#define CHUNK_SIZE	8	/* Physical alloc chunks when allocating handles */

typedef struct {
	LONG	Size;		/* Actual data follows */
} PtrHdr;

/*
 *	NewPtr
 *	Allocate a new Ptr, return NULL if size <= 0 or error
 */

Ptr NewPtr(LONG size)
{
	register PtrHdr *ptrHdr;

	if (size > 0 &&
		(ptrHdr = MemAlloc(size + sizeof(PtrHdr), 0)) != NULL) {
		ptrHdr->Size = size;
		return ((Ptr) (ptrHdr + 1));
	}
	return (NULL);
}

/*
 *	DisposePtr
 *	Free memory allocated by Ptr
 */

void DisposePtr(Ptr ptr)
{
	register PtrHdr *ptrHdr = ptr;

	if (ptrHdr--)
		MemFree(ptrHdr, ptrHdr->Size + sizeof(PtrHdr));
}

/*
 *	GetPtrSize
 *	Return the logical size of Ptr
 *	Return 0 if Ptr is NULL
 */

LONG GetPtrSize(Ptr ptr)
{
	register PtrHdr *ptrHdr = ptr;

	return ((ptrHdr--) ? ptrHdr->Size : 0);
}
