/*
 *	ProWrite
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Spell check engine routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/icon.h>
#include <proto/dos.h>

#include <string.h>

#include <IFF/IFF.h>				/* For GroupHeader */

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/List.h>
#include <Toolbox/StdFile.h>

#include "Flow.h"
#include "HEAD.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern BOOL			spellEnabled;
extern Options		options;

extern TextChar	strRAMName[], strDefaultDir[];
extern TextChar	strMainDictName[], strUserDictName[];

extern TextChar	toLower[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define IS_POSSESSIVE(word, len)	\
	((len)>=2 && (word)[(len)-1]=='s' && ((word)[(len)-2]==QUOTE))

#define MAX_CACHE_LIST	50		/* Max number of words in each cache list */

static ListHeadPtr	userList, skipList;
static WORD			oldNumUserWords;

static ListHeadPtr	suggestList;

/*
 *	Table of characters for each nibble in compressed dictionary
 *	Nibble of 0 signifies end of word
 *	If nibble is < 15 then use first table for character
 *	If nibble is 15 then use second table with second nibble for character
 *
 *	Format of dictionary is:
 *	LONG	FileID
 *	LONG	offset to tables
 *	NIBBLES	words of main dictionary, final zero length word denotes end
 *	NIBBLES	words of common dictionary, final zero length word denotes end
 *			Tables follow
 *	WORD	numLetters, number of letters (and other chars) used in dictionary
 *	LONG	offset to common words
 *	UBYTE	decodeTable1[16], decodeTable2[16], decodeTable3[16]
 *	BYTE	charIndex[256]
 *	LONG	numLetters*numLetters nibble indexes of word starts
 *
 *	Word entries have the following format:
 *	NIBBLE	number of characters from previous word to use
 *	NIBBLES	characters to add to word, nibble of 0 ends word
 *
 *	Each word that begins with a new first two character set is pointed to in
 *	the index, and the first nibble of these word entries is always zero
 */

static File	mainDict;		/* Inited to NULL */
static WORD	numLetters;
static LONG	commonWordLoc;
static LONG	*wordIndexTable;

static UBYTE decodeTable1[16], decodeTable2[16], decodeTable3[16];

static ListHeadPtr	*commonLists;

/*
 *	Index of given character (0 .. NUM_LETTERS-1) or -1 if not a valid char
 */

static BYTE charIndex[256];

/*
 *	Local prototypes
 */

BOOL	SeekWord(TextPtr);
WORD	ReadWord(TextPtr);
BOOL	WordInList(ListHeadPtr, TextPtr, WORD);
void	AddWord(ListHeadPtr, TextPtr, WORD);
BOOL	OpenMainDict(TextPtr, Dir);
BOOL	LoadCommonWords(void);
void	LoadUserDict(void);
WORD	DiffWord(TextPtr, TextPtr, WORD, WORD);
BOOL	SimilarWord(TextPtr, TextPtr, WORD, WORD);
BOOL	CreateMiscLists(void);
void	DisposeMiscLists(void);

/*
 *	Determine if character is a valid word character for current dictionary
 */

BOOL IsSpellWordChar(TextChar ch)
{
	if (ch == QUOTE)
		return ((BOOL) (charIndex[QUOTE] != -1));
	return ((BOOL) wordChar[ch]);
}

/*
 *	Seek to first indexed word before given word
 *	Set up for ReadWord()
 *	Return success status
 */

static BOOL SeekWord(TextPtr word)
{
	register WORD indx1, indx2, wordIndex;
	LONG nibbleIndex;

/*
	Get index
*/
	if ((indx1 = charIndex[*word++]) < 0 || (indx2 = charIndex[*word]) < 0)
		return (FALSE);
	wordIndex = indx1*numLetters + indx2;
/*
	Seek to indexed word
*/
	nibbleIndex = wordIndexTable[wordIndex];
	if (Seek(mainDict, (LONG) (nibbleIndex >> 1), OFFSET_BEGINNING) < 0)
		return (FALSE);
	ClearBuff();
	if (nibbleIndex & 1)
		(void) GetNibble(mainDict);
	return (TRUE);
}

/*
 *	Read (next) word from dictionary
 *	Return success word length, or 0 if error
 */

static WORD ReadWord(wordBuff)
register TextPtr wordBuff;
{
	register WORD len, code;

	if ((len = GetNibble(mainDict)) == -1)
		return (0);
	while ((code = GetNibble(mainDict)) != 0 && code != -1) {
		if (code < 15)
			wordBuff[len++] = decodeTable1[code];
		else {
			code = GetNibble(mainDict);
			if (code < 15)
				wordBuff[len++] = decodeTable2[code];
			else
				wordBuff[len++] = decodeTable3[GetNibble(mainDict)];
		}
	}
	return (len);
}

/*
 *	Check for word being in given (sorted) list
 *	All words will be in lower case
 */

static BOOL WordInList(list, word, len)
ListHeadPtr list;
TextPtr word;
register WORD len;
{
	register TextPtr text;
	register WORD match;
	register ListItemPtr listItem;

	for (listItem = list->First; listItem; listItem = listItem->Next) {
		text = listItem->Text;
		if ((match = CompareText(word, text, len, (WORD) strlen(text))) == 0)
			return (TRUE);
		if (match < 0)
			break;
	}
	return (FALSE);
}

/*
 *	Add word to specified list
 */

static void AddWord(list, word, len)
ListHeadPtr list;
register TextPtr word;
register WORD len;
{
	register WORD i;
	TextChar newWord[MAX_WORD_LEN];

	if (IS_POSSESSIVE(word, len))
		len -= 2;
	if (len > MAX_WORD_LEN)
		len = MAX_WORD_LEN;
	for (i = 0; i < len; i++)
		newWord[i] = toLower[word[i]];
	if (!WordInList(list, newWord, len))
		AddListItem(list, newWord, len, TRUE);
}

/*
 *	Look up given word in dictionary
 *	Return success status
 *	Search order is: Recently used words, disk dictionary, user dictionary,
 *		previously accepted words list
 *	All one character words are considered valid
 *	Ignores soft hyphens
 */

BOOL LookUpWord(word, len, save)
TextPtr word;
register WORD len;
BOOL save;
{
	register TextChar ch;
	register WORD i, newLen, match, listNum;

	TextChar checkWord[MAX_WORD_LEN], dictWord[MAX_WORD_LEN];

	if (IS_POSSESSIVE(word, len))
		len -= 2;			/* Remove possessive ending */
	if (len > MAX_WORD_LEN)
		return (FALSE);
/*
	Check to see if word has numbers in it
*/
	for (i = newLen = 0; i < len; i++) {
		if ((ch = word[i]) >= '0' && ch <= '9')
			return (TRUE);
		if (ch != SHYPH)
			checkWord[newLen++] = ch;
			
	}
	word = checkWord;
	len = newLen;
	if (len < 2)
		return (TRUE);
/*
	Only check main dictionary for valid words
*/
	if ((listNum = charIndex[*word]) >= 0) {
/*
	Check common words list
*/
		if (WordInList(commonLists[listNum], word, len))
			return (TRUE);
/*
	Check main dictionary
	If found then add to cacheList
*/
		if (SeekWord(word)) {
			while ((i = ReadWord(dictWord)) > 0) {
				if ((match = CompareText(word, dictWord, len, i)) == 0) 
					return (TRUE);
				if (match < 0)
					break;
			}
		}
	}
/*
	Check user list
*/
	if (WordInList(userList, word, len))
		return (TRUE);
/*
	Check previously accepted words list
*/
	if (WordInList(skipList, word, len))
		return (TRUE);
/*
	Word not found
*/
	return (FALSE);
}

/*
 *	Open main dictionary and load location pointers
 *	If dictName is NULL then use default dict name
 *	Check for dictionary in RAM disk first (unless dir is not NULL)
 *	Return success status
 *	Note:	dictName must not contain a path
 *			dir lock is UnLocked
 */

static BOOL OpenMainDict(TextPtr dictName, Dir dir)
{
	LONG fileID, position, size;
	TextChar name[120];

	if (dictName == NULL)
		dictName = strMainDictName;
/*
	Open main dictionary and check to see if it is the right file format
*/
	mainDict = NULL;
	if (dir) {
		strcpy(name, dictName);
		SetCurrentDir(dir);
		UnLock(dir);
		mainDict = Open(name, MODE_OLDFILE);
	}
	else {
		if (DevMounted(strRAMName)) {
			strcpy(name, strRAMName);
			strcat(name, dictName);
			mainDict = Open(name, MODE_OLDFILE);
		}
		if (mainDict == NULL && DirAssigned(strDefaultDir)) {
			strcpy(name, strDefaultDir);
			strcat(name, dictName);
			mainDict = Open(name, MODE_OLDFILE);
		}
		if (mainDict == NULL) {		/* Not in RAM: or default dir */
			SetPathName(name, dictName, TRUE);
			mainDict = Open(name, MODE_OLDFILE);
		}
	}
	if (mainDict == NULL)
		return (FALSE);
	if (Read(mainDict, (UBYTE *) &fileID, sizeof(LONG)) != sizeof(LONG) ||
		fileID != ID_DICTFILE) {
		Close(mainDict);
		return (FALSE);
	}
/*
	Seek to beginning of tables and load information
*/
	size = sizeof(LONG);
	if (Read(mainDict, (UBYTE *) &position, size) != size ||
		Seek(mainDict, position, OFFSET_BEGINNING) < 0) {
		Close(mainDict);
		return (FALSE);
	}
	size = sizeof(WORD);
	if (Read(mainDict, (UBYTE *) &numLetters, size) != size) {
		Close(mainDict);
		return (FALSE);
	}
	size = sizeof(LONG);
	if (Read(mainDict, (UBYTE *) &commonWordLoc, size) != size) {
		Close(mainDict);
		return (FALSE);
	}
	size = 16*sizeof(UBYTE);
	if (Read(mainDict, (UBYTE *) decodeTable1, size) != size ||
		Read(mainDict, (UBYTE *) decodeTable2, size) != size ||
		Read(mainDict, (UBYTE *) decodeTable3, size) != size) {
		Close(mainDict);
		return (FALSE);
	}
	size = 256*sizeof(BYTE);
	if (Read(mainDict, (UBYTE *) charIndex, size) != size) {
		Close(mainDict);
		return (FALSE);
	}
	size = numLetters*numLetters*sizeof(LONG);
	if ((wordIndexTable = MemAlloc(size, 0)) == NULL ||
		Read(mainDict, (UBYTE *) wordIndexTable, size) != size) {
		if (wordIndexTable)
			MemFree(wordIndexTable, size);
		Close(mainDict);
		return (FALSE);
	}
	return (TRUE);
}

/*
 *	Load common words into commonLists
 *	Return success status
 */

static BOOL LoadCommonWords()
{
	WORD len, listNum;
	ListItemPtr listItem;
	TextChar wordBuff[MAX_WORD_LEN];

/*
	Seek to start of common word list
*/
	if (Seek(mainDict, (LONG) commonWordLoc, OFFSET_BEGINNING) < 0)
		return (FALSE);
	ClearBuff();
/*
	Read each word and put in correct list
	Words are already sorted, so just add to end of list
*/
	while ((len = ReadWord(wordBuff)) > 0) {
		if ((listNum = charIndex[*wordBuff]) >= 0 &&
			(listItem = CreateListItem(wordBuff, len)) != NULL)
			AddToList(commonLists[listNum], commonLists[listNum]->Last, listItem);
	}
	return (TRUE);
}

/*
 *	Open user dictionary and load words
 *	Words are assumed to already be in alphabetical order
 */

static void LoadUserDict()
{
	register TextPtr wordPtr;
	register WORD byte;
	File userDict;
	ListItemPtr listItem;
	GroupHeader grpHeader;
	TextChar buff[120];

	SetPathName(buff, strUserDictName, TRUE);
	if ((userDict = Open(buff, MODE_OLDFILE)) == NULL)
		return;
/*
	Check to make sure dictionary is not WORD or HEAD file
*/
	if (Read(userDict, (UBYTE *) &grpHeader, sizeof(GroupHeader)) == sizeof(GroupHeader) &&
		grpHeader.ckID == FORM && ( grpHeader.grpSubID == ID_HEAD || grpHeader.grpSubID == ID_WORD) ) {
		Close(userDict);
		return;
	}
	if (Seek(userDict, 0, OFFSET_BEGINNING) < 0) {
		Close(userDict);
		return;
	}
/*
	Read words one by one and add to userList
	Make sure words are in lower case
*/
	ClearBuff();
	for (;;) {
		wordPtr = buff;
		while ((byte = GetByte(userDict)) != -1 && byte != '\n')
			*wordPtr++ = toLower[byte];
		if (wordPtr != buff) {
			*wordPtr = '\0';
			if ((listItem = CreateListItem(buff, 0)) == NULL)
				break;
			AddToList(userList, userList->Last, listItem);
		}
		if (byte == -1)
			break;
	}
	oldNumUserWords = NumListItems(userList);
	Close(userDict);
}

/*
 *	Query to save user dictionary
 *	Save if response is yes
 */

void SaveUserDict(window)
WindowPtr window;
{
	register TextPtr word;
	register WORD item, len;
	register BPTR userDict;
	register BOOL error;
	register ListItemPtr listItem;
	DialogPtr saveDialog;
	TextChar fileName[120];

	if (NumListItems(userList) == oldNumUserWords)
		return;
/*
	Ask if user wants to save file
*/
	BeginWait();
	if ((saveDialog = GetDialog(dlgList[DLG_SAVEDICT], screen , mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return;
	}
	OutlineOKButton(saveDialog);
	StdBeep();
	do {
		item = ModalDialog(mainMsgPort, saveDialog, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON);
	DisposeDialog(saveDialog);
	EndWait();
	if (item == CANCEL_BUTTON)
		return;
/*
	Save the file
*/
	SetStdPointer(window, POINTER_WAIT);
	error = FALSE;
	SetPathName(fileName, strUserDictName, TRUE);
	if ((userDict = Open(fileName, MODE_NEWFILE)) == NULL)
		error = TRUE;
	else {
		for (listItem = userList->First; listItem; listItem = listItem->Next) {
			word = listItem->Text;
			len = strlen(word);
			if (Write(userDict, word, len) != len ||
				Write(userDict, "\n", 1)   != 1) {
				error = TRUE;
				break;
			}
		}
		Close(userDict);
	}
	if (error)
		Error(ERR_SAVE_DICT);
	else {
		SaveIcon(fileName, ICON_DICT);
		oldNumUserWords = NumListItems(userList);
	}
}

/*
 *	Compute differences between words
 */

static WORD DiffWord(word1, word2, len1, len2)
register TextPtr word1, word2;			/* Must be unsigned */
register WORD len1, len2;
{
	register WORD i, sum;
	BYTE charTable[256];				/* Must be signed */

	memset(charTable + 0x20, 0,  0x80 - 0x20);
	memset(charTable + 0xA0, 0, 0x100 - 0xA0);
	for (i = 0; i < len1; i++)
		charTable[*word1++]++;
	for (i = 0; i < len2; i++)
		charTable[*word2++]--;
	sum = 0;
	for (i = 0x20; i < 0x7F; i++)
		sum += ABS(charTable[i]);
	for (i = 0xA0; i < 0x100; i++)
		sum += ABS(charTable[i]);
	return (sum);
}

/*
 *	Determine if words are similar
 */

static BOOL SimilarWord(word1, word2, len1, len2)
TextPtr word1, word2;
WORD len1, len2;
{
	register WORD diff;

	diff = DiffWord(word1, word2, len1, len2);
	return ((BOOL) (diff <= 2 || diff <= len1/3));
}


/*
 *	Get suggested words list for given word
 */

void GetSuggestList(word, len)
TextPtr word;
WORD len;
{
	register TextChar ch;
	register TextPtr text;
	register WORD i;
	register ListItemPtr listItem;
	TextChar source[MAX_WORD_LEN], dictWord[MAX_WORD_LEN];

	if (IS_POSSESSIVE(word, len))
		len -= 2;			/* Remove possessive ending */
	if (len < 2)
		return;
/*
	Make source word all lower case (since SimilarWord() needs this)
*/
	for (i = 0; i < len; i++) {
		ch = word[i];
		source[i] = toLower[ch];
	}
/*
	Look for similar words and add to list
*/
	if (SeekWord(source)) {
		while ((i = ReadWord(dictWord)) > 0 &&
			   *source == *dictWord && *(source + 1) == *(dictWord + 1)) {
			if (SimilarWord(source, dictWord, len, i))
				AddWord(suggestList, dictWord, i);
		}
	}
	for (listItem = userList->First; listItem; listItem = listItem->Next) {
		text = listItem->Text;
		i = strlen(text);
		if (SimilarWord(source, text, len, i))
			AddWord(suggestList, text, i);
	}
/*
 *	Check for transposition of first two characters
 *	(All single character words are considered valid)
 */
	swmem(source, source + 1, 1);
	if (LookUpWord(source, len, FALSE))
		AddWord(suggestList, source, len);
	if (len > 2) {
		swmem(source, source + 1, 1);
		swmem(source + 1, source + 2, 1);
		if (LookUpWord(source, len, FALSE))
			AddWord(suggestList, source, len);
	}
	else {				/* len == 2 */
		AddWord(suggestList, source, 1);
		AddWord(suggestList, source + 1, 1);
	}
}

/*
 *	Remove all words from suggest list
 */

void DisposeSuggestWords()
{
	DisposeListItems(suggestList);
}

/*
 *	Return number of words in suggest list
 */

WORD NumSuggestWords()
{
	return ((WORD) NumListItems(suggestList));
}

/*
 *	Return pointer to specified suggest word
 */

TextPtr GetSuggestWord(item)
WORD item;
{
	return (GetListItem(suggestList, item));
}

/*
 *	Determine the closest word in the suggest list to the given word
 */

WORD BestSuggestWord(word, len)
TextPtr word;
WORD len;
{
	TextPtr newWord;
	register WORD i, numWords, best, diff, newDiff, newLen;

	if (IS_POSSESSIVE(word, len))
		len -= 2;			/* Remove possessive ending */
	best = 0;
	diff = 0x7FFF;
	numWords = NumSuggestWords();
	for (i = 0; i < numWords; i++) {
		newWord = GetSuggestWord(i);
		newLen = strlen(newWord);
		if (CompareText(word, newWord, len, newLen) == 0)	/* Exact match? */
			return (i);
		newDiff = DiffWord(word, newWord, len, newLen);
		if (newDiff < diff) {
			diff = newDiff;
			best = i;
		}
	}
	return (best);
}

/*
 *	Add word to skip list
 */

void AddSkipWord(word, len)
TextPtr word;
WORD len;
{
	AddWord(skipList, word, len);
}

/*
 *	Remove all words from skip list
 */

void DisposeSkipWords()
{
	DisposeListItems(skipList);
}

/*
 *	Add word to user list
 */

void AddUserWord(word, len)
TextPtr word;
WORD len;
{
	AddWord(userList, word, len);
}

/*
 *	Create common and cache word lists headers
 *	Return success status
 */

static BOOL CreateMiscLists()
{
	register WORD i;

	commonLists = NULL;
	if ((commonLists = MemAlloc(numLetters*sizeof(ListHeadPtr *), 0)) == NULL)
		return (FALSE);
	for (i = 0; i < numLetters; i++) {
		commonLists[i] = NULL;
	}
	for (i = 0; i < numLetters; i++) {
		if ((commonLists[i] = CreateList()) == NULL)
			return (FALSE);
	}

	return (TRUE);
}

/*
 *	Remove common and cache word lists
 */

static void DisposeMiscLists()
{
	register WORD i;

	if (commonLists) {
		for (i = 0; i < numLetters; i++) {
			if (commonLists[i])
				DisposeList(commonLists[i]);
		}
		MemFree(commonLists, numLetters*sizeof(ListHeadPtr *));
	}
	commonLists = NULL;
}

/*
 *	Initialize the spelling checker
 */

BOOL InitSpell(TextPtr dictName, Dir dir)
{
/*
	Check to see if already initialized
*/
	if (spellEnabled)
		return (TRUE);
/*
	Create lists
*/
	userList = skipList = suggestList = NULL;
	if ((userList = CreateList()) == NULL ||
		(skipList = CreateList()) == NULL ||
		(suggestList = CreateList()) == NULL) {
		DisposeList(userList);
		DisposeList(skipList);
		DisposeList(suggestList);
		return (FALSE);
	}
/*
	Open main dictionary and load user dictionary
*/
	if (!OpenMainDict(dictName, dir)) {
		mainDict = NULL;
		ShutDownSpell();
		return (FALSE);
	}
	LoadUserDict();
/*
	Allocate and load common word lists
	(Need to do this after openning main dict, since parameters are loaded from it)
*/
	if (!CreateMiscLists()) {
		ShutDownSpell();
		return (FALSE);
	}
	(void) LoadCommonWords();				/* Ignore failure */
	spellEnabled = TRUE;
	options.AutoSpell = FALSE;
	return (TRUE);
}

/*
 *	Change main dictionary
 *	Return success status
 */

BOOL ChangeMainDict(TextPtr dictName, Dir dir)
{
	BOOL oldAutoSpell, success;

/*
	First shut down old dictionary
*/
	oldAutoSpell = options.AutoSpell;
	ShutDownSpell();
/*
	Now open new one
*/
	success = InitSpell(dictName, dir);
	if (success)
		options.AutoSpell = oldAutoSpell;
	return (success);
}

/*
 *	Shut down the spelling checker
 *	If changes made to user dictionary then try to save those changes
 */

void ShutDownSpell()
{
	DisposeMiscLists();
	if (userList)
		DisposeList(userList);
	if (skipList)
		DisposeList(skipList);
	if (suggestList)
		DisposeList(suggestList);
	if (mainDict) {
		Close(mainDict);
		MemFree(wordIndexTable, numLetters*numLetters*sizeof(LONG));
	}
	userList = skipList = suggestList = NULL;
	mainDict = NULL;
	wordIndexTable = NULL;
	spellEnabled = options.AutoSpell = FALSE;
}
