/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Color definitions
 */

#ifndef TOOLBOX_COLOR_H
#define TOOLBOX_COLOR_H

typedef UWORD		RGBColor;
typedef RGBColor	*RGBColorPtr;

#define RED(rgb)	(((rgb) >> 8) & 0x0F)
#define GREEN(rgb)	(((rgb) >> 4) & 0x0F)
#define BLUE(rgb)	((rgb) & 0x0F)

#define RGBCOLOR(r,g,b)	(((r) << 8) + ((g) << 4) + (b))

#define RGBDARKEN(rgb)	RGBCOLOR(RED(rgb) >> 1, GREEN(rgb) >> 1, BLUE(rgb) >> 1)

#define RGBCOLOR_BLACK		0x000
#define RGBCOLOR_WHITE		0xFFF
#define RGBCOLOR_RED		0xF00
#define RGBCOLOR_YELLOW		0xFF0
#define RGBCOLOR_GREEN		0x0F0
#define RGBCOLOR_CYAN		0x0FF
#define RGBCOLOR_BLUE		0x00F
#define RGBCOLOR_MAGENTA	0xF0F

#define RGBCOLOR_TRANSPARENT	0xF000

typedef RGBColor	ColorTable[];
typedef ColorTable	*ColorTablePtr;

typedef UBYTE	PenNum, *PenNumPtr;

#endif
