/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	File save routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>

#include <proto/intuition.h>
#include <proto/icon.h>
#include <proto/dos.h>

#include <IFF/GIO.h>
#include <IFF/IFF.h>

#include <string.h>

#include "Flow.h"
#include "HEAD.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE	progPathName[];

extern UBYTE	fileBuff[];
extern LONG		iffError;

/*
 *	Local prototypes
 */

IFFP	PutOPTS(GroupContext *, DocDataPtr);
IFFP	PutDOC(GroupContext *, DocDataPtr);
IFFP	PutPAGE(GroupContext *, DocDataPtr);
IFFP	PutNEST(GroupContext *, WORD);
IFFP	PutHSTL(GroupContext *, WORD);
IFFP	PutHeadings(GroupContext *, HeadPtr);
IFFP	PutHINF(GroupContext *, HeadInfo *);	/* New for V3.0 */
IFFP	PutPREC(GroupContext *, DocDataPtr);	/* New for V3.0 */

/*
 *	Put OPTS chunk
 */

static IFFP PutOPTS(GroupContext *context, DocDataPtr docData)
{
	FileOptions opts;

	opts.IndentSpace = docData->Indent;
	opts.LabelStyle = docData->LabelStyle;
	opts.LabelFlags = docData->LabelFlags;
	opts.pad1 = 0;
	return (PutCk(context, ID_OPTS, sizeof(FileOptions), &opts));
}

/*
 * Put DOC chunk, new for V3.0
 */
 
static IFFP PutDOC(GroupContext *context, DocDataPtr docData)
{
	DocHdr docHdr;

	docHdr.StartPage = docData->StartPage;
	docHdr.PageNumStyle = docData->PageNumStyle;
	docHdr.DateStyle = docData->DateStyle;
	docHdr.TimeStyle = docData->TimeStyle;
	docHdr.TabChar = '.';
	docHdr.pad = 0;	
	return (PutCk(context, ID_DOC, sizeof(DocHdr), (BYTE *) &docHdr));
}

/*
 *	Put PAGE chunk
 */

static IFFP PutPAGE(GroupContext *context, DocDataPtr docData)
{
	FlowPageInfo pageInfo;

	pageInfo.PageWidth = docData->PageWidth;
	pageInfo.PageHeight = docData->PageHeight;
	pageInfo.PitchCPI = docData->CPI;
	pageInfo.SpacingLPI = docData->LPI;
	pageInfo.LeftMargin = docData->LeftMargin;
	pageInfo.RightMargin = docData->RightMargin;
	pageInfo.pad = 0;
	return (PutCk(context, ID_PAGE, sizeof(FlowPageInfo), (BYTE *) &pageInfo));
}

/*
 *	Put NEST chunk
 */

static IFFP PutNEST(GroupContext *context, WORD nest)
{
	NestLevel nestLevel;

	nestLevel.Nest = nest;
	return (PutCk(context, ID_NEST, sizeof(NestLevel), (BYTE *) &nestLevel));
}

/*
 *	Put HSTL chunk
 */

static IFFP PutHSTL(GroupContext *context, WORD style)
{
	HeadStyle headStyle;

	headStyle.Style = style;
	return (PutCk(context, ID_HSTL, sizeof(HeadStyle), (BYTE *) &headStyle));
}

/*
 * Put HINF chunk - New for V3.0
 * Supersedes NEST and HSTL structures.
 */
 
static IFFP PutHINF(GroupContext *context, HeadInfo *headInfo)
{
	return( PutCk(context, ID_HINF, sizeof(HeadInfo), (BYTE *) headInfo));
}

/*
 * Put PREC chunk - New for V3.0
 * Supersedes PAGE structure
 */
 
static IFFP PutPREC(GroupContext *context, DocDataPtr docData)
{
	PrintRecInfo printRecInfo;
	
	BlockMove(docData->PrintRec, &printRecInfo.PrintRec, sizeof(PrintRecord));
	return( PutCk(context, ID_PREC, sizeof(PrintRecInfo), (BYTE *) &printRecInfo));
}

/*
 *	Put TEXT chunk
 */

#define PutTEXT(context, text, len)		\
	PutCk(context, ID_TEXT, len, (BYTE *) text)

/*
 *	Put HEDR chunk
 */

#define PutHEDR(context, header)		\
	PutCk(context, ID_HEDR, strlen(header), (BYTE *) header)

/*
 *	Put FOTR chunk
 */

#define PutFOTR(context, footer)		\
	PutCk(context, ID_FOTR, strlen(footer), (BYTE *) footer)

/*
 * Put CLAB chunk
 */

#define PutCLAB(context, customLabel)  \
	PutCk(context, ID_CLAB, strlen(customLabel), (BYTE *) customLabel)

/*
 *	Put heading chunks
 */

static IFFP PutHeadings(GroupContext *context, register HeadPtr head)
{
	register IFFP iffp;
	register WORD newNest;
	register WORD start;
	HeadInfo headInfo;
	register BOOL newInfo;
	
	BlockClear( (Ptr) &headInfo, sizeof(HeadInfo));	
	headInfo.Style = FS_NORMAL;
	while( head != NULL ) {
		NextBusyPointer();
		newNest = HeadDepth(head);
		newInfo = FALSE;
		if( headInfo.Nest != newNest ) {
			newInfo = TRUE;
			headInfo.Nest = newNest;
			if( (iffp = PutNEST(context, headInfo.Nest)) != IFF_OKAY)
				break;
		}
		if( headInfo.Style != head->Style ) {
			newInfo = TRUE;
			headInfo.Style = head->Style;
			if( (iffp = PutHSTL(context, headInfo.Style)) != IFF_OKAY)
				break;
		}
		if( ( headInfo.Flags != head->Flags ) || newInfo ) {
			headInfo.Flags = head->Flags;
			headInfo.Reserved1 = 0;
			headInfo.Reserved2 = 0;
			if( (iffp = PutHINF(context, &headInfo)) != IFF_OKAY)
				break;
		}
		start = head->BodyStart;
		if ((iffp = PutTEXT(context, &head->Text[start], head->Len-start)) != IFF_OKAY)
			break;
		head = NextHeadAll(head);
	}
	return (iffp);
}

/*
 *	Save document in normal format
 *	Return success status
 */

BOOL SaveHEADFile(DocDataPtr docData, TextPtr fileName)
{
	register BOOL success = FALSE;
	register File file;
	GroupContext fileContext, formContext;

	iffError = DOS_ERROR;
/*
	Create a new file / Delete the file if it exists
*/
	if( (file = GOpen(fileName, MODE_NEWFILE)) != NULL) {
		
		(void) GWriteDeclare(file, fileBuff, FILEBUFF_SIZE);
/*
	Save the document
*/
		iffError = OpenWIFF(file, &fileContext, szNotYetKnown);
		if (iffError == IFF_OKAY)
			iffError = StartWGroup(&fileContext, FORM, szNotYetKnown, ID_HEAD, &formContext);
		if (iffError == IFF_OKAY)
			iffError = PutPAGE(&formContext, docData);
		if (iffError == IFF_OKAY)
			iffError = PutPREC(&formContext, docData);
		if (iffError == IFF_OKAY)
			iffError = PutOPTS(&formContext, docData);
		if (iffError == IFF_OKAY)
			iffError = PutDOC(&formContext, docData);
		if (iffError == IFF_OKAY && docData->Header[0] != '\0')
			iffError = PutHEDR(&formContext, docData->Header);
		if (iffError == IFF_OKAY && docData->Footer[0] != '\0')
			iffError = PutFOTR(&formContext, docData->Footer);
		if (iffError == IFF_OKAY )
			iffError = PutCLAB(&formContext, docData->CustomLabel);
		if (iffError == IFF_OKAY)
			iffError = PutHeadings(&formContext, docData->FirstHead);
		if (iffError == IFF_OKAY)
			iffError = EndWGroup(&formContext);
		if (iffError == IFF_OKAY)
			iffError = CloseWGroup(&fileContext);
		GClose(file);
		if (iffError == IFF_OKAY)
			success = TRUE;
	}
	return (success);
}

/*
 *	Save document in text-only format
 */

BOOL SaveTextFile(DocDataPtr docData, TextPtr fileName,BOOL headings)
{
	register WORD nest;
	register BOOL success = FALSE;
	register File file;
	register HeadPtr head;
	register WORD len;
	register WORD start;
		
	iffError = DOS_ERROR;
/*
	Create a new file / Delete existing file
*/
	if ((file = Open(fileName, MODE_NEWFILE)) != NULL) {
/*
	Save the document
*/
		success = TRUE;
		for (head = docData->FirstHead; head; head = NextHeadAll(head)) {
			for (nest = HeadDepth(head); nest > 0; nest--) {
				if (Write(file, "\t", 1) != 1) {
					success = FALSE;
					break;
				}
			}
			len = (headings) ? head->Len : head->Len - head->BodyStart;
			start = (headings) ? 0 : head->BodyStart; 
			if (Write(file, &head->Text[start], len) != len ||
				Write(file, "\n", 1) != 1) {
				success = FALSE;
				break;
			}
			NextBusyPointer();
		}
		Close(file);
		if (success)
			iffError = IFF_DONE;
	}
	return (success);
}

/*
 *	Make backup of specified file
 *	Return success status
 */

BOOL MakeBackup(TextPtr fileName)
{
	BPTR lock;
	TextChar iconName[50], backupFileName[50], backupIconName[50];

	strcpy(iconName, fileName);
	strcat(iconName, ".info");
	strcpy(backupFileName, fileName);
	strcat(backupFileName, ".old");
	strcpy(backupIconName, backupFileName);
	strcat(backupIconName, ".info");
	(void) DeleteFile(backupFileName);
	(void) DeleteFile(backupIconName);
	if ((lock = Lock(fileName, ACCESS_READ)) != NULL) {
		UnLock(lock);
		if (!Rename(fileName, backupFileName))
			return (FALSE);
	}
	if ((lock = Lock(iconName, ACCESS_READ)) != NULL) {
		UnLock(lock);
		if (!Rename(iconName, backupIconName))
			return (FALSE);
	}
	return (TRUE);
}

