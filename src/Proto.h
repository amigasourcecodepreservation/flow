/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global routine prototypes
 */

/*
 *	Main.c
 */

void	main(int, char **);
void	DoIntuiMessage(IntuiMsgPtr);
BOOL  DoMenu(WindowPtr, UWORD, UWORD, BOOL);
BOOL  DoCursorKey(WindowPtr, UWORD, UWORD);

/*
 *	Init.c
 */

void	Init(int, char **);
void	ShutDown(void);
void	SetUp(int, char **);

/*
 *	Monitor.c
 */

void	__saveds MonitorTask(void);

/*
 *	Project.c
 */

void	CheckAutoSave(void);
BOOL	DoNew(void);
BOOL	OpenFile(TextPtr, LONG);
BOOL	DoOpen(WindowPtr, UWORD, TextPtr );
BOOL	DoClose(WindowPtr);
BOOL	DoSave(WindowPtr, UWORD);
BOOL	DoSaveAs(WindowPtr);
BOOL	DoRevert(WindowPtr);
BOOL	DoPageSetup(WindowPtr);
BOOL	DoPrint(WindowPtr, UWORD, BOOL );
BOOL	DoProjectMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	Edit.c
 */

void	ClearPaste(void);

HeadPtr	CopyHead(DocDataPtr);
BOOL	PasteHead(DocDataPtr, HeadPtr);
BOOL	DeleteHead(DocDataPtr);

TextPtr	CopyText(DocDataPtr);
BOOL	PasteText(DocDataPtr, TextPtr);
BOOL	DeleteText(DocDataPtr);

BOOL	DoCut(WindowPtr);
BOOL	DoCopy(WindowPtr);
BOOL	DoPaste(WindowPtr);
BOOL	DoErase(WindowPtr);
void	DoSelectHead(WindowPtr);
BOOL	DoSelectAll(WindowPtr);
BOOL	DoEditMenu(WindowPtr, UWORD, UWORD, UWORD);

TextChar CellChar(WORD,WORD);
void	GetCellRect(DialogPtr,WORD,WORD,RectPtr);
void	DrawInsertItem(DialogPtr,WORD,WORD,BOOL);

/*
 *  Options.c
 */
 
BOOL  DoPreferences(void);
BOOL  DoScreenColors(void);
void  SetStdOptions(Options *,BOOL);

/*
 *	Search.c
 */

BOOL  SetFindOption(TextPtr, WORD);
void  SetFindButton(void);
void  SetChangeButtons(void);
void  OffChangeButtons(void);
BOOL	DoFind(TextPtr);
BOOL	DoFindNext(WindowPtr);
BOOL	DoChange(void);
BOOL  DoGoToPage(WindowPtr);
BOOL  WordSelected(DocDataPtr, TextPtr);
BOOL  FindText(TextPtr);
BOOL  ChangeText(TextPtr);
void  SetFindEnd(HeadPtr, WORD);
HeadPtr FindFirstPageLine(DocDataPtr, UWORD, UWORD *, UWORD *);
BOOL  DoSearchMenu(WindowPtr, UWORD, UWORD, UWORD);
void	RevealHead(DocDataPtr);
WORD	WordLen(HeadPtr, WORD);
WORD	SkipSpace(HeadPtr, WORD);
WORD	SkipWord(HeadPtr, WORD);

/*
 *	SubHead.c
 */

void	RecalibrateOutline(WindowPtr,HeadPtr);
BOOL	DoCollapse(WindowPtr);
BOOL	DoExpand(WindowPtr);
BOOL	DoExpandAll(WindowPtr);
BOOL	DoIndent(WindowPtr);
BOOL	DoUnIndent(WindowPtr);
BOOL	DoMoveUp(WindowPtr);
BOOL	DoMoveDown(WindowPtr);
BOOL	DoSort(WindowPtr, WORD);
BOOL  DoSplitHead(WindowPtr);
BOOL  DoJoinHead(WindowPtr);
BOOL  DoSubHeadMenu(WindowPtr, UWORD, UWORD);

/*
 *	Format.c
 */

BOOL	DoStyle(WindowPtr, UBYTE);
BOOL  DoLayout(WindowPtr);
void	DoIndentSpace(WindowPtr, UWORD);
BOOL  DoPageBreak(WindowPtr);
BOOL  DoLabel(WindowPtr);
BOOL	DoColors(WindowPtr);
BOOL	DoStyleMenu(WindowPtr, UWORD, UWORD);

/*
 *	Text.c
 */

BOOL	AdjustBuffer(HeadPtr, WORD);
BOOL  ExpandBuffer(HeadPtr, WORD, WORD);
BOOL  AddText(HeadPtr, TextPtr, WORD);
BOOL	DoDelete(WindowPtr, TextChar, UWORD);
BOOL	JoinHead(DocDataPtr);
void  UpdateChars(void);
void  AddChar(WindowPtr, TextChar);
BOOL  NewHead(WindowPtr, UWORD);
void  DocumentModified(DocDataPtr);
WORD  BackupSpaces(TextPtr, WORD, WORD);
WORD  AdvanceSpaces(TextPtr, WORD, WORD);
WORD  BackupSentence(TextPtr, WORD, WORD, WORD);
WORD  AdvanceSentence(TextPtr, WORD, WORD);
BOOL  CreateLabel(DocDataPtr, HeadPtr);
void  BumpLevel(DocDataPtr, HeadPtr, WORD);
void  RedoLabel(DocDataPtr, HeadPtr);
UWORD LabelDepth(DocDataPtr, HeadPtr);
UWORD	FindDepth(DocDataPtr, UWORD);
BOOL	BuildLevelTable(DocDataPtr, UWORD *, UWORD *);
void	RenumberHeads(DocDataPtr);

/*
 *	Head.c
 */

HeadPtr	NextHead(HeadPtr);
HeadPtr	NextHeadAll(HeadPtr);
HeadPtr	PrevHead(HeadPtr);

WORD		NumSubLines(HeadPtr);
WORD		HeadDepth(HeadPtr);
WORD		SubDepth(HeadPtr);
void		SwapHeads(HeadPtr);

HeadPtr	AllocHead(void);
void		DisposeHead(HeadPtr,DocDataPtr);
void		DisposeAllHeads(HeadPtr,DocDataPtr);

HeadPtr	DupHead(HeadPtr);
HeadPtr	DupHeadList(HeadPtr, HeadPtr);
HeadPtr	LastHead(DocDataPtr);

void		UnSelectHeads(DocDataPtr);
HeadPtr	LastSelHead(HeadPtr);

HeadPtr 	GetFirstHead(HeadPtr);

/*
 *	Clipboard.c
 */
void	InitClipboard(void);
void	ShutDownClipboard(void);
LONG	CurrentClipID(void);
void	GetClipboard(WindowPtr);
void	PutClipboard(void);

/*
 *	Window.c
 */

WindowPtr	OpenBackWindow(void);

void	SetWindowClip(WindowPtr);
void	SetHorizScrollClip(WindowPtr);

BOOL	IsDocWindow(WindowPtr);
WORD	WindowNum(WindowPtr);

WindowPtr	CreateWindow(TextPtr);
void	RemoveWindow(WindowPtr);

void	DoGoAwayWindow(WindowPtr, UWORD);
void	DoWindowActivate(WindowPtr, BOOL);
void	DoNewSize(WindowPtr);
void	DoWindowUpdate(WindowPtr);
void	UpdateWindows(void);
void 	RefreshWindow(WindowPtr);
void	DoWindowRefresh(WindowPtr);

void	SelectWindow(WindowPtr);
void	GetCmdWindow(void);
void	CauseUpdate(WindowPtr);

void	GetTextRect(WindowPtr,Rectangle *);
void 	RefreshWindows(void);

/*
 *	Menu.c
 */

void  SetProjectMenu(void);
void  SetEditMenu(void);
void  SetSearchMenu(void);
void  SetSubHeadMenu(void);
void  SetFormatMenu(void);
void	SetViewMenu(void);
void	SetMacroMenu(void);
void	SetAllMenus(void);
void  OnOffRevertMenuItem(void);

/*
 *	Display.c
 */

BOOL	CalcLineStarts(DocDataPtr, HeadPtr);
void	ReformatSubs(DocDataPtr, HeadPtr);
void  ReformatRestOfLevel(DocDataPtr, HeadPtr);
void	ReformatOutline(DocDataPtr);

HeadPtr	WhichHead(DocDataPtr, WORD);
WORD	WhichLine(HeadPtr, WORD);
WORD	LineNumber(DocDataPtr, HeadPtr, WORD);

void	CountLines(DocDataPtr);
LONG	VertPosition(WindowPtr, HeadPtr, WORD);
WORD	HorizPosition(WindowPtr, HeadPtr, WORD);

void	DrawText(WindowPtr, HeadPtr, WORD);
void	DrawHead(WindowPtr, HeadPtr);
void	DrawWindowRange(WindowPtr, WORD, WORD);
void	DrawWindow(WindowPtr);

/*
 *	Gadget.c
 */

LONG  VertOffset(DocDataPtr, WORD, WORD);
LONG  TopOffset(WindowPtr);
WORD  LeftOffset(WindowPtr);
void  MaxWindowOffsets(WindowPtr, WORD *, WORD *, WORD *);
WORD  PageNumber(DocDataPtr, UWORD);
void  SetPageIndic(WindowPtr, WORD);
void	AdjustScrollBars(WindowPtr);
void	ScrollToOffset(WindowPtr, WORD, WORD);
void	ScrollToLocation(WindowPtr, HeadPtr, WORD);
void	ScrollToCursor(WindowPtr);
void	AdjustToVertScroll(WindowPtr);
void	AdjustToHorizScroll(WindowPtr);
void	ScrollUp(WindowPtr);
void	ScrollDown(WindowPtr);
void	ScrollLeft(WindowPtr);
void	ScrollRight(WindowPtr);
void	ScrollWindow(WindowPtr, WORD);
void	DoGadgetDown(WindowPtr, GadgetPtr, UWORD);
void	DoGadgetUp(WindowPtr, GadgetPtr);
void  DoPageUp(WindowPtr, BOOL);
void  DoPageDown(WindowPtr, BOOL);
BOOL	InPageIndic(WindowPtr,WORD,WORD);
void	DoInPageIndic(WindowPtr);

/*
 *	Cursor.c
 */

void	DragLineOn(WindowPtr);
void	DragLineOff(WindowPtr);
void	CursorDrawOn(WindowPtr);
void	CursorBlink(WindowPtr);
void	CursorOn(WindowPtr);
void	CursorOff(WindowPtr);
void	CursorChangeOn(WindowPtr, WORD, WORD);
BOOL	DoCursorUp(WindowPtr, UWORD);
BOOL	DoCursorDown(WindowPtr, UWORD);
BOOL	DoCursorLeft(WindowPtr, UWORD);
BOOL	DoCursorRight(WindowPtr, UWORD);
void 	InvertRect(WindowPtr,WORD,WORD,WORD,WORD);
BOOL	DoCursorHeading(WindowPtr,UBYTE,UWORD);

/*
 *	Mouse.c
 */

void	SetSelRange(DocDataPtr, WORD);
void	DoMouseDown(WindowPtr, UWORD, WORD, WORD, ULONG, ULONG);
void 	ResetMultiClick(void);

/*
 *	Load.c
 */

void  SetPageParams(DocDataPtr);
BOOL	NewOutline(DocDataPtr);
void  DisposeAll(DocDataPtr);
BOOL	LoadHEADFile(DocDataPtr, File);
BOOL	LoadTextFile(DocDataPtr, File);
BOOL	LoadFile(WindowPtr, DocDataPtr, TextPtr, Dir);

/*
 *	Save.c
 */

BOOL	SaveHEADFile(DocDataPtr, TextPtr);
BOOL	SaveTextFile(DocDataPtr, TextPtr,BOOL);
BOOL	MakeBackup(TextPtr);

/*
 *	PrHandler.c
 */

void	InitPrintHandler(void);
void	PrValidate(PrintRecPtr);
void	PrintDefault(PrintRecPtr);
BOOL	PageSetupDialog(WindowPtr, PrintRecPtr);
BOOL	PrintDialog(WindowPtr, PrintRecPtr);
BOOL	SetPrintOption(PrintRecPtr, TextPtr, WORD);

/*
 *	Print.c
 */

BOOL  GetSysPrefs(void);
void  GetPrinterType(void);
BOOL	PrintOutline(WindowPtr,DocDataPtr);
BOOL	AdvanceLines(File, WORD);
BOOL	PrintSpaces(File, WORD);
/*BOOL  PrintError(void);*/

/*
 *	REXX.c
 */

void	InitRexx(void);
void	ShutDownRexx(void);
void	DoRexxMsg(struct RexxMsg *);
void	DoMacro(TextPtr,TextPtr);
BOOL	DoMacroMenu(WindowPtr, UWORD, UWORD);
void	DoAutoExec(void);
void	LoadFKeys(void);

/*
 *	Icon.c
 */

void	InitIcons(void);
void	ShutDownIcons(void);
void	DoAppMsg(struct AppMessage *);
void	SaveIcon(TextPtr, WORD);

/*
 *	Error.c
 */

void	Error(WORD);
void	FixTitle(void);
void	DOSError(WORD, LONG);
void  InfoDialog(TextPtr, WORD);
BOOL	DoHelp(WindowPtr);

/*
 *	Misc.c
 */

void	SetPointerShape(void);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);
void  OutlineOKButton(WindowPtr);
void  DrawArrowBorder(WindowPtr, WORD);
void	BeginWait(void);
void	EndWait(void);

void  SetBusyPointer(WindowPtr);
void  NextBusyPointer(void);

void	ErrBeep(void);
void  StdBeep(void);

void	BltImage(RastPtr, ImagePtr, WORD, WORD, WORD);
BOOL	CheckNumber(TextPtr);
void	CopyStringBuffer(GadgetPtr, WORD, TextPtr);

WORD	CompareText(TextPtr, TextPtr, WORD, WORD);
BOOL	DevMounted(TextPtr);
BOOL	DirAssigned(TextPtr);
void  DecipointToText(WORD, TextPtr, WORD);
WORD  TextToDecipoint(TextPtr);
WORD  GetFracValue(GadgetPtr, WORD);

WORD  MakeRomanNum(WORD, TextPtr, TextChar, TextChar, TextChar);
TextPtr  AppendNumberStyle(WORD, TextPtr, TextChar);

UWORD	Random(UWORD);

void	DrawColorBorder(WindowPtr, GadgetPtr);
void	GetNumAcrossDown(WORD, WORD *, WORD *);
void	ToggleCheckbox(BOOL *, WORD, WindowPtr);
WORD	StdDialog(WORD);
void 	DrawColorItem(DialogPtr,WORD,BOOL,UWORD);

/*
 *	Memory.c
 */

Ptr		NewPtr(LONG);
void	DisposePtr(Ptr);
LONG	GetPtrSize(Ptr);

/*
 * View.c
 */

void  AddWindowItem(WindowPtr);
void  RemoveWindowItem(WindowPtr);
void  ChangeWindowItem(WindowPtr);
BOOL  DoViewMenu(WindowPtr,UWORD,UWORD);

/*
 * Dialog.c
 */

void  DoDialogIdle(WindowPtr);
void	DoDialogItem(WindowPtr, IntuiMsgPtr, WORD);
void  DoDialogGadgetDown(WindowPtr, IntuiMsgPtr);
void  DoDialogGadgetUp(WindowPtr, IntuiMsgPtr);
void  DoDialogActivate(WindowPtr, BOOL);
void	DoDialogOpen(WindowPtr);
void  DoDialogClose(WindowPtr);
void  DoDialogCloseAll(void);

/*
 * Spell.c
 */

void	DoAutoSpell(WindowPtr);
BOOL	DoSpellingMenu(WindowPtr, UWORD, UWORD);
void	OffSpellButtons(void);
void	SetSpellButtons(void);
void	SetSpellRange(DocDataPtr);
void	NewWord(WORD);
void	SetSuspectWord(TextPtr, WORD);
void	SetEditWord(TextPtr, WORD);
void	ShowSuggestList(void);
WORD	CheckSpelling(void);
void	ActivateSpellDialog(void);
void	DoSpellDialogItem(WORD);
void	DoSpellDialogKey(WindowPtr, UWORD);

/*
 * SpellEngine.c
 */

BOOL	IsSpellWordChar(TextChar);
BOOL	LookUpWord(TextPtr, WORD, BOOL);
void	SaveUserDict(WindowPtr);
void	GetSuggestList(TextPtr, WORD);
void	DisposeSuggestWords(void);
WORD	NumSuggestWords(void);
TextPtr	GetSuggestWord(WORD);
void	AddSkipWord(TextPtr, WORD);
void	DisposeSkipWords(void);
void	AddUserWord(TextPtr, WORD);
WORD	BestSuggestWord(TextPtr, WORD);
BOOL	InitSpell(TextPtr, Dir);
BOOL	ChangeMainDict(TextPtr, Dir);
void	ShutDownSpell(void);

/*
 * Color.c
 */

void	CheckColorTable(void);
void 	BuildDefaultColorTable(ColorTablePtr);

/*
 *	BuffIO.c
 */

void	ClearBuff(void);
WORD	GetByte(File);
WORD	GetNibble(File);
WORD	GetWord(File);

/*
 *  Prefs.c
 */
 
void SetPathName(TextPtr,TextPtr,BOOL);
BOOL SaveProgPrefs(WindowPtr,TextPtr);
void GetProgPrefs(void);
BOOL ReadProgPrefs(TextPtr);
BOOL IsPrefsFile(TextPtr);

/*
 *	PostScript.c
 */

TextPtr	PSDeviceName(void);

BOOL	PSBeginJob(PrintRecPtr, TextPtr);
BOOL	PSEndJob(PrintRecPtr);

BOOL	PSBeginPage(PrintRecPtr, WORD);
BOOL	PSEndPage(PrintRecPtr);

BOOL	PSSelectFont(PrintRecPtr, WORD, WORD, WORD);
BOOL	PSText(PrintRecPtr, WORD, WORD, WORD, WORD, UBYTE, TextPtr, WORD,WORD);

void	PostScriptOptsDialog(void);
  