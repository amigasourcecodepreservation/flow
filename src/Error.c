/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Error/Help report routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Image.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern MsgPort		rexxMsgPort;
extern struct RexxLib	*RexxSysBase;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;

extern BOOL			titleChanged;
extern TextChar	screenTitle[];

extern TextPtr		strsErrors[];

extern TextChar	strDemo[], strBadFile[], strMemory[];
extern TextChar	strDOSError[], strFileNotFound[], strDiskLocked[];
extern TextChar	strFileNoDelete[], strFileNoRead[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define ERROR_TEXT		1
#define ERROR_DOSNUM		2
#define ERROR_ICON		3

#define ABOUT_CHIPMEM	1
#define ABOUT_FASTMEM	2
#define ABOUT_AREXX		3

/*
 *	Error report routine
 *	Put error message in dialog, or if no memory, on screen title bar
 */

void Error(WORD errNum)
{
	register WORD item;
	register WORD num;
	DialogPtr dlg;
	
	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = strsErrors[errNum];
	BeginWait();
	if( (dlg = GetDialog( dlgList[DLG_ERROR], screen, mainMsgPort)) != NULL ) {
		OutlineOKButton( dlg ) ;
		ErrBeep();
		do {
			item = ModalDialog( mainMsgPort, dlg, DialogFilter );
		} while( item != OK_BUTTON ) ;
		DisposeDialog( dlg ) ;
	} else {
		SetWindowTitles(backWindow, (TextPtr) -1, strsErrors[errNum]);
		for (num = 0; num < numWindows; num++)
			SetWindowTitles(windowList[num], (TextPtr) -1, strsErrors[errNum]);
		ErrBeep();
		titleChanged = TRUE;
	}
	EndWait();
}

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	register WORD num;

	SetWindowTitles(backWindow, (TextPtr) -1, screenTitle);
	for (num = 0; num < numWindows; num++)
		SetWindowTitles(windowList[num], (TextPtr) -1, screenTitle);
	titleChanged = FALSE;
}

/*
 *	Display DOS error message dialog in specified window
 *	If not enough memory, then use Error() routine
 *	If dosError is 0, then cause was not enough memory
 */

void DOSError( WORD errNum, LONG dosError)
{
	register TextPtr errText;
	
	switch( dosError ) {
	case -2:
		errText = strDemo;
		break;
	case -1:
		errText = strBadFile;
		break;
	case 0:
	case ERROR_NO_FREE_STORE:
		errText = strsErrors[ERR_NO_MEM];
		break;
	case ERROR_OBJECT_NOT_FOUND:
	case ERROR_DIR_NOT_FOUND:
	case ERROR_DEVICE_NOT_MOUNTED:
	case ERROR_OBJECT_WRONG_TYPE:
		errText = strFileNotFound;
		break;
	case ERROR_DELETE_PROTECTED:
		errText = strFileNoDelete;
		break;
	case ERROR_DISK_WRITE_PROTECTED:
		errText = strDiskLocked;
		break;
	case ERROR_READ_PROTECTED:
		errText = strFileNoRead;
		break;
	default:
		if( dosError >= 0 && dosError <= 999 ) {
			NumToString(dosError, strDOSError + strlen(strDOSError) - 3);
			errText = strDOSError;
		} else
			errText = strsErrors[ERR_UNKNOWN];
		break;
	}
	dlgList[DLG_ERROR]->Gadgets[ERROR_DOSNUM].Info = errText;
	Error(errNum);
	dlgList[DLG_ERROR]->Gadgets[ERROR_DOSNUM].Info = NULL;
}

/*
 *	Show information dialog with number parameter
 *	Replace first '#' character with actual number value
 */

void InfoDialog(TextPtr text, WORD num)
{
	register WORD i;
	TextChar buff[50];

	for (i = 0; *text && *text != '#'; i++)
		buff[i] = *text++;
	if (*text == '#') {
		NumToString(num, buff + i);
		strcat(buff, ++text);
	}
	else
		buff[i] = '\0';
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = buff;
	dlgList[DLG_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_NOTE;
	(void) StdDialog(DLG_ERROR);
	dlgList[DLG_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_STOP;
}

/*
 *	Display help dialog in specified window
 *	If cannot show full help dialog, just show memory info
 */

BOOL DoHelp(WindowPtr window)
{
	register DialogPtr dlg;
	TextChar chipMem[10], fastMem[10];
	register BOOL success = FALSE;
	
	BeginWait();
	NumToString(AvailMem(MEMF_CHIP)/1024, chipMem);
	NumToString(AvailMem(MEMF_FAST)/1024, fastMem);
	strcat(chipMem, "K");
	strcat(fastMem, "K");
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_CHIPMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_CHIPMEM].Info = chipMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_FASTMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_FASTMEM].Info = fastMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_AREXX].Info =
		(RexxSysBase) ? rexxMsgPort.mp_Node.ln_Name : "----";
	if (((dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) == NULL ) &&
		((dlg = GetDialog(dlgList[DLG_MEMORY], screen, mainMsgPort)) == NULL )) {
		Error(ERR_NO_MEM);
	} else {
		success = TRUE;
		OutlineOKButton(dlg);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
	EndWait();
	return(success);
}
