/*
 *	ProWrite
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Buffered I/O routines
 *	Get routines return byte/nibble or -1 if EOF/error
 *	MUST call ClearBuff() before using routines, or after Seek()
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/dos.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE	fileBuff[];

/*
 *	Local variables and definitions
 */

static BOOL		needByte;
static WORD		buffPtr, buffSize, byteBuff;

void ClearBuff()
{
	needByte = TRUE;
	buffPtr = buffSize = FILEBUFF_SIZE;
}

WORD GetByte(File file)		/* Always gets from a byte boundary */
{
	needByte = TRUE;
	if (buffPtr == buffSize) {
		if ((buffSize = Read(file, fileBuff, FILEBUFF_SIZE)) <= 0)
			return (-1);
		buffPtr = 0;
	}
	return ((WORD) fileBuff[buffPtr++]);
}

#define NEXT_BYTE(file)	((buffPtr == buffSize) ? GetByte(file) : fileBuff[buffPtr++])

WORD GetNibble(File file)
{
	if (needByte) {
		if ((byteBuff = NEXT_BYTE(file)) == -1)
			return (-1);
		needByte = FALSE;
		return ((WORD) ((byteBuff >> 4) & 0x0F));
	}
	needByte = TRUE;
	return ((WORD) (byteBuff & 0x0F));
}

WORD GetWord(File file)
{
	return ((WORD) ((GetByte(file) << 8) | GetByte(file)));
}
