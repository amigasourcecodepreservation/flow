/*
 *	ProWrite
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	View menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow, windowList[];
extern WORD			numWindows;

extern DlgTemplPtr	dlgList[];
extern DialogPtr	dlgWindow;
extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;


/*
 *	Add window to window list and view menu
 */

void AddWindowItem(WindowPtr window)
{
	UWORD item;
	MenuItemPtr menuItem;
	TextChar title[100];

	GetWTitle(window, title);
	item = MENUITEM(VIEW_MENU, numWindows + WINDOW_ITEM, NOSUB);
	InsertMenuItem(backWindow, item, title);
	menuItem = ItemAddress(backWindow->MenuStrip, item);
	menuItem->Flags |= CHECKIT;
	menuItem->MutualExclude = ~(1 << numWindows) << WINDOW_ITEM;
	windowList[numWindows++] = window;
}

/*
 *	Remove window from window list and view menu
 */

void RemoveWindowItem(WindowPtr window)
{
	register WORD i;
	MenuItemPtr menuItem;

	if ((i = WindowNum(window)) == -1)
		return;
	DeleteMenuItem(backWindow, (UWORD) MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB));
	numWindows--;
	while (i < numWindows) {
		windowList[i] = windowList[i + 1];
		i++;
	}
	for (i = 0; i < numWindows; i++) {
		menuItem = ItemAddress(backWindow->MenuStrip, MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB));
		menuItem->MutualExclude = ~(1 << i) << WINDOW_ITEM;
	}
}

/*
 *	Change window name in view menu
 */

void ChangeWindowItem(WindowPtr window)
{
	WORD i;
	TextChar title[100];

	if ((i = WindowNum(window)) == -1)
		return;
	GetWTitle(window, title);
	SetMenuItem(window, (WORD) MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB), title);
}

/*
 *	Handle View menu command
 */

BOOL DoViewMenu(window, item, sub)
register WindowPtr window;
register UWORD item, sub;
{
	register WORD i;
	register BOOL success;

	if( item == ABOUT_ITEM )
		success = DoHelp(window);
	else {
		i = item - WINDOW_ITEM;
		if (i >= 0 && i < numWindows)
			SelectWindow(windowList[i]);
		success = TRUE;
	}
	SetViewMenu();
	return(success);
}
