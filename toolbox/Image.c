/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Image routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <TypeDefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/ColorPick.h>

#include <proto/exec.h>
#include <proto/intuition.h>

/*
 *	External variables
 */

extern ScreenPtr	_tbScreen;

extern WORD	_tbXSize, _tbYSize;

extern UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight;

/*
 *	Stop icon
 */

static UWORD stopIconColors[] = {
	0xFFF, 0xF60, 0x000, 0x000
};

static ULONG chip stopIconData[] = {
	0x007FFE00, 0x00C00300, 0x01BFFD80, 0x037FFEC0,
	0x06FFFF60, 0x0DFFFFB0, 0x1BFFFFD8, 0x37FFFFEC,
	0x6FFFFFF6, 0xDFFFFFFB, 0xBFFFFFFD, 0xBFFFFFFD,
	0xBFFFFFFD, 0xBFFFFFFD, 0xBFFFFFFD, 0xBFFFFFFD,
	0xBFFFFFFD, 0xBFFFFFFD, 0xBFFFFFFD, 0xBFFFFFFD,
	0xBFFFFFFD, 0xBFFFFFFD, 0xDFFFFFFB, 0x6FFFFFF6,
	0x37FFFFEC, 0x1BFFFFD8, 0x0DFFFFB0, 0x06FFFF60,
	0x037FFEC0, 0x01BFFD80, 0x00C00300, 0x007FFE00,

	0x007FFE00, 0x00C00300, 0x01800180, 0x030000C0,
	0x06000060, 0x0C000030, 0x18000018, 0x3001800C,
	0x6003C006, 0xC003C003, 0x8003C001, 0x8003C001,
	0x8003C001, 0x8003C001, 0x8003C001, 0x8003C001,
	0x8003C001, 0x8003C001, 0x80018001, 0x80018001,
	0x80018001, 0x80000001, 0xC0018003, 0x6003C006,
	0x3001800C, 0x18000018, 0x0C000030, 0x06000060,
	0x030000C0, 0x01800180, 0x00C00300, 0x007FFE00,
};

static ULONG chip stopIconMask[] = {
	0x007FFE00, 0x00FFFF00, 0x01FFFF80, 0x03FFFFC0,
	0x07FFFFE0, 0x0FFFFFF0, 0x1FFFFFF8, 0x3FFFFFFC,
	0x7FFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x7FFFFFFE,
	0x3FFFFFFC, 0x1FFFFFF8, 0x0FFFFFF0, 0x07FFFFE0,
	0x03FFFFC0, 0x01FFFF80, 0x00FFFF00, 0x007FFE00,
};

static Icon stopIcon = {
	ICON_WIDTH, ICON_HEIGHT, 2,
	&stopIconColors[0],
	(PLANEPTR) &stopIconData[0], (PLANEPTR) &stopIconMask[0]
};

/*
 *	Caution icon
 */

static UWORD cautionIconColors[] = {
	0xFFF, 0xF60, 0x000, 0x000
};

static ULONG chip cautionIconData[] = {
	0x00018000, 0x0003C000, 0x00066000, 0x000DB000,
	0x001BD800, 0x0037EC00, 0x006FF600, 0x00DFFB00,
	0x01BFFD80, 0x037FFEC0, 0x06FFFF60, 0x0DFFFFB0,
	0x1BFFFFD8, 0x37FFFFEC, 0x6FFFFFF6, 0xDFFFFFFB,
	0xDFFFFFFB, 0x6FFFFFF6, 0x37FFFFEC, 0x1BFFFFD8,
	0x0DFFFFB0, 0x06FFFF60, 0x037FFEC0, 0x01BFFD80,
	0x00DFFB00, 0x006FF600, 0x0037EC00, 0x001BD800,
	0x000DB000, 0x00066000, 0x0003C000, 0x00018000,

	0x00018000, 0x0003C000, 0x00066000, 0x000C3000,
	0x00181800, 0x00300C00, 0x00600600, 0x00C3C300,
	0x018FF180, 0x031FF8C0, 0x061E7860, 0x0C383C30,
	0x18383C18, 0x30003C0C, 0x60007806, 0xC0007803,
	0xC000F003, 0x6001E006, 0x3003C00C, 0x1803C018,
	0x0C018030, 0x06000060, 0x030180C0, 0x0183C180,
	0x00C18300, 0x00600600, 0x00300C00, 0x00181800,
	0x000C3000, 0x00066000, 0x0003C000, 0x00018000,
};

static ULONG chip cautionIconMask[] = {
	0x00018000, 0x0003C000, 0x0007E000, 0x000FF000,
	0x001FF800, 0x003FFC00, 0x007FFE00, 0x00FFFF00,
	0x01FFFF80, 0x03FFFFC0, 0x07FFFFE0, 0x0FFFFFF0,
	0x1FFFFFF8, 0x3FFFFFFC, 0x7FFFFFFE, 0xFFFFFFFF,
	0xFFFFFFFF, 0x7FFFFFFE, 0x3FFFFFFC, 0x1FFFFFF8,
	0x0FFFFFF0, 0x07FFFFE0, 0x03FFFFC0, 0x01FFFF80,
	0x00FFFF00, 0x007FFE00, 0x003FFC00, 0x001FF800,
	0x000FF000, 0x0007E000, 0x0003C000, 0x00018000,
};

static Icon cautionIcon = {
	ICON_WIDTH, ICON_HEIGHT, 2,
	&cautionIconColors[0],
	(PLANEPTR) &cautionIconData[0], (PLANEPTR) &cautionIconMask[0]
};

/*
 *	Note icon
 */

static UWORD noteIconColors[] = {
	0xFFF, 0xBB0, 0x000, 0x000
};

static ULONG chip noteIconData[] = {
	0x00018000, 0x0003C000, 0x00066000, 0x000DB000,
	0x001BD800, 0x0037EC00, 0x006FF600, 0x00DFFB00,
	0x01BFFD80, 0x037FFEC0, 0x06FFFF60, 0x0DFFFFB0,
	0x1BFFFFD8, 0x37FFFFEC, 0x6FFFFFF6, 0xDFFFFFFB,
	0xDFFFFFFB, 0x6FFFFFF6, 0x37FFFFEC, 0x1BFFFFD8,
	0x0DFFFFB0, 0x06FFFF60, 0x037FFEC0, 0x01BFFD80,
	0x00DFFB00, 0x006FF600, 0x0037EC00, 0x001BD800,
	0x000DB000, 0x00066000, 0x0003C000, 0x00018000,

	0x00018000, 0x0003C000, 0x00066000, 0x000C3000,
	0x00181800, 0x00300C00, 0x00600600, 0x00C00300,
	0x01800180, 0x030180C0, 0x06018060, 0x0C199830,
	0x181DB818, 0x300FF00C, 0x6007E006, 0xC07FFE03,
	0xC07FFE03, 0x6007E006, 0x300FF00C, 0x181DB818,
	0x0C199830, 0x06018060, 0x030180C0, 0x01800180,
	0x00C00300, 0x00600600, 0x00300C00, 0x00181800,
	0x000C3000, 0x00066000, 0x0003C000, 0x00018000,
};

static ULONG chip noteIconMask[] = {
	0x00018000, 0x0003C000, 0x0007E000, 0x000FF000,
	0x001FF800, 0x003FFC00, 0x007FFE00, 0x00FFFF00,
	0x01FFFF80, 0x03FFFFC0, 0x07FFFFE0, 0x0FFFFFF0,
	0x1FFFFFF8, 0x3FFFFFFC, 0x7FFFFFFE, 0xFFFFFFFF,
	0xFFFFFFFF, 0x7FFFFFFE, 0x3FFFFFFC, 0x1FFFFFF8,
	0x0FFFFFF0, 0x07FFFFE0, 0x03FFFFC0, 0x01FFFF80,
	0x00FFFF00, 0x007FFE00, 0x003FFC00, 0x001FF800,
	0x000FF000, 0x0007E000, 0x0003C000, 0x00018000,
};

static Icon noteIcon = {
	ICON_WIDTH, ICON_HEIGHT, 2,
	&noteIconColors[0],
	(PLANEPTR) &noteIconData[0], (PLANEPTR) &noteIconMask[0]
};

/*
 *	Local prototypes
 */

#define SCALE(x, srcWidth, dstWidth)	\
	(((LONG) (x)*(dstWidth - 1) + (srcWidth - 1)/2)/(srcWidth - 1))

ImagePtr	ArrowImage(WORD, WORD, WORD, BOOL);

/*
 *	NewImage
 *	Create image and image data structures
 */

ImagePtr NewImage(WORD width, WORD height, WORD depth)
{
	WORD i;
	ImagePtr image;

	if (width == 0 || height == 0 || depth == 0)
		return (NULL);
	if ((image = MemAlloc(sizeof(Image), MEMF_CLEAR)) == NULL)
		return (NULL);
	image->Width  = width;
	image->Height = height;
	image->Depth  = depth;
	if ((image->ImageData = (UWORD *) AllocRaster(width, height*depth)) == NULL) {
		MemFree(image, sizeof(Image));
		return (NULL);
	}
	for (i = 0; i < depth; i++)
		image->PlanePick |= (1 << i);
	return (image);
}

/*
 *	FreeImage
 *	Free memory allocated by NewImage()
 */

void FreeImage(ImagePtr image)
{
	register ImagePtr nextImage;

	WaitBlit();					/* Make sure image data is not in use */
	while (image) {
		nextImage = image->NextImage;
		if (image->ImageData)
			FreeRaster((PLANEPTR) image->ImageData, image->Width, image->Height*image->Depth);
		MemFree(image, sizeof(Image));
		image = nextImage;
	}
}

/*
 *	AppendImage
 *	Append image to end of another image
 */

void AppendImage(ImagePtr image1, ImagePtr image2)
{
	while (image1->NextImage)
		image1 = image1->NextImage;
	image1->NextImage = image2;
}

/*
 *	MakeIconImage
 *	Create image from icon
 *	If width or height are 0, scale icon's width or height to screen
 */

ImagePtr MakeIconImage(register IconPtr icon, WORD width, WORD height, WORD boxType)
{
	register WORD x1, y1, x2, y2, pen;
	WORD i, planeSize, depth, numSrcPens, numDstPens;
	BOOL hasMask;
	ImagePtr image;
	RastPort rPort1, rPort2;
	BitMap bitMap1, bitMap2;
	PLANEPTR imageData;
	UBYTE penTable[256];

	if (width == 0)
		width  = (icon->Width*_tbXSize)/8;
	if (height == 0)
		height = (icon->Height*_tbYSize)/11;
/*
	Initialize source bitMap and rPort
*/
	hasMask = (icon->Mask != NULL);
	depth = icon->Depth;
	if (hasMask)
		depth++;
	InitBitMap(&bitMap1, depth, icon->Width, icon->Height);
	InitRastPort(&rPort1);
	rPort1.BitMap = &bitMap1;
	imageData = icon->Data;
	planeSize = RASSIZE(icon->Width, icon->Height);
	for (i = 0; i < icon->Depth; i++)
		bitMap1.Planes[i] = imageData + i*planeSize;
	if (hasMask)
		bitMap1.Planes[icon->Depth] = icon->Mask;
/*
	Allocate image data
*/
	depth = _tbScreen->RastPort.BitMap->Depth;
	if ((image = NewImage(width, height, depth)) == NULL)
		return (NULL);
/*
	Initialize destination bitMap and rPort
*/
	InitBitMap(&bitMap2, depth, width, height);
	InitRastPort(&rPort2);
	rPort2.BitMap = &bitMap2;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE(width, height);
	for (i = 0; i < depth; i++)
		bitMap2.Planes[i] = imageData + i*planeSize;
/*
	Make pen number translation table
*/
	numSrcPens = 1 << icon->Depth;
	numDstPens = 1 << _tbScreen->RastPort.BitMap->Depth;
	for (i = 0; i < numSrcPens; i++)
		penTable[i] = PenNumber(icon->ColorTable[i], _tbScreen->ViewPort.ColorMap,
								numDstPens);
/*
	Draw scaled icon
*/
	if (width == icon->Width && height == icon->Height &&
		icon->Depth == 1 && !hasMask) {
		SetAPen(&rPort2, penTable[1]);
		SetBPen(&rPort2, penTable[0]);
		SetDrMd(&rPort2, JAM2);
		BltTemplate(bitMap1.Planes[0], 0, ((width + 15) >> 3) & 0xFFFE, &rPort2, 0, 0,
					width, height);
	}
	else {
		SetRast(&rPort2, _tbPenLight);
		for (y2 = 0; y2 < height; y2++) {
			for (x2 = 0; x2 < width; x2++) {
				x1 = (width == icon->Width)   ? x2 : SCALE(x2, width,  icon->Width);
				y1 = (height == icon->Height) ? y2 : SCALE(y2, height, icon->Height);
				pen = ReadPixel(&rPort1, x1, y1);
				if (hasMask) {
					if ((pen & numSrcPens) == 0)
						continue;
					pen &= ~numSrcPens;
				}
				SetAPen(&rPort2, penTable[pen]);
				WritePixel(&rPort2, x2, y2);
			}
		}
	}
	if (boxType == ICON_BOX_SHADOWOUT || boxType == ICON_BOX_SHADOWIN)
		DrawShadowBox(&rPort2, 0, 0, width, height, 0, (boxType == ICON_BOX_SHADOWOUT));
	return (image);
}

/*
 *	Create arrow image
 *	If width or height are 0, use default width or height
 */

static ImagePtr ArrowImage(WORD id, WORD width, WORD height, BOOL selected)
{
	register WORD i, planeSize, depth, numPts;
	ImagePtr image;
	RastPort rPort;
	BitMap bitMap;
	PLANEPTR imageData;
	Point ptList[4];

	if (width == 0)
		width  = ARROW_WIDTH;
	if (height == 0)
		height = ARROW_HEIGHT;
/*
	Allocate image data
*/
	depth = _tbScreen->RastPort.BitMap->Depth;
	if ((image = NewImage(width, height, depth)) == NULL)
		return (NULL);
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE(width, height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw into rPort
*/
	SetRast(&rPort, (selected) ? _tbPenDark : _tbPenLight);
	switch (id) {
	case IMAGE_ARROW_UP:
		numPts = 4;
		ptList[0].x = 9;	ptList[0].y = 2;
		ptList[1].x = 14;	ptList[1].y = 7;
		ptList[2].x = 3;	ptList[2].y = 7;
		ptList[3].x = 8;	ptList[3].y = 2;
		break;
	case IMAGE_ARROW_DOWN:
		numPts = 4;
		ptList[0].x = 9;	ptList[0].y = 7;
		ptList[1].x = 14;	ptList[1].y = 2;
		ptList[2].x = 3;	ptList[2].y = 2;
		ptList[3].x = 8;	ptList[3].y = 7;
		break;
	case IMAGE_ARROW_LEFT:
		numPts = 3;
		ptList[0].x = 12;	ptList[0].y = 2;
		ptList[1].x = 5;	ptList[1].y = 5;
		ptList[2].x = 12;	ptList[2].y = 8;
		break;
	case IMAGE_ARROW_RIGHT:
		numPts = 3;
		ptList[0].x = 5;	ptList[0].y = 2;
		ptList[1].x = 12;	ptList[1].y = 5;
		ptList[2].x = 5;	ptList[2].y = 8;
		break;
	}
	if (width != ARROW_WIDTH || height != ARROW_HEIGHT) {
		for (i = 0; i < numPts; i++) {
			ptList[i].x = SCALE(ptList[i].x, ARROW_WIDTH, width);
			ptList[i].y = SCALE(ptList[i].y, ARROW_HEIGHT, height);
		}
	}
	SetAPen(&rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	FillPoly(&rPort, numPts, ptList);
	DrawShadowBox(&rPort, 0, 0, width, height, 0, !selected);
	return (image);
}

/*
 *	GetStdImage
 *	Return standard image scaled to given width and height
 */

ImagePtr GetStdImage(WORD id, WORD width, WORD height, BOOL selected)
{
	ImagePtr image;
	IconPtr icon;

	switch (id) {
	case IMAGE_ARROW_UP:
	case IMAGE_ARROW_DOWN:
	case IMAGE_ARROW_LEFT:
	case IMAGE_ARROW_RIGHT:
		image = ArrowImage(id, width, height, selected);
		break;
	case IMAGE_ICON_STOP:
	case IMAGE_ICON_CAUTION:
	case IMAGE_ICON_NOTE:
		if (id == IMAGE_ICON_STOP)
			icon = &stopIcon;
		else if (id == IMAGE_ICON_CAUTION)
			icon = &cautionIcon;
		else
			icon = &noteIcon;
		image = MakeIconImage(icon, width, height, ICON_BOX_NONE);
		break;
	default:
		image = NULL;
		break;
	}
	return (image);
}
