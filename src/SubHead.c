/*
 * Flow
 * Copyright (c) 1989 New Horizons Software, Inc.
 *
 * SubHeading functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Flow.h"
#include "Proto.h"

/*
 * External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern WORD			charHeight;

extern UWORD		levelArray[];

/*
 * Local variables and definitions
 */

void RecalibrateOutline(WindowPtr, HeadPtr);
void RecalcCursorPos(DocDataPtr, WORD);
void BumpSubordinateLabels(DocDataPtr, HeadPtr, WORD);

/*
 * Utility routine called by collapse/expand
 */

void RecalibrateOutline( window, head )
WindowPtr window;
HeadPtr head;
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CountLines(docData);
	DrawWindowRange(window, (WORD) VertPosition(window, head, 0),
				(WORD) (window->Height - window->BorderBottom - 1));
	CursorOn(window);
	SetSearchMenu();
	SetSubHeadMenu();
}

static void RecalcCursorPos( DocDataPtr docData, WORD oldStart )
{
	register WORD displacement;
	
	displacement = docData->SelHead->BodyStart - oldStart;
	docData->SelStart += displacement;
	docData->SelEnd += displacement;
	docData->DragStart += displacement;
}
	
/*
 * Collapse selected headings and all subordinate sub-headings
 */

BOOL DoCollapse(WindowPtr window)
{
	BOOL success = TRUE;
	register HeadPtr head, selHead, endHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	CursorOff(window);
	selHead = docData->SelHead;
	endHead = LastSelHead(selHead);
	endHead->Flags &= ~HD_EXPANDED;
	endHead = NextHead(endHead);	  /* Trick to get endHead */
/*
	Collapse selected headings
*/
	for (head = selHead; head && head != endHead; head = NextHeadAll(head)) {
		head->Flags &= ~HD_EXPANDED;
		if (head == docData->Mark.Head)
			docData->Mark.Head->Flags &= ~HD_EXPANDED;
	}
/*
	Draw new window contents and update internal structures
*/
	RecalibrateOutline(window, selHead);
	ScrollToCursor(window);
	AdjustScrollBars(window);
	return(success);
}

/*
 * Expand selected headings
 */

BOOL DoExpand(WindowPtr window)
{
	BOOL success = TRUE;
	register WORD line, lastLine, topLine, windowLines;
	register HeadPtr head, selHead, endHead, lastHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	endHead = lastHead->Next;
	CursorOff(window);
/*
	Expand selected headings
*/
	for (head = selHead; head && head != endHead; head = head->Next) {
		if (head->Sub)	{
			head->Flags |= HD_EXPANDED;
			if (head == docData->Mark.Head)
				docData->Mark.Head->Flags |= HD_EXPANDED;
		}
	}
/*
	Draw new window contents and update internal structures
*/
	RecalibrateOutline(window, selHead);
/*
	Scroll to show new subheads if not visible in window
*/
	line = LineNumber(docData, selHead, 0);
	lastLine = LineNumber(docData, lastHead, 0) + lastHead->NumLines - 1
				+ NumSubLines(lastHead);
	topLine = docData->TopLine;
	windowLines = WINDOW_LINES(window);
	if (line > topLine && lastLine >= topLine + windowLines) {
		topLine = lastLine - windowLines + 1;
		if (line < topLine)
			topLine = line;
		ScrollToOffset(window, topLine, docData->LeftOffset);
	}
	ScrollToCursor(window);
	AdjustScrollBars(window);
	return(success);
}

/*
 * Expand all outline headings
 */

BOOL DoExpandAll(WindowPtr window)
{
	BOOL success = TRUE;
	register WORD line, lastLine, topLine, windowLines;
	register HeadPtr head, endHead;
	HeadPtr selHead, lastHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	endHead = lastHead->Next;
	if( endHead == NULL ) {
		endHead = lastHead->Super;
		if( endHead != NULL )
			endHead = endHead->Next;
	}
		
	CursorOff(window);
/*
	Expanded selected headings
*/
	for (head = selHead; head && head != endHead; head = NextHeadAll(head)) {
		if (head->Sub)
			head->Flags |= HD_EXPANDED;
	}
/*
	Draw new window contents
*/
	RecalibrateOutline(window, selHead); 
/*
	Scroll to show new subheads if not visible in window
*/
	line = LineNumber(docData, selHead, 0);
	lastLine = LineNumber(docData, lastHead, 0) + lastHead->NumLines - 1
				+ NumSubLines(lastHead);
	topLine = docData->TopLine;
	windowLines = WINDOW_LINES(window);
	if (line > topLine && lastLine >= topLine + windowLines) {
		topLine = lastLine - windowLines + 1;
		if (line < topLine)
			topLine = line;
		ScrollToOffset(window, topLine, docData->LeftOffset);
	}
	ScrollToCursor(window);
	AdjustScrollBars(window);
	return(success);
}

/*
 * Indent heading
 */

BOOL DoIndent(WindowPtr window)
{
	register BOOL success = FALSE;
	register WORD depth, maxDepth;
	register HeadPtr selHead, lastHead, head;
	HeadPtr prevHead, nextHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD oldStart, inc;
	
	selHead = docData->SelHead;
	prevHead = selHead->Prev;
	lastHead = selHead;
	maxDepth = SubDepth(selHead);
	if (lastHead->Flags & HD_SELECTED) {
		while (lastHead->Next && (lastHead->Next->Flags & HD_SELECTED)) {
			lastHead = lastHead->Next;
			if ((depth = SubDepth(lastHead)) > maxDepth)
				maxDepth = depth;
		}
	}
	if( (prevHead == NULL || HeadDepth(selHead) + maxDepth >= MAX_DEPTH) ||
		( docData->PageWidth <= (HeadDepth(selHead)+2) * docData->Indent) ) {
		ErrBeep();
	} else {
		success = TRUE;
		CursorOff(window);
		BumpSubordinateLabels(docData, selHead, 1);
/*
	Indent selected headings, adjust links to next heading
*/
		if (lastHead->Next)
			lastHead->Next->Prev = prevHead;
		prevHead->Next = lastHead->Next;
/*
	Link current heading as last heading of sub-head list
*/
		if (prevHead->Sub == NULL) {
			prevHead->Sub = selHead;
			selHead->Prev = NULL;
		} else {
			for (head = prevHead->Sub; head->Next; head = head->Next) ;
			head->Next = selHead;
			selHead->Prev = head;
		}
		lastHead->Next = NULL;
		
		inc = 0;
		oldStart = selHead->BodyStart;				/* For restoring cursor */
		for (head = selHead; head != NULL ; head = nextHead) {
			head->Super = prevHead;
			nextHead = head->Next;
			head->Num = head->Prev ? head->Prev->Num + 1 : levelArray[LabelDepth(docData, head)];
			--inc;
			/*
			ReformatSubs(docData, head);
			CalcLineStarts(docData, head);			/* May relocate head */
		}
		BumpLevel(docData, selHead, 0);			  /* Redraw new siblings below */
		selHead->Num = (selHead->Prev != NULL) ? (selHead->Prev->Num)+1 : levelArray[LabelDepth(docData, selHead)];
		BumpLevel(docData, prevHead->Next, inc);  /* Redraw ex-siblings underneath */
		ReformatRestOfLevel(docData, selHead);				/* Reformat new siblings */
		ReformatRestOfLevel(docData, prevHead->Next);	  /* Reformat ex-siblings */
		RecalcCursorPos(docData, oldStart);
/*
	Draw new window contents
*/
		prevHead->Flags |= HD_EXPANDED;
		RecalibrateOutline(window, prevHead);
		DocumentModified(docData);
		ScrollToCursor(window);
		AdjustScrollBars(window);
	}
	return(success);
}

/*
	Renumber subordinate levels, necessary on indentation changes
*/
static void BumpSubordinateLabels(DocDataPtr docData, HeadPtr selHead, WORD inc )
{
	register HeadPtr head;
	register UWORD depth;
	register HeadPtr endHead, superHead;
	register BOOL cont;
	HeadPtr lastHead = LastSelHead(selHead);
	
	do {
		cont = selHead != lastHead;
		endHead = selHead->Next;
		superHead = selHead->Super;
		while( (endHead == NULL) && (superHead != NULL) ) { 
			endHead = superHead->Next;
			superHead = superHead->Super;
		}
		for( head = selHead ; head != endHead ; head = NextHeadAll(head) ) {
			depth = HeadDepth(head);
			head->Num += levelArray[FindDepth(docData, depth + inc)] - levelArray[FindDepth(docData, depth)];
		}
		selHead = selHead->Next;
	} while( cont );
}

/*
 * Un-indent heading
 */

BOOL DoUnIndent(WindowPtr window)
{
	register BOOL success = FALSE;
	register HeadPtr selHead, head, lastHead;
	HeadPtr superHead, nextHead, prevLastHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD oldStart, inc;
	
	selHead = docData->SelHead;
	superHead = selHead->Super;
	if (superHead == NULL) {
		ErrBeep();
	} else {
		success = TRUE;
		lastHead = LastSelHead(selHead);
		prevLastHead = lastHead->Next;
		CursorOff(window);
		BumpSubordinateLabels(docData, selHead, -1);
/*
	Un-link selected headings from sub-head list
*/
		if (lastHead->Next)
			lastHead->Next->Prev = selHead->Prev;
		if (selHead->Prev)
			selHead->Prev->Next = lastHead->Next;
		if (superHead->Sub == selHead)
			superHead->Sub = lastHead->Next;
/*
	Un-indent selected headings, adjust links to next heading
*/
		if (superHead->Next)
			superHead->Next->Prev = lastHead;
		lastHead->Next = superHead->Next;
		superHead->Next = selHead;
		selHead->Prev = superHead;
		lastHead = lastHead->Next;
		inc = 0;
		oldStart = selHead->BodyStart;
		for (head = selHead; head && head != lastHead; head = nextHead) {
			head->Super = superHead->Super;
			nextHead = head->Next;
			head->Num = (head->Prev != NULL) ? head->Prev->Num + 1 : levelArray[LabelDepth(docData, head)];
			++inc;
			/*
			ReformatSubs(docData, head);
			CalcLineStarts(docData, head);			/* May relocate head */
		}
		BumpLevel(docData, selHead, 0);
		BumpLevel(docData, prevLastHead, -inc);	/* Redraw ex-siblings as less */
		BumpLevel(docData, lastHead, inc);		  /* Redraw new siblings as more */
		ReformatRestOfLevel(docData, selHead);	 /* Reformat new siblings */
		ReformatRestOfLevel(docData, prevLastHead);		 /* Reformat ex-siblings */
		RecalcCursorPos(docData, oldStart);
		DocumentModified(docData);
/*
	Find new CurrentLine and display new heading list
*/
		RecalibrateOutline(window, superHead);
		ScrollToCursor(window);
		AdjustScrollBars(window);
	}
	return(success);
}

/*
 * Move current heading up one line
 */

BOOL DoMoveUp(WindowPtr window)
{
	register BOOL success = FALSE;
	register LONG yMax;
	register HeadPtr selHead, lastHead, prevHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD oldStart, diff;
	HeadPtr head;
	
	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
/*
	Find the amount to move the previous line down
*/
	diff = (lastHead->Num - selHead->Num) + 1;
	prevHead = selHead->Prev;
	if (prevHead == NULL) {
		ErrBeep();
	} else {
		success = TRUE;
		CursorOff(window);
/*
	Swap pointers to previous and next heads
*/
		if (docData->FirstHead == prevHead)
			docData->FirstHead = selHead;
		if (prevHead->Super && prevHead->Super->Sub == prevHead)
			prevHead->Super->Sub = selHead;
		if (prevHead->Prev)
			prevHead->Prev->Next = selHead;
		if (lastHead->Next)
			lastHead->Next->Prev = prevHead;
		prevHead->Next = lastHead->Next;
		selHead->Prev = prevHead->Prev;
		lastHead->Next = prevHead;
		prevHead->Prev = lastHead;
		for( head = selHead ; head != NULL ; ) {
			head->Num--;
			if( head == lastHead )
				head = NULL;
			else 
				head = head->Next;
		}
		prevHead->Num += diff;

		oldStart = selHead->BodyStart;
		BumpLevel(docData, selHead, 0);  
		ReformatRestOfLevel(docData, selHead);
		RecalcCursorPos(docData, oldStart);
/*
	Display changes
*/
		if (prevHead->Next)
			yMax = VertPosition(window, prevHead->Next, 0) - 1;
		else
			yMax = window->Height - window->BorderBottom - 1;
		DrawWindowRange(window, (WORD) VertPosition(window, selHead, 0), (WORD) yMax);
		CursorOn(window);
		ScrollToCursor(window);
		DocumentModified(docData);
		SetSearchMenu();
		SetSubHeadMenu();
	}
	return(success);
}

/*
 * Move current heading down one line
 */

BOOL DoMoveDown(WindowPtr window)
{
	register BOOL success = FALSE;
	register LONG yMax;
	register HeadPtr selHead, lastHead, nextHead;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD oldStart, diff;
	HeadPtr head;
		
	selHead = docData->SelHead;
	lastHead = LastSelHead(selHead);
	nextHead = lastHead->Next;
/*
	Find the amount to move the next line up
*/
	diff = (lastHead->Num - selHead->Num) + 1;

	if (nextHead == NULL) {
		ErrBeep();
	} else {
		success = TRUE;
		CursorOff(window);
/*
	Swap pointers to previous and next heads
*/
		if (docData->FirstHead == selHead)
			docData->FirstHead = nextHead;
		if (selHead->Super && selHead->Super->Sub == selHead)
			selHead->Super->Sub = nextHead;
		if (selHead->Prev)
			selHead->Prev->Next = nextHead;
		if (nextHead->Next)
			nextHead->Next->Prev = lastHead;
		lastHead->Next = nextHead->Next;
		nextHead->Prev = selHead->Prev;
		nextHead->Next = selHead;
		selHead->Prev = nextHead;
		
		for( head = selHead ; head != NULL ; ) {
			head->Num++;
			if( head == lastHead )
				head = NULL;
			else
				head = head->Next;
		}
		nextHead->Num -= diff;

		oldStart = selHead->BodyStart;
		BumpLevel(docData, nextHead, 0);	  /* Format subordinates too */
		ReformatRestOfLevel(docData, nextHead);
		RecalcCursorPos(docData, oldStart);
/*
	Display changes
*/
		if (lastHead->Next)
			yMax = VertPosition(window, lastHead->Next, 0) - 1;
		else
			yMax = window->Height - window->BorderBottom - 1;
		DrawWindowRange(window, (WORD) VertPosition(window, nextHead, 0), (WORD) yMax);
		CursorOn(window);
		ScrollToCursor(window);
		DocumentModified(docData);
		SetSearchMenu();
		SetSubHeadMenu();
	}
	return(success);
}

/*
 * Sort headings
 */

BOOL DoSort(WindowPtr window, WORD sortOrder)
{
	BOOL success = FALSE;
	TextPtr text1, text2;
	register WORD len1, len2, line, numLines, swapResult;
	BOOL wasSelected;
	UWORD swap, countLines;
	register HeadPtr head, selHead, head2;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD oldStart;
		
	selHead = docData->SelHead;
	if (selHead->Prev == NULL && selHead->Next == NULL) {
		ErrBeep();
	} else {
		success = TRUE;
/*
	Find first and last headings for sort
*/
		oldStart = selHead->BodyStart;
		head = GetFirstHead(selHead);
		for (numLines = 1; head->Next; head = head->Next, numLines++) ;
		if (sortOrder == ZTOA_SUBITEM)
			swapResult = -1;
		else						  /* sortOrder == ATOZ_SUBITEM */
			swapResult = 1;
/*
	Sort headings
*/
		CursorOff(window);
		wasSelected = (selHead->Flags & HD_SELECTED);
		UnSelectHeads(docData);
		BeginWait();
		countLines = numLines;
		if( numLines > 1 ) {
			do {
				head = GetFirstHead(selHead);
				if( sortOrder == RANDOM_SUBITEM ) {
					swap = TRUE;
					for( line = 1 ; line < countLines ; line++ ) {
						if( Random( 1 + (countLines >> 1) ) ) {
							SwapHeads(head);
						} else {
							head = head->Next;
						}
					}
				} else {
					swap = FALSE;
					text1 = &head->Text[head->BodyStart];
					len1 = head->Len;
					for( line = 1 ; line < numLines ; line++ ) {
						head2 = head->Next;
						text2 = &head2->Text[head2->BodyStart];
						len2 = head2->Len;
						if( CmpString(text1, text2, len1, len2, FALSE) == swapResult ) {
							SwapHeads(head);
							swap = TRUE;
						} else {
							head = head2;
							text1 = text2;
							len1 = len2;
						}
					}
				}
				numLines--;
			} while (swap && numLines > 1);
		}
		head = GetFirstHead(selHead);
		if (selHead->Super == NULL) {
			docData->FirstHead = head;
		}
		if (wasSelected)
			selHead->Flags |= HD_SELECTED;
		EndWait();
/*
	Display results
*/
		BumpLevel(docData, head, 0);
		ReformatRestOfLevel(docData, head);
		RecalcCursorPos(docData, oldStart);
		DrawWindow(window);
		CursorOn(window);
		ScrollToCursor(window);
		AdjustScrollBars(window);
		DocumentModified(docData);
		SetSearchMenu();
		SetSubHeadMenu();
	}
	return(success);
}

/*
 * Split a heading into two, same as ALT-ENTER
 */
 
BOOL DoSplitHead( WindowPtr window )
{
	return( NewHead(window, ALTKEYS) );
}

/*
 * Join two headings into one, similar to ALT-DELETE
 */
 
BOOL DoJoinHead( WindowPtr window )
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register WORD saveStartPos = docData->SelStart;
	register WORD saveEndPos = docData->SelEnd;
	BOOL success;
	
/*
	Joining changes SelStart and SelEnd because DeleteHead() is called
*/
	success = JoinHead(docData);
	docData->SelStart = saveStartPos;
	docData->SelEnd = saveEndPos;
	CalcLineStarts(docData, docData->SelHead);
	AdjustScrollBars(window);
	RecalibrateOutline(window, docData->SelHead);
	DocumentModified(docData);
	return(success);	
}

/*
 * Process SubHeadings menu selection
 */

BOOL DoSubHeadMenu(WindowPtr window, UWORD item, UWORD sub)
{
	register BOOL success;
	
	switch (item) {
	case COLLAPSE_ITEM:
		success = DoCollapse(window);
		break;
	case EXPAND_ITEM:
		success = DoExpand(window);
		break;
	case EXPANDALL_ITEM:
		success = DoExpandAll(window);
		break;
	case INDENT_ITEM:
		success = DoIndent(window);
		break;
	case UNINDENT_ITEM:
		success = DoUnIndent(window);
		break;
	case MOVEUP_ITEM:
		success = DoMoveUp(window);
		break;
	case MOVEDOWN_ITEM:
		success = DoMoveDown(window);
		break;
	case SPLIT_ITEM:
		success = DoSplitHead(window);
		break;
	case JOIN_ITEM:
		success = DoJoinHead(window);
		break; 
	case SORT_ITEM:
		success = DoSort(window, sub );
		break;
	}
	return(success);
}
