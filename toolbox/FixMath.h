/*
 *	Toolbox library
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Fixed point math definitions
 */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	Definitions
 */

typedef LONG	Fixed;		/* Integer portion in upper word */

#define FIXED_UNITY	0x00010000L

/*
 *	Prototypes
 */

#define Long2Fix(num)	((Fixed) (((LONG)  (num)) << 16))
#define Fix2Long(num)	((LONG)  (((Fixed) (num)) >> 16))
#define FixRound(num)	((WORD) ((((Fixed) (num)) + 0x8000L) >> 16))

Fixed	FixRatio(WORD, WORD);
Fixed	Ascii2Fix(TextPtr);
void	Fix2Ascii(Fixed, TextPtr, WORD);
Fixed	FixMul(Fixed, Fixed);
Fixed	FixDiv(Fixed, Fixed);
