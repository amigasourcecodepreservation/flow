/*
 *	Flow
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Image.h>
#include <Toolbox/Border.h>
#include <Toolbox/ColorPick.h>

#include <string.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD			intuiVersion;

extern BYTE			paperColor;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern TextFontPtr	smallFont;
extern TextChar	strPageNum[], strHeader[], strFooter[];
extern TextChar	strBuff[];

extern WORD	charHeight, charWidth;

extern BOOL	titleChanged;
extern BOOL drawOn;

extern BOOL	_tbOnPubScreen;

extern UBYTE _tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight;

/*
 * Local variables and definitions
 */
 
static BOOL altKey;	/* Set by DoGadgetDown() for gadget tracking routines */

/*
 * Local prototypes
 */
 
void GetPageIndicRect(WindowPtr, Rectangle *);
void TrackSliders(WindowPtr, WORD);
void ScrollWindow(WindowPtr, WORD);
void Scroll2Offset(WindowPtr, WORD, WORD);

/*
 *	Return vertical offset in window for given page and location on page
 */

LONG VertOffset(DocDataPtr docData, WORD page, WORD top)
{
	return ((LONG) page*PAGE_SEPARATION(docData) + top);
}

/*
 *	Return offset from top for window
 */

LONG TopOffset(WindowPtr window)
{
	LONG topOffset;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	topOffset = window->BorderTop
				- (LONG) docData->PageOffset*PAGE_SEPARATION(docData)
				- docData->TopLine + 1;
	return (topOffset);
}

/*
 *	Return offset from left for window
 */

WORD LeftOffset(WindowPtr window)
{
	WORD leftOffset;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	leftOffset = window->BorderLeft - docData->LeftOffset + 1;
	return (leftOffset);
}

/*
 *	Return maximum permitted page, top, and left offsets
 */

void MaxWindowOffsets(WindowPtr window, WORD *page, WORD *top, WORD *left)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;
	
	GetTextRect(window, &rect);
	*page = docData->Pages - 1;
	*top = PAGE_SEPARATION(docData) - (rect.MaxY - rect.MinY) ;
	if (*top < 0)
		*top = 0;
	*left = docData->MaxXDots + PAGE_GAP - (rect.MaxX - rect.MinX);
	if (*left < 0)
		*left = 0;
}

/*
 *	Return page number of page at given line offset
 * Since artificial breaks exist, V3.0 demands a traversal of the entire list. 
 */

WORD PageNumber(DocDataPtr docData, UWORD endLine)
{
	register UWORD line = 0;
	register BOOL found = FALSE;
	register UWORD page = 0;
	register HeadPtr head = docData->FirstHead;
	register UWORD startLine = 0;
	register UWORD headLine = 0;
	
	while( startLine < endLine && ( head != NULL ) ) {
		for( ; headLine < head->NumLines && !found ; headLine++, startLine++ ) {
			found = ++line == docData->PageHeight;
		}
		if( startLine <= endLine ) {
			if( !found ) {
/*
	No more lines in this head, advance to next head
*/
				head = NextHead(head);
				headLine = 0;
				if( head == NULL || ( head->Flags & HD_PAGEBREAK ) ) {
					page++;
					line = 0;
				}
			} else {
/*
	We've reached the bottom of the page
 */
				page++;
				if( headLine == head->NumLines ) {
					head = NextHead(head);
					headLine = 0;
				}
				line = 0;
			}
			found = FALSE;
		}
	}
	return ((WORD) (page + docData->StartPage));
}

/*
 *	Get rectangle of page indicator
 */

static void GetPageIndicRect(WindowPtr window, Rectangle *rect)
{
	GadgetPtr gadget;

	gadget = GadgetItem(window->FirstGadget, PAGEDOWN_ARROW);
	GetGadgetRect(gadget, window, NULL, rect);
	rect->MinX = rect->MaxX + 1;
	rect->MaxX += PGINDIC_WIDTH;
}

/*
 *	Draw page indicator in either normal or selected state
 */

static void DrawPageIndic(WindowPtr window, BOOL selected)
{
	WORD page;
	LayerPtr layer = window->WLayer;
	RastPtr rPort = window->RPort;
	RegionPtr oldRgn;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	page = docData->PageIndic;
	GetPageIndicRect(window, &rect);
	oldRgn = GetClip(layer);
	SetRectClip(layer, &rect);
	SetRast(rPort, (selected) ? _tbPenDark : _tbPenLight);
	SetFont(rPort, smallFont);
	SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	Move(rPort, rect.MinX + 2, rect.MinY + 7);
	
	Text(rPort, strPageNum, strlen(strPageNum));
	NumToString((LONG) page, strBuff);
	Text(rPort, strBuff, strlen(strBuff));
	
	if (intuiVersion >= OSVERSION_2_0 && !_tbOnPubScreen) {
		SetAPen(rPort, _tbPenBlack);
		Move(rPort, rect.MinX, rect.MaxY);
		Draw(rPort, rect.MaxX, rect.MaxY);
	}
	SetDrPt(rPort, 0xFFFF);
	DrawShadowBox(rPort, rect.MinX, rect.MinY, rect.MaxX - rect.MinX + 1, rect.MaxY - rect.MinY + 1,
				  0, !selected);
	SetClip(layer, oldRgn);
}

/*
 *	Set page number indicator to indicated page
 */

void SetPageIndic(WindowPtr window, WORD page)
{
	DocDataPtr docData = GetWRefCon(window);

	docData->PageIndic = page;
	DrawPageIndic(window, FALSE);
}

/*
 *	Adjust vertical and horizontal scroll bars to current values
 */

void AdjustScrollBars(WindowPtr window)
{
	UWORD page;
	register LONG totAmount, visAmount, currPos, newPot, newBody;
	LONG newPos;
	GadgetPtr gadget;
	PropInfoPtr propInfo;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( drawOn ) {
/*
	Adjust horizontal scroll bar
*/
		if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) != NULL &&
			!(gadget->Flags & SELECTED)) {
			propInfo = (PropInfoPtr) gadget->SpecialInfo;
			visAmount = WINDOW_COLUMNS(window);
			totAmount = docData->PageWidth - docData->LeftMargin - docData->RightMargin;
			currPos = docData->LeftOffset;
		
			if( totAmount < visAmount + currPos )
				totAmount = visAmount + currPos;
			newPot = (totAmount == visAmount ) ? 0xFFFF : (0xFFFFL*currPos)/(totAmount - visAmount);
			newBody = (0xFFFFL*visAmount)/totAmount;
			if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody) {
				WaitBOVP(&screen->ViewPort);
				NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEHORIZ,
							  newPot, 0, newBody, 0xFFFF, 1);
			}
		}
/*
	Adjust vertical scroll bar
*/
		if ((gadget = GadgetItem(window->FirstGadget, VERT_SCROLL)) != NULL ) {
			propInfo = (PropInfoPtr) gadget->SpecialInfo;
			totAmount = docData->TotalLines;
			visAmount = WINDOW_LINES(window);
			if( !(gadget->Flags & SELECTED) ) {
				currPos = docData->TopLine;
				if( totAmount < visAmount + currPos)
					totAmount = visAmount + currPos;
				while( totAmount > 0x7FFF ) {	 /* Avoid overflowing newPot calculation */
					totAmount >>= 1;
					visAmount >>= 1;
					currPos >>= 1;
				}
				newPot = (totAmount == visAmount ) ? 0xFFFF : (0xFFFFL*currPos)/(totAmount - visAmount);
				newBody = (0xFFFFL*visAmount)/totAmount;
				if( newBody == 0 )
					newBody =  1;
				if (newPot != propInfo->VertPot || newBody != propInfo->VertBody) {
					WaitBOVP(&screen->ViewPort);
					NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEVERT,
							  0, newPot, 0xFFFF, newBody, 1);
				}
			}
			newPos = propInfo->VertPot * (totAmount - visAmount);
			newPos = (newPos + 0x7FFFL)/0xFFFFL;
/*	
	Adjust page number indicator
*/
			page = PageNumber(docData, (UWORD) newPos);
			if (page != docData->PageIndic) {
				docData->PageOffset = page - docData->StartPage;
				SetPageIndic(window, page);
			}
		}
	}
}

/*
 *	Scroll to specified vert/horiz offset
 */

void ScrollToOffset( window, line, column)
register WindowPtr window;
register WORD line, column;
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( drawOn ) {
		if (line < 0)
			line = 0;
		else if (line >= docData->TotalLines)
			line = docData->TotalLines - 1;
		if (column < 0)
			column = 0;
		if (line != docData->TopLine || column != docData->LeftOffset ||
					column != (docData->PageWidth - docData->RightMargin)) {
			CursorOn(window);
			Scroll2Offset( window, line, column ) ;
		}
	}
}

void Scroll2Offset( window, line, column )
register WindowPtr window;
register WORD line, column;
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	register WORD dx;
	register LONG dy;
	struct Rectangle bounds;
	RegionPtr	oldRgn;
	
	GetTextRect(window, &bounds);
	oldRgn = GetClip(window->WLayer);
/*
	Scroll to new vertical position
*/
	if (line != docData->TopLine) {
		dy = (line - docData->TopLine)*charHeight;
		if (line > docData->TopLine) {
			if (dy > bounds.MaxY)
				dy = bounds.MaxY;
		} else {
			if (dy < -bounds.MaxY)
				dy = -bounds.MaxY;
		}
		docData->TopLine = line;
		ScrollRect(window->WLayer, 0, (WORD) dy, bounds.MinX, bounds.MinY,
					bounds.MaxX, bounds.MaxY);
	}
/*
	Scroll to new horizontal position
*/
	if (column != docData->LeftOffset || column != (docData->PageWidth - docData->RightMargin)) {
		bounds.MinX += LEFT_MARGIN;
		dx = (column - docData->LeftOffset)*charWidth;
		docData->LeftOffset = column;
		ScrollRect(window->WLayer, dx, 0, bounds.MinX, bounds.MinY,
					bounds.MaxX, bounds.MaxY);
	}
/*
	Update the cleared regions of the window
*/
	AdjustScrollBars(window);
	DoWindowUpdate(window);
	SetClip(window->WLayer,oldRgn);
}

/*
 *	Scroll to specified location
 */

void ScrollToLocation(WindowPtr window,
					  HeadPtr head,
					  WORD loc)
{
	WORD line, column, windowLines, windowColumns, newTopLine, newLeftOffset;
	WORD headLine;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if( ((line = LineNumber(docData, head, loc)) == -1 ) || !drawOn )
		return;
	line -= docData->TopLine;
	headLine = WhichLine(head, loc);
	column = loc - head->LineStarts[headLine] + HeadDepth(head)*docData->Indent
			 - docData->LeftOffset-1;
	if ((column + docData->LeftOffset) > docData->PageWidth)
		return;
	newTopLine = docData->TopLine;
	newLeftOffset = docData->LeftOffset;
	windowLines = WINDOW_LINES(window);
	windowColumns = WINDOW_COLUMNS(window);
	if (line < 0)
		newTopLine += line;
	else if (line >= windowLines)
		newTopLine += line - windowLines + 1;
	if (column < 0)
		newLeftOffset += column;
	else if (column >= windowColumns)
		newLeftOffset += column - windowColumns + 1;
	if (newTopLine != docData->TopLine || newLeftOffset != docData->LeftOffset)
		ScrollToOffset(window, newTopLine, newLeftOffset);
}

/*
 *	Adjust window scroll to make sure current line is visible
 */

void ScrollToCursor(WindowPtr window)
{
	register DocDataPtr docData = GetWRefCon(window);
	
	ScrollToLocation(window, docData->SelHead, docData->SelStart);
}

/*
 *	Adjust vertical position to value in scroll box
 */

void AdjustToVertScroll(WindowPtr window)
{
	register LONG visAmount, totAmount, currPos, newPos;
	register GadgetPtr gadget;
	register PropInfoPtr propInfo;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if ((gadget = GadgetItem(window->FirstGadget, VERT_SCROLL)) == NULL)
		return;
	propInfo = (PropInfoPtr ) gadget->SpecialInfo;
/*
	Get the top line
*/
	totAmount = docData->TotalLines;
	visAmount = WINDOW_LINES(window);
	currPos = docData->TopLine;
	if (totAmount - currPos < visAmount)
		totAmount = visAmount + currPos;
	newPos = propInfo->VertPot*(totAmount - visAmount);
	newPos = (newPos + 0x7FFFL)/0xFFFFL;
/*
	Scroll to new position and update window
*/
	ScrollToOffset(window, (WORD) newPos, docData->LeftOffset);
}

/*
 *	Adjust horizontal position to value in scroll box
 */

void AdjustToHorizScroll(WindowPtr window)
{
	register LONG visAmount, totAmount, currPos, newPos;
	register GadgetPtr gadget;
	register PropInfoPtr propInfo;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) == NULL)
		return;
	propInfo = (PropInfoPtr ) gadget->SpecialInfo;
/*
	Get the top line
*/
	totAmount = docData->PageWidth - docData->LeftMargin - docData->RightMargin;
	visAmount = WINDOW_COLUMNS(window);
	currPos = docData->LeftOffset;
	if (totAmount - currPos < visAmount)
		totAmount = visAmount + currPos;
	newPos = propInfo->HorizPot*(totAmount - visAmount);
	newPos = (newPos + 0x7FFFL)/0xFFFFL;
/*
	Scroll to new position and update window
*/
	ScrollToOffset(window, docData->TopLine, (WORD) newPos);
}

/*
 *	Scroll up one line
 */

void ScrollUp(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->TopLine)
		ScrollToOffset(window, (WORD) (docData->TopLine - 1), docData->LeftOffset);
}

/*
 *	Scroll down one line
 */

void ScrollDown(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->TopLine + WINDOW_LINES(window) < docData->TotalLines)
		ScrollToOffset(window, (WORD) (docData->TopLine + 1), docData->LeftOffset);
}

/*
 *	Scroll left one character
 */

void ScrollLeft(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	WORD horiz ;
/* register HeadPtr selHead = docData->SelHead;
	WORD headLine; */

	if( docData->LeftOffset ) {
		horiz = docData->LeftOffset - 1;
/*
		headLine = WhichLine(selHead, docData->SelStart);
		if( headLine == 0 && ( docData->SelStart == selHead->BodyStart ) )
			horiz -= selHead->BodyStart;
*/			
		ScrollToOffset(window, docData->TopLine, horiz);
	}
}

/*
 *	Scroll right one character
 */

void ScrollRight(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->LeftOffset + WINDOW_COLUMNS(window) <
		docData->PageWidth - docData->LeftMargin - docData->RightMargin)
		ScrollToOffset(window, docData->TopLine, (WORD) (docData->LeftOffset + 1));
}

/*
 *	Track vertical and horizontal sliders and update page number indicator
 *	Update window as scrolling if ALT key down
 */

static void TrackSliders(WindowPtr window, WORD gadgNum)
{
	register WORD page;
	register LONG visAmount, totAmount, currPos, newPos;
	register GadgetPtr gadget;
	register PropInfoPtr propInfo;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

/*
	Handle horizontal scroll box
*/
	if (gadgNum == HORIZ_SCROLL) {
		if (altKey)
			AdjustToHorizScroll(window);
		return;
	}
/*
	Handle vertical scroll box
*/
	if (gadgNum != VERT_SCROLL)
		return;
	if (altKey) {
		AdjustToVertScroll(window);
		return;
	}
/*
	Get the new position in 1/16 inch
*/
	if ((gadget = GadgetItem(window->FirstGadget, VERT_SCROLL)) == NULL)
		return;
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
/*
	Get the top line
*/
	totAmount = docData->TotalLines;
	visAmount = WINDOW_LINES(window);
	currPos = docData->TopLine;
	if (totAmount - currPos < visAmount)
		totAmount = visAmount + currPos;
	newPos = propInfo->VertPot*(totAmount - visAmount);
	newPos = (newPos + 0x7FFFL)/0xFFFFL;

/*
	Update page number indicator
*/
	page = PageNumber(docData, (UWORD) newPos);
	if (page != docData->PageIndic)
		SetPageIndic(window, page);
}

/*
 *	Scroll window in direction indicated by gadget
 *	Called by TrackGadget()
 */

void ScrollWindow(WindowPtr window,
				  WORD gadgetNum)
{
	switch (gadgetNum) {
	case UP_ARROW:
		ScrollUp(window);
		break;
	case DOWN_ARROW:
		ScrollDown(window);
		break;
	case LEFT_ARROW:
		ScrollLeft(window);
		break;
	case RIGHT_ARROW:
		ScrollRight(window);
		break;
	}
}

/*
 *	Handle gadget down event
 */

void DoGadgetDown(WindowPtr window, GadgetPtr gadget, UWORD modifier)
{
	register WORD gadgNum;
	
	if (titleChanged)
		FixTitle();
	altKey = ((modifier & ALTKEYS) != 0);

/*
	Ignore if not a window gadget
*/	
	gadgNum = GadgetNumber(gadget);
	if( gadget == GadgetItem(window->FirstGadget, gadgNum) ) {
		switch( gadgNum ) {
		case UP_ARROW:
		case DOWN_ARROW:
		case LEFT_ARROW:
		case RIGHT_ARROW:
			SetStdPointer(window, POINTER_ARROW);
			CursorOn(window);
			TrackGadget(mainMsgPort, window, gadget, ScrollWindow);
			SetPointerShape();
			break;
		case VERT_SCROLL:
		case HORIZ_SCROLL:
			SetStdPointer(window, POINTER_ARROW);
			CursorOn(window);
			TrackGadget(mainMsgPort, window, gadget, TrackSliders);
			break;
		case PAGEUP_ARROW:
			DoPageUp(window, altKey);
			break;
		case PAGEDOWN_ARROW:
			DoPageDown(window, altKey);
			break;
		}
	}
}

/*
 *	Handle gadget up event
 */

void DoGadgetUp(WindowPtr window, GadgetPtr gadget)
{
	WORD gadgNum;
	
/*
	Ignore if not a window gadget
*/
	gadgNum = GadgetNumber(gadget);
	if (gadget == GadgetItem(window->FirstGadget, gadgNum)) {
/*
	Handle gadget up
*/
		SetPointerShape();
		switch (gadgNum) {
		case VERT_SCROLL:
			AdjustToVertScroll(window);
			break;
		case HORIZ_SCROLL:
			AdjustToHorizScroll(window);
			break;
		}
	}
}

/*
 *	Scroll to top of previous page (or to beginning of document)
 */

void DoPageUp(WindowPtr window, BOOL top)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	UWORD newTop, dummy;

	newTop = 0;
	if( !top ) {
 		if( docData->PageOffset )
			(void) FindFirstPageLine(docData, docData->PageOffset-1, &dummy, &newTop);
	}
	ScrollToOffset(window, newTop, docData->LeftOffset);
}

/*
 *	Scroll to top of next page (or to end of document)
 */

void DoPageDown(WindowPtr window, BOOL bottom)
{
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	UWORD newTop, dummy ;
	
	if( bottom || ( (docData->Pages-1) == docData->PageOffset ) )
		newTop = docData->TotalLines-1;
	else {
		(void) FindFirstPageLine(docData, docData->PageOffset+1, &dummy, &newTop);
	}
	ScrollToOffset(window, newTop, docData->LeftOffset);
}

/*
 *	Determine if coordiantes are in page indicator
 */

BOOL InPageIndic(WindowPtr window, WORD mouseX, WORD mouseY)
{
	Rectangle rect;

	GetPageIndicRect(window, &rect);
	return ((BOOL) (mouseX >= rect.MinX && mouseX <= rect.MaxX &&
					mouseY >= rect.MinY && mouseY <= rect.MaxY));
}

/*
 *	Handle mouse down in page indicator
 */

void DoInPageIndic(WindowPtr window)
{
	BOOL select, selected;

	selected = TRUE;
	DrawPageIndic(window,selected);
/*
	Track mouse and see if it stays in page indicator
*/
	while (WaitMouseUp(mainMsgPort, window)) {
		select = InPageIndic(window, window->MouseX, window->MouseY);
		if ((select && !selected) || (!select && selected)) {
			selected = select;
			DrawPageIndic(window,selected);
		}
	}
	if (selected)
		DrawPageIndic(window, FALSE);
	if (selected)
		DoGoToPage(window);
}
