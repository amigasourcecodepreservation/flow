/*
 *	ProWrite
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Window, screen, and window gadget definitions
 *	(except for items that must be in chip memory)
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>

#include "Flow.h"

/*
 *	Window definitions
 *	Use simple refresh on document windows for improved menu speed
 */

struct NewWindow newBackWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | MENUPICK | REFRESHWINDOW | ACTIVEWINDOW | INACTIVEWINDOW |
		RAWKEY | NEWPREFS,
	SIMPLE_REFRESH | BACKDROP | BORDERLESS | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0, 0,
	CUSTOMSCREEN
};

struct NewWindow newWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW |
		MENUPICK | MENUVERIFY | NEWSIZE | REFRESHWINDOW | SIZEVERIFY |
		ACTIVEWINDOW | INACTIVEWINDOW | RAWKEY | INTUITICKS,
	WINDOWSIZING | SIZEBRIGHT | SIZEBBOTTOM | WINDOWDEPTH | WINDOWCLOSE |
		WINDOWDRAG | SIMPLE_REFRESH | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	200, 100, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

/*
 *	Gadget template for document window
 */

GadgetTemplate windowGadgets[] = {
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - 3*ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_UP },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - 2*ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_DOWN },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - 3*ARROW_WIDTH, 1 - ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_LEFT },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - 2*ARROW_WIDTH, 1 - ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_RIGHT },

	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK,
		0, 0, 1 - ARROW_WIDTH + 4, 2,
		0, 0, ARROW_WIDTH - 8, -3*ARROW_HEIGHT - 4,
		0, 0, NULL },

	{ GADG_PROP_HORIZ | GADG_PROP_NEWLOOK,
		0, 0, 3 + 2*ARROW_WIDTH + PGINDIC_WIDTH + 2, 1 - ARROW_HEIGHT + 2,
		0, 0, -3*ARROW_WIDTH - 3 - 2*ARROW_WIDTH - PGINDIC_WIDTH - 4, ARROW_HEIGHT - 4,
		0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 3, 1 - ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_UP },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 3 + ARROW_WIDTH, 1 - ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_DOWN },

	{ GADG_ITEM_NONE }
};
