/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Typedefs.h>

BOOL	_tbHiRes;		/* TRUE if using hi-res screen */
BOOL	_tbNoShadows;	/* TRUE if cannot do shadow rendering */

TextAttr	_tbTextAttr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT
};

ScreenPtr	_tbScreen;
BOOL		_tbOnPubScreen;
BOOL		_tbSmartWindows;

WORD	_tbXSize, _tbYSize;

UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight, _tbPenRed;

BOOL	_tbAutoActivate = TRUE;
