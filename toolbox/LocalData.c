/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Localization data
 */

#include <exec/types.h>

#include <Toolbox/Language.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>

#include <Typedefs.h>

/*
 *	Local definitions
 */

#define SL_GADG_BOX(left, top, width, num)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width-ARROW_WIDTH, (num)*11, -1, num, NULL }

#define SL_GADG_UPARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-2*ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_UP }

#define SL_GADG_DOWNARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define SL_GADG_SLIDER(left, top, width, num)	\
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,		\
		(left)+(width)-ARROW_WIDTH, top, 1, 0,	\
		ARROW_WIDTH, (num)*11-2*ARROW_HEIGHT, -2, num, NULL }

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *	Text for dialogs and requesters
 */

#if (AMERICAN | BRITISH)

TextChar _sfpSystemText[]		= "Mounted disks and assignments";
TextChar _sfpFileExistsText[]	= "File already exists,\nOK to replace?";
TextChar _sfpBadNameText[]		= "Improper file name.";
TextChar _sfpNoFileText[]		= "No such file.";
TextChar _sfpDiskLockText[]		= "Disk is locked.";

#elif GERMAN

#elif FRENCH

TextChar _sfpSystemText[]		= "Disques mont�s et assignations";
TextChar _sfpFileExistsText[]	= "Ce fichier existe d�ja,\nOK pour le remplacer?";
TextChar _sfpBadNameText[]		= "Nom de fichier incorrect.";
TextChar _sfpNoFileText[]		= "Fichier inexistant.";
TextChar _sfpDiskLockText[]		= "Le disque est verrouill�.";

#elif SWEDISH

TextChar _sfpSystemText[]		= "Monterade och tilldelade enheter:";
TextChar _sfpFileExistsText[]	= "Filen finns redan,\nSkall den skrivas �ver?";
TextChar _sfpBadNameText[]		= "Felaktigt filnamn.";
TextChar _sfpNoFileText[]		= "Filen finns ej";
TextChar _sfpDiskLockText[]		= "Disken �r l�st.";

#endif

/*
 *	Structures for SFPGetFile()
 */

#define LIST_WIDTH	(24*8)

#define OPEN_NUM	10

#if (AMERICAN | BRITISH)

static GadgetTemplate getGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'O', 0, "Open" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, OPEN_NUM),

	{ GADG_EDIT_TEXT, 20, -20, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpGetFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 180, &getGadgets[0], NULL
};

#elif GERMAN

#elif FRENCH

static GadgetTemplate getGadgets[] = {
	{ GADG_PUSH_BUTTON, -90, -60, 0, 0, 70, 20, 0, 0, 'O', 0, "Ouvre" },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 70, 20, 0, 0, 'A', 0, "Annule" },
		
	{ GADG_PUSH_BUTTON, -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON, -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON, -90,  90, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, OPEN_NUM),

	{ GADG_EDIT_TEXT, 20, -20, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpGetFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 125 + LIST_WIDTH, 180, &getGadgets[0], NULL
};


#elif SWEDISH

static GadgetTemplate getGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, -60, 0, 0, 80, 20, 0, 0, 'O', 0, "�ppna" },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, 'A', 0, "Avbryt" },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, 'T', 0, "Tillbaka" },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'E', 0, "Enheter" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, OPEN_NUM),

	{ GADG_EDIT_TEXT, 20, -20, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpGetFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 130 + LIST_WIDTH, 180, &getGadgets[0], NULL
};

#endif

/*
 *	Structures for SFPPutFile()
 */

#define SAVE_NUM	7

#if (AMERICAN | BRITISH)

static GadgetTemplate putGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'S', 0, "Save" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20,  15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -60, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpPutFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 180, &putGadgets[0], NULL
};

#elif GERMAN


#elif FRENCH

static GadgetTemplate putGadgets[] = {
	{ GADG_PUSH_BUTTON, -90, -60, 0, 0, 70, 20, 0, 0, 'S', 0, "Sauve" },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 70, 20, 0, 0, 'A', 0, "Annule" },

	{ GADG_PUSH_BUTTON, -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON, -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON, -90,  90, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20,  15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -60, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpPutFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 125 + LIST_WIDTH, 180, &putGadgets[0], NULL
};

#elif SWEDISH

static GadgetTemplate putGadgets[] = {
	{ GADG_PUSH_BUTTON, -100, -60, 0, 0, 80, 20, 0, 0, 'S', 0, "Spara" },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, 'A', 0, "Avbryt" },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, 'T', 0, "Tillbaka" },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'E', 0, "Enheter" },

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20,  15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -60, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpPutFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 130 + LIST_WIDTH, 180, &putGadgets[0], NULL
};

#endif

/*
 *	Misc requesters
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate req1Gadgets[] = {
	{ GADG_PUSH_BUTTON,  160, 40, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq1Templ = {
	-1, -1, 240, 70, &req1Gadgets[0]
};

static GadgetTemplate req2Gadgets[] = {
	{ GADG_PUSH_BUTTON,   40, 50, 0, 0, 60, 20, 0, 0, 'Y', 0, "Yes" },
	{ GADG_PUSH_BUTTON,  140, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq2Templ = {
	-1, -1, 240, 80, &req2Gadgets[0]
};

#elif GERMAN


#elif FRENCH

static GadgetTemplate req1Gadgets[] = {
	{ GADG_PUSH_BUTTON,  160, 40, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq1Templ = {
	-1, -1, 270, 70, &req1Gadgets[0]
};

static GadgetTemplate req2Gadgets[] = {
	{ GADG_PUSH_BUTTON,   50, 50, 0, 0, 60, 20, 0, 0, 'O', 0, "Oui" },
	{ GADG_PUSH_BUTTON,  160, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "No" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq2Templ = {
	-1, -1, 270, 80, &req2Gadgets[0]
};

#elif SWEDISH

static GadgetTemplate req1Gadgets[] = {
	{ GADG_PUSH_BUTTON,  160, 40, 0, 0, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq1Templ = {
	-1, -1, 240, 70, &req1Gadgets[0]
};

static GadgetTemplate req2Gadgets[] = {
	{ GADG_PUSH_BUTTON,   60, 50, 0, 0, 60, 20, 0, 0, 'J', 0, "Ja" },
	{ GADG_PUSH_BUTTON,  180, 50, 0, 0, 60, 20, 0, 0, 'N', 0, "Nej" },
	{ GADG_STAT_TEXT,     60, 10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq2Templ = {
	-1, -1, 300, 80, &req2Gadgets[0]
};

#endif

/*
 *	Color picker dialog
 */

#if (AMERICAN | BRITISH)

static GadgetTemplate colorGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0,  60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_STAT_STDBORDER,  70,  30, 0, 0,  20, 20, ARROW_WIDTH, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  45, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  90, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180, 135, 0, 0, 150, 15, 0, 0, 0, 0, NULL },

	GT_ADJUST_UPARROW(80, 70),
	GT_ADJUST_DOWNARROW(80, 70),
	GT_ADJUST_TEXT(80, 70),

	GT_ADJUST_UPARROW(80, 100),
	GT_ADJUST_DOWNARROW(80, 100),
	GT_ADJUST_TEXT(80, 100),

	GT_ADJUST_UPARROW(80, 130),
	GT_ADJUST_DOWNARROW(80, 130),
	GT_ADJUST_TEXT(80, 130),

	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Hue" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Brightness" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Saturation" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Red:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Green:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Blue:" },

	{ GADG_ITEM_NONE }
};

DialogTemplate _colorDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 190, &colorGadgets[0], NULL
};

#elif GERMAN


#elif FRENCH

static GadgetTemplate colorGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0,  60, 20, 0, 0, 'A', 0, "Annule" },

	{ GADG_STAT_STDBORDER,  70,  30, 0, 0,  20, 20, ARROW_WIDTH, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  45, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  90, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180, 135, 0, 0, 150, 15, 0, 0, 0, 0, NULL },

	GT_ADJUST_UPARROW(80, 70),
	GT_ADJUST_DOWNARROW(80, 70),
	GT_ADJUST_TEXT(80, 70),

	GT_ADJUST_UPARROW(80, 100),
	GT_ADJUST_DOWNARROW(80, 100),
	GT_ADJUST_TEXT(80, 100),

	GT_ADJUST_UPARROW(80, 130),
	GT_ADJUST_DOWNARROW(80, 130),
	GT_ADJUST_TEXT(80, 130),

	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Teinte" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Luminosit�" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Saturation" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Rouge:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Vert:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Bleu:" },

	{ GADG_ITEM_NONE }
};

DialogTemplate _colorDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 190, &colorGadgets[0], NULL
};

#elif SWEDISH

static GadgetTemplate colorGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  65, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0,  65, 20, 0, 0, 'A', 0, "Avbryt" },

	{ GADG_STAT_STDBORDER,  70,  30, 0, 0,  20, 20, ARROW_WIDTH, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  45, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  90, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180, 135, 0, 0, 150, 15, 0, 0, 0, 0, NULL },

	GT_ADJUST_UPARROW(80, 70),
	GT_ADJUST_DOWNARROW(80, 70),
	GT_ADJUST_TEXT(80, 70),

	GT_ADJUST_UPARROW(80, 100),
	GT_ADJUST_DOWNARROW(80, 100),
	GT_ADJUST_TEXT(80, 100),

	GT_ADJUST_UPARROW(80, 130),
	GT_ADJUST_DOWNARROW(80, 130),
	GT_ADJUST_TEXT(80, 130),

	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Nyans" },	/* hue */
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Intensitet" }, /* brightness */
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "M�ttning" }, /* saturation */
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "R�d:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Gr�n:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Bl�:" },

	{ GADG_ITEM_NONE }
};

DialogTemplate _colorDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 190, &colorGadgets[0], NULL
};

#endif

/*
 *	Month and day names
 */

#if (AMERICAN | BRITISH)

TextPtr _monthNames[] = {
	"January",		"February",		"March",		"April",
	"May",			"June",			"July",			"August",
	"September",	"October",		"November",		"December"
};

TextPtr _monthNamesAbbr[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

TextPtr _dayNames[] = {
	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
};

TextPtr _dayNamesAbbr[] = {
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

#elif GERMAN


#elif FRENCH

TextPtr _monthNames[] = {
	"Janvier",		"F�vrier",		"Mars",			"Avril",
	"Mai",			"Juin",			"Juillet",		"Ao�t",
	"Septembre",	"Octobre",		"Novembre",		"D�cembre"
};

TextPtr _monthNamesAbbr[] = {
	"Jan", "F�v", "Mar", "Avr", "Mai", "Jun",
	"Jul", "Ao�", "Sep", "Oct", "Nov", "Dec"
};

TextPtr _dayNames[] = {
	"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"
};

TextPtr _dayNamesAbbr[] = {
	"Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"
};

#elif SWEDISH

TextPtr _monthNames[] = {
	"januari",		"februari",		"mars",			"april",
	"maj",			"juni",			"juli",			"augusti",
	"september",	"oktober",		"november",		"december"
};

TextPtr _monthNamesAbbr[] = {
	"jan", "feb", "mar", "apr", "maj", "jun",
	"jul", "aug", "sep", "okt", "nov", "dec"
};

TextPtr _dayNames[] = {
	"s�ndag", "m�ndag", "tisdag", "onsdag", "torsdag", "fredag", "l�rdag"
};

TextPtr _dayNamesAbbr[] = {
	"s�n", "m�n", "tis", "ons", "tor", "fre", "l�r"
};

#endif

/*
 *	Date formats, indexed by date type
 */

#if AMERICAN

TextPtr _dateFormats[] = {
	"\x04/\x01/\x08",		/* MONTH_NUMBER/DAY_NUMBER/YEAR_SHORT */
	"\x06 \x01, \x07",		/* MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG */
	"\x05 \x01, \x07",		/* MONTH_NAME DAY_NUMBER, YEAR_LONG */
	"\x03, \x06 \x01, \x07",	/* DAY_NAMEABBREV, MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG */
	"\x02, \x05 \x01, \x07",	/* DAY_NAME, MONTH_NAME DAY_NUMBER, YEAR_LONG */
	"\x01 \x06 \x08"		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT */
};

#elif BRITISH

TextPtr _dateFormats[] = {
	"\x09/\x0A/\x07",		/* DAY_NUMZERO/MONTH_NUMZERO/YEAR_LONG */
	"\x06 \x01, \x07",		/* MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG */
	"\x05 \x01, \x07",		/* MONTH_NAME DAY_NUMBER, YEAR_LONG */
	"\x03, \x06 \x01, \x07",	/* DAY_NAMEABBREV, MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG */
	"\x02, \x05 \x01, \x07",	/* DAY_NAME, MONTH_NAME DAY_NUMBER, YEAR_LONG */
	"\x01 \x06 \x08"		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT */
};

#elif GERMAN

TextPtr _dateFormats[] = {
	"\x01.\x0A.\x07",		/* DAY_NUMBER.MONTH_NUMZERO.YEAR_LONG */
	"\x01. \x06 \x07",		/* DAY_NUMBER. MONTH_NAMEABBREV YEAR_LONG */
	"\x01. \x05 \x07",		/* DAY_NUMBER. MONTH_NAME YEAR_LONG */
	"\x03, \x01. \x06 \x07",	/* DAY_NAMEABBREV, DAY_NUMBER. MONTH_NAMEABBREV YEAR_LONG */
	"\x02, \x01. \x05 \x07",	/* DAY_NAME, DAY_NUMBER. MONTH_NAME YEAR_LONG */
	"\x01 \x06 \x08"		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT */
};

#elif FRENCH

TextPtr _dateFormats[] = {
	"\x01.\x0A.\x08",		/* DAY_NUMBER.MONTH_NUMZERO.YEAR_SHORT */
	"\x01 \x06 \x07",		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_LONG */
	"\x01 \x05 \x07",		/* DAY_NUMBER MONTH_NAME YEAR_LONG */
	"\x03 \x01 \x06 \x07",	/* DAY_NAMEABBREV DAY_NUMBER MONTH_NAMEABBREV YEAR_LONG */
	"\x02 \x01 \x05 \x07",	/* DAY_NAME DAY_NUMBER MONTH_NAME YEAR_LONG */
	"\x01 \x06 \x08"		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT */
};

#elif SWEDISH

TextPtr _dateFormats[] = {
	"\x01.\x04.\x08",		/* DAY_NUMBER.MONTH_NUMBER.YEAR_SHORT */
	"\x01. \x06 \x07",		/* DAY_NUMBER.MONTH_NAMEABBREV YEAR_LONG */
	"\x01. \x05 \x07",		/* DAY_NUMBER. MONTH_NAME YEAR_LONG */
	"\x03, \x01. \x06 \x07",	/* DAY_NAMEABBREV, DAY_NUMBER. MONTH_NAMEABBREV  YEAR_LONG */
	"\x02, \x01. \x05 \x07",	/* DAY_NAME, DAY_NUMBER. MONTH_NAME YEAR_LONG */
	"\x01 \x06 \x08"		/* DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT */
};

#endif
