/*
 *	ProWrite print record
 *	Copyright (c) 1990 New Horizons Software, Inc.
 */

#define PRINTHANDLER_VERSION	1

enum {								/* Page Size */
	PRT_CUSTOM,
	PRT_USLETTER,	PRT_USLEGAL,
	PRT_A4LETTER,	PRT_WIDECARRIAGE
};

enum {								/* Orientation */
	PRT_PORTRAIT,	PRT_LANDSCAPE
};

enum {								/* Quality */
	PRT_DRAFT,		PRT_NLQ,	PRT_GRAPHIC,	PRT_POSTSCRIPT
};

enum {								/* Paper Feed */
	PRT_CONTINUOUS,	PRT_CUTSHEET
};

/*
 *	Flags
 */

#define PRT_ASPECTADJ		0x0001
#define PRT_NOGAPS			0x0002	/* No gaps between pages */
#define PRT_SMOOTH			0x0004	/* Smoothing on */
#define PRT_PICTURES		   0x0008	/* Pictures with character print */
#define PRT_BACKTOFRONT		0x0010	/* Print back to front */
#define PRT_COLLATE			0x0020	/* Print in collated order */
#define PRT_ODDEVEN			0x0040	/* Print separate odd and even pages */
#define PRT_OPTSPACE		   0x0080	/* Optimal spacing */
#define PRT_FONTS			0x0100	/* Bit-map fonts with character print */
#define PRT_TOFILE			0x0200	/* Destination is file */

enum {								/* Print Pitch */
	PRT_PICA,	PRT_ELITE,	PRT_CONDENSED
};

enum {								/* Print Spacing */
	PRT_SIXLPI,	PRT_EIGHTLPI
};

typedef union  {
	struct IOStdReq		Std;
	struct IODRPReq		DRP;
	struct IOPrtCmdReq	Cmd;
} PrintIO, *PrintIOPtr;

typedef struct PrintRecord {
	WORD		Version;				/* Version number of record */
	Rectangle	PaperRect, PageRect;	/* Upper left of PaperRect is (0,0) */
	WORD		xDPI, yDPI;

	UWORD		PaperWidth, PaperHeight;	/* In decipoints */

	WORD		Copies, FirstPage, LastPage;

	UBYTE		PageSize;				/* From defines above */
	UBYTE		Orientation;
	UWORD		XScale, YScale;		/* Enlargement/reduction * 100 */
	UBYTE		PrintDensity;
	UBYTE		Quality;
	UBYTE		PaperFeed;
	UBYTE		FontNum;					/* Printer font number */
	UWORD		Flags;
	UBYTE		PrintPitch;				/* Not used by ProWrite */
	UBYTE		PrintSpacing;			/* Not used by ProWrite */
	UBYTE		Depth;					/* Not used by Flow */
	UBYTE		pad[120-107];			/* Pad to 120 bytes */
	TextChar 	FileName[32];			/* Print to file capability */
	Dir			FileDir;					/* Print to file capability */
	File		File;
	RastPtr		RPort;
	PrintIOPtr	PrintIO;
	ColorMapPtr	ColorMap;
	WORD		Top, Left, Width, Height;
} *PrintRecPtr;

#ifndef c_plusplus
typedef struct PrintRecord PrintRecord;
#endif

/*
 *	PostScript options
 */

enum {								/* PostScript output device */
	PS_SERIAL,	PS_PARALLEL,	PS_CUSTOM
};

enum {								/* Color options */
	PS_BLACKWHITE,	PS_GRAYSCALE,	PS_COLORFULL
};

typedef struct PostScriptOptions {
	UBYTE		Device;
	UBYTE		DeviceNumber;			/* Device number */
	TextChar	CustomName[100];
	UBYTE		Colors;
	UBYTE		pad[97];				/* Pad to 200 bytes */
} *PSOptsPtr;

#ifndef c_plusplus
typedef struct PostScriptOptions PostScriptOptions;
typedef struct PrintRecord PrintRecord;
#endif
