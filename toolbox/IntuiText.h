/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	IntuiText definitions
 */

#ifndef TOOLBOX_INTUITEXT_H
#define TOOLBOX_INTUITEXT_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	Prototypes
 */

IntuiTextPtr	NewIntuiText(WORD, WORD, TextPtr, WORD, WORD);
void			FreeIntuiText(IntuiTextPtr);

void	AppendIntuiText(IntuiTextPtr, IntuiTextPtr);

BOOL	ChangeIntuiText(IntuiTextPtr, TextPtr);
BOOL	SetIntuiTextFont(IntuiTextPtr, TextAttrPtr);

#endif
