/*
 *	Flow
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <TypeDefs.h>

#ifdef DEMO
TextChar screenTitle[] = " Flow DEMO 3.1 - \251 1991 New Horizons Software, Inc.";
#else
TextChar screenTitle[] = " Flow 3.1 - \251 1991 New Horizons Software, Inc.";
#endif

/*
 *	Version string for AmigaDOS 2.0
 */

static char	version[] = "$VER: Flow 3.1";

TextPtr initErrors[] = {
	"Bad system state",
	"Can't find icon.library",
	"Can't find diskfont.library",
	"Can't open screen",
	"Not enough memory"
};

TextPtr strsErrors[] = {
	" Unknown internal error.",
	" Not enough memory.",
	" Unable to open file.",
	" Unable to save file.",
	" Not enough memory - saving as 'Flow.Recover' on drive 0.",
	" Unable to print.",
	" Improper number of copies.",
	" No such page.",
	" Improper page number.",
	" Improper text entry.",
	" Text not found.",
	" Improper margin value.",
	" Unable to change text.",
	" Improper page size.",
	" Improper line height.",
  	" AREXX not present.",
	" Improper macro name.",
	" Macro execution failed.",
	" Unable to save dictionary.",
	" Unable to find dictionary.",
	" Unable to change dictionary.",
	" Unable to open clipboard.\n Full clipboard support\n is not available",
	" Can't find iffparse.library.\n Full clipboard support\n is not available",
};

TextChar strDemo[]		= " Demo version.";
TextChar strBadFile[]	= " Bad file contents.";

TextChar strDOSError[]		= " DOS error: 000";
TextChar strFileNotFound[] = " File not found.";
TextChar strDiskLocked[]	= " Disk is locked.";
TextChar strFileNoDelete[]	= " File is delete-protected.";
TextChar strFileNoRead[]	= " File is read-protected.";

TextChar strMemory[]		= " Getting low on memory -- please save your work.";

TextChar strUntitled[]		= "Untitled";
TextChar strSaveAs[]			= "Save document as:";
TextChar strSavePrAs[]		= "Save printout as:";
TextChar strOpenFile[]		= "Open document";
TextChar strOpenDict[]		= "Change dictionary";

TextChar strLoadDefaults[]		= "Select the settings to use";
TextChar strSaveAsDefaults[]	= "Save settings as:";

TextChar strChangedNum[]	= "Changed # occurrence(s)";

TextChar strRAMName[]		= "RAM:";

TextChar strProgName[] 		= "Flow";
TextChar strScreenName[]	= "Flow";
TextChar strPrefsName[]		= "Flow Defaults";
TextChar strFKeysName[]		= "Flow FKeys";
TextChar strAutoExecName[]	= "Flow Startup";
TextChar strAppIconName[]	= "Flow Deposit";
TextChar strDefaultDir[]	= "Flow:";

TextChar strPageNum[]	= "Page ";

TextChar strDone[]	= "Done";
TextChar strStart[]	= "Start";
TextChar strSkip[]	= "Skip";
TextChar strFind[]	= "Find";

TextChar strCancel[] = "Cancel";
TextChar strSave[]	= "Save";
TextChar strPrint[]  = "Print";
TextChar strPRT[]	= "PRT:";

TextChar strScreenColor[] = "Change Screen Color";

TextChar strMainDictName[]		= "Main Dictionary";
TextChar strUserDictName[]		= "User Dictionary";
TextChar strNoSpellError[]		= "No more misspellings found";
TextChar strNoSpellAlternate[]= "No alternative words found";

TextChar strAltChDict[]			= "Ch Dict";	/* For lo-res */

TextChar strChars[]				= " chars";
TextChar strWords[]				= " words";

TextChar strPrtSetup[]	= "Setting up...";		/* For "Cancel print" dialog */
TextChar strPrtRender[]	= "Rendering...";
TextChar strPrtPrint[]	= "Printing...";
TextChar strPrtCancel[]	= "Cancelling...";

/*
 *	Default macro names
 */

TextChar	strMacroNames1[10][32];		/* Not assigned */

TextChar	strMacroNames2[10][32] = {
	"Macro_1", "Macro_2", "Macro_3", "Macro_4", "Macro_5",
	"Macro_6", "Macro_7", "Macro_8", "Macro_9", "Macro_10"
};