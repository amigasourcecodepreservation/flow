/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Color picker definitions
 */

#ifndef TOOLBOX_COLORPICK_H
#define TOOLBOX_COLORPICK_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEF_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

/*
 *	Color picker routines
 */

BOOL	GetColor(ScreenPtr, MsgPortPtr, BOOL (*)(IntuiMsgPtr, WORD *), TextPtr,
				 RGBColor (*)(RastPtr, RGBColor), RGBColor, RGBColorPtr, WORD);

UBYTE	PenNumber(RGBColor, ColorMapPtr, WORD);

#endif
