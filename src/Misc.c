/*
 *	Flow
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Misc support functions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Gadget.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/FixMath.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Border.h>

#include "Flow.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;

extern WORD	waitCount;

extern BOOL inMacro;
extern Options		options;


extern DlgTemplPtr	dlgList[];
extern DialogPtr	findDialog, changeDialog, spellDialog, insertDialog;

extern UBYTE		_tbPenDark, _tbPenBlack, _tbPenWhite, _tbPenLight;	/* For DrawColorBorder */

/*
 *	Local variables and definitions
 */

#define DOCWIND_MSGS	(MENUVERIFY|GADGETDOWN|GADGETUP|NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW)
#define DLGWIND_MSGS (MENUVERIFY|GADGETDOWN|GADGETUP|NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW)
#define BACKWIND_MSGS	(MENUVERIFY|REFRESHWINDOW|NEWPREFS)

static ULONG oldIDCMPFlags[MAX_WINDOWS + 1];	/* Extra space for backWindow */

static ULONG findIDCMPFlags, changeIDCMPFlags, spellIDCMPFlags, insertIDCMPFlags;

/*
 * Local prototype
 */
 
void SetBusyData(void);
void WaitDialog(DialogPtr, ULONG *);

void  FrameRect1(RastPtr, RectPtr, WORD, WORD);
void  OffsetRect(RectPtr, WORD, WORD);
void  InsetRect(RectPtr, WORD, WORD);


/*
 *	Busy pointer images
 */

static UWORD chip busyPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000
};

static Pointer busyPointer = {
	14, 14, -7, -7, &busyPointerData[0]
};

static UWORD busyData1[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1E60, 0x1FE0, 0x3E10, 0x3FF0, 0x7E08,
	0x3FF0, 0x7E08, 0x7FF8, 0xFE04, 0x7FF8, 0xFE04, 0x7FF8, 0x81FC,
	0x7FF8, 0x81FC, 0x3FF0, 0x41F8, 0x3FF0, 0x41F8, 0x1FE0, 0x21F0,
	0x0780, 0x19E0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData2[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1FE0, 0x1FE0, 0x3FF0, 0x3FF0, 0x5FC8,
	0x3FF0, 0x4F88, 0x7FF8, 0x8704, 0x7FF8, 0x8204, 0x7FF8, 0x8104,
	0x7FF8, 0x8384, 0x3FF0, 0x47C8, 0x3FF0, 0x4FE8, 0x1FE0, 0x3FF0,
	0x0780, 0x1FE0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData3[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x19E0, 0x1FE0, 0x21F0, 0x3FF0, 0x41F8,
	0x3FF0, 0x41F8, 0x7FF8, 0x81FC, 0x7FF8, 0x81FC, 0x7FF8, 0xFE04,
	0x7FF8, 0xFE04, 0x3FF0, 0x7E08, 0x3FF0, 0x7E08, 0x1FE0, 0x3E10,
	0x0780, 0x1E60, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData4[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1FE0, 0x2010, 0x3FF0, 0x6038,
	0x3FF0, 0x7078, 0x7FF8, 0xF8FC, 0x7FF8, 0xFDFC, 0x7FF8, 0xFEFC,
	0x7FF8, 0xFC7C, 0x3FF0, 0x7838, 0x3FF0, 0x7018, 0x1FE0, 0x2010,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static WORD busyNum;
static WindowPtr busyWindow;
static UWORD *busyData[] = {
	&busyData1[0], &busyData2[0], &busyData3[0], &busyData4[0]
};

/*
 *	Set pointer to proper appearance for location in window
 */

void SetPointerShape()
{
	register WORD x;
	register LONG y;
	WORD globalX, globalY;		/* Need to take address of these */
	register WindowPtr window;
	register BOOL clearPointer;
	
	window = ActiveWindow();
	if( IsDocWindow(window) ) {
		clearPointer = waitCount || window->FirstRequest ||
			(window->Flags & (INREQUEST | MENUSTATE)) ;
		if( !clearPointer ) {
			x = window->MouseX;
			y = window->MouseY;
/*
	If pointer is over a different screen or in requester, menus,
	out of bounds or over a different window then reset pointer
*/
			globalX = x;
			globalY = y;
			LocalToGlobal(window, &globalX, &globalY);
			if( (WhichScreen(globalX, globalY) != screen) || ( window == backWindow ||
				y < window->BorderTop || y >= window->Height - window->BorderBottom ||
				x < window->BorderLeft || x >= window->Width - window->BorderRight ||
				(window->Flags & (INREQUEST | MENUSTATE)) ||
				WhichLayer(&(screen->LayerInfo), x + window->LeftEdge, y + window->TopEdge) != window->WLayer) )

				clearPointer = TRUE;
/*
	Otherwise set pointer to whatever is appropriate
*/	 	}
		
		if( clearPointer || ( window->Flags & MENUSTATE ) )
			SetStdPointer(window, POINTER_ARROW);
		else if( inMacro )
			SetStdPointer(window, POINTER_WAIT);
		else if (x < window->BorderLeft + SELECT_WIDTH)
			SetStdPointer(window, POINTER_PLUS);
		else
			SetStdPointer(window, POINTER_IBEAM);
	}
}

/*
 *	Draw border around dialog's up/down arrows, given gadget number of up arrow
 */

void DrawArrowBorder(DialogPtr dlg, WORD gadgNum)
{
	RastPtr rPort = dlg->RPort;
	GadgetPtr gadget;
	BorderPtr border;

	gadget = GadgetItem(dlg->FirstGadget, gadgNum);
	if ((border = BoxBorder(gadget->Width, (WORD) (2*gadget->Height), COLOR_BLUE, -1))
		!= NULL) {
		DrawBorder(rPort, border, gadget->LeftEdge, gadget->TopEdge);
		FreeBorder(border);
	}
}

/*
 *	Filter function for ModalDialog()
 *	Handles window re-sizes and updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register ULONG class;
	register WindowPtr window;
	BOOL success = FALSE;
	
	class = intuiMsg->Class;
	window = intuiMsg->IDCMPWindow;
	*item = -1;
	if( (IsDocWindow(window) && (class & DOCWIND_MSGS)) ||
		( window == backWindow && (class & BACKWIND_MSGS))) {
		DoIntuiMessage(intuiMsg);
		success = TRUE;
	}
	return(success);
}

/*
 *	Set up system for lengthy operation
 *	If window is not NULL then window should be set up for requester
 */

void BeginWait()
{
	register WORD num;
	register WindowPtr window;
/*	register ULONG classBits;*/
/* register IntuiMsgPtr msg, nextMsg; */

	if (waitCount++)
		return;
/*
	Set window pointers (Monitor will cancel menus when waitCount > 0)
	Also modify IDCMP to send only important messages, and remove verify msgs
*/
	Forbid();
	for (num = 0; num < numWindows; num++) {
		window = windowList[num];
		oldIDCMPFlags[num] = window->IDCMPFlags;
		ModifyIDCMP(window, DOCWIND_MSGS);
/*		SetStdPointer(window, POINTER_WAIT);*/
	}
/*	SetStdPointer(backWindow, POINTER_WAIT);*/

	WaitDialog(findDialog, &findIDCMPFlags);
	WaitDialog(changeDialog, &changeIDCMPFlags);
	WaitDialog(spellDialog, &spellIDCMPFlags);
	WaitDialog(insertDialog, &insertIDCMPFlags);
	oldIDCMPFlags[MAX_WINDOWS] = backWindow->IDCMPFlags;
	ModifyIDCMP(backWindow, BACKWIND_MSGS);
/*
	msg = (IntuiMsgPtr) mainMsgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		window = msg->IDCMPWindow;
		if( IsDocWindow( window ))
			classBits = DOCWIND_MSGS;
		else if( window == backWindow )
			classBits = BACKWIND_MSGS;
		if( (msg->Class & classBits ) == 0 ) {
			Remove((NodePtr) msg);
			ReplyMsg((MsgPtr) msg);
		}
		msg = nextMsg;
	}
*/
	Permit();
}

/*
 * Save modeless dialog flags
 */
 
static void WaitDialog(register DialogPtr dlg, ULONG *flags)
{
	if( dlg != NULL ) {
		SetStdPointer(dlg, POINTER_WAIT);
		*flags = dlg->IDCMPFlags;
		ModifyIDCMP(dlg, DLGWIND_MSGS);
		if( dlg == findDialog ) {
			EnableGadgetItem(dlg->FirstGadget, OK_BUTTON, dlg, NULL, FALSE);
		} else if( dlg == changeDialog ) {
			OffChangeButtons();
		} else if( dlg == spellDialog ) {
			OffSpellButtons();
		}
	}
	return;
}

/*
 *	Reset system to normal state
 */

void EndWait()
{
	register WORD num;
/*	register WindowPtr window;
	register IntuiMsgPtr intuiMsg;
	WORD item;
*/

/*
	Refresh window under requester
*/
	Delay(5L);
/* 
	while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
		if (!DialogFilter(intuiMsg, &item))
			ReplyMsg((MsgPtr) intuiMsg);
	}
*/
/*
	Handle BeginWait/EndWait nesting
*/
	if( --waitCount )
		return;
/*
	First enable messages, so we don't miss any menu messages
*/
	for (num = 0; num < numWindows; num++)
		ModifyIDCMP(windowList[num], oldIDCMPFlags[num]);
	ModifyIDCMP(backWindow, oldIDCMPFlags[MAX_WINDOWS]);
	
	if(findDialog != NULL) {
		ModifyIDCMP(findDialog, findIDCMPFlags);
		SetStdPointer(findDialog, POINTER_ARROW);
		SetFindButton();
	}
	if(changeDialog != NULL) {
		ModifyIDCMP(changeDialog, changeIDCMPFlags);
		SetStdPointer(changeDialog, POINTER_ARROW);
		SetChangeButtons();
	}
	if( spellDialog != NULL ) {
		ModifyIDCMP(spellDialog, spellIDCMPFlags);
		SetStdPointer(spellDialog, POINTER_ARROW);
		SetSpellButtons();
	}
	if( insertDialog != NULL ) {
		ModifyIDCMP(insertDialog, insertIDCMPFlags);
		SetStdPointer(insertDialog, POINTER_ARROW);
	}
/*
	Now re-attach menus and enable gadgets
*/
/*
	for (num = 0; num < numWindows; num++) {
		window = windowList[num];
		SetStdPointer(window, POINTER_ARROW);
	}
	SetStdPointer(backWindow, POINTER_ARROW);
*/
	SetPointerShape();			/* For active window */
}

/*
 *	Set pointer for window to busy pointer
 */

void SetBusyPointer(WindowPtr window)
{
	busyNum = 0;
	SetBusyData();
	busyWindow = window;
	busyNum -= 2;
}

static void SetBusyData()
{
	register WORD i;
	
	for (i = 2; i < 2*busyPointer.Height + 2; i++)
		busyPointerData[i] = busyData[busyNum][i];
}

/*
 *	Set busy pointer to next in sequence
 */

void NextBusyPointer()
{
	ULONG seconds, micros ;
	static ULONG lastMicro = 0;
	
	CurrentTime( &seconds, &micros );	
	
	if( ( micros - lastMicro ) > 100000 ) {
		lastMicro = micros;
		switch(++busyNum){
		case -2:
		case -1:
			break;
		case 0:
			SetPointer(busyWindow, busyPointer.PointerData, busyPointer.Height,
				busyPointer.Width, busyPointer.XOffset, busyPointer.YOffset);
			break;
		case 4:
			busyNum = 0;
		default:
			SetBusyData();
			break;
		}
	}
}

/*
 *	Flash the screen and/or beep the speaker
 */

void ErrBeep()
{
	if( options.BeepFlash )
		DisplayBeep(screen);
	if( options.BeepSound )
		StdBeep();
}

/*
 * Sound a single-beep on speaker
 */
 
void StdBeep()
{
	SysBeep(5);
}

/*
 *	Blit the given image into the RastPort using specified minterm transfer mode
 *	Ignores PlanePick, PlaneOnOff, and NextImage fields
 */

void BltImage( rPort, image, leftOffset, topOffset, minterm)
RastPtr rPort ;
register ImagePtr image ;
WORD leftOffset ;
WORD topOffset ;
WORD minterm ;
{
	WORD top, left ;
	register WORD i, width, height, depth, wordsPerPlane;
	BitMap bitMap;

	while( image != NULL ) {
		left = leftOffset + image->LeftEdge;
		top = topOffset + image->TopEdge;
		width = image->Width;
		height = image->Height;
		depth = image->Depth;
		InitBitMap(&bitMap, depth, width, height);
		wordsPerPlane = ((width + 15) >> 4)*height;
		for (i = 0; i < depth; i++)
			bitMap.Planes[i] = (PLANEPTR) &image->ImageData[i*wordsPerPlane];
		BltBitMapRastPort(&bitMap, 0, 0, rPort, left, top, width, height, minterm);
		image = image->NextImage;
	}
}

/*
 *	Check string for positive numeric value
 */

BOOL CheckNumber(TextPtr s)
{
	if (strlen(s) == 0)
		return (FALSE);
	while (*s) {
		if (*s < '0' || *s > '9')
			return (FALSE);
		s++;
	}
	return (TRUE);
}

/*
 *	Copy string from stringGadget buffer to specified buffer
 */

void CopyStringBuffer(GadgetPtr gadgetList,
					  WORD item,
					  TextPtr buffer)
{
	register GadgetPtr gadget;

	gadget = GadgetItem(gadgetList, item);
	strcpy(buffer, ((StrInfoPtr) gadget->SpecialInfo)->Buffer);
}


/*
 *	Convert decipoint value into decimal string in current measurement units
 */

void DecipointToText(WORD num, TextPtr text, WORD places)
{
	Fixed inches;

	inches = FixRatio(num, 720);
	if (options.MeasureUnit == MEASURE_CM)
		Fix2Ascii(FixMul(inches, FixRatio(254, 100)), text, places);
	else
		Fix2Ascii(inches, text, places);
}

/*
 *	Convert decimal string into decipoint value using current measurement units
 *	Clip to maximum value of 44 inches (less than 32,000 decipoints)
 */

WORD TextToDecipoint(TextPtr text)
{
	Fixed num;

	num = Ascii2Fix(text);
	if (options.MeasureUnit == MEASURE_CM)
		num = FixDiv(num, FixRatio(254, 100));	/* Convert to inches */
	if (num < 0)
		num = 0;
	else if (num > Long2Fix(44L))
		num = Long2Fix(44L);
	return (FixRound(FixMul(num, Long2Fix(720L))));
}

/*
 * Standard no-frills alert-type dialog call
 */
 
WORD StdDialog(WORD dlgNum)
{
	WORD item;
	DialogPtr dlg;

	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (CANCEL_BUTTON);
	}
	OutlineOKButton(dlg);
	StdBeep();
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON);
	DisposeDialog(dlg);
	EndWait();
	return (item);
}

/*
 *	Outline first button in requester
 */

void OutlineOKButton(DialogPtr dlg)
{
	OutlineButton( GadgetItem(dlg->FirstGadget, 0), dlg, NULL, TRUE);
} 

/*
 *	Get fractional inch value from gadget buffer and return value in decipoints
 */

WORD GetFracValue(GadgetPtr gadgList, WORD item)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	return (TextToDecipoint(((StrInfoPtr) gadget->SpecialInfo)->Buffer));
}

/*
 *	Make partial roman number string using specified character
 *	Return length of string
 */

WORD MakeRomanNum(num, numString, ch10, ch5, ch1)
register WORD num;
TextPtr numString;
register TextChar ch10, ch5, ch1;
{
	register TextPtr s;

	s = numString;
	if (num == 9) {
		*s++ = ch1;
		*s++ = ch10;
	}
	else if (num == 4) {
		*s++ = ch1;
		*s++ = ch5;
	}
	else {
		if (num >= 5) {
			*s++ = ch5;
			num -= 5;
		}
		while (num--)
			*s++ = ch1;
	}
	*s = '\0';
	return ((WORD) strlen(numString));
}

/*
 * Cranks out a number in the format specified by the ASCII character "style",
 * appending it to the string passed. Used in page numbering and labelling.
 */
 
TextPtr AppendNumberStyle(num, numString, style)
WORD num;
TextPtr numString;
TextChar style;
{
	register TextPtr s = numString;
	register WORD digit, ones, tens, huns, thous;
	register TextPtr str;
	
	switch(style){
	case LOWER_NUMERIC_CHAR:
	case UPPER_NUMERIC_CHAR:
		NumToString((long) num, numString);
		s += strlen(numString);
		break;
	case LOWER_CASE_CHAR:
	case UPPER_CASE_CHAR:
		if( num == 0 )
			*s++ = '-';
		else {
			if( num > 27*26 ) {
				digit = (num - 27)/(26*26);
				*s++ = ('A' - 1) + digit;
				num -= digit*(26*26);
			}
			if( num > 26 ) {
				digit = (num - 1)/26;
				*s++ = ('A' - 1) + digit;
				num -= digit*26;
			}
			*s++ = ('A' - 1) + num;
		}
		break;
	case LOWER_ROMAN_CHAR:
	case UPPER_ROMAN_CHAR:
		if( num == 0 )
			*s++ = '-';
		else {
			ones = num % 10;
			tens = (num / 10) % 10;
			huns = (num / 100) % 10;
			thous = (num/1000);
			if(thous)
				s += MakeRomanNum(thous, s, '?', '?', 'M');
			if(huns)
				s += MakeRomanNum(huns, s, 'M', 'D', 'C');
			if(tens || huns || thous)
				s += MakeRomanNum(tens, s, 'C', 'L', 'X');
			s += MakeRomanNum(ones, s, 'X', 'V', 'I');
		}
		break;
	default:
		break;
	}
	if( (style == LOWER_CASE_CHAR) || (style == LOWER_ROMAN_CHAR) ) {
		str = s;
		for( s = numString ; *s; s++ )
			*s = toLower[*s];
		s = str;
	}
	return(s);
} 

/*
 * Returns random number from 0 to max-1
 * (Stink-a-roo)
 */
 
UWORD Random(UWORD max)
{
	static UBYTE wild = 255;
	UBYTE rnd ;
	
	rnd = VBeamPos() + wild;
	wild *= 7;
	return( (UWORD) ( (ULONG) ( ((UWORD) rnd) * max ) >> 8 ) ) ;
}

/*
 *	Compare two text strings
 *	Return -1 if first less than second, 0 if equal, +1 if first greater than second
 *	Ignore case
 *	(Treats all upper case as lower case, and all open and close quotes as normal quotes)
 */

WORD CompareText(s1, s2, len1, len2)
register TextPtr s1, s2;
register WORD len1, len2;
{
	register TextChar ch1, ch2;
	register WORD len;

	len = MIN(len1, len2);
	while (len--) {
		if ((ch1 = toLower[*s1++]) != (ch2 = toLower[*s2++])) {
			if (ch1 < ch2)
				return (-1);
			return (1);
		}
	}
	if (len1 < len2)
		return (-1);
	if (len1 > len2)
		return (1);
	return (0);
}

/*
 *	Check to see if given device is mounted
 */

typedef struct DeviceList *DevListPtr;

BOOL DevMounted(TextPtr devName)
{
	WORD len;
	register TextPtr name;
	register DevListPtr device;
	struct DosInfo *dosInfo;
	struct RootNode *rootNode;

	len = strlen(devName) - 1;
	rootNode = (struct RootNode *) DOSBase->dl_Root;
	dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
	Forbid();
	for (device = (DevListPtr) BADDR(dosInfo->di_DevInfo);
		 device;
		 device = (DevListPtr) BADDR(device->dl_Next)) {
		name = (TextPtr) BADDR(device->dl_Name);
		if (device->dl_Type == DLT_DEVICE && device->dl_Task &&
			CmpString(devName, name + 1, len, (WORD) *name, FALSE) == 0)
			break;
	}
	Permit();
	return ((BOOL) (device != NULL));
}

/*
 *	Check to see if given directory is assigned or mounted as a volume
 */

BOOL DirAssigned(TextPtr volName)
{
	WORD len;
	register TextPtr name;
	register DevListPtr device;
	struct DosInfo *dosInfo;
	struct RootNode *rootNode;

	len = strlen(volName) - 1;
	rootNode = (struct RootNode *) DOSBase->dl_Root;
	dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
	Forbid();
	for (device = (DevListPtr) BADDR(dosInfo->di_DevInfo);
		 device;
		 device = (DevListPtr) BADDR(device->dl_Next)) {
		name = (TextPtr) BADDR(device->dl_Name);
		if ((device->dl_Type == DLT_DIRECTORY || device->dl_Type == DLT_VOLUME) &&
			CmpString(volName, name + 1, len, (WORD) *name, FALSE) == 0)
			break;
	}
	Permit();
	return ((BOOL) (device != NULL));
}

/*
 * Draw a box around the specified color gadget
 */
 
void DrawColorBorder(WindowPtr dlg, GadgetPtr colorsGadg)
{
	WORD width,height;
	Rectangle rect;
	
	GetGadgetRect(colorsGadg, dlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	DrawShadowBox(dlg->RPort, rect.MinX, rect.MinY, width, height, -1, TRUE);
}

/*
 *	Offset rectangle by given x and y amounts
 *	Movement for positive values is right and down
 */

static void OffsetRect(RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX += dx;
	rect->MaxY += dy;
}

/*
 *	Move rectangle edges inward by given amount
 */

static void InsetRect(RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX -= dx;
	rect->MaxY -= dy;
}

/*
 *	Draw rectangle frame
 */

static void FrameRect1(RastPtr rPort, RectPtr rect, WORD penWidth, WORD penHeight)
{
	WORD x, y;
	Rectangle drawRect;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			drawRect = *rect;
			if (x != 0 || y != 0)
				OffsetRect(&drawRect, x, y);
			FrameRect(rPort, &drawRect);
		}
	}
}

/*
 *	Get number of colors across and down in color gadget
 */

void GetNumAcrossDown(WORD numColors, WORD *numAcross, WORD *numDown)
{
	if (numColors == 256)
		*numDown = 16;
	else if (numColors > 64)
		*numDown = 8;
	else if (numColors > 16)
		*numDown = 4;
	else if (numColors > 4)
		*numDown = 2;
	else
		*numDown = 1;
	*numAcross = numColors/(*numDown);
}

/*
 *	Draw specified color item in dialog, hiliting if seleted
 */

void DrawColorItem(DialogPtr dlg, WORD penNum, BOOL hilite, UWORD item)
{
	WORD x, y, width, height;
	WORD numAcross, numDown;
	RastPtr rPort;
	GadgetPtr gadget;
	Rectangle rect;
	register UWORD numColors;	

	numColors = 1 << screen->RastPort.BitMap->Depth;
	GetNumAcrossDown(numColors, &numAcross, &numDown);
	rPort = dlg->RPort;
	gadget = GadgetItem(dlg->FirstGadget, item);
	GetGadgetRect(gadget, dlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	x = rect.MinX + (penNum % numAcross)*width/numAcross;
	y = rect.MinY + (penNum/numAcross)*height/numDown;
	SetRect(&rect,  x, y, x + width/numAcross - 1, y + height/numDown - 1);
	if (hilite) {
		SetAPen(rPort, _tbPenBlack);
		FrameRect(rPort, &rect);
		InsetRect(&rect, 1, 1);
		FrameRect(rPort, &rect);
	}
	else {
		SetAPen(rPort, _tbPenLight);
		FrameRect(rPort, &rect);
	}
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, penNum);
	FillRect(rPort, &rect);
}

/*
 * Toggle checkbox boolean value and change its appearance
 */
 
void ToggleCheckbox( BOOL *value, WORD item, DialogPtr dlg)
{
	*value = !*value;
	SetGadgetItemValue(dlg->FirstGadget, item, dlg, NULL, *value);
	return;
}
